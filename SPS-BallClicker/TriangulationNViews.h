#pragma once

#include <opencv.hpp>

struct HomogeneousPoint
{
	HomogeneousPoint()
		: pt(0.0,0.0,0.0), w(0.0)
	{
		
	}
	HomogeneousPoint(cv::Mat m)
	{
		pt.x = m.at<double>(0);
		pt.y = m.at<double>(1);
		pt.z = m.at<double>(2);
		w = m.at<double>(3);
	}
	cv::Point3d toWorldCoords()
	{
		return cv::Point3d(pt.x / w, pt.y / w, pt.z / w);
	}
	cv::Point3d pt;
	double w;
};

class TriangulationNViews
{
public:
	TriangulationNViews()
		: err(-1.0)
	{

	}
	void add(const cv::Point3d& camPos, const cv::Point3d& ballPos);
	double getError()    const;
	void clear();
	cv::Point3d triangulate();
	inline int size() const { return views.size(); }

protected:
	std::vector<std::tuple<cv::Matx31d, cv::Matx33d, cv::Point3d>> views;

	mutable double err;  // retprojection error, mutable since modified in compute(...) const;
};
