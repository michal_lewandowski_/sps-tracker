#include "TriangulationNViews.h"

extern double ballRadius;

void TriangulationNViews::add(const cv::Point3d& camPos, const cv::Point3d& ballPos)
{
	cv::Matx13d directionVec(ballPos.x - camPos.x, ballPos.y - camPos.y, ballPos.z - camPos.z);
	cv::Matx13d directionVecNorm;
	cv::normalize(directionVec, directionVecNorm, 1.0, 0.0, cv::NORM_L2);

	cv::Matx33d Vi = cv::Matx33d::zeros();
	Vi(0, 1) = -directionVecNorm(2);
	Vi(0, 2) = directionVecNorm(1);
	Vi(1, 0) = directionVecNorm(2);
	Vi(1, 2) = -directionVecNorm(0);
	Vi(2, 0) = -directionVecNorm(1);
	Vi(2, 1) = directionVecNorm(0);

	cv::Matx33d Ri = Vi.t()* Vi;
	cv::Matx31d pi(camPos.x, camPos.y, camPos.z);

	views.push_back(std::make_tuple(pi, Ri, ballPos));
}
cv::Point3d TriangulationNViews::triangulate()
{
	if(views.size() > 1)
	{
		cv::Matx33d A(0, 0, 0, 0, 0, 0, 0, 0, 0);
		cv::Matx31d b(0, 0, 0);
		cv::Matx<double, 1, 1> c(0.0);
		for (int i = 0; i < views.size(); ++i)
		{
			A += std::get<1>(views[i]);
			b += std::get<1>(views[i]) * std::get<0>(views[i]);
			c += std::get<0>(views[i]).t() * std::get<1>(views[i]) * std::get<0>(views[i]);
		}
		cv::Matx31d position = A.inv() * b;
		cv::Matx<double, 1, 1> e = c - b.t() * A.inv() * b;
		err = e(0, 0);
		err = sqrt(err / views.size());
		return cv::Point3d(position(0), position(1), position(2));
	}
	else if (views.size() == 1)
	{
		return std::get<2>(views[0]);
	}
	else
	{
		return cv::Point3d(-1, -1, -1);
	}
}

double TriangulationNViews::getError()    const
{
	return err;
}
void TriangulationNViews::clear()
{
	views.clear();
	err = -1.0;
}