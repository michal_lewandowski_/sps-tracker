#pragma once

#include <windows.h>
#include <string>
#include <vector>

#pragma warning(push, 0)
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/algorithm/string.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#pragma warning(pop)

namespace bfs = boost::filesystem;
namespace ba = boost::algorithm;


struct worldCoords
{
	double x;
	double y;
	double z;
};
struct imgCoords
{
	double x;
	double y;
};
struct homogeneousCoords
{
	homogeneousCoords(void)
	{

	}
	homogeneousCoords(cv::Mat m)
	{
		x = m.at<double>(0);
		y = m.at<double>(1);
		z = m.at<double>(2);
		w = m.at<double>(3);
	}
	cv::Point3d toWorldCoords()
	{
		return cv::Point3d(x / w, y / w, z / w);
	}
	double x, y, z, w;
};

class Video
{
public:
	Video(std::string sName);

	void next(int iStep=1);
	void grab();
	long getFrameNumber();
	cv::Mat& getFrame();
	void goToFrame(long iFrame);
	void LoadImg(std::string sFilePath);
	void LoadWorld(std::string sFilePath);
	void LoadTriangulateWorld(std::string sFilePath);
	
	void LoadVideo(std::string sFilePath, bool bLoad = true);
	size_t getTotalFrames(void);
	std::string getName(void);
	void save(std::vector<cv::Vec6d> v, std::string sFilePath);

	static worldCoords makeWorld(cv::Point3d pt);
	static cv::Point3d makeWorld(homogeneousCoords hc);
	static imgCoords makeImg(cv::Point2i pt);

public:
	cv::VideoCapture video;
	std::string m_sName;
	cv::Mat m_frame;
	cv::Mat m_tmp;
	cv::Mat m_camMatrix;
	long m_iFrame;
	bool m_bVidLoad;
	size_t m_iNumFrames;
	int m_iWidth;
	int m_iHeight;

	std::vector<cv::Point3f> m_wCoords;
	std::vector<cv::Point2i> m_imgCoords;
	std::vector<cv::Vec6d> m_wNewCoords;
	std::vector<bool> m_isInitFrame;
};