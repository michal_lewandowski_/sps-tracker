#include "Field.h"
#include "CameraCalibration.h"

Field::Field(int id, cv::Rect r, int margin, int camsNum)
	: fid(id), 
	field(r),
	maxAbsVelocityGlobal(std::numeric_limits<double>::epsilon()),
	maxBallProbabilityGlobal(std::numeric_limits<double>::max())
{
	numObservations.resize(camsNum, 0);

	fieldExtended = cv::Rect(field.tl() - cv::Point(margin, margin), field.br() + cv::Point(margin, margin));

	zonePlayer1.push_back(cv::Rect(field.tl(), field.tl() + cv::Point(2000, 2000)));
	zonePlayer1.push_back(cv::Rect(cv::Point(field.tl().x + 2000, field.tl().y), cv::Point(field.br().x - 2000, field.tl().y + 2000)));
	zonePlayer1.push_back(cv::Rect(cv::Point(field.tl().x + 4000, field.tl().y), cv::Point(field.br().x, field.tl().y + 2000)));

	
	zonePlayer2.push_back(cv::Rect(cv::Point(field.tl().x, field.br().y - 2000), cv::Point(field.tl().x + 2000, field.br().y)));
	zonePlayer2.push_back(cv::Rect(cv::Point(field.tl().x + 2000, field.br().y - 2000), cv::Point(field.tl().x + 4000, field.br().y)));
	zonePlayer2.push_back(cv::Rect(field.br() - cv::Point(2000, 2000), field.br()));

	zoneGame = cv::Rect(field.tl() + cv::Point(0, 2000), field.br() - cv::Point(0, 2000));
}


Field::~Field()
{
}

void Field::prepare()
{
	maxAbsVelocityGlobal = std::numeric_limits<double>::epsilon();
	maxBallProbabilityGlobal = std::numeric_limits<double>::max();
	detectionWindow = cv::Rect(0,0,0,0);
}
void Field::show(cv::Mat frame, double windowScale)
{
	cv::rectangle(frame, cv::Point(cvRound(field.tl().x / windowScale), frame.rows - cvRound(field.tl().y / windowScale)), cv::Point(cvRound(field.br().x / windowScale), frame.rows - cvRound(field.br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	cv::rectangle(frame, cv::Point(cvRound(fieldExtended.tl().x / windowScale), frame.rows - cvRound(fieldExtended.tl().y / windowScale)), cv::Point(cvRound(fieldExtended.br().x / windowScale), frame.rows - cvRound(fieldExtended.br().y / windowScale)), cv::Scalar(128, 128, 128), 1);

	cv::rectangle(frame, cv::Point(cvRound(zonePlayer1[0].tl().x / windowScale), frame.rows - cvRound(zonePlayer1[0].tl().y / windowScale)), cv::Point(cvRound(zonePlayer1[0].br().x / windowScale), frame.rows - cvRound(zonePlayer1[0].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	cv::rectangle(frame, cv::Point(cvRound(zonePlayer2[0].tl().x / windowScale), frame.rows - cvRound(zonePlayer2[0].tl().y / windowScale)), cv::Point(cvRound(zonePlayer2[0].br().x / windowScale), frame.rows - cvRound(zonePlayer2[0].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);

	cv::rectangle(frame, cv::Point(cvRound(zonePlayer1[1].tl().x / windowScale), frame.rows - cvRound(zonePlayer1[1].tl().y / windowScale)), cv::Point(cvRound(zonePlayer1[1].br().x / windowScale), frame.rows - cvRound(zonePlayer1[1].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	cv::rectangle(frame, cv::Point(cvRound(zonePlayer2[1].tl().x / windowScale), frame.rows - cvRound(zonePlayer2[1].tl().y / windowScale)), cv::Point(cvRound(zonePlayer2[1].br().x / windowScale), frame.rows - cvRound(zonePlayer2[1].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);

	cv::rectangle(frame, cv::Point(cvRound(zonePlayer1[2].tl().x / windowScale), frame.rows - cvRound(zonePlayer1[2].tl().y / windowScale)), cv::Point(cvRound(zonePlayer1[2].br().x / windowScale), frame.rows - cvRound(zonePlayer1[2].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	cv::rectangle(frame, cv::Point(cvRound(zonePlayer2[2].tl().x / windowScale), frame.rows - cvRound(zonePlayer2[2].tl().y / windowScale)), cv::Point(cvRound(zonePlayer2[2].br().x / windowScale), frame.rows - cvRound(zonePlayer2[2].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);

	cv::rectangle(frame, cv::Point(cvRound(zoneGame.tl().x / windowScale), frame.rows - cvRound(zoneGame.tl().y / windowScale)), cv::Point(cvRound(zoneGame.br().x / windowScale), frame.rows - cvRound(zoneGame.br().y / windowScale)), cv::Scalar(200, 200, 200), 1);

	cv::putText(frame, "Field " + std::to_string(fid), cv::Point(cvRound((field.tl().x + 200) / windowScale), cvRound((field.br().y + 400) / windowScale)), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(200, 200, 200));


	
}
std::vector<cv::Point> Field::project2d(cv::Rect f, CameraCalibration& calibration) const
{
	std::vector<cv::Point> v(4);
	cv::Point2d pt;
	calibration.transformToPixel(cv::Point3d(f.tl().x, f.tl().y, 0.0), pt);
	v[0] = cv::Point(static_cast<int>(pt.x), static_cast<int>(pt.y));
	calibration.transformToPixel(cv::Point3d(f.tl().x, f.br().y, 0.0), pt);
	v[1] = cv::Point(static_cast<int>(pt.x), static_cast<int>(pt.y));
	calibration.transformToPixel(cv::Point3d(f.br().x, f.br().y, 0.0), pt);
	v[2] = cv::Point(static_cast<int>(pt.x), static_cast<int>(pt.y));
	calibration.transformToPixel(cv::Point3d(f.br().x, f.tl().y, 0.0), pt);
	v[3] = cv::Point(static_cast<int>(pt.x), static_cast<int>(pt.y));
	return v;
}
void Field::show(cv::Mat frame1, cv::Mat frame2, std::string camID, CameraCalibration& calibration, cv::Scalar color, bool show)
{
	std::vector<std::vector<cv::Point>> vv;
	std::vector<std::vector<cv::Point>> vvx;

	vv.push_back(project2d(field, calibration));
	vvx.push_back(project2d(fieldExtended, calibration));

	cv::drawContours(frame1, vvx, -1, cv::Scalar(128, 128, 128), 1);
	if(!frame2.empty())
		cv::drawContours(frame2, vv, -1, cv::Scalar(200, 200, 200), 1);

	if (camID == "cam1")
	{
		cv::putText(frame1, "Field " + std::to_string(fid), vv[0][1] + cv::Point(-100, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
		if (!frame2.empty())
			cv::putText(frame2, "Field " + std::to_string(fid), vv[0][1] + cv::Point(-100, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
	}
	if (camID == "cam2")
	{
		cv::putText(frame1, "Field " + std::to_string(fid), vv[0][0] + cv::Point(15, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
		if (!frame2.empty())
			cv::putText(frame2, "Field " + std::to_string(fid), vv[0][0] + cv::Point(15, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
	}
	if (camID == "cam3")
	{
		cv::putText(frame1, "Field " + std::to_string(fid), vv[0][3] + cv::Point(-100, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
		if (!frame2.empty())
			cv::putText(frame2, "Field " + std::to_string(fid), vv[0][3] + cv::Point(-100, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
	}
	if (camID == "cam4")
	{
		cv::putText(frame1, "Field " + std::to_string(fid), vv[0][2] + cv::Point(15, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
		if (!frame2.empty())
			cv::putText(frame2, "Field " + std::to_string(fid), vv[0][2] + cv::Point(15, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
	}
	for(int i=0; i < 3; ++i)
	{
		{
			std::vector<std::vector<cv::Point>> vv;
			vv.push_back(project2d(zonePlayer1[i], calibration));
			cv::drawContours(frame1, vv, -1, cv::Scalar(200, 200, 200), 1);
			if (!frame2.empty())
				cv::drawContours(frame2, vv, -1, cv::Scalar(200, 200, 200), 1);
		}
		{
			std::vector<std::vector<cv::Point>> vv;
			vv.push_back(project2d(zonePlayer2[i], calibration));
			cv::drawContours(frame1, vv, -1, cv::Scalar(200, 200, 200), 1);
			if (!frame2.empty())
				cv::drawContours(frame2, vv, -1, cv::Scalar(200, 200, 200), 1);
		}
	}
	{
		std::vector<std::vector<cv::Point>> vv;
		vv.push_back(project2d(zoneGame, calibration));
		cv::drawContours(frame1, vv, -1, cv::Scalar(200, 200, 200), 1);
		if (!frame2.empty())
			cv::drawContours(frame2, vv, -1, cv::Scalar(200, 200, 200), 1);
	}

#ifdef SPS_DEBUB_DETS
	if(ballColorMap)
	{
		if (ballColorMap)
		{
			cv::Mat imgAllColors = ballColorMap->quantiziser()->getColorQuantizisationImage();
			if(!imgAllColors.empty())
			{
				//cv::imshow(std::to_string(fid) + " Color Quantizisation", imgAllColors);
			}
		}
		std::string prefixStr;
		std::stringstream ss;
		cv::Mat imgColor = ballColorMap->getImage();
		cv::Mat imgProb = ballColorMap->getProbImage(ss);
		cv::Mat imgAll;
		cv::vconcat(imgColor, imgProb, imgAll);

		//imgAll.copyTo(frame1(cv::Rect((fid-1) * 387, frame1.rows - imgAll.rows, imgAll.cols, imgAll.rows)));
		imgAll.copyTo(frame2(cv::Rect((fid-1) * 387, frame2.rows - imgAll.rows, imgAll.cols, imgAll.rows)));
		
		if(show)
		{
		//	std::cout << std::endl << camID << "[" << fid << "]: " << std::endl << ss.str() << std::endl;
		}
	}
#endif
}
bool Field::isInPlayerZone(const cv::Point3d& pos)
{
	if (zonePlayer1[0].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer1[1].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer1[2].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer2[0].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer2[1].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer2[2].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	return false;
}
std::pair<cv::Point3d, cv::Rect> Field::closestPlayerZone(cv::Point3d pos)
{
	cv::Point3d centerZone1(zonePlayer1[0].tl().x + zonePlayer1[0].width * 0.5, zonePlayer1[0].tl().y + zonePlayer1[0].height * 0.5, 0);
	cv::Point3d centerZone2(zonePlayer1[2].tl().x + zonePlayer1[2].width * 0.5, zonePlayer1[2].tl().y + zonePlayer1[2].height * 0.5, 0);
	cv::Point3d centerZone3(zonePlayer2[0].tl().x + zonePlayer2[0].width * 0.5, zonePlayer2[0].tl().y + zonePlayer2[0].height * 0.5, 0);
	cv::Point3d centerZone4(zonePlayer2[2].tl().x + zonePlayer2[2].width * 0.5, zonePlayer2[2].tl().y + zonePlayer2[2].height * 0.5, 0);
	double dist1 = cv::norm(pos - centerZone1);
	double dist2 = cv::norm(pos - centerZone2);
	double dist3 = cv::norm(pos - centerZone3);
	double dist4 = cv::norm(pos - centerZone4);
	if(dist1 < dist2 && dist1 < dist3 && dist1 < dist4)
	{
		return std::make_pair(centerZone1, zonePlayer1[0] | zonePlayer1[1] | zonePlayer1[2]);
	}
	else if (dist2 < dist3 && dist2 < dist4)
	{
		return std::make_pair(centerZone2, zonePlayer1[0] | zonePlayer1[1] | zonePlayer1[2]);
	}
	else if (dist3 < dist4)
	{
		return std::make_pair(centerZone3, zonePlayer2[0] | zonePlayer2[1] | zonePlayer2[2]);
	}
	else
	{
		return std::make_pair(centerZone4, zonePlayer2[0] | zonePlayer2[1] | zonePlayer2[2]);
	}
}
Field* FieldList::inField(const cv::Point3d& pos)
{
	for (int i = 0; i < size(); ++i)
	{
		if ((*this)[i].fieldExtended.contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		{
			return &(*this)[i];
		}
	}
	return nullptr;
}
const Field* FieldList::inField(const cv::Point3d& pos) const
{
	for (int i = 0; i < size(); ++i)
	{
		if ((*this)[i].fieldExtended.contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		{
			return &(*this)[i];
		}
	}
	return nullptr;
}
Field* FieldList::find(int fid)
{
	for (int i = 0; i < size(); ++i)
	{
		if ((*this)[i].fid == fid)
			return &(*this)[i];
	}
	return nullptr;
}
const Field* FieldList::find(int fid) const
{
	for (int i = 0; i < size(); ++i)
	{
		if ((*this)[i].fid == fid)
			return &(*this)[i];
	}
	return nullptr;
}