#include "Video.h"

#pragma warning(push, 0)
#include <boost/property_tree/info_parser.hpp>
#include <boost/make_shared.hpp>
#include <boost/lexical_cast.hpp>
#pragma warning(pop)

extern float g_fExtraAreaInMM;

Video::Video(std::string sName) : m_sName(sName)
{
	m_iFrame = -1;
	m_bVidLoad = false;
	m_iNumFrames = std::numeric_limits<size_t>::max();
}
void Video::LoadVideo(const std::string file, bool bLoad)
{
	video.open(file);
	if (!video.isOpened())
	{
		std::stringstream s;
		s << "no file " << file;
		throw std::exception(s.str().c_str());
	}
	m_bVidLoad = bLoad;

	long iNumFrames = static_cast<long>(video.get(CV_CAP_PROP_FRAME_COUNT));
	if (m_iNumFrames == std::numeric_limits<size_t>::max())
		m_iNumFrames = iNumFrames;
	if (iNumFrames != m_iNumFrames)
	{
		std::stringstream s;
		s << "Number of frames " << iNumFrames << " of video '" << file << "' does not match image coordinates of size " << m_iNumFrames << std::endl;
		throw std::exception(s.str().c_str());
	}
	m_iWidth = static_cast<int>(video.get(CV_CAP_PROP_FRAME_WIDTH));
	m_iHeight = static_cast<int>(video.get(CV_CAP_PROP_FRAME_HEIGHT));
	if (!m_bVidLoad)
		video.release();
}
void Video::next(int iStep)
{
	if (m_bVidLoad)
	{
		int i = 1;
		while (i < iStep)
		{
			video.grab();
			i++;
		}

		/*bool b = */ video.read(m_tmp);

		if (!m_tmp.empty())
		{
			std::vector<cv::Mat> v;
			cv::split(m_tmp, v);
			cv::cvtColor(v[0], m_frame, cv::COLOR_BayerBG2BGR);
		}
		else
		{
			m_frame.release();
		}
	}
	else
	{
		if (m_frame.empty())
			m_frame.create(m_iHeight, m_iWidth,CV_8UC3);
		m_frame.setTo(0);
	}
	m_iFrame++;
}
void Video::grab()
{
	if (m_bVidLoad)
	{
		if (video.grab())
		{
			m_iFrame++;
		}
		else m_frame.release();
	}
	else m_iFrame++;
}
long Video::getFrameNumber()
{
	return m_iFrame;
}
cv::Mat& Video::getFrame()
{
	return m_frame;
}
void Video::goToFrame(long iFrame)
{
	if (m_bVidLoad)
		video.set(CV_CAP_PROP_POS_FRAMES, iFrame);
	m_iFrame = iFrame - 1;
}
void Video::LoadImg(std::string sFilePath)
{
	m_isInitFrame.clear();
	m_imgCoords.clear();
	bfs::fstream f(bfs::path(sFilePath), std::ios_base::in);
	if (f.is_open() == true)
	{
		std::string s;
		cv::Point2i pt;

		while (f)
		{
			std::getline(f, s);
			if (!f)
				break;
			if (s[s.length() - 1] != ']')
			{
				m_isInitFrame.push_back(s[s.length() - 1] == '1');
				s = s.substr(1, s.length() - 4);
			}
			else
			{
				s = s.substr(1, s.length() - 2);
				m_isInitFrame.push_back(false);
			}
			std::vector<std::string> ss;
			boost::split(ss, s, boost::is_any_of(","));
			ba::trim(ss[0]);
			ba::trim(ss[1]);
			pt.x = boost::lexical_cast<int>(ss[0]);
			pt.y = boost::lexical_cast<int>(ss[1]);
			m_imgCoords.push_back(pt);
		}
		f.close();
		
		size_t iNumFrames = m_imgCoords.size();
		if (m_iNumFrames == std::numeric_limits<size_t>::max())
			m_iNumFrames = iNumFrames;
		else if (iNumFrames != m_iNumFrames)
		{
			std::stringstream s;
			s << "Number of frames is incorrect during loading image coordinates " << sFilePath << ". It is " << iNumFrames << ", but should be " << m_iNumFrames;
			throw std::exception(s.str().c_str());
		} 
	}
	else
	{
		std::stringstream s;
		s << "cannot open file " << sFilePath << std::endl;
		throw std::exception(s.str().c_str());
	}
}
void Video::LoadWorld(std::string sFilePath)
{
	bfs::fstream f(bfs::path(sFilePath), std::ios_base::in);
	if (f.is_open() == true)
	{
		std::string s;
		cv::Point3f pt;

		while (f)
		{
			std::getline(f, s);
			if (!f)
				break;
			s = s.substr(1, s.length() - 2);
			std::vector<std::string> ss;
			boost::split(ss, s, boost::is_any_of(","));
			ba::trim(ss[0]);
			ba::trim(ss[1]);
			ba::trim(ss[2]);
			pt.x = boost::lexical_cast<float>(ss[0]);
			pt.y = boost::lexical_cast<float>(ss[1]);
			pt.z = boost::lexical_cast<float>(ss[2]);
			m_wCoords.push_back(pt);
		}
		f.close();

		size_t iNumFrames = m_wCoords.size();
		if (m_iNumFrames == std::numeric_limits<size_t>::max())
			m_iNumFrames = iNumFrames;
		else if (iNumFrames != m_iNumFrames)
		{
			std::stringstream s;
			s << "Number of frames is incorrect during loading world coordinates from " << sFilePath << ". It is " << iNumFrames << ", but should be " << m_iNumFrames;
			throw std::exception(s.str().c_str());
		}
	}
	else
	{
		std::stringstream s;
		s << "cannot open file " << sFilePath;
		throw std::exception(s.str().c_str());
	}
}
void Video::LoadTriangulateWorld(std::string sFilePath)
{
	bfs::fstream f(bfs::path(sFilePath), std::ios_base::in);
	if (f.is_open() == true)
	{
		std::string s;
		cv::Vec6d v;

		while (f)
		{
			std::getline(f, s);
			if (!f)
				break;
			s = s.substr(1, s.length() - 2);
			std::vector<std::string> ss;
			boost::split(ss, s, boost::is_any_of(","));
			ba::trim(ss[0]);
			ba::trim(ss[1]);
			ba::trim(ss[2]);
			ba::trim(ss[3]);
			ba::trim(ss[4]);
			v[0] = boost::lexical_cast<double>(ss[0]);
			v[1] = boost::lexical_cast<double>(ss[1]);
			v[2] = boost::lexical_cast<double>(ss[2]);
			v[3] = boost::lexical_cast<double>(ss[3]);
			v[4] = boost::lexical_cast<double>(ss[4]);
			v[5] = 0.0;
			m_wNewCoords.push_back(v);
		}
		f.close();
	}
	else
	{
		std::stringstream s;
		s << "cannot open file " << sFilePath;
		throw std::exception(s.str().c_str());
	}
}
void Video::save(std::vector<cv::Vec6d> v, std::string sFilePath)
{
	bfs::fstream f(bfs::path(sFilePath), std::ios_base::out | std::ios_base::trunc);
	if (f.is_open() == true)
	{
		for (long i = 0; i < m_wNewCoords.size(); ++i)
			f << m_wNewCoords[i] << std::endl;
		f.close();
	}
	else
	{
		std::stringstream s;
		s << "cannot create file " << sFilePath;
		throw std::exception(s.str().c_str());
	}
}
size_t Video::getTotalFrames(void)
{
	return m_iNumFrames;
}
std::string Video::getName(void)
{
	return m_sName;
}

worldCoords Video::makeWorld(cv::Point3d pt)
{
	worldCoords wc;
	wc.x = pt.x;
	wc.y = pt.y;
	wc.z = pt.z;
	return wc;
}
cv::Point3d Video::makeWorld(homogeneousCoords hc)
{
	cv::Point3d pt;
	pt.x = hc.x;
	pt.y = hc.y;
	pt.z = hc.z;
	return pt;
}

imgCoords Video::makeImg(cv::Point2i pt)
{
	imgCoords ic;
	ic.x = static_cast<double>(pt.x);
	ic.y = static_cast<double>(pt.y);
	return ic;
}