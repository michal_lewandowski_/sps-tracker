#include "GenereteGT.h"
#include <regex>
#include "Field.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include "TriangulationNViews.h"

GenereteGT::GenereteGT()
	: stop(false), step(false), windowScale(20.0)
{
}


GenereteGT::~GenereteGT()
{
}

void GenereteGT::addJob(std::string name, std::string videoFile, std::string configFile, std::string dir)
{
	names.push_back(name);
	videoFiles.push_back(videoFile);
	configFiles.push_back(configFile);
	inputDir = dir;
}
void GenereteGT::init(int field)
{
	fieldID = field;
	imgCoords.resize(names.size());
	for (int i = 0; i < names.size(); ++i)
	{
		caps.push_back(cv::VideoCapture(inputDir + std::string("\\") + videoFiles[i]));
		calibs.push_back(CameraCalibration(inputDir + std::string("\\") + configFiles[i]));

		std::vector<std::string> strs;
		boost::algorithm::split(strs, videoFiles[i], boost::is_any_of("."));
		CV_Assert(strs.size() == 6);
		outputfile = strs[0] + "." + strs[1] + "." + strs[2] + "." + strs[3];
		std::string filename = strs[0] + "." + strs[1] + "." + strs[2] + "." + strs[3] + "." + strs[4];
		std::string filefile = inputDir + "\\" + filename + ".field" + std::to_string(fieldID) + ".gt";

		std::ifstream infile;
		infile.open(filefile, std::ios_base::in);
		if (infile.is_open())
		{
			std::string line;
			while (std::getline(infile, line))
			{
				std::stringstream ss(line);
				int x, y;
				ss >> x >> y;
				if(x>=80 || y>=80)
				{
					imgCoords[i].push_back(cv::Point(x, y));
				}
				else
				{
					imgCoords[i].push_back(cv::Point(-1, -1));
				}
			}
			infile.close();
		}
		else
		{
			std::cout << "No input filed " << filefile << std::endl;
			return;
		}
	}
	int minSize = std::numeric_limits<int>::max();
	for (int i = 0; i < names.size(); ++i)
	{
		minSize = std::min(minSize, static_cast<int>(imgCoords[i].size()));
	}
	for (int i = 0; i < names.size(); ++i)
	{
		while (minSize < imgCoords[i].size())
		{
			imgCoords[i].pop_back();
		}
	}
}
void GenereteGT::process()
{
	/*for (int j = 0; j < caps.size(); ++j)
	{
	int i = 79;
	cv::Mat frameTemp;
	while (i > 0)
	{
	caps[j].read(frameTemp);
	i--;
	}
	}*/
	TriangulationNViews tnv;
	std::vector<std::deque<cv::Point>> v(caps.size());
	std::deque<cv::Point3d> v3;
	std::ofstream outfile;
	outfile.open(inputDir + "\\" + outputfile + ".gt.field" + std::to_string(fieldID) + ".csv", std::ios_base::out | std::ios_base::trunc);
	if(!outfile.is_open())
	{
		std::cout << "cannot open file to write " << (inputDir + "\\" + outputfile + ".gt.field" + std::to_string(fieldID) + ".csv") << std::endl;
		return;
	}
	outfile << "frame,x1,y1,z1" << std::endl;
	while (systemTime.getFrameNum() - 1 < imgCoords[0].size())
	{
		tnv.clear();
		for (int i = 0; i < caps.size(); ++i)
		{
			cv::Point pt = imgCoords[i][systemTime.getFrameNum() - 1];
			if (pt == cv::Point(-1, -1))
			{
				v[i].clear();
			}
			else
			{
				v[i].push_back(pt);
				tnv.add(calibs[i].getCameraPosition(), calibs[i].transformToRealWorld(pt, 110));
				if (v[i].size() > 100)
					v[i].pop_front();
			}
		}
		cv::Point3d gt3 = tnv.triangulate();
		outfile << systemTime.getFrameNum() << "," << gt3.x << "," << gt3.y << "," << gt3.z << std::endl;
		if (gt3 == cv::Point3d(-1, -1, -1))
		{
			v3.clear();
		}
		else
		{
			v3.push_back(gt3);
			if (v3.size() > 100)
				v3.pop_front();
		}
		for (int i = 0; i < caps.size(); ++i)
		{
			cv::Mat frameTemp, frameGray, frameBGR, frame;
			if (caps[i].read(frameTemp))
			{
				frameGray.create(frameTemp.rows, frameTemp.cols, CV_8UC1);

				static int from_to[] = { 0,0 };
				cv::mixChannels(&frameTemp, 1, &frameGray, 1, from_to, 1);

				cv::demosaicing(frameGray, frameBGR, cv::COLOR_BayerBG2BGR);

				if (frameBGR.size() != calibs[i].getImageSize())
				{
					cv::resize(frameBGR, frame, calibs[i].getImageSize(), 0.0, 0.0, cv::INTER_LINEAR);
				}
				else
				{
					frame = frameBGR;
				}
				cv::Mat world(cvRound(15000.0 / windowScale), cvRound(41000.0 / windowScale), CV_8UC3);
				world.setTo(0);
				for (int j = 0; j < fields.size(); ++j)
				{
					fields[j].show(world, windowScale);
					fields[j].show(frame, cv::Mat(), "cam" + std::to_string(fieldID), calibs[i], cv::Scalar(255, 255, 255), true);
				}
				if(v[i].size()>1)
				{
					std::deque<cv::Point>::const_iterator itPrev = v[i].begin();
					std::deque<cv::Point>::const_iterator itCurr = itPrev + 1;
					for (; itCurr != v[i].end(); itPrev = itCurr, ++itCurr)
					{
						cv::line(frame, (*itPrev), (*itCurr), cv::Scalar(0, 0, 255), 2, CV_AA);
					}
				}
				{
					std::deque<cv::Point3d>::const_iterator itPrev = v3.begin();
					std::deque<cv::Point3d>::const_iterator itCurr = itPrev + 1;
					for (; itCurr != v3.end(); itPrev = itCurr, ++itCurr)
					{
						cv::Point2d pt1;
						calibs[i].transformToPixel(*itPrev, pt1);
						cv::Point2d pt2;
						calibs[i].transformToPixel(*itCurr, pt2);

						cv::line(frame, pt1, pt2, cv::Scalar(0, 255, 255), 2, CV_AA);
						cv::line(world, cv::Point(cvRound(itPrev->x / windowScale), world.rows - cvRound(itPrev->y / windowScale)), cv::Point(cvRound(itCurr->x / windowScale), world.rows - cvRound(itCurr->y / windowScale)), cv::Scalar(0,255,255), 2, CV_AA);
					}
					cv::Point2d pt1;
					calibs[i].transformToPixel(*itPrev, pt1);
					cv::circle(frame, pt1, 8, cv::Scalar(0, 255, 255), -1);
					cv::Point pt2 = cv::Point(cvRound(itPrev->x / windowScale), world.rows - cvRound(itPrev->y / windowScale));
					cv::circle(world, pt2, 6, cv::Scalar(0, 255, 255), -1);

					cv::putText(frame, "Fr " + std::to_string(time()) + " [" + std::to_string(tnv.size()) + " " + std::to_string(cvRound(tnv.getError())) + "]", cv::Point(pt1) + cv::Point(10, 10), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 255, 255));
					cv::putText(world, "Fr " + std::to_string(time()) + " [" + std::to_string(tnv.size()) + " " + std::to_string(cvRound(tnv.getError())) + "]", cv::Point(pt2) + cv::Point(10, 10), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 255, 255));
				}
				
				cv::imshow(names[i], frame);
				cv::imshow("world", world);
			}
		}
		
		handleKey();

		systemTime.advance();
	}
	outfile.close();
}
void GenereteGT::setFields(std::vector<cv::Rect> f, int margin, int numCameras)
{
	for (int i = 0; i < f.size(); ++i)
	{
		fields.push_back(Field(i, f[i], margin, numCameras));
	}
}
void GenereteGT::handleKey()
{
	int k = 0;

	if (step)
	{
		k = cv::waitKey(0);
		step = false;
	}
	else
	{
		k = cv::waitKey(40);
		if (stop)
		{
			step = true;
		}
	}
	if (k == ' ')
	{
		stop = true;
		step = true;
	}
	if (k == 'g')
	{
		stop = false;
		step = false;
	}
}