// BallClicker.cpp : Defines the entry point for the console application.
//
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>


#pragma warning(push, 0)
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/info_parser.hpp>


//#include "StaticCameraModel.h"
//#include "Configuration.h"
//#include "ImageSourceAPI/ImageSource.h"
//#include "ImageSourceImpl/OpenCVImageSource.h"
#pragma warning(pop)

#include "Video.h"
#include "GenereteGT.h"

namespace po = boost::program_options;
namespace bfs = boost::filesystem;
namespace ba = boost::algorithm;


using namespace std;


#define KEY_QUIT 113
#define KEY_SAVE 115
#define KEY_YES 121
#define KEY_NO 110
#define KEY_ESC 27
#define KEY_BACK 98
#define KEY_SPACE 32
#define KEY_SWITCH_BALL_STATE 103
#define KEY_CHANGE_BALL_STATE 104
#define BALL_HEIGHT_RADIUS 110.0f
#define BALL_HEIGHT_UNKNOWN 0.0f
int g_iScale = 4;
bool g_bVerify;
bool g_bCancel;
bool g_bBack;
bool g_bGround;
bool g_bGoTo;
bool g_bStop;
bool g_bInit;
bool g_triangulate;
int g_iGoToFrame;
int g_iJumpToFrame;
int g_iStep = 1;
int field;

struct WindowData
{
	std::vector<cv::Point2i> vImgCoords;
	std::vector<bool> vInit;
	boost::shared_ptr<Video> video;
	std::string sTitle;
	cv::Rect roi;
	cv::Mat prevFrame;
	cv::Mat currFrame;
};

void parseConfigFile(const std::string& config, const po::options_description& options, po::variables_map& vm);
void CallBackFuncFull(int event, int x, int y, int flags, void* userdata);
void CallBackFuncZoom(int event, int x, int y, int flags, void* userdata);
void Save(std::vector<cv::Point> v, std::vector<bool> vInit, std::string sFilePath);
void Load(std::vector<cv::Point2i>& v, std::vector<bool>& vInit, std::string sFilePath);
void Load(std::vector<cv::Point3f>& v, std::string sFilePath);
cv::Mat ShowCurrentFrame(WindowData* pwd);
void ShowZoomFrame(WindowData* pwd, cv::Mat frame);
void UpdateCoords(WindowData* pwd, cv::Point2i pt2);
cv::Rect GetROI(cv::Point2i pt2, cv::Mat frame);
bool NextFrame(WindowData* pwd);
void ApplyFilter(WindowData* pwd, std::vector<cv::Point2i>& imgCoords);

void triangulate(std::string inputDir);
//start -d D:\sps\VR012_A001_r_run2 -n VR012.A001.r.run2.cam1 -f 1
//go to last frame -d D:\sps\VR012_A001_r_run2 -n VR012.A001.r.run2.cam1 -g 0 -f 1
//go to specific frame -d D:\sps\VR012_A001_r_run2 -n VR012.A001.r.run2.cam1 -g 150 -f 1
//middle button mouse skip if we edit
//left button mouse add/update position
//right button mouse continue to end

//-d D:\sps\VR007.A002.r.run1 -n VR007.A002.r.run1.cam1 -f 1
//-d D:\sps\VR007.A002.r.run1 -t 1 -f 2
int main(int ac, char* av[])
{
	int exitCode = 0;
	try
	{
		po::options_description genericp("Generic options");
		genericp.add_options()
			("dir,d", po::value<std::string>()->required(), "working directory")
			("filename,n", po::value<std::string>()->default_value(""), "file name")
			("goto,g", po::value<int>()->default_value(-1), "go to frame")
			("triangulate,t", po::value<bool>()->default_value(false), "triangulate")
			("field,f", po::value<int>()->required(), "field number");

		po::options_description cmdline_options;
		cmdline_options.add(genericp);

		po::variables_map vm;
		po::command_line_parser parser(ac, av);
		store(parser.options(cmdline_options).run(), vm);
		notify(vm);

		g_triangulate = false;
		g_bCancel = false;
		g_bBack = false;
		g_bGround = true;
		g_bStop = false;
		g_bInit = false;
		g_bVerify = vm.count("verify") > 0;
		g_bInit = vm.count("init") > 0;
		field = vm["field"].as<int>();
		std::string path = vm["dir"].as<string>();
		std::string configPath = path + "\\" + vm["filename"].as<string>() + ".cfg";
		std::string outputPathImgGT = path + "\\" + vm["filename"].as<string>() + ".field" +std::to_string(field)+ ".gt";
		std::string videoFile = vm["filename"].as<string>() + ".avi";
		g_triangulate = vm["triangulate"].as<bool>();
		

		if(g_triangulate)
		{
			triangulate(path);
			return 0;
		}
		if (g_iGoToFrame < -1)
		{
			std::stringstream s;
			s << "Wrong value for go to frame number " << g_iGoToFrame << "! it must be 0 (go to last processed frame) or value greater than 0 (go to specific frame)";
			throw std::exception(s.str().c_str());
		}
		if (g_bVerify && g_iGoToFrame == 0)
			g_iGoToFrame = -1;

		if (!g_bVerify)
		{
			std::cout << "initialisation of frames is only available in verification mode" << std::endl;
			g_bInit = false;
		}
		if (!bfs::exists(path))
		{
			std::stringstream s;
			s << "Directory " << path << " doesnt exist!";
			throw std::exception(s.str().c_str());
		}
		std::cout << path + "\\" + videoFile << std::endl;
		if (!bfs::exists(path + "\\" + videoFile) || !bfs::is_regular_file(path + "\\" + videoFile))
		{
			std::stringstream s;
			s << "Video file " << videoFile << " doesnt exist in directory " << path << "!";
			throw std::exception(s.str().c_str());
		}

		if (g_bVerify && (!bfs::exists(outputPathImgGT) || !bfs::is_regular_file(outputPathImgGT)))
		{
			std::stringstream s;
			s << "GT image data " << outputPathImgGT << " doesnt exist!";
			throw std::exception(s.str().c_str());
		}

		WindowData wd;
		wd.roi = cv::Rect(0, 0, 0, 0);

		if (g_iGoToFrame >= 0)
			Load(wd.vImgCoords, wd.vInit, outputPathImgGT);

		if (g_iGoToFrame == 0)
			g_iGoToFrame = static_cast<int>(wd.vImgCoords.size() + 1);
		else if (g_iGoToFrame > static_cast<signed int>(wd.vImgCoords.size()))
			g_iGoToFrame = static_cast<int>(wd.vImgCoords.size());
		g_bGoTo = (g_iGoToFrame > 0);

		wd.video = boost::make_shared<Video>(path + "\\" + vm["filename"].as<string>());
		wd.video->LoadVideo(path + "\\" + vm["filename"].as<string>() + ".avi");
		wd.sTitle = std::string("Video ") + videoFile;

		if (g_iJumpToFrame > -1)
			wd.video->goToFrame(g_iJumpToFrame);

		RECT desktop;
		SystemParametersInfo(SPI_GETWORKAREA, 0, &desktop, 0);

		cv::namedWindow(wd.sTitle, cv::WINDOW_NORMAL);
		cv::setMouseCallback(wd.sTitle, CallBackFuncFull, reinterpret_cast<void *>(&wd));
		//cv::resizeWindow(wd.sTitle, desktop.right, desktop.bottom);
		cv::moveWindow(wd.sTitle, desktop.right / 3, 0);
		cv::namedWindow(wd.sTitle + " zoom", cv::WINDOW_AUTOSIZE);
		cv::setMouseCallback(wd.sTitle + " zoom", CallBackFuncZoom, reinterpret_cast<void *>(&wd));
		cv::moveWindow(wd.sTitle + " zoom", 0, desktop.bottom / 2 + 50);

		if (!NextFrame(&wd))
		{
			std::stringstream s;
			s << "cannot read first frame of video file " << videoFile;
			throw std::exception(s.str().c_str());
		}
		for (;;)
		{
			wd.currFrame = ShowCurrentFrame(&wd);
			ShowZoomFrame(&wd, wd.currFrame);

			//std::cout << "Frame " << wd.video->getFrameNumber() + 1 << (IsGround(&wd) ? " ball on ground " : " ball in air    ");
			int key = -1;
			if (!g_bCancel && g_iGoToFrame != -1 && wd.video->getFrameNumber() + 1 < g_iGoToFrame && !g_bStop)
			{
				key = cv::waitKey(1);
				wd.prevFrame = wd.currFrame;
				if (!NextFrame(&wd))
				{
					std::cout << std::endl << "No more frames, press [s] to save data or [q] to quit without saving (( 1 ))" << std::endl;
					g_bCancel = true;
				}
				std::cout << std::endl;
			}
			else key = cv::waitKey(0);
			//std::cout << key << std::endl;
			switch (key)
			{
			case KEY_SAVE:
			{
				Save(wd.vImgCoords, wd.vInit, outputPathImgGT);

				std::cout << std::endl << wd.vImgCoords.size() << " ball coordinates have been saved to " << outputPathImgGT << std::endl;
				if ((g_bVerify && (wd.video->getFrameNumber() + 1) < wd.vImgCoords.size()) || (!g_bVerify))
				{
					std::cout << "continue" << std::endl;
					break;
				}
				else
				{
					std::cout << "finished" << std::endl;
					{
						cv::destroyAllWindows();
						return 0;
					}
				}
				break;
			}
			case KEY_ESC:
			{
				cv::destroyAllWindows();
				return 0;
			}
			}
		}
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
		exitCode = 1;
	}
	cv::destroyAllWindows();
	return exitCode;
}
void CallBackFuncFull(int event, int x, int y, int /*flags*/, void* userdata)
{
	if (!g_bInit && !g_bCancel && event == cv::EVENT_LBUTTONDOWN)
	{
		WindowData* pwd = reinterpret_cast<WindowData*>(userdata);
		cv::Point2i pt2(x, y);

		std::cout << " " << pt2;
		UpdateCoords(pwd, pt2);

		pwd->prevFrame = pwd->currFrame;
		if ((g_bVerify && NextFrame(pwd) && (pwd->video->getFrameNumber()) < pwd->vImgCoords.size()) || (!g_bVerify && NextFrame(pwd)))
		{
			pwd->currFrame = ShowCurrentFrame(pwd);
			ShowZoomFrame(pwd, pwd->currFrame);
		}
		else
		{
			std::cout << "No more frames, press [s] to save data or [q] to quit without saving (( 2 ))" << std::endl;
			g_bCancel = true;
		}
	}
	if (event == cv::EVENT_MBUTTONDOWN)
	{
		WindowData* pwd = reinterpret_cast<WindowData*>(userdata);
		if (pwd->video->getFrameNumber() < pwd->vImgCoords.size())
		{
			pwd->prevFrame = pwd->currFrame;
			std::cout << " skip " << std::endl;

			if ((g_bVerify && NextFrame(pwd) && (pwd->video->getFrameNumber()) < pwd->vImgCoords.size()) || (!g_bVerify && NextFrame(pwd)))
			{
				pwd->currFrame = ShowCurrentFrame(pwd);
				ShowZoomFrame(pwd, pwd->currFrame);
			}
			else
			{
				std::cout << "No more frames, press [s] to save data or [q] to quit without saving (( 4 ))" << std::endl;
				g_bCancel = true;
			}
		}
	}
	if (event == cv::EVENT_RBUTTONDOWN)
	{
		WindowData* pwd = reinterpret_cast<WindowData*>(userdata);
		if (pwd->video->getFrameNumber() < pwd->vImgCoords.size())
		{
			g_iGoToFrame = static_cast<int>(pwd->vImgCoords.size()) + 1;
			for (;;)
			{
				if (!g_bCancel && g_iGoToFrame != -1 && pwd->video->getFrameNumber() < g_iGoToFrame)
				{
					std::cout << " skip " << std::endl;
					cv::waitKey(1);
					pwd->prevFrame = pwd->currFrame;
					if (!NextFrame(pwd))
					{
						std::cout << std::endl << "No more frames, press [s] to save data or [q] to quit without saving (( 1 ))" << std::endl;
						g_bCancel = true;
					}

					pwd->currFrame = ShowCurrentFrame(pwd);
					ShowZoomFrame(pwd, pwd->currFrame);

					if (pwd->video->getFrameNumber() + 1 == g_iGoToFrame)
						break;
				}
			}
		}
	}
}
void CallBackFuncZoom(int event, int x, int y, int /*flags*/, void* userdata)
{
	if (event == cv::EVENT_LBUTTONDOWN)
	{
		WindowData* pwd = reinterpret_cast<WindowData*>(userdata);
		cv::Point2i pt2(x, y);
		pt2.x = cvRound(pt2.x / g_iScale);
		pt2.y = cvRound(pt2.y / g_iScale);

		pt2 = pwd->roi.tl() + pt2;

		std::cout << " " << pt2;
		UpdateCoords(pwd, pt2);

		pwd->prevFrame = pwd->currFrame;
		if ((g_bVerify && NextFrame(pwd) && (pwd->video->getFrameNumber()) < pwd->vImgCoords.size()) || (!g_bVerify && NextFrame(pwd)))
		{
			pwd->currFrame = ShowCurrentFrame(pwd);
			ShowZoomFrame(pwd, pwd->currFrame);
		}
		else
		{
			std::cout << "No more frames, press [s] to save data or [q] to quit without saving (( 4 ))" << std::endl;
			g_bCancel = true;
		}
	}
	if (event == cv::EVENT_MBUTTONDOWN)
	{
		WindowData* pwd = reinterpret_cast<WindowData*>(userdata);
		if (pwd->video->getFrameNumber() < pwd->vImgCoords.size())
		{
			pwd->prevFrame = pwd->currFrame;
			std::cout << " skip " << std::endl;

			if ((g_bVerify && NextFrame(pwd) && (pwd->video->getFrameNumber()) < pwd->vImgCoords.size()) || (!g_bVerify && NextFrame(pwd)))
			{
				pwd->currFrame = ShowCurrentFrame(pwd);
				ShowZoomFrame(pwd, pwd->currFrame);
			}
			else
			{
				std::cout << "No more frames, press [s] to save data or [q] to quit without saving (( 4 ))" << std::endl;
				g_bCancel = true;
			}
		}
	}
	if (event == cv::EVENT_RBUTTONDOWN)
	{
		WindowData* pwd = reinterpret_cast<WindowData*>(userdata);
		if (pwd->video->getFrameNumber() < pwd->vImgCoords.size())
		{
			g_iGoToFrame = static_cast<int>(pwd->vImgCoords.size()) + 1;
			for (;;)
			{
				if (!g_bCancel && g_iGoToFrame != -1 && pwd->video->getFrameNumber() < g_iGoToFrame)
				{
					std::cout << " skip " << std::endl;
					cv::waitKey(1);
					pwd->prevFrame = pwd->currFrame;
					if (!NextFrame(pwd))
					{
						std::cout << std::endl << "No more frames, press [s] to save data or [q] to quit without saving (( 1 ))" << std::endl;
						g_bCancel = true;
					}

					pwd->currFrame = ShowCurrentFrame(pwd);
					ShowZoomFrame(pwd, pwd->currFrame);

					if (pwd->video->getFrameNumber() + 1 == g_iGoToFrame)
						break;
				}
			}
		}
	}

}
bool NextFrame(WindowData* pwd)
{
	if (!g_bBack)
	{
		int i = 1;
		while (i < g_iStep)
		{
			pwd->video->grab();
			i++;
		}
		pwd->video->next();
		if (pwd->video->getFrame().empty())
			return false;
	}
	return true;
}
cv::Mat ShowCurrentFrame(WindowData* pwd)
{
	cv::Mat frame = pwd->video->getFrame().clone();
	cv::Mat fr = frame.clone();
	if (pwd->video->getFrameNumber() < pwd->vImgCoords.size())
		cv::circle(frame, pwd->vImgCoords[pwd->video->getFrameNumber()], 5, cv::Scalar(0, 255, 255), -1);
	cv::rectangle(frame, cv::Point(0, 0), cv::Point(80, 80), cv::Scalar(0, 0, 255), 2);
	cv::putText(frame, "no", cv::Point(15, 25), CV_FONT_HERSHEY_PLAIN, 2, cv::Scalar(0, 0, 255), 2);
	cv::putText(frame, "ball", cv::Point(15, 55), CV_FONT_HERSHEY_PLAIN, 2, cv::Scalar(0, 0, 255), 2);
	cv::imshow(pwd->sTitle, frame);
	g_bBack = false;
	std::cout << "Frame " << pwd->video->getFrameNumber() + 1;
	/*if (pwd->video->getFrameNumber() >= 1700 && pwd->video->getFrameNumber() <= 1900)
	cv::imwrite((boost::format("E:\\M60_MSV_HZ1_1_org_corr\\%5d.jpg") % pwd->video->getFrameNumber()).str(), frame);*/
	return fr;
}
void ShowZoomFrame(WindowData* pwd, cv::Mat frame)
{
	if (pwd->video->getFrameNumber() > 0)
	{
		frame = frame.clone();
		if (pwd->video->getFrameNumber() + 1 < pwd->vImgCoords.size())
		{
			cv::circle(frame, pwd->vImgCoords[pwd->video->getFrameNumber()], 5, cv::Scalar(0, 255, 255), -1);
			pwd->roi = GetROI(pwd->vImgCoords[pwd->video->getFrameNumber()], frame);
		}
		else
		{
			pwd->roi = GetROI(pwd->vImgCoords[pwd->video->getFrameNumber() - 1], frame);
		}
		if (pwd->video->getFrameNumber() < pwd->vImgCoords.size())
		{
			std::cout << " " << pwd->vImgCoords[pwd->video->getFrameNumber()];
		}

		cv::Mat zoom = frame(pwd->roi);
		cv::resize(zoom, zoom, zoom.size() * g_iScale);

		std::stringstream ss;
		if (pwd->video->getFrameNumber() + 1 < pwd->vImgCoords.size())
		{
			ss << "Frame " << pwd->video->getFrameNumber() + 1 << " -> current" << " " << pwd->vImgCoords[pwd->video->getFrameNumber()];
		}
		else
		{
			ss << "Frame " << pwd->video->getFrameNumber() + 1 << " -> previous" << " " << pwd->vImgCoords[pwd->video->getFrameNumber() - 1];
		}
		cv::putText(zoom, ss.str(), cv::Point(25, 25), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(255, 255, 255));
		cv::imshow(pwd->sTitle + " zoom", zoom);

	}
}
void UpdateCoords(WindowData* pwd, cv::Point2i pt2)
{
	if (pwd->video->getFrameNumber() < pwd->vImgCoords.size())
	{
		pwd->vImgCoords[pwd->video->getFrameNumber()] = pt2;
		std::cout << " update " << std::endl;
	}
	else
	{
		pwd->vImgCoords.push_back(pt2);
		std::cout << " add " << std::endl;
	}
}
cv::Rect GetROI(cv::Point2i pt2, cv::Mat frame)
{
	cv::Rect r(pt2.x - 100, pt2.y - 100, 200, 200);
	if (r.tl().x < 0)
		r.x = 0;
	if (r.tl().y < 0)
		r.y = 0;
	if (r.br().x >= frame.size().width)
		r.width = frame.size().width - r.x;
	if (r.br().y >= frame.size().height)
		r.height = frame.size().height - r.y;
	return r;
}
void Save(std::vector<cv::Point> v, std::vector<bool> vInit, std::string sFilePath)
{
	try
	{
		//if (bfs::exists(sFilePath) && bfs::is_regular_file(sFilePath))
		//{
		//	std::string sBackupFilePath = sFilePath;
		//	boost::replace_last(sBackupFilePath, ".gt", "_backup.gt");

		//	//std::cout << sFilePath << std::endl;
		//	//std::cout << sBackupFilePath << std::endl;

		//	boost::system::error_code ec;
		//	bfs::copy_file(sFilePath, sBackupFilePath, bfs::copy_option::overwrite_if_exists, ec);

		//	//std::cout << ec.message() << std::endl;
		//}
		bfs::fstream f(bfs::path(sFilePath), std::ios_base::out | std::ios_base::trunc);
		if (f.is_open() == true)
		{
			int j = 0;
			for (long i = 0; i < v.size(); ++i)
			{
				if (v[i].x >= 80 || v[i].y >= 80)
				{
					f << v[i].x << " " << v[i].y << std::endl;
				}
				else
				{
					f << -1 << " " << -1 << std::endl;
				}
			}
			f.close();
		}
		else
		{
			std::stringstream s;
			s << "cannot create file " << sFilePath;
			throw std::exception(s.str().c_str());
		}
	}
	catch (...)
	{
		std::stringstream s;
		s << "Unknown exception.";
		throw std::exception(s.str().c_str());
	}
}
void Load(std::vector<cv::Point2i>& v, std::vector<bool>& vInit, std::string sFilePath)
{
	bfs::fstream f(bfs::path(sFilePath), std::ios_base::in);
	if (f.is_open() == true)
	{
		std::string s;
		cv::Point2i pt;

		while (f)
		{
			std::getline(f, s);
			if (!f)
				break;
			if (s[s.length() - 1] != ']')
			{
				vInit.push_back(s[s.length() - 1] == '1');
				s = s.substr(1, s.length() - 4);
			}
			else
			{
				s = s.substr(1, s.length() - 2);
				vInit.push_back(false);
			}
			std::vector<std::string> ss;
			boost::split(ss, s, boost::is_any_of(","));
			ba::trim(ss[0]);
			ba::trim(ss[1]);
			pt.x = boost::lexical_cast<int>(ss[0]);
			pt.y = boost::lexical_cast<int>(ss[1]);
			v.push_back(pt);
		}
		f.close();
	}
}
void Load(std::vector<cv::Point3f>& v, std::string sFilePath)
{
	bfs::fstream f(bfs::path(sFilePath), std::ios_base::in);
	if (f.is_open() == true)
	{
		std::string s;
		cv::Point3f pt;

		while (f)
		{
			std::getline(f, s);
			if (!f)
				break;
			if (s[s.length() - 1] != ']')
				s = s.substr(1, s.length() - 4);
			else s = s.substr(1, s.length() - 2);
			std::vector<std::string> ss;
			boost::split(ss, s, boost::is_any_of(","));
			ba::trim(ss[0]);
			ba::trim(ss[1]);
			ba::trim(ss[2]);
			pt.x = boost::lexical_cast<float>(ss[0]);
			pt.y = boost::lexical_cast<float>(ss[1]);
			pt.z = boost::lexical_cast<float>(ss[2]);
			v.push_back(pt);
		}
		f.close();
	}
	else
	{
		std::stringstream s;
		s << "cannot open file " << sFilePath;
		throw std::exception(s.str().c_str());
	}
}
void ApplyFilter(WindowData* pwd, std::vector<cv::Point2i>& imgCoords)
{
	std::vector<cv::Point2i> r = imgCoords;
	for (int i = 1; i < r.size() - 1; ++i)
	{
		imgCoords[i].x = cvRound((r[i - 1].x + r[i].x + r[i + 1].x) / 3);
		imgCoords[i].y = cvRound((r[i - 1].y + r[i].y + r[i + 1].y) / 3);
	}
}
void triangulate(std::string inputDir)
{
	if (!boost::filesystem::exists(inputDir))
	{
		std::cout << "Wrong input directory " << inputDir << std::endl;
		return;
	}
	boost::filesystem::path configFile(inputDir + std::string("\\config.xml"));
	if (!boost::filesystem::exists(configFile))
	{
		std::cout << "No config file " << configFile.string() << std::endl;
		return;
	}
	std::string vidFile;
	std::string conFile;
	std::string outFile;
	GenereteGT generator;
	try
	{
		cv::FileStorage fs(configFile.string(), cv::FileStorage::READ);
		if (fs.isOpened())
		{
			int numFields, numPoints, margin;
			cv::FileNode n = fs["fields"];
			n["margin"] >> margin;
			n["num"] >> numFields;

			std::vector<cv::Rect> fields(numFields);
			for (int i = 1; i <= numFields; ++i)
			{
				cv::Point tl;
				cv::Point br;
				cv::FileNode nn = n["field" + std::to_string(i)];
				nn["tl"] >> tl;
				nn["br"] >> br;
				fields[i - 1] = cv::Rect(tl, br);
			}
			std::vector<cv::Point3d> mask;
			n = fs["mask"];
			n["num"] >> numPoints;
			mask.resize(numPoints);
			for (int j = 1; j <= numPoints; ++j)
			{
				n["pt" + std::to_string(j)] >> mask[j - 1];
			}

			int numCameras;
			n = fs["cameras"];
			n["num"] >> numCameras;

			generator.setFields(fields, margin, numCameras);

			for (int i = 1; i <= numCameras; ++i)
			{
				std::string workerID = "cam" + std::to_string(i);

				cv::FileNode nn = n[workerID];
				nn["video"] >> vidFile;
				nn["config"] >> conFile;
				nn["output"] >> outFile;

				if (!boost::filesystem::exists(inputDir + std::string("\\") + vidFile))
				{
					std::cout << "Video file doesnt exist: " << vidFile << std::endl;
					return;
				}
				if (!boost::filesystem::exists(inputDir + std::string("\\") + conFile))
				{
					std::cout << "Config file doesnt exist: " << conFile << std::endl;
					return;
				}
				generator.addJob(workerID, vidFile, conFile, inputDir);
			}
			fs.release();
		}
		else
		{
			std::cout << "Cannot open config file " << configFile << std::endl;
			return;
		}
	}
	catch (cv::Exception& ex)
	{
		std::cout << "Config processing exception: " << ex.msg << std::endl;
		return;
	}

	generator.init(field);
	generator.process();
}