#pragma once
#include <string>
#include <vector>
#include <core/mat.hpp>
#include "CameraCalibration.h"
#include "TimeWatch.hpp"
#include "Field.h"

class GenereteGT
{
public:
	GenereteGT();
	virtual ~GenereteGT();

	void addJob(std::string name, std::string videoFile, std::string configFile, std::string inputDir);
	void process();
	void setFields(std::vector<cv::Rect> f, int margin, int numCameras);
	void init(int field);
	void handleKey();

	inline int64_t time() { return systemTime.getFrameNum(); };
private:
	TimeWatch systemTime;
	FieldList fields;
	double windowScale;
	int fieldID;

	std::vector<std::string> names;
	std::vector<std::string> videoFiles;
	std::vector<std::string> configFiles;
	std::string inputDir;
	std::string outputfile;
	std::vector<std::vector<cv::Point>> imgCoords;

	std::vector<CameraCalibration> calibs;
	std::vector<cv::VideoCapture> caps;
	std::vector<std::vector<std::vector<std::tuple<cv::Point3d, int, int>>>> ballData;

	bool stop;
	bool step;
};

