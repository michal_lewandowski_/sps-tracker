#pragma once
#include <opencv.hpp>
#include <memory>
#include <unordered_map>

class ColorBallMap;
class CameraCalibration;
class KFTrack2d;
class KFTrack3d;

class Field
{
public:
	Field(int id, cv::Rect r, int margin, int camsNum);
	virtual ~Field();

	void show(cv::Mat frame, double windowScale);
	void show(cv::Mat frame1, cv::Mat frame2, std::string camID, CameraCalibration& calibration, cv::Scalar color, bool show);
	void prepare();
	std::vector<cv::Point> project2d(cv::Rect f, CameraCalibration& calibration) const;
	bool isInPlayerZone(const cv::Point3d& pos);
	std::pair<cv::Point3d, cv::Rect> closestPlayerZone(cv::Point3d pos);

	int fid;
	cv::Rect field;
	cv::Rect fieldExtended;
	std::vector<int> numObservations;
	std::vector<cv::Rect> zonePlayer1;
	std::vector<cv::Rect> zonePlayer2;
	cv::Rect zoneGame;
	cv::Rect detectionWindow;

	double maxAbsVelocityGlobal;
	double maxBallProbabilityGlobal;
};

class FieldList : public std::vector<Field>
{
public:
	Field* inField(const cv::Point3d& pos);
	const Field* inField(const cv::Point3d& pos) const;
	Field* find(int fid);
	const Field* find(int fid) const;
};