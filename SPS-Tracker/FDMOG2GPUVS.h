#pragma once
#include <core/cuda.hpp>
#include "vt_bgfs_mog2.h"
#include <cudafilters.hpp>

class FDMOG2GPUVT_MASK
{
public:
	//! Ctor Foreground detection. Call init(shared_ptr<ImageSource>, const vistrack::SoccerLibrary::Configuration&) afterwards.
	FDMOG2GPUVT_MASK();
	//! Dtor
	~FDMOG2GPUVT_MASK();
	/*!\param nMixtures number of gaussian to model the background
		* \warning this method is not threadsafe
		*/
	void setNumberOfGaussiansPerPixel(unsigned nMixtures);
	/*! variance threshold to determine if background or foreground
		* \warning this method is not threadsafe
		*/
	void setVarianceThreshold(float varThreshold);
	void setBackgroundRatio(float backgroundRatio);
	void setInitialVariance(float initialVariance);
	void setMinVariance(float minVariance);
	void setMaxVariance(float maxVariance);
	void setComplexityReductionPrior(float ct);
	void enableShadowDetection(bool shadowDetection = true);
	void setShadowThreshold(float tau);
	void setLearningRate(float learningRate);
	void setHfactor(float fHSVFactorH);
	void setSfactor(float fHSVFactorS);
	void setVfactor(float fHSVFactorV);
	void enableHSV(bool hsvInsteadOfRGB);
	void enableRegressiveLearning(bool learningIsRegressiv = true);
	void setMaxLearningRate(float maxLearningRate);
	void setMinLearningRate(float minLearningRate);
	//! regressionFactor factor used for decreasing the learning factor from max to min (typical value range: 0.01 - 0.2)
	void setRegressionFactor(float regressionFactor);
	//! \param closingSize closing element size must be odd; if closing <= 0 no morphological closing operation will be done
	void setClosingSize(int closingSize);
	/*! Initialises this detector.
	\param imageSourcePtr pointer to an image source that provides the image sequence to process
	\param config configuration parameters
	*/
	void init(cv::Mat fieldMask);
	/*! Detect the foreground
	*/
	void detectForeground(cv::Mat img, cv::Mat& mask);
	//! \return name for the algorithm used to detect the foreground in the image
	std::string getName() const;

private:
	//! copy ctor should not work
	FDMOG2GPUVT_MASK(const FDMOG2GPUVT_MASK&);
	//! assignment operator should not work
	FDMOG2GPUVT_MASK& operator=(const FDMOG2GPUVT_MASK&);
	//! method to addapt the learning rate
	void addaptLearingRate();

protected:
	//! the background model
	VT_MOG2_GPU m_backgroundModel;
	cv::cuda::GpuMat m_morphBuf1;
	cv::cuda::GpuMat m_morphBuf2;
	//! learning rate for the background model
	float m_learningRate;
	cv::Mat m_closingElement;
	bool m_isHSV;
	//! learning addaptiv in the start phase
	bool m_learningIsRegressiv;
	//! max learning rate in the begining
	float m_maxLearningRate;
	//! min learning rate in the end
	float m_minLearningRate;
	//! factor used for degreasing the learing factor from max to min 
	float m_regressionFactor;
	//! number of frames till next degrease of learning rate
	unsigned m_framesToNextDegrase;
	cv::cuda::GpuMat m_fieldMaskGpuMat;
	cv::cuda::GpuMat m_gpuMask;
	cv::cuda::GpuMat m_gpStructuringElem;
	cv::Ptr<cv::cuda::Filter> morph;
};
