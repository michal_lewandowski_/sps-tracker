#include "Utils.h"
#include <regex>
#include "../SPS-BallClicker/Field.h"

void Utils::parse_csint(const std::string& str, std::vector<float>& result)
{

	typedef std::regex_iterator<std::string::const_iterator> re_iterator;
	typedef re_iterator::value_type re_iterated;

	std::regex re("([-+]?[0-9]*\\.?[0-9]+)");

	re_iterator rit(str.begin(), str.end(), re);
	re_iterator rend;

	std::transform(rit, rend, std::back_inserter(result),
		[](const re_iterated& it) { return std::stof(it[1]); });

}

std::vector<cv::Point3d> Utils::readGT(std::string filePath)
{
	std::vector<cv::Point3d> data;

	std::ifstream infile;
	infile.open(filePath, std::ios_base::in);
	if(infile.is_open())
	{
		std::string line;
		std::getline(infile, line);
		while (std::getline(infile, line))
		{
			std::vector<float> pos;
			Utils::parse_csint(line, pos);
			data.push_back(cv::Point3d(pos[1], pos[2], pos[3]));
		}
		infile.close();
	}
	return data;
}
unsigned int popcount32(unsigned int n)
{
	n -= ((n >> 1) & 0x55555555);
	n = (n & 0x33333333) + ((n >> 2) & 0x33333333);
	return (((n + (n >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24;
}

void tldRectToPoints(cv::Rect rect, cv::Point *p1, cv::Point *p2)
{
	p1->x = rect.x;
	p1->y = rect.y;
	p2->x = rect.x + rect.width;
	p2->y = rect.y + rect.height;
}

void tldBoundingBoxToPoints(int *bb, cv::Point *p1, cv::Point *p2)
{
	p1->x = bb[0];
	p1->y = bb[1];
	p2->x = bb[0] + bb[2];
	p2->y = bb[1] + bb[3];
}

cv::Rect tldBoundaryToRect(int *boundary)
{
	return cv::Rect(boundary[0], boundary[1], boundary[2], boundary[3]);
}

float tldCalcMean(const cv::Mat& intImg, int x, int y, int w, int h)
{
	cv::Point A(x, y);
	cv::Point B(x + w, y);
	cv::Point C(x + w, y + h);
	cv::Point D(x, y + h);

	float area = static_cast<float>(w * h);
	float mX = (intImg.at<int>(C) - intImg.at<int>(B) - intImg.at<int>(D) + intImg.at<int>(A)) / area;

	return mX;
}
float tldCalcVariance(const cv::Mat& intImg, const cv::Mat& intSqImg, int x, int y, int w, int h)
{
	cv::Point A(x, y);
	cv::Point B(x + w, y);
	cv::Point C(x + w, y + h);
	cv::Point D(x, y + h);

	CV_DbgAssert(!intImg.empty() && !intSqImg.empty());

	double area = w * h;
	double mX = (intImg.at<int>(C) - intImg.at<int>(B) - intImg.at<int>(D) + intImg.at<int>(A)) / area;
	double mX2 = (intSqImg.at<double>(C) - intSqImg.at<double>(B) - intSqImg.at<double>(D) + intSqImg.at<double>(A)) / area;

	return static_cast<float>(mX2 - mX * mX);
}
void tldCalcBinary(const cv::Mat& intImg, const cv::Rect *bb, int& binaryP, int& binaryN)
{
	tldCalcBinary(intImg, bb->x, bb->y, bb->width, bb->height, binaryP, binaryN);
}
void tldCalcBinary(const cv::Mat& intImg, const int *bb, int& binaryP, int& binaryN)
{
	tldCalcBinary(intImg, bb[0], bb[1], bb[2], bb[3], binaryP, binaryN);
}
void tldCalcBinary(const cv::Mat& intImg, int x, int y, int w, int h, int& binaryP, int& binaryN)
{
	binaryP = 0;
	binaryN = 0;
	float m = tldCalcMean(intImg, x, y, w, h);
	int stepX = cvFloor(w / static_cast<double>(TLD_BINARY_BLOCK_SIZE));
	int stepY = cvFloor(h / static_cast<double>(TLD_BINARY_BLOCK_SIZE));
	int s = 0;
	for (int yy = 0; yy < TLD_BINARY_BLOCK_SIZE; ++yy)
	{
		for (int xx = 0; xx < TLD_BINARY_BLOCK_SIZE; ++xx)
		{
			int xx1 = x + xx * stepX;
			int yy1 = y + yy * stepY;
			int xx2 = std::min(xx1 + stepX, x + w);
			int yy2 = std::min(yy1 + stepY, y + h);
			cv::Rect bb(cv::Point(xx1, yy1), cv::Point(xx2, yy2));
			float bm = tldCalcMean(intImg, bb.x, bb.y, bb.width, bb.height);
			if (m > bm)
			{
				binaryP += (1 << s);
			}
			else
			{
				binaryN += (1 << s);
			}
			s++;
		}
	}
}
float tldBBOverlap(cv::Rect *bb1, int *bb2)
{
	int bb3[4];
	bb3[0] = bb1->x;
	bb3[1] = bb1->y;
	bb3[2] = bb1->width;
	bb3[3] = bb1->height;

	return tldBBOverlap(bb3, bb2);
}

float tldBBOverlap(int *bb1, int *bb2)
{

	if (bb1[0] > bb2[0] + bb2[2])
	{
		return 0.0f;
	}

	if (bb1[1] > bb2[1] + bb2[3])
	{
		return 0.0f;
	}

	if (bb1[0] + bb1[2] < bb2[0])
	{
		return 0.0f;
	}

	if (bb1[1] + bb1[3] < bb2[1])
	{
		return 0.0f;
	}

	int colInt = std::min(bb1[0] + bb1[2], bb2[0] + bb2[2]) - std::max(bb1[0], bb2[0]);
	int rowInt = std::min(bb1[1] + bb1[3], bb2[1] + bb2[3]) - std::max(bb1[1], bb2[1]);

	int intersection = colInt * rowInt;
	int area1 = bb1[2] * bb1[3];
	int area2 = bb2[2] * bb2[3];
	return intersection / static_cast<float>(area1 + area2 - intersection);
}

void tldOverlapOne(int *windows, int numWindows, int index, std::vector<int> * indices, float *overlap)
{
	for (size_t i = 0; i < indices->size(); ++i)
	{
		CV_Assert(TLD_WINDOW_SIZE * index < TLD_WINDOW_SIZE * numWindows);
		CV_Assert(TLD_WINDOW_SIZE * indices->at(i) < TLD_WINDOW_SIZE * numWindows);

		overlap[i] = tldBBOverlap(&windows[TLD_WINDOW_SIZE * index], &windows[TLD_WINDOW_SIZE * indices->at(i)]);
	}
}

float tldOverlapRectRect(cv::Rect r1, cv::Rect r2)
{
	int bb1[4];
	int bb2[4];
	tldRectToArray<int>(r1, bb1);
	tldRectToArray<int>(r2, bb2);

	return tldBBOverlap(bb1, bb2);

}

cv::Rect *tldCopyRect(cv::Rect *r)
{
	cv::Rect *r2 = new cv::Rect();
	r2->x = r->x;
	r2->y = r->y;
	r2->width = r->width;
	r2->height = r->height;
	return r2;
}

void tldOverlapRect(int *windows, int numWindows, cv::Rect *boundary, float *overlap)
{
	int bb[4];
	bb[0] = boundary->x;
	bb[1] = boundary->y;
	bb[2] = boundary->width;
	bb[3] = boundary->height;

	tldOverlap(windows, numWindows, bb, overlap);
}

void tldOverlap(int *windows, int numWindows, int *boundary, float *overlap)
{
	for (int i = 0; i < numWindows; i++)
	{
		overlap[i] = tldBBOverlap(boundary, &windows[TLD_WINDOW_SIZE * i]);
	}
}

bool tldSortByOverlapDesc(std::pair<int, float> bb1, std::pair<int, float> bb2)
{
	return bb1.second > bb2.second;
}

bool tldSortByOverlapAsc(std::pair<int, float> bb1, std::pair<int, float> bb2)
{
	return bb1.second < bb2.second;
}

//Checks whether bb1 is completely inside bb2
int tldIsInside(int *bb1, int *bb2)
{

	if (bb1[0] > bb2[0] && bb1[1] > bb2[1] && bb1[0] + bb1[2] < bb2[0] + bb2[2] && bb1[1] + bb1[3] < bb2[1] + bb2[3])
	{
		return 1;
	}
	else return 0;

}

cv::Mat showDetectionModel(const std::deque<NormalizedPatch>& m)
{
	if (m.size()>0)
	{
		cv::Mat img(TLD_PATCH_SIZE, TLD_PATCH_SIZE, CV_8UC1);
		cv::Mat rimg(TLD_VIS_PATCH_SCALE*TLD_PATCH_SIZE, TLD_VIS_PATCH_SCALE*TLD_PATCH_SIZE, CV_8UC1);
		int size = cvCeil(sqrtf(m.size()));
		cv::Mat res(TLD_VIS_PATCH_SCALE*TLD_PATCH_SIZE * size, TLD_VIS_PATCH_SCALE*TLD_PATCH_SIZE * size, CV_8UC1);
		res.setTo(0);
		for (int ind = 0; ind < std::min(static_cast<int>(m.size()), size*size); ++ind)
		{
			int i = ind % size;
			int j = ind / size;
			for (int x = 0; x < TLD_PATCH_SIZE; x++)
			{
				for (int y = 0; y < TLD_PATCH_SIZE; y++)
				{
					img.at<uchar>(y, x) = cv::saturate_cast<uchar>(m[ind].values[y * TLD_PATCH_SIZE + x] + m[ind].mean);
				}
			}
			cv::resize(img, rimg, cv::Size(), TLD_VIS_PATCH_SCALE, TLD_VIS_PATCH_SCALE, CV_INTER_CUBIC);
			rimg.copyTo(res(cv::Rect(i*TLD_VIS_PATCH_SCALE*TLD_PATCH_SIZE, j*TLD_VIS_PATCH_SCALE*TLD_PATCH_SIZE, TLD_VIS_PATCH_SCALE*TLD_PATCH_SIZE, TLD_VIS_PATCH_SCALE*TLD_PATCH_SIZE)));
		}
		return res;
	}
	return cv::Mat();
}
//cv::Mat showCurrentPatch(cv::Mat frame, cv::Mat integral, FieldList& fields)
//{
//	cv::Mat m;
//	for (auto& f : fields)
//	{
//		if (o->currBB)
//		{
//			cv::Mat p = showCurrentPatch(frame, integral, *o->currBB);
//			if (m.empty())
//			{
//				m = p;
//			}
//			else
//			{
//				cv::vconcat(m, p, m);
//			}
//		}
//	}
//	return m;
//}
cv::Mat showCurrentPatch(cv::Mat frame, cv::Mat integral, cv::Rect r)
{
	cv::Mat gray;
	if (frame.channels() > 1)
		cv::cvtColor(frame, gray, CV_BGR2GRAY);
	else gray = frame;

	NormalizedPatch np;
	np.extract(gray, integral, r.x, r.y, r.width, r.height);
	cv::Mat i, l, h;
	std::deque<NormalizedPatch> d;
	d.push_back(np);

	cv::Mat img;
	if (!i.empty())
	{
		img = i;
		if (!l.empty())
			cv::hconcat(img, l, img);
	}
	return img;
}

cv::Mat tan_triggs_preprocessing(cv::InputArray src, float alpha, float tau, float gamma, int sigma0, int sigma1)
{
	cv::Mat Y;
	cv::Mat X = src.getMat().clone();
	if (X.channels() > 1)
		cv::cvtColor(X, Y, CV_BGR2GRAY);
	else Y = X.clone();

	Y.convertTo(X, CV_32FC1);
	// Start preprocessing:
	cv::Mat I;
	cv::pow(X, gamma, I);
	// Calculate the DOG Image:
	{
		cv::Mat gaussian0, gaussian1;
		// Kernel Size:
		int kernel_sz0 = (3 * sigma0);
		int kernel_sz1 = (3 * sigma1);
		// Make them odd for OpenCV:
		kernel_sz0 += ((kernel_sz0 % 2) == 0) ? 1 : 0;
		kernel_sz1 += ((kernel_sz1 % 2) == 0) ? 1 : 0;
		cv::GaussianBlur(I, gaussian0, cv::Size(kernel_sz0, kernel_sz0), sigma0, sigma0, cv::BORDER_REPLICATE);
		cv::GaussianBlur(I, gaussian1, cv::Size(kernel_sz1, kernel_sz1), sigma1, sigma1, cv::BORDER_REPLICATE);
		cv::subtract(gaussian0, gaussian1, I);
	}

	{
		double meanI = 0.0;
		{
			cv::Mat tmp;
			cv::pow(abs(I), alpha, tmp);
			meanI = cv::mean(tmp).val[0];

		}
		I = I / pow(meanI, 1.0 / alpha);
	}

	{
		double meanI = 0.0;
		{
			cv::Mat tmp;
			cv::pow(min(abs(I), tau), alpha, tmp);
			meanI = cv::mean(tmp).val[0];
		}
		I = I / cv::pow(meanI, 1.0 / alpha);
	}

	// Squash into the tanh:
	{
		cv::Mat exp_x, exp_negx;
		cv::exp(I / tau, exp_x);
		cv::exp(-I / tau, exp_negx);
		cv::divide(exp_x - exp_negx, exp_x + exp_negx, I);
		I = tau * I;
	}
	cv::Mat res;
	cv::normalize(I, res, 0, 255, cv::NORM_MINMAX, CV_8UC1);
	return res;
}

cv::Point2f pointOnRect(cv::Rect bb, cv::Rect stopZone)
{
	float midX = stopZone.x + stopZone.width * 0.5f;
	float midY = stopZone.y + stopZone.height * 0.5f;
	float x = bb.x + bb.width * 0.5f;
	float y = bb.y + bb.height * 0.5f;

	float m = (midY - y) / (midX - x);

	if (x <= midX)
	{	// check "left" side
		float minXy = m * (stopZone.tl().x - x) + y;
		if (stopZone.tl().y <= minXy && minXy <= stopZone.br().y)
		{
			return cv::Point2f(stopZone.tl().x, minXy);
		}
	}

	if (x >= midX)
	{ // check "right" side
		float maxXy = m * (stopZone.br().x - x) + y;
		if (stopZone.tl().y <= maxXy && maxXy <= stopZone.br().y)
		{
			return cv::Point2f(stopZone.br().x, maxXy);
		}
	}

	if (y <= midY)
	{ // check "top" side
		float minYx = (stopZone.tl().y - y) / m + x;
		if (stopZone.tl().x <= minYx && minYx <= stopZone.br().x)
		{
			return cv::Point2f(minYx, stopZone.tl().y);
		}
	}

	if (y >= midY)
	{ // check "bottom" side
		float maxYx = (stopZone.br().y - y) / m + x;
		if (stopZone.tl().x <= maxYx && maxYx <= stopZone.br().x)
		{
			return cv::Point2f(maxYx, stopZone.br().y);
		}
	}

	// edge case when finding midpoint intersection: m = 0/0 = NaN
	if (x == midX && y == midY)
	{
		return cv::Point2f(x, y);
	}
	return cv::Point2f(midX, midY);
}

#define ELEM_SWAP(a,b) { register float t=(a);(a)=(b);(b)=t; }

float tldGetMedianUnmanaged(float arr[], int n)
{
	int low, high;
	int median;
	int middle, ll, hh;

	low = 0;
	high = n - 1;
	median = (low + high) / 2;

	for (;;)
	{
		if (high <= low)
			return arr[median];

		if (high == low + 1)
		{
			if (arr[low] > arr[high])
				ELEM_SWAP(arr[low], arr[high]);

			return arr[median];
		}

		middle = (low + high) / 2;

		if (arr[middle] > arr[high])
			ELEM_SWAP(arr[middle], arr[high]);

		if (arr[low] > arr[high])
			ELEM_SWAP(arr[low], arr[high]);

		if (arr[middle] > arr[low])
			ELEM_SWAP(arr[middle], arr[low]);

		ELEM_SWAP(arr[middle], arr[low + 1]);

		ll = low + 1;
		hh = high;

		for (;;)
		{
			do
				ll++;

			while (arr[low] > arr[ll]);

			do
				hh--;

			while (arr[hh] > arr[low]);

			if (hh < ll)
				break;

			ELEM_SWAP(arr[ll], arr[hh]);
		}

		ELEM_SWAP(arr[low], arr[hh]);

		if (hh <= median)
			low = ll;

		if (hh >= median)
			high = hh - 1;
	}
}

float tldGetMedian(float arr[], int n)
{
	float *temP = (float *)malloc(sizeof(float) * n);
	memcpy(temP, arr, sizeof(float) * n);
	float median;
	median = tldGetMedianUnmanaged(temP, n);
	free(temP);
	return median;
}
#undef ELEM_SWAP