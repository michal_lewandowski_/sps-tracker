#pragma once

#include "LBSP.h"
#include "IBGS.h"

#define BGSSUBSENSE_DEFAULT_LBSP_REL_SIMILARITY_THRESHOLD (0.333f)
#define BGSSUBSENSE_DEFAULT_DESC_DIST_THRESHOLD_OFFSET (3)
#define BGSSUBSENSE_DEFAULT_MIN_COLOR_DIST_THRESHOLD (30)
#define BGSSUBSENSE_DEFAULT_NB_BG_SAMPLES (50)
#define BGSSUBSENSE_DEFAULT_REQUIRED_NB_BG_SAMPLES (2)
#define BGSSUBSENSE_DEFAULT_N_SAMPLES_FOR_MV_AVGS (100)

class BgsSUBSENSE : public IBGS
{
public:
	BgsSUBSENSE(float fRelLBSPThreshold = BGSSUBSENSE_DEFAULT_LBSP_REL_SIMILARITY_THRESHOLD,
		size_t nDescDistThresholdOffset = BGSSUBSENSE_DEFAULT_DESC_DIST_THRESHOLD_OFFSET,
		size_t nMinColorDistThreshold = BGSSUBSENSE_DEFAULT_MIN_COLOR_DIST_THRESHOLD,
		size_t nBGSamples = BGSSUBSENSE_DEFAULT_NB_BG_SAMPLES,
		size_t nRequiredBGSamples = BGSSUBSENSE_DEFAULT_REQUIRED_NB_BG_SAMPLES,
		size_t nSamplesForMovingAvgs = BGSSUBSENSE_DEFAULT_N_SAMPLES_FOR_MV_AVGS);

	virtual ~BgsSUBSENSE();
	virtual void init(const cv::Mat& oInitImg, const cv::Mat& oROI);
	virtual void refreshModel(float fSamplesRefreshFrac, bool bForceFGUpdate = false);
	virtual void apply(const cv::Mat& image, cv::Mat& fgmask, double learningRateOverride = 0);
	virtual void preprocess(const cv::Mat& iframe, const cv::Size& size, cv::Mat& pframe, int code = cv::COLOR_BayerBG2BGR);
	virtual void loadParams();

	void setAutomaticModelReset(bool);
	void getBackgroundImage(cv::OutputArray backgroundImage) const;

protected:
	struct PxInfoBase
	{
		int nImgCoord_Y;
		int nImgCoord_X;
		size_t nModelIdx;
	};
	cv::Size oImgSize;
	size_t nImgChannels;
	int nImgType;
	const size_t nLBSPThresholdOffset;
	const float fRelLBSPThreshold;
	size_t nTotPxCount, nTotRelevantPxCount;
	size_t nFrameIndex, nFramesSinceLastReset, nModelResetCooldown;
	size_t anLBSPThreshold_8bitLUT[UCHAR_MAX + 1];
	size_t* aPxIdxLUT;
	PxInfoBase* aPxInfoLUT;
	const int nDefaultMedianBlurKernelSize;
	bool bInitialized;
	bool bAutoModelResetEnabled;
	bool bUsingMovingCamera;
	cv::Mat oLastColorFrame;
	cv::Mat oLastDescFrame;
	cv::Mat oLastFGMask;

	const size_t nMinColorDistThreshold;
	const size_t nDescDistThresholdOffset;
	const size_t nBGSamples;
	const size_t nRequiredBGSamples;
	const size_t nSamplesForMovingAvgs;
	float fLastNonZeroDescRatio;
	bool bLearningRateScalingEnabled;
	float fCurrLearningRateLowerCap, fCurrLearningRateUpperCap;
	int nMedianBlurKernelSize;
	bool bUse3x3Spread;
	cv::Size oDownSampledFrameSize;

	std::vector<cv::Mat> voBGColorSamples;
	std::vector<cv::Mat> voBGDescSamples;

	cv::Mat oUpdateRateFrame;
	cv::Mat oDistThresholdFrame;
	cv::Mat oVariationModulatorFrame;
	cv::Mat oMeanLastDistFrame;
	cv::Mat oMeanMinDistFrame_LT, oMeanMinDistFrame_ST;
	cv::Mat oMeanDownSampledLastDistFrame_LT, oMeanDownSampledLastDistFrame_ST;
	cv::Mat oMeanRawSegmResFrame_LT, oMeanRawSegmResFrame_ST;
	cv::Mat oMeanFinalSegmResFrame_LT, oMeanFinalSegmResFrame_ST;
	cv::Mat oUnstableRegionMask;
	cv::Mat oBlinksFrame;
	cv::Mat oDownSampledFrame_MotionAnalysis;
	cv::Mat oLastRawFGMask;

	cv::Mat oFGMask_PreFlood;
	cv::Mat oFGMask_FloodedHoles;
	cv::Mat oLastFGMask_dilated;
	cv::Mat oLastFGMask_dilated_inverted;
	cv::Mat oCurrRawFGBlinkMask;
	cv::Mat oLastRawFGBlinkMask;
};

