#pragma once
#include "BallObject.h"
#include <memory>

class PredictedBall : public BallObject
{
public:
	PredictedBall(const std::size_t id, std::string cid, cv::Rect b);
	PredictedBall(const std::size_t id, std::string cid, cv::Point3d p, int views = 0, float error= 0.0f);
	virtual ~PredictedBall();
	PredictedBall(const PredictedBall& other);
	PredictedBall& operator=(const PredictedBall& other);

	virtual std::shared_ptr<MovingObject> clone() const override;
	virtual bool isDetected(void) const override { return false; }
	virtual bool isBacktracked() const override { return false; }
	virtual void show(cv::Mat frame, cv::Scalar color) const override;
	virtual void show3d(cv::Mat frame, cv::Scalar color, double windowScale) const override;

	void setImage(cv::Mat i, cv::Mat m);
};
