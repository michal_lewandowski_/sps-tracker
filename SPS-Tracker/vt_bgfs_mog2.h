#pragma once
#include <core/cuda.hpp>


class VT_MOG2_GPU
{
public:
	//! the default constructor
	VT_MOG2_GPU(int nmixtures = -1);

	//! re-initiaization method
	void initialize(cv::Size frameSize, int frameType);

	//! the update operator
	void operator()(const cv::cuda::GpuMat& frame, const cv::cuda::GpuMat& validmask, cv::cuda::GpuMat& fgmask, float learningRate = -1.0f, cv::cuda::Stream& stream = cv::cuda::Stream::Null());

	//! computes a background image which are the mean of all background gaussians
	void getBackgroundImage(cv::cuda::GpuMat& backgroundImage, cv::cuda::Stream& stream = cv::cuda::Stream::Null()) const;

	//! releases all inner buffers
	void release();

	void setNumberOfGaussiansPerPixel(int nMixtures);
	void setVarianceThreshold(float varThreshold);
	void setBackgroundRatio(float backgroundRatio);
	void setVarianceThresholdGen(float varThresholdGen);
	void setInitialVariance(float initialVariance);
	void setMinVariance(float minVariance);
	void setMaxVariance(float maxVariance);
	void setShadowValue(unsigned char nShadowDetection);
	void setShadowThreshold(float tau);
	void setHfactor(float HSVFactorH);
	void setSfactor(float HSVFactorS);
	void setVfactor(float HSVFactorV);
	void setComplexityReductionPrior(float ct);
	void enableShadowDetection(bool shadowDetection);

//private:
	// parameters
	// you should call initialize after parameters changes

	//! here it is the maximum allowed number of mixture components.
	//! Actual number is determined dynamically per pixel
	int history;

	/*! threshold on the squared Mahalanobis distance to decide if it is well described
		by the background model or not. Related to Cthr from the paper.
		This does not influence the update of the background. A typical value could be 4 sigma
		and that is varThreshold=4*4=16; Corresponds to Tb in the paper.
		*/
	float varThreshold;

	/////////////////////////
	// less important parameters - things you might change but be carefull
	////////////////////////

	/*! corresponds to fTB=1-cf from the paper
		TB - threshold when the component becomes significant enough to be included into
		the background model. It is the TB=1-cf from the paper. So I use cf=0.1 => TB=0.
		For alpha=0.001 it means that the mode should exist for approximately 105 frames before
		it is considered foreground
		float noiseSigma;
		*/
	float backgroundRatio;
	/*! corresponds to Tg - threshold on the squared Mahalan. dist. to decide
	when a sample is close to the existing components. If it is not close
	to any a new component will be generated. I use 3 sigma => Tg=3*3=9.
	Smaller Tg leads to more generated components and higher Tg might make
	lead to small number of components but they can grow too large
	*/
	float varThresholdGen;

	/*! initial variance  for the newly generated components.
	It will will influence the speed of adaptation. A good guess should be made.
	A simple way is to estimate the typical standard deviation from the images.
	I used here 10 as a reasonable value
		min and max can be used to further control the variance
		*/
	float fVarInit;
	/*! lower limit for the variance
		*/
	float fVarMin;
	/*! upper limit for the variance
		*/
	float fVarMax;

	/*! CT - complexity reduction prior
	this is related to the number of samples needed to accept that a component
	actually exists. We use CT=0.05 of all the samples. By setting CT=0 you get
	the standard Stauffer&Grimson algorithm (maybe not exact but very similar)
	*/
	float fCT;

	//! shadow detection parameters; default 1 - do shadow detection
	bool bShadowDetection;
	///if doing shadow detection - insert this value as the detection result - 127 default value
	unsigned char nShadowDetection; 
	/*! Tau - shadow threshold. The shadow is detected if the pixel is darker
	version of the background. Tau is a threshold on how much darker the shadow can be.
	Tau= 0.5 means that if pixel is more than 2 times darker then it is not shadow
	See: Prati,Mikic,Trivedi,Cucchiarra,"Detecting Moving Shadows...",IEEE PAMI,2003.
	*/
	float fTau;
	float fHSVFactorH;
	float fHSVFactorS;
	float fHSVFactorV;

	int nmixtures_;

	cv::Size frameSize_;
	int frameType_;
	int nframes_;

	cv::cuda::GpuMat weight_;
	cv::cuda::GpuMat variance_;
	cv::cuda::GpuMat mean_;

	///keep track of number of modes per pixel
	cv::cuda::GpuMat bgmodelUsedModes_; 
};
