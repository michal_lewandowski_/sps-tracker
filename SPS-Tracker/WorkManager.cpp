#include "stdafx.h"
#include <omp.h>
#include "WorkManager.h"
#include "KFBallTrack2d.h"
#include "Field.h"

double WorkManager::frameRate = 0.0;
std::size_t WorkManager::numFrame = 1;

WorkManager::WorkManager()
	: tracker3d(this), stop(false), step(false), windowScale(20.0), showOutput(true)
{
	colors.push_back(cv::Scalar(255, 0, 0));
	colors.push_back(cv::Scalar(200, 50, 255));
	colors.push_back(cv::Scalar(50, 150, 0));
	colors.push_back(cv::Scalar(255, 255, 50));
}
WorkManager::~WorkManager()
{
	
}

bool WorkManager::init(bool show)
{
	showOutput = show;
	bool init = true;
	for(int i=0; i < jobs.size(); ++i)
	{
		if(!jobs[i]->init())
		{
			init = false;
		}
		else
		{
			if(i==0)
			{
				frameRate = jobs[i]->getFrameRate();
				KFBallTrack2d::maxRecorderBallSpeedMMperFrame = KFBallTrack2d::maxRecorderBallSpeedMMperSec * frameRate / 1000.0;
			}
			else
			{
				CV_Assert(static_cast<int>(frameRate) == static_cast<int>(jobs[i]->getFrameRate()));
			}
			jobs[i]->setROIT3d(2500.0);
		}
	}
	return init;
}
void WorkManager::setFields(const std::vector<std::pair<cv::Rect, std::string>>& f, int margin, int numCameras)
{
	for(int i=0; i < f.size(); ++i)
	{
		fields.emplace_back(std::move(Field(i, f[i].first, margin, numCameras, f[i].second)));
		fields.back().stats.field = &fields.back();
	}
}

void WorkManager::process(std::vector<std::deque<BallSaveData>>& ballData, std::vector<std::vector<std::vector<int>>>& observationsPerCamera)
{
	//std::cout << "Start processing with " << size() << " cameras..." << std::endl;
	CV_Assert(jobs.size() > 0);
	ballData.resize(5);

	/*#pragma omp parallel
	{
		int tid = omp_get_thread_num();
		printf("Hello world from thread = %d \n", tid);
		if (tid == 0)
		{
			int nthreads = omp_get_num_threads();
			printf("Number of threads = %d\n", nthreads);
		}
	}*/

	while(true)
	{
		std::cout << "Frame " << systemTime.getFrameNum() << ": " << std::endl;
		std::vector<bool> isend(jobs.size(), false);

		tr3ddead().clear();

#ifdef RELEASE
		//#pragma omp parallel for
#endif
		for (int i=0; i<jobs.size();++i)
		{
			isend[i] = !jobs[i]->process(tr3dlive());
		}
		
		for (auto& f : fields)
		{
			f.maxAbsVelocityGlobal = std::numeric_limits<double>::epsilon();
			f.maxBallProbabilityGlobal = std::numeric_limits<double>::max();
		}

		detections = std::move(TW.triangulate(jobs, getFields()));
		tracker3d.track(detections, tr3dlive(), tr3ddead(), ballData);
		TW.triangulate(jobs, tr3dlive());

		observationsPerCamera.push_back(std::vector<std::vector<int>>());
		for (auto& f : getFields())
		{
			observationsPerCamera.back().push_back(f.numObservations);
		}

		for (int i = 0; i < jobs.size(); ++i)
		{
			if (isend[i])
			{
				for(auto& t : tr3dlive())
				{
					tracker3d.saveData(t, ballData[t->fieldID()]);
				}
				systemTime.advance();
				return;
			}
		}
		for (auto& f : getFields())
		{
			if(f.ballTrack3d)
			{
				for (int i = 0; i<jobs.size(); ++i)
				{
					jobs[i]->feedback(f.ballTrack3d);
					//MICHOLO TLD
					//jobs[i]->isGoodForTLD(f.ballTrack3d);
				}
			}
		}

		for(auto t : tr3dlive())
		{
			Probability prob;
			CV_Assert(t->getCurrentDet()->getProbability("pl", prob));
		}
		if(showOutput)
		{
			show();
			handleKey();
		}
		

		std::cout << std::endl;

		systemTime.advance();

		//if (time() == 500)
		//	break;
	}
	std::cout << "Finish processing with " << size() << " cameras in time " << systemTime.getFrameNum() << std::endl;
}

void WorkManager::show()
{
	CV_Assert(jobs.size() < 5);

	cv::Mat world(cvRound(15000.0 / windowScale), cvRound(41000.0 / windowScale), CV_8UC3);
	//cv::Mat world(cvRound(15000.0 / windowScale), cvRound(9000.0 / windowScale), CV_8UC3);
	world.setTo(0);
	for (int j = 0; j < fields.size(); ++j)
	{
		fields[j].show(systemTime.getFrameNum(), world, windowScale, std::vector<int>());
	}

	std::vector<cv::Mat> allFramesDisp(2);
	std::vector<cv::Mat> allFramesMotion(2);
	cv::Mat tempD, tempM;
	std::vector<std::vector<cv::Mat>> imgFields1(jobs.size());
	std::vector<std::vector<cv::Mat>> imgFields2(jobs.size());
	for(int j=0; j < jobs.size(); ++j)
	{
		imgFields1[j].resize(getFields().size());
		imgFields2[j].resize(getFields().size());

		std::pair<cv::Mat, cv::Mat> frameShow = jobs[j]->show(true, step, tr3dlive(), -1);
		cv::resize(frameShow.first, tempD, cv::Size(), 0.5, 0.5, cv::INTER_AREA);
		cv::resize(frameShow.second, tempM, cv::Size(), 0.5, 0.5, cv::INTER_AREA);
		if(j < 2)
		{
			if(j<1)
			{
				tempD.copyTo(allFramesDisp[0]);
				tempM.copyTo(allFramesMotion[0]);
			}
			else
			{
				cv::hconcat(allFramesDisp[0], tempD, allFramesDisp[0]);
				cv::hconcat(allFramesMotion[0], tempM, allFramesMotion[0]);
			}
		}
		else
		{
			if(jobs.size() == 3)
			{
				tempD.copyTo(allFramesDisp[1]);
				tempM.copyTo(allFramesMotion[1]);
				cv::Mat dummyDisp(tempD.size(), tempD.type());
				cv::Mat dummyMotion(tempM.size(), tempM.type());
				cv::hconcat(allFramesDisp[1], dummyDisp, allFramesDisp[1]);
				cv::hconcat(allFramesMotion[1], dummyMotion, allFramesMotion[1]);
			}
			else
			{
				if (j<3)
				{
					tempD.copyTo(allFramesDisp[1]);
					tempM.copyTo(allFramesMotion[1]);
				}
				else
				{
					cv::hconcat(allFramesDisp[1], tempD, allFramesDisp[1]);
					cv::hconcat(allFramesMotion[1], tempM, allFramesMotion[1]);
				}
			}
		}
		jobs[j]->show3d(world, windowScale);
		for (int f = 0; f < jobs[j]->getFields().size(); ++f)
		{
			if (jobs[j]->getFields()[f].detectionWindow.area() > 0)
			{
				std::pair<cv::Mat, cv::Mat> frameShowField = jobs[j]->show(false, false, tr3dlive(), jobs[j]->getFields()[f].fid);

				cv::Rect dw(jobs[j]->getFields()[f].detectionWindow.x - 20, jobs[j]->getFields()[f].detectionWindow.y - 20, jobs[j]->getFields()[f].detectionWindow.width + 40, jobs[j]->getFields()[f].detectionWindow.height + 40);
				dw &= cv::Rect(cv::Point(0, 0), frameShowField.first.size());

				imgFields1[j][f].create(cv::Size(jobs[j]->getDetectionWindowSize() + 20, jobs[j]->getDetectionWindowSize() + 20) * 2, frameShowField.first.type()); imgFields1[j][f].setTo(0);
				imgFields2[j][f].create(cv::Size(jobs[j]->getDetectionWindowSize() + 20, jobs[j]->getDetectionWindowSize() + 20) * 2, frameShowField.first.type()); imgFields2[j][f].setTo(0);

				frameShowField.first(dw).copyTo(imgFields1[j][f](cvRect(0, 0, dw.width, dw.height)));
				frameShowField.second(dw).copyTo(imgFields2[j][f](cvRect(0, 0, dw.width, dw.height)));
			}
		}
	}
	cv::Mat allFramesDispFinal;
	cv::Mat allFramesMotionFinal;
	if(jobs.size() > 2)
	{
		cv::vconcat(allFramesDisp[0], allFramesDisp[1], allFramesDispFinal);
		cv::vconcat(allFramesMotion[0], allFramesMotion[1], allFramesMotionFinal);
	}
	else
	{
		allFramesDisp[0].copyTo(allFramesDispFinal);
		allFramesMotion[0].copyTo(allFramesMotionFinal);
	}
	/*for (int i = 0; i < detections.size(); ++i)
	{
		cv::circle(world, cv::Point(cvRound(detections[i]->pos3d().x / windowScale), cvRound(world.rows - detections[i]->pos3d().y / windowScale)), 8, cv::Scalar(100, 205, 205), 2);
	}*/
	for (auto& t : tr3dlive())
	{
		t->show(world, windowScale, cv::Scalar(0, 255, 0));
#ifdef SPS_PRINT_INFO_3D
		t->printInfo(std::vector<cv::Point>(), getFields());
#endif
	}
	
	/*{
		std::shared_ptr<KFTrack3d> bestTrack = nullptr;
		for (auto& t : tr3dlive())
		{
#ifdef SPS_PRINT_INFO_3D
			t->printInfo(std::vector<cv::Point>(), getFields());
#endif
			if(t->isVisible())
			{
				if (bestTrack==nullptr || t->getAverageProbability() < bestTrack->getAverageProbability())
				{
					bestTrack = t;
				}
			}
		}
		if(bestTrack && bestTrack->isVisible())
		{
			bestTrack->show(world, windowScale, cv::Scalar(0, 255, 0));
		}
	}*/

#ifdef SPS_PRINT_INFO_3D
	for (auto& t : tr3ddead())
	{
		t->printInfo(std::vector<cv::Point>(), getFields());
	}
#endif

	/*for (int j = 0; j < fields.size(); ++j)
	{
		cv::line(world, cv::Point(cvRound(fields[j].aaa.x / windowScale), world.rows - cvRound(fields[j].aaa.y / windowScale)), cv::Point(cvRound((fields[j].aaa.x + fields[j].velocity[0]) / windowScale), world.rows - cvRound((fields[j].aaa.y + fields[j].velocity[1]) / windowScale)), cv::Scalar(0, 0, 255), 2);
		cv::line(world, cv::Point(cvRound(fields[j].aaa.x / windowScale), world.rows - cvRound(fields[j].aaa.y / windowScale)), cv::Point(cvRound((fields[j].aaa.x + fields[j].direction[0]) / windowScale), world.rows - cvRound((fields[j].aaa.y + fields[j].direction[1]) / windowScale)), cv::Scalar(0, 255, 0), 2);
		cv::line(world, cv::Point(cvRound(fields[j].aaa.x / windowScale), world.rows - cvRound(fields[j].aaa.y / windowScale)), cv::Point(cvRound((fields[j].aaa.x + fields[j].velocityTowards[0]) / windowScale), world.rows - cvRound((fields[j].aaa.y + fields[j].velocityTowards[1]) / windowScale)), cv::Scalar(255, 0, 0), 2);

		cv::circle(world, cv::Point(cvRound((fields[j].aaa.x + fields[j].velocity[0]) / windowScale), world.rows - cvRound((fields[j].aaa.y + fields[j].velocity[1]) / windowScale)), 3, cv::Scalar(0, 0, 205), -1);
		cv::circle(world, cv::Point(cvRound((fields[j].aaa.x + fields[j].direction[0]) / windowScale), world.rows - cvRound((fields[j].aaa.y + fields[j].direction[1]) / windowScale)), 3, cv::Scalar(0, 205, 0), -1);
		cv::circle(world, cv::Point(cvRound((fields[j].aaa.x + fields[j].velocityTowards[0]) / windowScale), world.rows - cvRound((fields[j].aaa.y + fields[j].velocityTowards[1]) / windowScale)), 3, cv::Scalar(205, 0, 0), -1);

		cv::circle(world, cv::Point(cvRound((fields[j].aaa.x) / windowScale), world.rows - cvRound((fields[j].aaa.y) / windowScale)), 4, cv::Scalar(0, 255, 255), -1);
		cv::circle(world, cv::Point(cvRound((fields[j].bbb.x) / windowScale), world.rows - cvRound((fields[j].bbb.y) / windowScale)), 3, cv::Scalar(0, 128, 255), -1);
	}*/

	//cv::imshow("frames", allFramesDispFinal);
	//cv::imshow("motion", allFramesMotionFinal);
	if(systemTime.getFrameNum() == 1 && getFields().size() > 1)
	{
		cv::namedWindow("world", cv::WINDOW_NORMAL | cv::WINDOW_KEEPRATIO);
		cv::resizeWindow("world", 2000, 750);
	}
	if (getFields().size() == 1)
	{
		cv::Mat fieldWorld;
		cv::Rect r(cv::Point(cvRound((fields[0].fieldExtended.tl().x - 100) / windowScale), world.rows - cvRound((fields[0].fieldExtended.tl().y - 100) / windowScale)), cv::Point(cvRound((fields[0].fieldExtended.br().x + 100) / windowScale), world.rows - cvRound((fields[0].fieldExtended.br().y + 100) / windowScale)));
		r &= cv::Rect(cv::Point(0, 0), world.size());
		world(r).copyTo(fieldWorld);
		cv::imshow("world1", fieldWorld);
	}
	for (int f = 0; f < getFields().size(); ++f)
	{
		cv::Mat img;
		for (int i = 0; i < jobs.size(); ++i)
		{
			cv::Mat imfi;
			if(!imgFields1[i][f].empty() && !imgFields2[i][f].empty())
			{
				cv::vconcat(imgFields1[i][f], imgFields2[i][f], imfi);
				if (img.empty())
				{
					img = imfi;
				}
				else
				{
					cv::hconcat(img, imfi, img);
				}
			}
		}
		if (!img.empty())
		{
			cv::Mat fieldWorld;
			cv::Rect r(cv::Point(cvRound((fields[f].fieldExtended.tl().x - 100) / windowScale), world.rows - cvRound((fields[f].fieldExtended.tl().y - 100) / windowScale)), cv::Point(cvRound((fields[f].fieldExtended.br().x + 100) / windowScale), world.rows - cvRound((fields[f].fieldExtended.br().y + 100) / windowScale)));
			r &= cv::Rect(cv::Point(0, 0), world.size());
			world(r).copyTo(fieldWorld);
			cv::Mat fieldWorldResize;
			double scaleH = img.rows / static_cast<double>(fieldWorld.rows);
			cv::resize(fieldWorld, fieldWorldResize, cv::Size(), scaleH, scaleH);
			cv::hconcat(fieldWorldResize, img, img);
			cv::imshow("Field " + std::to_string(getFields()[f].fid+1), img);
		}
		else
		{
			cv::destroyWindow("Field " + std::to_string(getFields()[f].fid + 1));
		}
	}
	if (getFields().size() > 1)
	{
		cv::imshow("world", world);
	}
}
void WorkManager::addJob(std::string name, std::string videoFile, std::string configFile)
{
	jobs.push_back(std::make_shared<Worker>(systemTime, name, videoFile, configFile, fields, colors[jobs.size()]));
}
void WorkManager::handleKey()
{
	int k = 0;
	
	if(step)
	{
		k = cv::waitKey(0);
		step = false;
	}
	else
	{
		k = cv::waitKey(40);
		if (stop)
		{
			step = true;
		}
	}
	if (k == ' ')
	{
		stop = true;
		step = true;
	}
	if (k == 'g')
	{
		stop = false;
		step = false;
	}
}
Field* WorkManager::inField(const std::shared_ptr<MovingObject> obj)
{
	return fields.inField(obj->pos3d());
}