#include "stdafx.h"
#include "DetectedBall.h"
#include <boost/lexical_cast.hpp>

DetectedBall::DetectedBall(const std::size_t id, std::string cid, cv::Rect b, cv::RotatedRect e, double br, cv::Mat im, cv::Mat mm)
	: BallObject(id, cid, b, e, br, im, mm)
{
	numDetected_ = 1;
	type = BallType::DETECTED;
}
DetectedBall::DetectedBall(const std::size_t id, std::string cid, cv::Point3d p, int views, int numDet, float error, const std::unordered_map<std::string, int>& tracks, const std::vector<PreviousBallInCam>& prevLoc)
	: BallObject(id, cid, p, views, numDet, error, tracks, prevLoc)
{
	type = BallType::DETECTED;
}
std::shared_ptr<MovingObject> DetectedBall::clone() const
{
	return std::make_shared<DetectedBall>(*this);
}
DetectedBall::~DetectedBall() 
{
}

DetectedBall::DetectedBall(const DetectedBall& other)
	: BallObject(other)
{
}

DetectedBall& DetectedBall::operator=(const DetectedBall& other) 
{
	if(this != &other)
	{
		this->BallObject::operator=(other);
	}
	return *this;
}

void DetectedBall::show(cv::Mat frame, cv::Scalar color) const
{
	cv::ellipse(frame, ebb, color, -1);
	if(isValid())
	{
		cv::ellipse(frame, ebb, cv::Scalar(0, 255, 255), 3);
	}
	else
	{
		cv::ellipse(frame, ebb, cv::Scalar(0, 128, 255), 3);
	}
#ifdef SPS_DEBUB_DETS
	if(forceLearn)
	{
		cv::circle(frame, pos2d(), 20, cv::Scalar(255, 255, 255), 1);
		cv::circle(frame, pos2d(), 25, cv::Scalar(255, 255, 255), 1);
	}
#endif

	cv::putText(frame, boost::lexical_cast<std::string>(detID), cv::Point(bb_.br().x, bb_.tl().y) + cv::Point(7, -7), CV_FONT_HERSHEY_PLAIN, 1, color);
}
void DetectedBall::show3d(cv::Mat frame, cv::Scalar color, double windowScale) const
{
	cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 5, color, -1);
	if (isValid())
	{
		cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 8, cv::Scalar(0, 255, 255), 2);
	}
	else
	{
		cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 8, cv::Scalar(0, 128, 255), 2);
	}
	//cv::putText(frame, boost::lexical_cast<std::string>(detID), cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - (cvRound(pos3dEst().y / windowScale)) + cv::Point(7, -7)), CV_FONT_HERSHEY_PLAIN, 1, color);
}