#include "stdafx.h"
#include "Visualizer.h"
#include <regex>
#include "Field.h"
#include "Utils.h"

Visualizer::Visualizer()
	: stop(false), step(false), windowScale(20.0)
{
}


Visualizer::~Visualizer()
{
}

void Visualizer::addJob(std::string name, std::string videoFile, std::string configFile, std::string dir)
{
	names.push_back(name);
	videoFiles.push_back(videoFile);
	configFiles.push_back(configFile);
	inputDir = dir;
}
bool Visualizer::init(bool all, std::string inputName, bool count)
{
	countStats = count;
	if (countStats)
	{
		countData.resize(8, 0);
	}
	if (count && fields.size() > 1)
	{
		std::cout << "Only one field acceptable in stats mode" << std::endl;
		return false;
	}

	cv::Mat frameTemp;
	for(int i=0; i < names.size(); ++i)
	{
		caps.push_back(cv::VideoCapture(videoFiles[i]));
		calibs.push_back(CameraCalibration(configFiles[i]));

		if (!caps.back().read(frameTemp))
		{
			std::cout << "Cannot read frame in file: " << videoFiles[i] << std::endl;
			return false;
		}
	}
	ballEst.resize(fields.size());
	for (int i = 0; i < fields.size(); ++i)
	{
		std::stringstream ss;
		ss << inputDir << "\\" << inputName << ".field";
		ss << fields[i].fid;
		ss << ".csv";
		std::cout << ss.str() << std::endl;
		std::ifstream infile;
		infile.open(ss.str() , std::ios_base::in);
		if(infile.is_open())
		{
			ballEst[i].reserve(10000);
			std::string line;
			std::getline(infile, line);
			std::getline(infile, line);
			int frameNum = 1;
			std::vector<std::tuple<cv::Point3d, int, int>> frameData;
			frameData.reserve(5);
			while (std::getline(infile, line))
			{
				frameData.clear();
				std::vector<float> pos;
				Utils::parse_csint(line, pos);
				frameData.push_back(std::make_tuple(cv::Point3d(pos[1], pos[2], pos[3]), static_cast<int>(pos[6]), static_cast<int>(pos[7])));
				if (all && pos[8] > -1)
				{
					frameData.push_back(std::make_tuple(cv::Point3d(pos[8], pos[9], pos[10]), static_cast<int>(pos[13]), static_cast<int>(pos[14])));
					if (pos[15] > -1)
					{
						frameData.push_back(std::make_tuple(cv::Point3d(pos[15], pos[16], pos[17]), static_cast<int>(pos[20]), static_cast<int>(pos[21])));
						if (pos[22] > -1)
						{
							frameData.push_back(std::make_tuple(cv::Point3d(pos[22], pos[23], pos[24]), static_cast<int>(pos[27]), static_cast<int>(pos[28])));
							if (pos[29] > -1)
							{
								frameData.push_back(std::make_tuple(cv::Point3d(pos[29], pos[30], pos[31]), static_cast<int>(pos[34]), static_cast<int>(pos[35])));
							}
						}
					}
				}
				ballEst[i].push_back(frameData);
				fields[i].stats.computeError(frameNum++, std::get<0>(frameData[0]));
			}
			infile.close();
		}
		else
		{
			std::cout << "No Results for " << ss.str() << std::endl;
			return false;
		}
	}
	for (int f = 0; f < fields.size(); ++f)
	{
		fields[f].stats.computeStats(inputDir, inputName);
	}
	return true;
}
void Visualizer::process()
{
	/*for (int j = 0; j < caps.size(); ++j)
	{
		int i = 79;
		cv::Mat frameTemp;
		while (i > 0)
		{
			caps[j].read(frameTemp);
			i--;
		}
	}*/
	int cc = 1;
	std::vector<std::vector<std::vector<std::tuple<cv::Point3d, int, int>>>> v(caps.size());
	while(true)
	{
		int done = 0;;
		for (int i = 0; i < caps.size(); ++i)
		{
			v[i].resize(fields.size());

			cv::Mat frameTemp, frameGray, frameBGR, frame;
			if (caps[i].read(frameTemp))
			{
				frameGray.create(frameTemp.rows, frameTemp.cols, CV_8UC1);

				static int from_to[] = { 0,0 };
				cv::mixChannels(&frameTemp, 1, &frameGray, 1, from_to, 1);

				cv::demosaicing(frameGray, frameBGR, cv::COLOR_BayerBG2BGR);

				if (frameBGR.size() != calibs[i].getImageSize())
				{
					cv::resize(frameBGR, frame, calibs[i].getImageSize(), 0.0, 0.0, cv::INTER_LINEAR);
				}
				else
				{
					frame = frameBGR;
				}
				cv::Mat world(cvRound(15000.0 / windowScale), cvRound(41000.0 / windowScale), CV_8UC3);
				world.setTo(0);
				for (int j = 0; j < fields.size(); ++j)
				{
					int modify = 0;
					if (j > 0)
						modify = 16000;
					fields[j].show(systemTime.getFrameNum(), world, windowScale, countData, modify);
					//fields[j].show(systemTime.getFrameNum(), frame, cv::Mat(), "", calibs[i], cv::Scalar(255,255,255), true);
				}

				for (int f = 0; f < fields.size(); ++f)
				{
					if(ballEst[f][time() - 1].size() == 0)
					{
						v[i][f].clear();
					}
					else
					{
						for (auto& ball : ballEst[f][time() - 1])
						{
							if (std::get<0>(ball) != cv::Point3d(-1, -1, -1))
							{
								v[i][f].push_back(ball);

								fields[f].stats.computeError(systemTime.getFrameNum(), std::get<0>(ball));
							}
							else
							{
								v[i][f].clear();
							}
						}
					}
				}
				for (int f = 0; f < fields.size(); ++f)
				{
					if(v[i][f].size() > 1)
					{
						int modify = 0;
						if (f > 0)
							modify = 16000;

						int kk = 0;
						std::vector<std::tuple<cv::Point3d, int, int>>::reverse_iterator itPrev = v[i][f].rbegin();
						std::vector<std::tuple<cv::Point3d, int, int>>::reverse_iterator itCurr = itPrev + 1;
						for (; itCurr != v[i][f].rend(); itPrev = itCurr, ++itCurr)
						{
							cv::Point2d pt1;
							calibs[i].transformToPixel(std::get<0>(*itPrev), pt1);
							cv::Point2d pt2;
							calibs[i].transformToPixel(std::get<0>(*itCurr), pt2);

							cv::Scalar color = cv::Scalar(0, 255, 0);// (std::get<1>(*itPrev) == 1 ? cv::Scalar(0, 255, 0) : (std::get<1>(*itPrev) == 2 ? cv::Scalar(0, 128, 255) : ((std::get<1>(*itPrev) == 3 ? cv::Scalar(255, 0, 0) : ((std::get<1>(*itPrev) == 4 ? cv::Scalar(0, 0, 255) : (cv::Scalar(0, 0, 0))))))));
							cv::line(frame, pt1, pt2, color, 2, CV_AA);
							cv::line(world, cv::Point(cvRound((std::get<0>(*itPrev).x- modify) / windowScale), world.rows - cvRound(std::get<0>(*itPrev).y / windowScale)), cv::Point(cvRound((std::get<0>(*itCurr).x - modify) / windowScale), world.rows - cvRound(std::get<0>(*itCurr).y / windowScale)), color, 2, CV_AA);
							if (kk > 20)
								break;
							kk++;
						}
						itPrev = v[i][f].rbegin();
						cv::Point2d pt1;
						calibs[i].transformToPixel(std::get<0>(*itPrev), pt1);
						cv::circle(frame, pt1, 5, cv::Scalar(0, 255, 255), -1);
						//cv::putText(frame, "Frame " + std::to_string(time()), cv::Point(pt1) + cv::Point(7, 7), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 255, 255));
						cv::circle(world, cv::Point(cvRound((std::get<0>(*itPrev).x - modify) / windowScale), world.rows - cvRound(std::get<0>(*itPrev).y / windowScale)), 6, cv::Scalar(0, 255, 255), -1);
					}
				}
				cv::imshow(names[i], frame);

				{
					char filename[180];
					sprintf(filename, "D:\\aaa\\sworld_final_%d\\seq_%06d.jpg", i, cc);
					cv::imwrite(filename, frame);
				}

				if (fields.size() == 1)// && countStats
				{
					cv::Mat fieldWorld;
					cv::Rect r(cv::Point(cvRound((fields[0].fieldExtended.tl().x - 1000) / windowScale), world.rows - cvRound((fields[0].fieldExtended.tl().y - 1000) / windowScale)), cv::Point(cvRound((fields[0].fieldExtended.br().x + 1000) / windowScale), world.rows - cvRound((fields[0].fieldExtended.br().y + 1000) / windowScale)));
					r &= cv::Rect(cv::Point(0, 0), world.size());
					world(r).copyTo(fieldWorld);
					cv::imshow("world", fieldWorld);
				}
				else
				{
					

					cv::Mat fieldWorld;
					cv::Rect r(cv::Point(cvRound((fields[0].fieldExtended.tl().x - 1000) / windowScale), world.rows - cvRound((fields[0].fieldExtended.tl().y - 1000) / windowScale)), cv::Point(cvRound((fields[0].fieldExtended.br().x + 17000) / windowScale), world.rows - cvRound((fields[0].fieldExtended.br().y + 1000) / windowScale)));
					r &= cv::Rect(cv::Point(0, 0), world.size());
					world(r).copyTo(fieldWorld);
					cv::imshow("world", fieldWorld);

					if (i == 0)
					{
						char filename[180];
						sprintf(filename, "D:\\aaa\\sworld\\seq_%06d.jpg", cc);
						cv::imwrite(filename, fieldWorld);
					}
				}

				

				done++;
			}
		}
		cc++;
		if (done < caps.size())
		{
			break;
		}

		handleKey();

		systemTime.advance();
		/*if(systemTime.getFrameNum() == 500)
		{
			break;
		}*/
	}
}
void Visualizer::setFields(const std::vector<std::pair<cv::Rect, std::string>>& f, int margin, int numCameras)
{
	for (int i = 0; i < f.size(); ++i)
	{
		fields.emplace_back(std::move(Field(i, f[i].first, margin, numCameras, f[i].second)));
	}
	for (int i = 0; i < f.size(); ++i)
	{
		fields[i].stats.field = &fields[i];
	}
}
void Visualizer::handleKey()
{
	static int* lastChange = nullptr;
	int k = 0;

	if (step)
	{
		k = cv::waitKey(0);
		step = false;
	}
	else
	{
		k = cv::waitKey(40);
		if (stop)
		{
			step = true;
		}
	}
	if (k == ' ')
	{
		stop = true;
		step = true;
	}
	if (k == 'g')
	{
		stop = false;
		step = false;
	}
	if (countStats)
	{
		if (k == '1')
		{
			countData[0]++;
			lastChange = &countData[0];
		}
		if (k == '2')
		{
			countData[4]++;
			lastChange = &countData[4];
		}
		if (k == '3')
		{
			countData[1]++;
			lastChange = &countData[1];
		}
		if (k == '4')
		{
			countData[5]++;
			lastChange = &countData[5];
		}
		if (k == '5')
		{
			countData[2]++;
			lastChange = &countData[2];
		}
		if (k == '6')
		{
			countData[6]++;
			lastChange = &countData[6];
		}
		if (k == '7')
		{
			countData[3]++;
			lastChange = &countData[3];
		}
		if (k == '8')
		{
			countData[7]++;
			lastChange = &countData[7];
		}
		if (k == 'q' && lastChange)
		{
			(*lastChange)--;
		}
	}
}