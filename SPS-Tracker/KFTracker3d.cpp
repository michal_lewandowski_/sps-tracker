#include "stdafx.h"
#include "KFTracker3d.h"
#include <memory>
#include "Track.h"
#include "BallObject.h"
#include "IDFactory.h"
#include "HungarianN3.h"
#include "Worker.h"
#include "KFBallTrack3d.h"
#include "WorkManager.h"

KFTracker3d::KFTracker3d(WorkManager* w)
	: stateParams(6)
	, mesaureParams(3)
	, maxNumTracks(100)
	, workerManager(w)
	, minTrackLength(10)
{
	
}

KFTracker3d::~KFTracker3d()
{
	cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
	file["outputToFileTracksLongerThan"] >> minTrackLength;
	file.release();
}

void KFTracker3d::track(const std::vector<std::shared_ptr<MovingObject>>& detections, std::deque<std::shared_ptr<KFTrack3d>>& liveTracks, std::deque<std::shared_ptr<KFTrack3d>>& deadTracks, std::vector<std::deque<BallSaveData>>& ballData)
{
	if (liveTracks.size() == 0)
	{
		for (unsigned int x = 0; x < detections.size(); ++x)
		{
			if (detections[x]->isValid())
			{
				std::shared_ptr<KFTrack3d> tr = createTrack(detections[x], workerManager->time(), liveTracks.size());
				if (tr)
				{
					liveTracks.push_back(tr);
				}
			}
		}
		return;
	}

	cv::Mat costMatrix(static_cast<int>(liveTracks.size()), static_cast<int>(detections.size()), CV_64FC1);
	std::vector<size_t> assignments;
	for (unsigned int y = 0; y < liveTracks.size(); ++y)
	{
		liveTracks[y]->predict(workerManager->time());
		for (unsigned int x = 0; x < detections.size(); ++x)
		{
			if(detections[x]->isValid() && detections[x]->fieldID() == liveTracks[y]->fieldID())
			{
				//uses estimation from prediction to compute distance
				costMatrix.at<double>(y, x) = liveTracks[y]->getCost(detections[x]);
				if (costMatrix.at<double>(y, x) > liveTracks[y]->getCostThreshold())
				{
					costMatrix.at<double>(y, x) = liveTracks[y]->getCostMax();
				}
			}
			else
			{
				costMatrix.at<double>(y, x) = liveTracks[y]->getCostMax();
			}
		}
	}

	try
	{
		xray::HungarianN3::assignmentSolve(costMatrix, assignments);
	}
	catch (std::exception&)
	{
		for (int i = 0; i < costMatrix.rows; ++i)
		{
			assignments.push_back(xray::HungarianN3::NOT_ASSIGNED());
		}
	}

	for (unsigned int y = 0; y < liveTracks.size(); ++y)
	{
		if (assignments[y] == xray::HungarianN3::NOT_ASSIGNED() || costMatrix.at<double>(y, static_cast<int>(assignments[y])) >= liveTracks[y]->getCostMax() || costMatrix.at<double>(y, static_cast<int>(assignments[y])) > liveTracks[y]->getCostThreshold())
		{
			liveTracks[y]->missFrame(nullptr);
			assignments[y] = xray::HungarianN3::NOT_ASSIGNED();
		}
		else
		{
			CV_Assert(costMatrix.at<double>(y, static_cast<int>(assignments[y])) <= liveTracks[y]->getCostThreshold());
			liveTracks[y]->update3d(nullptr, detections[assignments[y]]);
		}
	}
	for (unsigned int y = 0; y < liveTracks.size(); ++y)
	{
		if (liveTracks[y]->assignProbability())
		{
			liveTracks[y]->field()->ballTrack3d = liveTracks[y];
		}
		if (liveTracks[y]->isDead(std::vector<cv::Point>(), workerManager->getFields()))
		{
			liveTracks[y]->setEndTimestamp(workerManager->time());
			if(liveTracks[y]->isVisible() && liveTracks[y]->getLength() >= minTrackLength)
			{
				saveData(liveTracks[y], ballData[liveTracks[y]->fieldID()]);
			}
			deadTracks.push_back(liveTracks[y]);

			liveTracks.erase(liveTracks.begin() + y);
			assignments.erase(assignments.begin() + y);
			y--;
		}
		else if(liveTracks[y]->isVisible())
		{
			
		}
	}

	std::vector<size_t>::iterator it;
	for (unsigned int x = 0; x < detections.size(); ++x)
	{
		if (detections[x]->isValid())
		{
			it = find(assignments.begin(), assignments.end(), x);
			if (it == assignments.end())
			{
				std::shared_ptr<KFTrack3d> tr = createTrack( detections[x], workerManager->time(), liveTracks.size());
				if (tr)
				{
					liveTracks.push_back(tr);
				}
			}
		}
	}
}
std::shared_ptr<KFTrack3d> KFTracker3d::createTrack(const std::shared_ptr<MovingObject> obj, const int64_t dStartTS, const size_t iNumTracks)
{
	if (iNumTracks > maxNumTracks)
	{
		return nullptr;
	}
	Field* field = workerManager->inField(obj);
	if (!field)
	{
		return nullptr;
	}
	CV_Assert(field->fid == obj->fieldID());
	return std::make_shared<KFBallTrack3d>(IDFactory::generateID(ID_TRACK3D), obj, stateParams, mesaureParams, dStartTS, field->maxAbsVelocityGlobal, field->maxBallProbabilityGlobal);
}
void KFTracker3d::saveData(std::shared_ptr<KFTrack3d> track, std::deque<BallSaveData>& ballData) const
{
	for (int i = 0; i < track->trace().size() - track->getMissedFrames(); ++i)
	{
		std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(track->getDet(i));
		CV_Assert(ball);
		ballData.push_back({ track->getAverageProbability(), static_cast<std::size_t>(track->getStartTimestamp() + i), static_cast<std::size_t>(track->getStartTimestamp()), ball->pos3d(), ball->recError(), ball->views(), ball->isDetected(), ball->ballType(), track->getTID() });
	}
}
