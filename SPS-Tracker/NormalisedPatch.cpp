#include "NormalisedPatch.h"
#include "Utils.h"

NormalizedPatch::NormalizedPatch()
{
	mean = 0.0f;
	norm = 0.0f;
	positive = 0;
	conf = 0.0f;
	valid = 0;
	values = static_cast<float*>(_aligned_malloc(sizeof(float) * TLD_PATCH_SIZE *TLD_PATCH_SIZE, TLD_MEM_ALIGNMENT));
	memset(values, 0, sizeof(float) * TLD_PATCH_SIZE *TLD_PATCH_SIZE);
}

NormalizedPatch::~NormalizedPatch()
{
	if(values)
	{
		_aligned_free(values);
	}
}

NormalizedPatch::NormalizedPatch(const NormalizedPatch& np)
	:	roi(np.roi),
		mean(np.mean),
		norm(np.norm),
		positive(np.positive),
		conf(np.conf),
		valid(np.valid)
{
	values = static_cast<float*>(_aligned_malloc(sizeof(float) * TLD_PATCH_SIZE *TLD_PATCH_SIZE, TLD_MEM_ALIGNMENT));
	memcpy(values, np.values, sizeof(float)*TLD_PATCH_SIZE *TLD_PATCH_SIZE);

#ifdef SPS_LAB
	subImage = np.subImage.clone();
#endif
}
NormalizedPatch& NormalizedPatch::operator=(const NormalizedPatch& np)
{
	if (this != &np)
	{
		roi = np.roi;
		mean = np.mean;
		norm = np.norm;
		positive = np.positive;
		conf = np.conf;
		valid = np.valid;

		memcpy(values, np.values, sizeof(float)*TLD_PATCH_SIZE *TLD_PATCH_SIZE);

#ifdef SPS_LAB
		subImage = np.subImage.clone();
#endif
	}
	return *this;
}
std::istream& operator>>(std::istream& is, NormalizedPatch& np)
{
	is.read(reinterpret_cast<char*>(&np.roi), std::streamsize(sizeof(cv::Rect)));
	is.read(reinterpret_cast<char*>(np.values), std::streamsize(sizeof(float)*TLD_PATCH_SIZE *TLD_PATCH_SIZE));
	is.read(reinterpret_cast<char*>(&np.positive), std::streamsize(sizeof(bool)));
	is.read(reinterpret_cast<char*>(&np.valid), std::streamsize(sizeof(int)));
	is.read(reinterpret_cast<char*>(&np.conf), std::streamsize(sizeof(float)));
	is.read(reinterpret_cast<char*>(&np.mean), std::streamsize(sizeof(double)));

	return is;
}
std::ostream& operator<<(std::ostream& os, const NormalizedPatch& np)
{
	os.write(reinterpret_cast<const char*>(&np.roi), std::streamsize(sizeof(cv::Rect)));
	os.write(reinterpret_cast<const char*>(np.values), std::streamsize(sizeof(float)*TLD_PATCH_SIZE *TLD_PATCH_SIZE));
	os.write(reinterpret_cast<const char*>(&np.positive), std::streamsize(sizeof(bool)));
	os.write(reinterpret_cast<const char*>(&np.valid), std::streamsize(sizeof(int)));
	os.write(reinterpret_cast<const char*>(&np.conf), std::streamsize(sizeof(float)));
	os.write(reinterpret_cast<const char*>(&np.mean), std::streamsize(sizeof(double)));
	return os;
}
void NormalizedPatch::extract(const cv::Mat &img, const cv::Mat &intImg, int x, int y, int w, int h)
{
	roi = cv::Rect(x, y, w, h) & cv::Rect(cv::Size(0,0), img.size());

	cv::Mat subImage = img(roi).clone();
	mean = tldCalcMean(intImg, roi.x, roi.y, roi.width, roi.height);

	cv::Mat result;
	cv::resize(subImage, result, cv::Size(TLD_PATCH_SIZE, TLD_PATCH_SIZE), 0, 0, cv::INTER_AREA);
	unsigned char *imgData = (unsigned char *)result.data;
	for (int i = 0; i < TLD_PATCH_SIZE; i++)
	{
		for (int j = 0; j < TLD_PATCH_SIZE; j++)
		{
			values[j * TLD_PATCH_SIZE + i] = static_cast<float>(imgData[j * result.step + i] - mean);
			norm += values[j * TLD_PATCH_SIZE + i] * values[j * TLD_PATCH_SIZE + i];
		}
	}

#ifdef SPS_LAB
	this->subImage = img(roi).clone();
#endif
}
void NormalizedPatch::extract(const cv::Mat &img, const cv::Mat &intImg, int* boundary)
{
	extract(img, intImg, boundary[0], boundary[1], boundary[2], boundary[3]);
}
void NormalizedPatch::extract(const cv::Mat &img, const cv::Mat &intImg, const cv::Rect& r)
{
	extract(img, intImg, r.x, r.y, r.width, r.height);
}