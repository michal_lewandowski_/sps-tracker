#pragma once

#include <opencv.hpp>
#include "KFTrack2d.h"

class MedianFlowTracker
{
public:
	MedianFlowTracker();
	virtual ~MedianFlowTracker();
	
	void release();
	void initImgs();
	void track(FieldList& fields);
	int trackLK(IplImage *imgI, IplImage *imgJ, float ptsI[], int nPtsI, float ptsJ[], int nPtsJ, int level, float *fbOut, float *nccOut, char *statusOut);
	int fbtrack(IplImage *imgI, IplImage *imgJ, float *bb, float *bbnew, float *scaleshift);

	void euclideanDistance(CvPoint2D32f *point1, CvPoint2D32f *point2, float *match, int nPts);
//	void normCrossCorrelation(cv::Mat pimg, cv::Mat cimg, CvPoint2D32f *points0, CvPoint2D32f *points1, int nPts, char *status, float *match);

	int calculateBBCenter(float bb[4], float center[2]);
	int getFilledBBPoints(float *bb, int numM, int numN, int margin, float *pts);
	int predictbb(float *bb0, CvPoint2D32f *pt0, CvPoint2D32f *pt1, int nPts, float *bb1, float *shift);
	float getBbWidth(float *bb);
	float getBbHeight(float *bb);
	void setCurrImg(const cv::Mat &img);

	IplImage **PYR;
	CvPoint2D32f* points[3];

	float* currPatchData;
	float* prevPatchData;
	cv::Mat currImg;
	cv::Mat prevImg;
	cv::Mat currPatch;
	cv::Mat prevPatch;
	static int minSize;
	static int maxSize;
	static const int winSizeLK = 4;
	static const cv::Size winSizeNCC;
	static const int pyramidLevel = 5;
	static const int points_gridX = 10;
	static const int points_gridY = 10;
	static const int trackPoints = points_gridX * points_gridY;
	static float error_forwardBackward[trackPoints];
	static float error_ncc[trackPoints];
	static char status[trackPoints];
	static float error_forwardBackward1[trackPoints];
	static float error_ncc1[trackPoints];
	static char status1[trackPoints];
};
