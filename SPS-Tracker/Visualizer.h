#pragma once
#include <string>
#include <vector>
#include <core/mat.hpp>
#include "CameraCalibration.h"
#include "TimeWatch.hpp"
#include "Field.h"

class Visualizer
{
public:
	Visualizer();
	virtual ~Visualizer();

	void addJob(std::string name, std::string videoFile, std::string configFile, std::string inputDir);
	void process();
	void setFields(const std::vector<std::pair<cv::Rect, std::string>>& f, int margin, int numCameras);
	bool init(bool all, std::string inputName, bool count);
	void handleKey();

	inline int64_t time() { return systemTime.getFrameNum(); };
	inline std::vector<int>& getCountData() { return countData; };

private:
	TimeWatch systemTime;
	FieldList fields;
	double windowScale;

	std::vector<std::string> names;
	std::vector<std::string> videoFiles;
	std::vector<std::string> configFiles;
	std::string inputDir;

	std::vector<CameraCalibration> calibs;
	std::vector<cv::VideoCapture> caps;
	std::vector<std::vector<std::vector<std::tuple<cv::Point3d, int, int>>>> ballEst;

	bool stop;
	bool step;
	bool countStats;
	std::vector<int> countData;
};

