#include "Field.h"
#include "CameraCalibration.h"
#include "ColorBallMap.h"
#include "Utils.h"
#include "NPClassifier.h"
#include "BinaryClassifier.h"
#include "DetectorCascade.h"
#include "VarianceFilter.h"
#include "EnsembleClassifier.h"

extern std::default_random_engine dre;

Field::Field(int id, cv::Rect r, int margin, int camsNum, std::string gtPath)
	: 
	stats(id, gtPath, this),
	fid(id), 
	field(r), 
	ballColorMap(nullptr),
	ballTrack2d(nullptr),
	ballTrack3d(nullptr),
	maxAbsVelocityGlobal(std::numeric_limits<double>::epsilon()),
	maxBallProbabilityGlobal(std::numeric_limits<double>::max()), 
	posteriors(nullptr),
	positives(nullptr), 
	negatives(nullptr),
	currBB(nullptr),
	binaryP(0), 
	binaryN(0),
	currState(VisualState::NONE),
	valid(false),
	wasValid(false),
	isLearning(false),
	minVar(std::numeric_limits<float>::max()),
	currConf(0.0f),
	currLabel(0),
	prevBB(new cv::Rect(0, 0, 0, 0)),
	numWindows(0),
	numScales(0),
	isInitTLD(false)
{
	numObservations.resize(camsNum, 0);

	/*if (field.tl() == cv::Point(1000, 1000))
	{
		fid = 1;
	}
	if (field.tl() == cv::Point(9000, 1000))
	{
		fid = 2;
	}
	if (field.tl() == cv::Point(17000, 1000))
	{
		fid = 3;
	}
	if (field.tl() == cv::Point(25000, 1000))
	{
		fid = 4;
	}
	if (field.tl() == cv::Point(33000, 1000))
	{
		fid = 5;
	}*/
	fieldExtended = cv::Rect(field.tl() - cv::Point(margin, margin), field.br() + cv::Point(margin, margin));

	zonePlayer1.push_back(cv::Rect(field.tl(), field.tl() + cv::Point(2000, 2000)));
	zonePlayer1.push_back(cv::Rect(cv::Point(field.tl().x + 2000, field.tl().y), cv::Point(field.br().x - 2000, field.tl().y + 2000)));
	zonePlayer1.push_back(cv::Rect(cv::Point(field.tl().x + 4000, field.tl().y), cv::Point(field.br().x, field.tl().y + 2000)));

	zonePlayer2.push_back(cv::Rect(cv::Point(field.tl().x, field.br().y - 2000), cv::Point(field.tl().x + 2000, field.br().y)));
	zonePlayer2.push_back(cv::Rect(cv::Point(field.tl().x + 2000, field.br().y - 2000), cv::Point(field.tl().x + 4000, field.br().y)));
	zonePlayer2.push_back(cv::Rect(field.br() - cv::Point(2000, 2000), field.br()));

	zoneGame = cv::Rect(field.tl() + cv::Point(0, 2000), field.br() - cv::Point(0, 2000));
}
Field::Field(const Field& f)
	: stats(f.stats)
{
	fid = f.fid;
	field = f.field;
	fieldExtended = f.fieldExtended;
	numObservations = f.numObservations;
	zonePlayer1 = f.zonePlayer1;
	zonePlayer2 = f.zonePlayer2;
	zoneGame = f.zoneGame;
	detectionWindow = f.detectionWindow;

	maxAbsVelocityGlobal = f.maxAbsVelocityGlobal;
	maxBallProbabilityGlobal = f.maxBallProbabilityGlobal;

	ballColorMap = f.ballColorMap;
	ballTrack2d = f.ballTrack2d;
	ballTrack3d = f.ballTrack3d;

	binaryP = f.binaryP;
	binaryN = f.binaryN;
	minVar = f.minVar;
	int numIndices = static_cast<int>(pow(2, EnsembleClassifier::numFeatures));
	
	if(f.posteriors)
	{
		posteriors = new float[EnsembleClassifier::numTrees * numIndices];
		memcpy(posteriors, f.posteriors, sizeof(float) * EnsembleClassifier::numTrees * numIndices);
	}
	if (f.positives)
	{
		positives = new int[EnsembleClassifier::numTrees * numIndices];
		memcpy(positives, f.positives, sizeof(int) * EnsembleClassifier::numTrees * numIndices);
	}
	if (f.negatives)
	{
		negatives = new int[EnsembleClassifier::numTrees * numIndices];
		memcpy(negatives, f.negatives, sizeof(int) * EnsembleClassifier::numTrees * numIndices);
	}

	numWindows = f.numWindows;
	numScales = f.numScales;
	windows = f.windows;
	windowOffsets = f.windowOffsets;
	featureOffsets = f.featureOffsets;
	scales = f.scales;

	isInitTLD = f.isInitTLD;
	isLearning = f.isLearning;
	valid = f.valid;
	wasValid = f.wasValid;
	currConf = f.currConf;
	currLabel = f.currLabel;
	currState = f.currState;
	CV_Assert(f.prevBB);
	prevBB = new cv::Rect(*f.prevBB);
	if(f.currBB)
		currBB = new cv::Rect(*f.currBB);
	else currBB = nullptr;

	trackResult = f.trackResult;
	detectionResult = f.detectionResult;

	falsePositives = f.falsePositives;
	truePositives = f.truePositives;
}
Field::Field(Field&& f)
	: stats(std::move(f.stats))
{
	fid = f.fid;
	field = std::move(f.field);
	fieldExtended = std::move(f.fieldExtended);
	numObservations = std::move(f.numObservations);
	zonePlayer1 = std::move(f.zonePlayer1);
	zonePlayer2 = std::move(f.zonePlayer2);
	zoneGame = std::move(f.zoneGame);
	detectionWindow = std::move(f.detectionWindow);

	maxAbsVelocityGlobal = f.maxAbsVelocityGlobal;
	maxBallProbabilityGlobal = f.maxBallProbabilityGlobal;

	ballColorMap = f.ballColorMap;
	ballTrack2d = f.ballTrack2d;
	ballTrack3d = f.ballTrack3d;
	f.ballColorMap = nullptr;
	f.ballTrack2d = nullptr;
	f.ballTrack3d = nullptr;

	binaryP = f.binaryP;
	binaryN = f.binaryN;
	minVar = f.minVar;
	posteriors = f.posteriors;
	positives = f.positives;
	negatives = f.negatives;
	f.posteriors = nullptr;
	f.positives = nullptr;
	f.negatives = nullptr;

	numWindows = f.numWindows;
	numScales = f.numScales;
	windows = std::move(f.windows);
	windowOffsets = std::move(f.windowOffsets);
	featureOffsets = std::move(f.featureOffsets);
	scales = std::move(f.scales);

	isInitTLD = f.isInitTLD;
	isLearning = f.isLearning;
	valid = f.valid;
	wasValid = f.wasValid;
	currConf = f.currConf;
	currLabel = f.currLabel;
	currState = f.currState;

	prevBB = f.prevBB;
	currBB = f.currBB;
	f.prevBB = nullptr;
	f.currBB = nullptr;

	trackResult = std::move(f.trackResult);
	detectionResult = std::move(f.detectionResult);

	falsePositives = std::move(f.falsePositives);
	truePositives = std::move(f.truePositives);
}
Field::~Field()
{
	if (currBB)
	{
		delete currBB;
		currBB = nullptr;
	}

	if (prevBB)
	{
		delete prevBB;
		prevBB = nullptr;
	}
	if (posteriors)
	{
		delete[] posteriors;
		posteriors = nullptr;
	}
	if (positives)
	{
		delete[] positives;
		positives = nullptr;
	}
	if (negatives)
	{
		delete[] negatives;
		negatives = nullptr;
	}
	windows.clear();
	windowOffsets.clear();
	featureOffsets.clear();
}
void Field::generateMask(CameraCalibration& calibration)
{
	fieldMask.create(calibration.getImageSize(), CV_8UC1);
	fieldMask.setTo(0);
	
	std::vector<std::vector<cv::Point>> vv;
	vv.push_back(project2d(fieldExtended, calibration));
	cv::drawContours(fieldMask, vv, -1, 255, -1);
}
void Field::prepare()
{
	maxAbsVelocityGlobal = std::numeric_limits<double>::epsilon();
	maxBallProbabilityGlobal = std::numeric_limits<double>::max();
	ballTrack2d = nullptr;
	ballTrack3d = nullptr;
	if(ballColorMap)
	{
		ballColorMap->prepare();
	}
	detectionWindow = cv::Rect(0,0,0,0);
}
void Field::show(int64_t frameNum, cv::Mat frame, double windowScale, std::vector<int> countData, int modify)
{
	cv::rectangle(frame, cv::Point(cvRound(field.tl().x / windowScale), frame.rows - cvRound(field.tl().y / windowScale)), cv::Point(cvRound(field.br().x / windowScale), frame.rows - cvRound(field.br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	
	if (countData.size() == 0)
	{
		cv::rectangle(frame, cv::Point(cvRound((fieldExtended.tl().x- modify) / windowScale), frame.rows - cvRound(fieldExtended.tl().y / windowScale)), cv::Point(cvRound((fieldExtended.br().x - modify) / windowScale), frame.rows - cvRound(fieldExtended.br().y / windowScale)), cv::Scalar(128, 128, 128), 1);
	}
	
	cv::rectangle(frame, cv::Point(cvRound((zonePlayer1[0].tl().x- modify) / windowScale), frame.rows - cvRound(zonePlayer1[0].tl().y / windowScale)), cv::Point(cvRound((zonePlayer1[0].br().x - modify) / windowScale), frame.rows - cvRound(zonePlayer1[0].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	cv::rectangle(frame, cv::Point(cvRound((zonePlayer2[0].tl().x - modify) / windowScale), frame.rows - cvRound(zonePlayer2[0].tl().y / windowScale)), cv::Point(cvRound((zonePlayer2[0].br().x - modify) / windowScale), frame.rows - cvRound(zonePlayer2[0].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);

	cv::rectangle(frame, cv::Point(cvRound((zonePlayer1[1].tl().x - modify) / windowScale), frame.rows - cvRound(zonePlayer1[1].tl().y / windowScale)), cv::Point(cvRound((zonePlayer1[1].br().x - modify) / windowScale), frame.rows - cvRound(zonePlayer1[1].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	cv::rectangle(frame, cv::Point(cvRound((zonePlayer2[1].tl().x - modify) / windowScale), frame.rows - cvRound(zonePlayer2[1].tl().y / windowScale)), cv::Point(cvRound((zonePlayer2[1].br().x - modify) / windowScale), frame.rows - cvRound(zonePlayer2[1].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);

	cv::rectangle(frame, cv::Point(cvRound((zonePlayer1[2].tl().x - modify) / windowScale), frame.rows - cvRound(zonePlayer1[2].tl().y / windowScale)), cv::Point(cvRound((zonePlayer1[2].br().x - modify) / windowScale), frame.rows - cvRound(zonePlayer1[2].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	cv::rectangle(frame, cv::Point(cvRound((zonePlayer2[2].tl().x - modify) / windowScale), frame.rows - cvRound(zonePlayer2[2].tl().y / windowScale)), cv::Point(cvRound((zonePlayer2[2].br().x - modify) / windowScale), frame.rows - cvRound(zonePlayer2[2].br().y / windowScale)), cv::Scalar(200, 200, 200), 1);

	cv::rectangle(frame, cv::Point(cvRound((zoneGame.tl().x - modify) / windowScale), frame.rows - cvRound(zoneGame.tl().y / windowScale)), cv::Point(cvRound((zoneGame.br().x - modify) / windowScale), frame.rows - cvRound(zoneGame.br().y / windowScale)), cv::Scalar(200, 200, 200), 1);
	/*if (countData.size() == 0)
	{
		cv::putText(frame, "Field " + std::to_string(fid), cv::Point(cvRound((zonePlayer2[1].tl().x + zonePlayer2[1].width / 3) / windowScale), cvRound((zonePlayer2[1].br().y + 400) / windowScale)), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(200, 200, 200));
	}*/
	
	if(countData.size() > 0)
	{
		cv::Point pt1(cvRound((fieldExtended.tl().x - 300) / windowScale), frame.rows - cvRound((fieldExtended.tl().y - 100) / windowScale));
		cv::putText(frame, std::to_string(countData[0]),pt1, CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 250, 00), 2);

		cv::Point pt11(cvRound((fieldExtended.tl().x + 400) / windowScale), frame.rows - cvRound((fieldExtended.tl().y - 100) / windowScale));
		cv::putText(frame, std::to_string(countData[4]), pt11, CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 0, 250), 2);

		cv::Point pt2(cvRound((fieldExtended.br().x - 450) / windowScale), frame.rows - cvRound((fieldExtended.tl().y - 100) / windowScale));
		cv::putText(frame, std::to_string(countData[1]), pt2, CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 250, 0), 2);

		cv::Point pt22(cvRound((fieldExtended.br().x + 250) / windowScale), frame.rows - cvRound((fieldExtended.tl().y - 100) / windowScale));
		cv::putText(frame, std::to_string(countData[5]), pt22, CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 0, 250), 2);

		cv::Point pt3(cvRound((fieldExtended.tl().x - 300) / windowScale), frame.rows - cvRound((fieldExtended.br().y - 100) / windowScale));
		cv::putText(frame, std::to_string(countData[2]), pt3, CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 250, 0), 2);

		cv::Point pt33(cvRound((fieldExtended.tl().x + 400) / windowScale), frame.rows - cvRound((fieldExtended.br().y - 100) / windowScale));
		cv::putText(frame, std::to_string(countData[6]), pt33, CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 0, 250), 2);

		cv::Point pt4(cvRound((fieldExtended.br().x - 450) / windowScale), frame.rows - cvRound((fieldExtended.br().y - 100) / windowScale));
		cv::putText(frame, std::to_string(countData[3]), pt4, CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 250, 0), 2);

		cv::Point pt44(cvRound((fieldExtended.br().x + 250) / windowScale), frame.rows - cvRound((fieldExtended.br().y - 100) / windowScale));
		cv::putText(frame, std::to_string(countData[7]), pt44, CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 0, 250), 2);
	}

	stats.show3d(frameNum, frame, windowScale);
}
std::vector<cv::Point> Field::project2d(cv::Rect f, CameraCalibration& calibration) const
{
	std::vector<cv::Point> v(4);
	cv::Point2d pt;
	calibration.transformToPixel(cv::Point3d(f.tl().x, f.tl().y, 0.0), pt);
	v[0] = cv::Point(static_cast<int>(pt.x), static_cast<int>(pt.y));
	calibration.transformToPixel(cv::Point3d(f.tl().x, f.br().y, 0.0), pt);
	v[1] = cv::Point(static_cast<int>(pt.x), static_cast<int>(pt.y));
	calibration.transformToPixel(cv::Point3d(f.br().x, f.br().y, 0.0), pt);
	v[2] = cv::Point(static_cast<int>(pt.x), static_cast<int>(pt.y));
	calibration.transformToPixel(cv::Point3d(f.br().x, f.tl().y, 0.0), pt);
	v[3] = cv::Point(static_cast<int>(pt.x), static_cast<int>(pt.y));
	return v;
}
void Field::show(int64_t frameNum, cv::Mat frame1, cv::Mat frame2, std::string camID, CameraCalibration& calibration, cv::Scalar color, bool show)
{
	std::vector<std::vector<cv::Point>> vv;
	std::vector<std::vector<cv::Point>> vvx;

	vv.push_back(project2d(field, calibration));
	vvx.push_back(project2d(fieldExtended, calibration));

	cv::drawContours(frame1, vvx, -1, cv::Scalar(128, 128, 128), 1);
	cv::drawContours(frame1, vv, -1, cv::Scalar(200, 200, 200), 1);
	if(!frame2.empty())
	{
		cv::drawContours(frame2, vvx, -1, cv::Scalar(128, 128, 128), 1);
		cv::drawContours(frame2, vv, -1, cv::Scalar(200, 200, 200), 1);
	}

	if (camID == "cam1")
	{
		cv::putText(frame1, "Field " + std::to_string(fid), vv[0][1] + cv::Point(-100, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
		cv::putText(frame2, "Field " + std::to_string(fid), vv[0][1] + cv::Point(-100, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
	}
	if (camID == "cam2")
	{
		cv::putText(frame1, "Field " + std::to_string(fid), vv[0][0] + cv::Point(15, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
		cv::putText(frame2, "Field " + std::to_string(fid), vv[0][0] + cv::Point(15, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
	}
	if (camID == "cam3")
	{
		cv::putText(frame1, "Field " + std::to_string(fid), vv[0][3] + cv::Point(-100, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
		cv::putText(frame2, "Field " + std::to_string(fid), vv[0][3] + cv::Point(-100, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
	}
	if (camID == "cam4")
	{
		cv::putText(frame1, "Field " + std::to_string(fid), vv[0][2] + cv::Point(15, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
		cv::putText(frame2, "Field " + std::to_string(fid), vv[0][2] + cv::Point(15, 15), CV_FONT_HERSHEY_PLAIN, 2, color, 2);
	}
	for(int i=0; i < 3; ++i)
	{
		{
			std::vector<std::vector<cv::Point>> vv;
			vv.push_back(project2d(zonePlayer1[i], calibration));
			cv::drawContours(frame1, vv, -1, cv::Scalar(200, 200, 200), 1);
			if (!frame2.empty())
				cv::drawContours(frame2, vv, -1, cv::Scalar(200, 200, 200), 1);
		}
		{
			std::vector<std::vector<cv::Point>> vv;
			vv.push_back(project2d(zonePlayer2[i], calibration));
			cv::drawContours(frame1, vv, -1, cv::Scalar(200, 200, 200), 1);
			if (!frame2.empty())
				cv::drawContours(frame2, vv, -1, cv::Scalar(200, 200, 200), 1);
		}
	}
	{
		std::vector<std::vector<cv::Point>> vv;
		vv.push_back(project2d(zoneGame, calibration));
		cv::drawContours(frame1, vv, -1, cv::Scalar(200, 200, 200), 1);
		if (!frame2.empty())
			cv::drawContours(frame2, vv, -1, cv::Scalar(200, 200, 200), 1);
	}

	stats.show(frameNum, frame1, calibration);
	if (!frame2.empty())
		stats.show(frameNum, frame2, calibration);

#ifdef SPS_DEBUB_DETS
	if(ballColorMap && !frame2.empty())
	{
		if (ballColorMap)
		{
			cv::Mat imgAllColors = ballColorMap->quantiziser()->getColorQuantizisationImage();
			if(!imgAllColors.empty())
			{
				//cv::imshow(std::to_string(fid) + " Color Quantizisation", imgAllColors);
			}
		}
		std::string prefixStr;
		std::stringstream ss;
		cv::Mat imgColor = ballColorMap->getImage();
		cv::Mat imgProb = ballColorMap->getProbImage(ss);
		cv::Mat imgAll;
		cv::vconcat(imgColor, imgProb, imgAll);

		//imgAll.copyTo(frame1(cv::Rect((fid-1) * 387, frame1.rows - imgAll.rows, imgAll.cols, imgAll.rows)));
		imgAll.copyTo(frame2(cv::Rect(fid * 387, frame2.rows - imgAll.rows, imgAll.cols, imgAll.rows)));
		
		if(show)
		{
		//	std::cout << std::endl << camID << "[" << fid << "]: " << std::endl << ss.str() << std::endl;
		}
	}
#endif
	if (isInitTLD)
	{
		cv::Mat pos = showPositiveDetectionModel();
		cv::Mat neg = showNegativeDetectionModel();
		cv::imshow("pos cam " + camID + " field " + std::to_string(fid), pos);
		cv::imshow("neg cam " + camID + " field " + std::to_string(fid), neg);
	}
}
bool Field::isInPlayerZone(const cv::Point3d& pos) const
{
	if (zonePlayer1[0].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer1[1].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer1[2].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer2[0].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer2[1].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	if (zonePlayer2[2].contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		return true;
	return false;
}
bool Field::isInGameZone(const cv::Point3d& pos) const
{
	return zoneGame.contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y)));
}
std::pair<cv::Point3d, cv::Rect> Field::closestPlayerZone(cv::Point3d pos)
{
	cv::Point3d centerZone1(zonePlayer1[0].tl().x + zonePlayer1[0].width * 0.5, zonePlayer1[0].tl().y + zonePlayer1[0].height * 0.5, 0);
	cv::Point3d centerZone2(zonePlayer1[2].tl().x + zonePlayer1[2].width * 0.5, zonePlayer1[2].tl().y + zonePlayer1[2].height * 0.5, 0);
	cv::Point3d centerZone3(zonePlayer2[0].tl().x + zonePlayer2[0].width * 0.5, zonePlayer2[0].tl().y + zonePlayer2[0].height * 0.5, 0);
	cv::Point3d centerZone4(zonePlayer2[2].tl().x + zonePlayer2[2].width * 0.5, zonePlayer2[2].tl().y + zonePlayer2[2].height * 0.5, 0);
	double dist1 = cv::norm(pos - centerZone1);
	double dist2 = cv::norm(pos - centerZone2);
	double dist3 = cv::norm(pos - centerZone3);
	double dist4 = cv::norm(pos - centerZone4);
	if(dist1 < dist2 && dist1 < dist3 && dist1 < dist4)
	{
		return std::make_pair(centerZone1, zonePlayer1[0] | zonePlayer1[1] | zonePlayer1[2]);
	}
	else if (dist2 < dist3 && dist2 < dist4)
	{
		return std::make_pair(centerZone2, zonePlayer1[0] | zonePlayer1[1] | zonePlayer1[2]);
	}
	else if (dist3 < dist4)
	{
		return std::make_pair(centerZone3, zonePlayer2[0] | zonePlayer2[1] | zonePlayer2[2]);
	}
	else
	{
		return std::make_pair(centerZone4, zonePlayer2[0] | zonePlayer2[1] | zonePlayer2[2]);
	}
}
void Field::addConfidenceIndex(int idx)
{
	if (idx < numWindows && !detectionResult.lastStageInvalid[idx])
	{
		detectionResult.confidentIndicesThread[idx] = idx + 1;
	}
}
bool Field::fuseHypothesis(cv::Ptr<NPClassifier> expertClassifier)
{
	int numClusters = detectionResult.numClusters;

	cv::Rect *trackerBB = trackResult.trackerBB;
	cv::Rect *detectorBB = detectionResult.detectorBB;

	if (currBB)
	{
		delete currBB;
		currBB = nullptr;
	}
	currState = VisualState::UNKNOWN;
	currConf = 0;
	currLabel = 0;
	valid = 0;

	if (numClusters == 1 && detectorBB)
	{
		detectionResult.conf = expertClassifier->classifyBBrel(*this, *detectorBB, currLabel);
	}
	if (trackerBB)
	{
		trackResult.conf = expertClassifier->classifyBBrel(*this, *trackerBB, currLabel);
		//results of detector must overlap to some extent with a result of tracker
		if (numClusters == 1 && detectionResult.conf > trackResult.conf && tldOverlapRectRect(*trackerBB, *detectorBB) > 0.5)
		{
			currBB = tldCopyRect(detectorBB);
			currConf = detectionResult.conf;
			currState = VisualState::DETECTOR;
			if (detectionResult.conf > NPClassifier::thetaFP)
			{
				valid = 3;
			}
		}
		else if (trackResult.conf > 0.25)
		{
			currBB = tldCopyRect(trackerBB);
			currConf = trackResult.conf;
			currState = VisualState::TRACKER;

			if (trackResult.conf > NPClassifier::thetaTP)
			{
				valid = 2;
			}
			else if (wasValid>0 && trackResult.conf > NPClassifier::thetaFP)
			{
				valid = 3;
			}
			else
			{
				valid = wasValid;
			}
		}
	}
	else if (numClusters == 1)
	{
		currBB = tldCopyRect(detectorBB);
		currConf = detectionResult.conf;
		currState = VisualState::DETECTOR;
		if (detectionResult.conf > NPClassifier::thetaFP)
		{
			valid = 3;
		}
	}
	CV_Assert((currBB == nullptr && currConf == 0) || (currBB != nullptr && currConf > 0));
	return currBB != nullptr;
}
void Field::storePreviousData()
{
	if (currBB)
	{
		prevBB->x = currBB->x;
		prevBB->y = currBB->y;
		prevBB->width = currBB->width;
		prevBB->height = currBB->height;
	}
	else
	{
		prevBB->x = 0;
		prevBB->y = 0;
		prevBB->width = 0;
		prevBB->height = 0;
	}
	trackResult.reset();
	detectionResult.reset();
	wasValid = valid;
}
void Field::initialLearning(const cv::Mat& frame, const cv::Mat& intImg, const cv::Mat& intSqImg, const DetectorCascade& detCasc)
{
	CV_Assert(currBB);

	float *overlap = new float[numWindows];

	tldOverlapRect(&windows[0], numWindows, currBB, overlap);

	std::vector< std::pair<int, float> > positiveIndices;
	std::vector<int> negativeIndices;

	for (int i = 0; i < numWindows; i++)
	{
		int* box = &windows[TLD_WINDOW_SIZE * i];

		float variance = detCasc.varianceFilter->calcVariance(box);
		if (variance >= minVar)
		{
			if (overlap[i] > 0.6)
			{
				int bP = 0;
				int bN = 0;
				tldCalcBinary(intImg, box[0], box[1], box[2], box[3], bP, bN);
				if(BinaryClassifier::compareBinary(binaryP, bP, binaryN, bN))
				{
					positiveIndices.push_back(std::pair<int, float>(i, overlap[i]));
				}
			}
			if (overlap[i] < 0.2)
			{
				negativeIndices.push_back(i);
			}
		}
	}
	{
		cv::Mat aaa = frame.clone();
		cv::Mat bbb, ccc;

		cv::resize(aaa(*currBB), bbb, cv::Size(TLD_PATCH_SIZE, TLD_PATCH_SIZE), 0, 0, cv::INTER_AREA);

		cv::resize(bbb, ccc, cv::Size(), TLD_VIS_PATCH_SCALE, TLD_VIS_PATCH_SCALE, CV_INTER_CUBIC);
		cv::imshow("what the fuck ", ccc);
	}
	
	int* featureVector = new int[detCasc.ensembleClassifier->numTrees];

	std::sort(positiveIndices.begin(), positiveIndices.end(), tldSortByOverlapAsc);
	int numIterationsP = std::min(static_cast<int>(positiveIndices.size()), 100);
	for (int i = 0; i < numIterationsP; i++)
	{
		int idx = positiveIndices.at(i).first;
		/*
		int* box = &windows[TLD_WINDOW_SIZE * idx];

		cv::Mat aaa = frame.clone();
		cv::Mat bbb,ccc;

		
		cv::resize(aaa(cv::Rect(box[0], box[1], box[2], box[3])), bbb, cv::Size(TLD_PATCH_SIZE, TLD_PATCH_SIZE), 0, 0, cv::INTER_AREA);

		cv::resize(bbb,ccc, cv::Size(), TLD_VIS_PATCH_SCALE, TLD_VIS_PATCH_SCALE, CV_INTER_CUBIC);
		cv::imshow("aaa init ", ccc);
		cv::waitKey(0);*/

		detCasc.ensembleClassifier->calcFeatureVector(*this, idx, featureVector);
		detCasc.ensembleClassifier->learn(*this, true, featureVector);
	}
	std::vector<NormalizedPatch> patches;
	
	NormalizedPatch patchPos;
	patchPos.positive = 1;
	patchPos.conf = currConf;
	patchPos.valid = valid;
	patchPos.extract(frame, intImg, *currBB);
	patches.push_back(patchPos);

	detCasc.ensembleClassifier->calcFeatureVector(*this, *currBB, featureVector);
	detCasc.ensembleClassifier->learn(*this, true, featureVector);

	if (negativeIndices.size() > 200)
		std::shuffle(negativeIndices.begin(), negativeIndices.end(), dre);

	int numIterationsN = std::min(static_cast<int>(negativeIndices.size()), 200);
	for (size_t i = 0; i < numIterationsN; i++)
	{
		int idx = negativeIndices.at(i);
		CV_Assert(EnsembleClassifier::numTrees * idx < numWindows * EnsembleClassifier::numTrees);
		CV_Assert(EnsembleClassifier::numTrees * idx + EnsembleClassifier::numTrees <= numWindows * EnsembleClassifier::numTrees);

		detCasc.ensembleClassifier->calcFeatureVector(*this, idx, featureVector);
		detCasc.ensembleClassifier->learn(*this, false, featureVector);

		NormalizedPatch negPatch;
		negPatch.extract(frame, intImg, &windows[TLD_WINDOW_SIZE * idx]);
		negPatch.positive = 0;
		patches.push_back(negPatch);
	}
	detCasc.expertClassifier->initLearn(*this, patches);

	if (featureVector)
		delete[] featureVector;
	delete[] overlap;
}
void Field::learn(const cv::Mat& frame, const cv::Mat& intImg, const cv::Mat& intSqImg, const DetectorCascade& detCasc)
{
	isLearning = false;

	if (currLabel == -1) // patch is too highly correlated with negative data
	{
		//std::cout << "neg patch" << std::endl;
		valid = 0;
		return;
	}
	if (currConf < NPClassifier::thetaFP) // too fast change of appearance
	{
		//std::cout << "currConf < 0.5" << std::endl;
		valid = 0;
		return;
	}
	if (detCasc.varianceFilter->calcVariance(currBB) < minVar) // too low variance of the patch
	{
		//std::cout << "var failed" << std::endl;
		valid = 0;
		return;
	}
	CV_Assert(detectionResult.containsValidData);

	float *overlap = new float[numWindows];
	tldOverlapRect(&windows[0], numWindows, currBB, overlap);

	std::vector<std::pair<int, float> > positiveIndices;
	std::vector<int> negativeIndices;

	for (int i = 0; i < numWindows; i++)
	{
		int* box = &windows[TLD_WINDOW_SIZE * i];
		detectionResult.variances[i] = detCasc.varianceFilter->calcVariance(box);
		
		float variance = detectionResult.variances[i];
		if (variance >= minVar)
		{
			if (overlap[i] > 0.6)
			{
				tldCalcBinary(intImg, box[0], box[1], box[2], box[3], detectionResult.binaryP[i], detectionResult.binaryN[i]);
				if(BinaryClassifier::compareBinary(binaryP, detectionResult.binaryP[i], binaryN, detectionResult.binaryN[i]))
				{
					positiveIndices.push_back(std::pair<int, float>(i, overlap[i]));
				}
			}

			if (overlap[i] < 0.2)
			{
				if (detectionResult.posteriors[i] > 0.5)
				{
					negativeIndices.push_back(i);
				}
			}
		}
	}

	std::sort(positiveIndices.begin(), positiveIndices.end(), tldSortByOverlapDesc);

	std::vector<NormalizedPatch> patches;

	NormalizedPatch patchPos;
	patchPos.extract(frame, intImg, *currBB);
	patchPos.positive = 1;
	patchPos.conf = currConf;
	patchPos.valid = valid;
	patches.push_back(patchPos);

	int numIterationsP = std::min(static_cast<int>(positiveIndices.size()), 10);
	for (int i = 0; i < numIterationsP; i++)
	{
		int idx = positiveIndices.at(i).first;

		detCasc.ensembleClassifier->calcFeatureVector(*this, idx, &detectionResult.featureVectors[EnsembleClassifier::numTrees * idx]);
		detCasc.ensembleClassifier->learn(*this, true, &detectionResult.featureVectors[EnsembleClassifier::numTrees * idx]);
	}

	if (negativeIndices.size() > 100)
		std::shuffle(negativeIndices.begin(), negativeIndices.end(), dre);

	int numIterationsN = std::min(100, static_cast<int>(negativeIndices.size()));
	for (size_t i = 0; i < numIterationsN; i++)
	{
		int idx = negativeIndices.at(i);
		/*if (!BinaryClassifier::compareBinary(binaryP, detectionResult.binaryP[idx], binaryN, detectionResult.binaryN[idx]))
		{
			detCasc.ensembleClassifier->calcFeatureVector(*this, windows[TLD_WINDOW_SIZE * idx], &detectionResult.featureVectors[EnsembleClassifier::numTrees * idx]);
		}*/
		
		detCasc.ensembleClassifier->calcFeatureVector(*this, idx, &detectionResult.featureVectors[EnsembleClassifier::numTrees * idx]);
		detCasc.ensembleClassifier->learn(*this, false, &detectionResult.featureVectors[EnsembleClassifier::numTrees * idx]);

		NormalizedPatch negPatch;
		negPatch.extract(frame, intImg, &windows[TLD_WINDOW_SIZE * idx]);
		negPatch.positive = 0;
		patches.push_back(negPatch);
	}

	detCasc.expertClassifier->forget(*this);
	detCasc.expertClassifier->learn(*this, patches);

	delete[] overlap;

	if (currConf > NPClassifier::thetaTP)
	{
		float initVar = tldCalcVariance(intImg, intSqImg, currBB->x, currBB->y, currBB->width, currBB->height);
		minVar = initVar / 2;
		tldCalcBinary(intImg, currBB, binaryP, binaryN);
	}
	isLearning = true;
}
void Field::reinit(const cv::Mat& ballSizeMap, const cv::Mat& mask)
{
	initWindowsAndScales(ballSizeMap, mask);
	initWindowOffsets();
	initFeatureOffsets();

	detectionResult.init(numWindows);
	trackResult.init();

	int numIndices = static_cast<int>(pow(2, EnsembleClassifier::numFeatures));
	posteriors = new float[EnsembleClassifier::numTrees * numIndices];
	positives = new int[EnsembleClassifier::numTrees * numIndices];
	negatives = new int[EnsembleClassifier::numTrees * numIndices];
	memset(posteriors, 0, sizeof(float) * EnsembleClassifier::numTrees * numIndices);
	memset(positives, 0, sizeof(int) * EnsembleClassifier::numTrees * numIndices);
	memset(negatives, 0, sizeof(int) * EnsembleClassifier::numTrees * numIndices);
}
void Field::showSizeMap(std::string camID, cv::Mat frame, int scaleIndex)
{
	cv::Mat map = frame.clone();

	for (int i = 0; i < numWindows; ++i)
	{
		int* box = &windows[TLD_WINDOW_SIZE * i];
		if (box[4] == scaleIndex)
		{
			int x = box[0];
			int y = box[1];
			int w = box[2];
			int h = box[3];
			if ((x % 30) == 0 && (y % 30) == 0)
			{
				cv::Rect r(cv::Point(x - cvRound(0.5 * w), y - cvRound(0.5 * h)), cv::Point(x + cvRound(0.5 * w), y + cvRound(0.5 * h)));
				cv::rectangle(map, r.tl(), r.br(), cv::Scalar(0, 255, 0), 1);
			}
		}
	}
	cv::imshow(camID + " field " + std::to_string(fid) + " scale " + std::to_string(scaleIndex), map);
}
/* returns number of bounding boxes, bounding boxes, number of scales, scales
* bounding boxes are stored in an array of size 5*numBBs using the format <x y w h scaleIndex>
* scales are stored using the format <w h>
*
*/
void Field::initWindowsAndScales(const cv::Mat& ballSizeMap, const cv::Mat& mask)
{
	int scanAreaX = 1; // It is important to start with 1/1, because the integral images aren't defined at pos(-1,-1) due to speed reasons
	int scanAreaY = 1;
	int scanAreaW = DetectorCascade::imgWidth - 1;
	int scanAreaH = DetectorCascade::imgHeight - 1;

	int windowIndex = 0;

	scales.clear();
	scales.reserve(DetectorCascade::maxScale - DetectorCascade::minScale + 1);

	numWindows = 0;

	int scaleIndex = 0;

	float minBB = std::numeric_limits<float>::max();
	float maxBB = std::numeric_limits<float>::min();
	CV_Assert(mask.size() == ballSizeMap.size());
	CV_Assert(mask.size() == fieldMask.size());

	std::vector<float> allRadius;
	allRadius.reserve(cv::countNonZero(fieldMask));
	for (int y = 0; y < ballSizeMap.rows; ++y)
	{
		for (int x = 0; x < ballSizeMap.cols; ++x)
		{
			if (fieldMask.at<uchar>(y, x) > 0 && mask.at<uchar>(y, x) == 0)
			{
				float radius = ballSizeMap.at<float>(y, x);
				if(radius > maxBB)
				{
					maxBB = radius;
				}
				if (radius < minBB)
				{
					minBB = radius;
				}
				//allRadius.push_back(radius);
			}
		}
	}
	int bbW = cvRound((minBB + 0.5 * (maxBB - minBB)) * 2.0f);
	int bbH = bbW;

	for (int i = DetectorCascade::minScale; i <= DetectorCascade::maxScale; ++i)
	{
		float scale = pow(1.2f, i);
		int w = static_cast<int>(bbW * scale);
		int h = static_cast<int>(bbH * scale);
		
		//if (w < DetectorCascade::minSize || h < DetectorCascade::minSize || w > scanAreaW || h > scanAreaH || w > DetectorCascade::maxSize || h > DetectorCascade::maxSize)
		//	continue;

		scales.push_back(cv::Size(w, h));
	}
	numScales = static_cast<int>(scales.size());
	numWindows = cv::countNonZero(fieldMask) * numScales;

	windows.resize(TLD_WINDOW_SIZE * numWindows);

	for (scaleIndex = 0; scaleIndex < numScales; scaleIndex++)
	{
		int w = scales[scaleIndex].width;
		int h = scales[scaleIndex].height;
		int ssw = 1;// static_cast<int>(std::max(1.0f, w * DetectorCascade::shift));
		int ssh = 1; // static_cast<int>(std::max(1.0f, h * DetectorCascade::shift));

		for (int y = cvRound(scanAreaY + 0.5 * h); y <= cvRound(scanAreaH - 0.5 * h); y += ssh)
		{
			for (int x = cvRound(scanAreaX + 0.5 * w); x <= cvRound(scanAreaW - 0.5 * w); x += ssw)
			{
				if (fieldMask.at<uchar>(y, x) > 0 && mask.at<uchar>(y, x) == 0)
				{
					int *bb = &windows[TLD_WINDOW_SIZE * windowIndex];
					tldCopyBoundaryToArray<int>(cvRound(x - 0.5 * w), cvRound(y - 0.5 * h), w, h, bb);
					bb[4] = scaleIndex;
					CV_Assert(bb[0] >= scanAreaX && bb[0] <= scanAreaW);
					CV_Assert(bb[1] >= scanAreaY && bb[1] <= scanAreaH);
					CV_Assert(bb[0] + bb[2] >= scanAreaX && bb[0] + bb[2] <= scanAreaW);
					CV_Assert(bb[1] + bb[3] >= scanAreaY && bb[1] + bb[3] <= scanAreaH);
					windowIndex++;
				}
			}
		}
	}
	numWindows = windowIndex;
	windows.resize(TLD_WINDOW_SIZE * numWindows);

	std::cout << "field " << fid << ": " << cv::countNonZero(fieldMask) << " " << numWindows << std::endl;
}

#define sub2idx(x,y,imgWidthStep) ((int) (cvFloor((x)+0.5) + cvFloor((y)+0.5)*(imgWidthStep)))

void Field::initWindowOffsets()
{
	windowOffsets.resize(TLD_WINDOW_OFFSET_SIZE * numWindows);
	int* off = &windowOffsets[0];
	int windowSize = TLD_WINDOW_SIZE;

	for (int i = 0; i < numWindows; ++i)
	{
		int *window = &windows[0] + windowSize * i;
		*off++ = sub2idx(window[0] - 1, window[1] - 1, DetectorCascade::imgWidthStep);
		*off++ = sub2idx(window[0] - 1, window[1] + window[3] - 1, DetectorCascade::imgWidthStep);
		*off++ = sub2idx(window[0] + window[2] - 1, window[1] - 1, DetectorCascade::imgWidthStep);
		*off++ = sub2idx(window[0] + window[2] - 1, window[1] + window[3] - 1, DetectorCascade::imgWidthStep);
		*off++ = window[4] * 2 * EnsembleClassifier::numFeatures * EnsembleClassifier::numTrees;
		*off++ = window[2] * window[3];
	}
}

void Field::initFeatureOffsets()
{
	featureOffsets.resize(numScales * EnsembleClassifier::numTrees * EnsembleClassifier::numFeatures * 2);
	int* off = &featureOffsets[0];

	for (int k = 0; k < numScales; k++)
	{
		cv::Size scale = scales[k];

		for (int i = 0; i < EnsembleClassifier::numTrees; i++)
		{
			for (int j = 0; j < EnsembleClassifier::numFeatures; j++)
			{
				float *currentFeature = EnsembleClassifier::features + (4 * EnsembleClassifier::numFeatures) * i + 4 * j;
				*off++ = sub2idx((scale.width - 1) * currentFeature[0] + 1, (scale.height - 1) * currentFeature[1] + 1, DetectorCascade::imgWidthStep); //We add +1 because the index of the bounding box points to x-1, y-1
				*off++ = sub2idx((scale.width - 1) * currentFeature[2] + 1, (scale.height - 1) * currentFeature[3] + 1, DetectorCascade::imgWidthStep);
			}
		}
	}
}
cv::Mat Field::showPositiveDetectionModel()
{
	return showDetectionModel(truePositives);
}
cv::Mat Field::showNegativeDetectionModel()
{
	return showDetectionModel(falsePositives);
}
Field* FieldList::inField(const cv::Point3d& pos)
{
	for (int i = 0; i < size(); ++i)
	{
		if ((*this)[i].fieldExtended.contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		{
			return &(*this)[i];
		}
	}
	return nullptr;
}
const Field* FieldList::inField(const cv::Point3d& pos) const
{
	for (int i = 0; i < size(); ++i)
	{
		if ((*this)[i].fieldExtended.contains(cv::Point(static_cast<int>(pos.x), static_cast<int>(pos.y))))
		{
			return &(*this)[i];
		}
	}
	return nullptr;
}
Field* FieldList::find(int fid)
{
	for (int i = 0; i < size(); ++i)
	{
		if ((*this)[i].fid == fid)
			return &(*this)[i];
	}
	return nullptr;
}
const Field* FieldList::find(int fid) const
{
	for (int i = 0; i < size(); ++i)
	{
		if ((*this)[i].fid == fid)
			return &(*this)[i];
	}
	return nullptr;
}
void FieldList::addConfidenceIndex(int idx)
{
	for (int i = 0; i < size(); ++i)
	{
		(*this)[i].addConfidenceIndex(idx);
	}
}