#include "stdafx.h"
#include "ColorQuantiziser.h"
#include <unordered_map>

ColorQuantiziser::ColorQuantiziser()
{
	clear();
}

ColorQuantiziser::~ColorQuantiziser()
{
}
void ColorQuantiziser::digest(const cv::Vec3b& color, size_t count)
{
	inputColors.push_back(vectorise(color, count));
	m_colors.push_back(cv::Point3f(static_cast<float>(color[0]) * 1.0f / 255.0f, static_cast<float>(color[1]) * 1.0f / 255.0f, static_cast<float>(color[2]) * 1.0f / 255.0f));
	m_iTotalCounts += count;
}

void ColorQuantiziser::quantize(size_t maxColors, std::vector<cv::Vec3b>& out)
{
	CV_Assert(maxColors > 0);
	if (maxColors > m_colors.size())
	{
		return;
	}

	cv::Mat labels, centers;
	cv::Mat colors(m_colors, false);
	assert(!colors.empty());
	cv::Mat colorsLAB;

	cv::cvtColor(colors, colorsLAB, CV_BGR2Lab);

	colorsLAB = colorsLAB.reshape(1);

#ifndef RELEASE
	cv::setRNGSeed(42);
#endif
	cv::kmeans(colorsLAB, static_cast<int>(maxColors), labels, cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 10, 1.0), 3, cv::KMEANS_PP_CENTERS, centers);

	int* data = reinterpret_cast<int*>(labels.data);
	int len = labels.rows * labels.cols;
	std::vector<int> vec(len);
	std::copy(data + 0, data + len, vec.begin());

	std::unordered_map<int, unsigned int> counts;
	for (unsigned int i = 0; i < vec.size(); ++i)
	{
		std::unordered_map<int, unsigned int>::iterator it(counts.find(vec[i]));
		if (it != counts.end())
		{
			it->second++;
		}
		else
		{
			counts[vec[i]] = 1;
		}
	}

	centers = centers.reshape(3);
	cv::cvtColor(centers, centers, CV_Lab2BGR);
	centers *= 255.0f;
	centers.convertTo(centers, CV_8UC3);

	out.clear();
	for (unsigned int i = 0; i < maxColors; ++i)
	{
		const cv::Vec3b& c = centers.at<cv::Vec3b>(i, 0);
		const cv::Vec3b color(c[0], c[1], c[2]);

		out.push_back(color);

		std::unordered_map<int, unsigned int>::iterator it(counts.find(i));
		outputColors.push_back(vectorise(color, it->second));
	}
	std::sort(out.begin(), out.end(), [](const cv::Vec3b& a, const cv::Vec3b& b)
	{
		if (a[0] < b[0])
			return true;
		if (a[0] > b[0])
			return false;

		if (a[1] < b[1])
			return true;
		if (a[1] > b[1])
			return false;

		return a[2] < b[2];
	});
#ifndef RELEASE
	imgHistI = showInputHist();
	imgHistO = showOutputHist();
#endif
}

void ColorQuantiziser::clear()
{
	inputColors.clear();
	outputColors.clear();
	m_colors.clear();
	m_iTotalCounts = 0;
}
std::vector<float> ColorQuantiziser::vectorise(const cv::Vec3b& color, size_t iCount) const
{
	std::vector<float> v(5);
	v[0] = static_cast<float>(color[0]);
	v[1] = static_cast<float>(color[1]);
	v[2] = static_cast<float>(color[2]);
	v[3] = static_cast<float>(iCount);
	//v[4] = (0.2126f * static_cast<int>(color[0]) + 0.7152f * static_cast<int>(color[1])) + 0.0722f * static_cast<int>(color[2]));//luminance
	//v[4] = (0.299f * static_cast<int>(color[0]) + 0.587f * static_cast<int>(color[1]) + 0.114f * static_cast<int>(color[2]));//contrast
	v[4] = /*sqrt*/(0.299f * static_cast<int>(color[0]) * static_cast<int>(color[0]) + 0.587f * static_cast<int>(color[1])*static_cast<int>(color[1]) + 0.114f * static_cast<int>(color[2]) * static_cast<int>(color[2]));//HSP model
	return v;
}
cv::Mat ColorQuantiziser::showInputHist()
{
	cv::Mat h1 = showHist(inputColors);
	cv::Mat h2 = showSortedHist(inputColors);
	cv::Mat H;
	cv::vconcat(h1, h2, H);
	return H;
}
cv::Mat ColorQuantiziser::showOutputHist()
{
	cv::Mat h1 = showHist(outputColors);
	cv::Mat h2 = showSortedHist(outputColors);
	cv::Mat H;
	cv::vconcat(h1, h2, H);
	return H;
}
cv::Mat ColorQuantiziser::showHist(std::vector<std::vector<float>>& colors)
{
	cv::Mat image(100, 1000, CV_8UC3);
	image.setTo(cv::Scalar(0, 0, 255));
	const double factor = image.cols / static_cast<double>(m_iTotalCounts);

	image.setTo(0);
	double start = 0;
	for (unsigned int i = 0; i< colors.size(); ++i)
	{
		int x = static_cast<int>(start + 0.5);
		start += factor * colors[i][3];
		int width = static_cast<int>(start - x + 0.5);
		if (width > 0)
		{
			cv::Mat roi(image, cv::Rect(x, 0, width, image.rows));
			cv::Scalar c(colors[i][0], colors[i][1], colors[i][2]);
			roi.setTo(c);
		}
	}
	return image;
}
cv::Mat ColorQuantiziser::showSortedHist(std::vector<std::vector<float>>& colors)
{
	std::sort(colors.begin(), colors.end(), [](const std::vector<float>& a, const std::vector<float>& b)
	{
		return a[4] < b[4];
	});
	return showHist(colors);
}
cv::Mat ColorQuantiziser::getColorQuantizisationImage() const
{
	if(!imgHistI.empty() && !imgHistO.empty())
	{
		cv::Mat imgAllColors;
		cv::vconcat(imgHistI, imgHistO, imgAllColors);
		return imgAllColors;
	}
	return cv::Mat();
}
