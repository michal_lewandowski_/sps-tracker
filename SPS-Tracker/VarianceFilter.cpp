#include "VarianceFilter.h"
#include "NormalisedPatch.h"
#include "Utils.h"
#include "Field.h"

VarianceFilter::VarianceFilter()
{
}

VarianceFilter::~VarianceFilter()
{

}
void VarianceFilter::init()
{
	release();
}

void VarianceFilter::release()
{
}
float VarianceFilter::calcVariance(int*win)
{
	return tldCalcVariance(img1, img2, win[0], win[1], win[2], win[3]);
}

float VarianceFilter::calcVariance(cv::Rect* win)
{
	return tldCalcVariance(img1, img2, win->x, win->y, win->width, win->height);
}

bool VarianceFilter::filter(int idx, FieldList& fields)
{
	bool nextStage = false;
	for (auto& f : fields)
	{
		if (idx < f.numWindows)
		{
			int* box = &f.windows[TLD_WINDOW_SIZE * idx];
			float bboxvar = calcVariance(box);
			f.detectionResult.variances[idx] = bboxvar;
			if (bboxvar < f.minVar)
			{
				f.detectionResult.posteriors[idx] = 0;
				f.detectionResult.lastStageInvalid[idx] = 1;
			}
			else
			{
				nextStage = true;
			}
		}
	}
	return nextStage;
}