#include "stdafx.h"
#include "KFTracker2d.h"
#include <memory>
#include "Track.h"
#include "KFBallTrack2d.h"
#include "BallObject.h"
#include "IDFactory.h"
#include "HungarianN3.h"
#include "Worker.h"
#include "PredictedBall.h"

KFTracker2d::KFTracker2d(Worker* w)
	: stateParams(8)
	, mesaureParams(6)
	, maxNumTracks(100)
	, worker(w)
{

}

KFTracker2d::~KFTracker2d()
{
	
}

void KFTracker2d::track(const cv::Mat& frame, const cv::Mat& mask, const std::vector<std::shared_ptr<MovingObject>>& detections, std::deque<std::shared_ptr<KFTrack2d>>& liveTracks, std::deque<std::shared_ptr<KFTrack2d>>& deadTracks)
{
	if (liveTracks.size() == 0)
	{
		for (unsigned int x = 0; x < detections.size(); ++x)
		{
			if(detections[x]->isValid() && std::static_pointer_cast<BallObject>(detections[x])->ballType() != BallType::VISUAL)
			{
				std::shared_ptr<KFTrack2d> tr = createTrack(worker->getCalibration(), detections[x], worker->time(), liveTracks.size(), worker->getImageSize(), worker->getDetectionWindowSize());
				if (tr)
				{
					liveTracks.push_back(tr);
				}
			}
		}
		return;
	}

	cv::Mat costMatrix(static_cast<int>(liveTracks.size()), static_cast<int>(detections.size()), CV_64FC1);
	std::vector<size_t> assignments;
	for (unsigned int y = 0; y < liveTracks.size(); ++y)
	{
		liveTracks[y]->predict(worker->time());
		for (unsigned int x = 0; x < detections.size(); ++x)
		{
			if(detections[x]->isValid() && detections[x]->fieldID() == liveTracks[y]->fieldID() && std::static_pointer_cast<BallObject>(detections[x])->ballType() != BallType::VISUAL)
			{
				//uses estimation from prediction to compute distance
				costMatrix.at<double>(y, x) = liveTracks[y]->getCost(detections[x]);
				//CV_Assert(costMatrix.at<double>(y, x) >= 0.0 && costMatrix.at<double>(y, x) <= 1.0);

				if (costMatrix.at<double>(y, x) > liveTracks[y]->getCostThreshold())
				{
					costMatrix.at<double>(y, x) = liveTracks[y]->getCostMax();
				}
			}
			else
			{
				costMatrix.at<double>(y, x) = liveTracks[y]->getCostMax();
			}
		}
	}
	
	try
	{
		xray::HungarianN3::assignmentSolve(costMatrix, assignments);
	}
	catch (std::exception&)
	{
		for (int i = 0; i < costMatrix.rows; ++i)
		{
			assignments.push_back(xray::HungarianN3::NOT_ASSIGNED());
		}
	}

	
	for (unsigned int y = 0; y < liveTracks.size(); ++y)
	{
		if (assignments[y] == xray::HungarianN3::NOT_ASSIGNED() || costMatrix.at<double>(y, static_cast<int>(assignments[y])) >= liveTracks[y]->getCostMax() || costMatrix.at<double>(y, static_cast<int>(assignments[y])) > liveTracks[y]->getCostThreshold())
		{
			std::shared_ptr<PredictedBall> ball = std::static_pointer_cast<PredictedBall>(liveTracks[y]->missFrame(&worker->getCalibration()));
			ball->setImage(frame, mask);
			if(ball->field()->ballColorMap)
			{
				ball->field()->ballColorMap->classify(ball);
				ball->field()->ballColorMap->assignProbability(ball);
			}
			assignments[y] = xray::HungarianN3::NOT_ASSIGNED();
		}
		else
		{
			CV_Assert(costMatrix.at<double>(y, static_cast<int>(assignments[y])) <= liveTracks[y]->getCostThreshold());
			liveTracks[y]->update2d(&worker->getCalibration(), detections[assignments[y]]);
		}
	}
	for (unsigned int y = 0; y < liveTracks.size(); ++y)
	{
		if(liveTracks[y]->assignProbability())
		{
			liveTracks[y]->field()->ballTrack2d = liveTracks[y];
		}
		if (liveTracks[y]->isDead(worker->getMask(), worker->getFields()))
		{
			liveTracks[y]->setEndTimestamp(worker->time());
			deadTracks.push_back(liveTracks[y]);

			liveTracks.erase(liveTracks.begin() + y);
			y--;
		}
		else
		{
			if (liveTracks[y]->isVisible())
			{
				worker->updateEvidenceCount(liveTracks[y]->getNumDetected());
			}
		}
	}

	std::vector<size_t>::iterator it;
	for (unsigned int x = 0; x < detections.size(); ++x)
	{
		if (detections[x]->isValid() && std::static_pointer_cast<BallObject>(detections[x])->ballType() != BallType::VISUAL)
		{
			it = find(assignments.begin(), assignments.end(), x);
			if (it == assignments.end())
			{
				std::shared_ptr<KFTrack2d> tr = createTrack(worker->getCalibration(), detections[x], worker->time(), liveTracks.size(), worker->getImageSize(), worker->getDetectionWindowSize());
				if (tr)
				{
					liveTracks.push_back(tr);
				}
			}
		}
	}
}
std::shared_ptr<KFTrack2d> KFTracker2d::createTrack(const CameraCalibration& calib, const std::shared_ptr<MovingObject> obj, const int64_t dStartTS, const size_t iNumTracks, const cv::Size imgSize, const int detectionWindow)
{
	if (iNumTracks > maxNumTracks)
	{
		return nullptr;
	}
	Field* field = worker->inField(obj);
	if (!field)
	{
		return nullptr;
	}
	CV_Assert(field->fid == obj->fieldID());
	return std::make_shared<KFBallTrack2d>(IDFactory::generateID(ID_TRACK2D), calib, obj, stateParams, mesaureParams, dStartTS, imgSize, detectionWindow, field->maxAbsVelocityGlobal, field->maxBallProbabilityGlobal);
}