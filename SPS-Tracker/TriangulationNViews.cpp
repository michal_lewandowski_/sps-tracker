#include "stdafx.h"
#include "TriangulationNViews.h"
#include "DetectedBall.h"
#include "PredictedBall.h"
#include "IDFactory.h"
#include "BacktrackBall.h"

extern double ballRadius;

void TriangulationNViews::add(const cv::Point3d& camPos, const cv::Point3d& ballPos)
{
	cv::Matx13d directionVec(ballPos.x - camPos.x, ballPos.y - camPos.y, ballPos.z - camPos.z);
	cv::Matx13d directionVecNorm;
	cv::normalize(directionVec, directionVecNorm, 1.0, 0.0, cv::NORM_L2);

	cv::Matx33d Vi = cv::Matx33d::zeros();
	Vi(0, 1) = -directionVecNorm(2);
	Vi(0, 2) = directionVecNorm(1);
	Vi(1, 0) = directionVecNorm(2);
	Vi(1, 2) = -directionVecNorm(0);
	Vi(2, 0) = -directionVecNorm(1);
	Vi(2, 1) = directionVecNorm(0);

	cv::Matx33d Ri = Vi.t()* Vi;
	cv::Matx31d pi(camPos.x, camPos.y, camPos.z);

	views.push_back(std::make_pair(pi, Ri));
}
cv::Point3d TriangulationNViews::triangulate()
{
	cv::Matx33d A(0, 0, 0, 0, 0, 0, 0, 0, 0);
	cv::Matx31d b(0, 0, 0);
	cv::Matx<double, 1, 1> c(0.0);
	for (int i = 0; i < views.size(); ++i)
	{
		A += views[i].second;
		b += views[i].second * views[i].first;
		c += views[i].first.t() * views[i].second * views[i].first;
	}
	cv::Matx31d position = A.inv() * b;
	cv::Matx<double, 1, 1> e = c - b.t() * A.inv() * b;
	err = e(0, 0);
	err = sqrt(err / views.size());
	return cv::Point3d(position(0), position(1), position(2));
}

double TriangulationNViews::getError()    const
{
	return err;
}

std::vector<std::shared_ptr<MovingObject>> TriangulationNViews::triangulate(std::vector<std::shared_ptr<Worker>>& jobs, FieldList& fields)
{
	std::vector<std::shared_ptr<MovingObject>> results;
	for (auto& f : jobs[0]->getFields())
	{
		std::vector<std::vector<Det2Dinfo>> candidates;
		std::vector<std::vector<Det2Dinfo>> fieldCandsInCam(jobs.size());
		for (int i = 0; i < jobs.size(); ++i)
		{
			f.numObservations[i] = 0;
			for (auto& t : jobs[i]->tr2dlive())
			{
				if (t->isVisible() && f.fieldExtended.contains(cv::Point(cvRound(t->getCurrentDet()->pos3d().x), cvRound(t->getCurrentDet()->pos3d().y))))
				{
					fieldCandsInCam[i].push_back({ (int)t->getTID(), i, jobs[i]->getCamID(), t->getCurrentDet(), jobs[i]->getCalibration().getCameraPosition(), std::vector<PreviousBallLoc>() });
					t->fillPastLocations(fieldCandsInCam[i].back().prevLocation);
					CV_Assert(fieldCandsInCam[i].back().prevLocation.size() == t->showTraceAfter);
				}
			}
		}

		std::sort(fieldCandsInCam.begin(), fieldCandsInCam.end(), [](const std::vector<Det2Dinfo>& l, const std::vector<Det2Dinfo>& r)
		{
			return l.size() > r.size();
		});

		for (auto& fc0 : fieldCandsInCam[0])
		{
			candidates.push_back({ fc0 });
			if (fieldCandsInCam.size() > 1)
			{
				for (auto& fc1 : fieldCandsInCam[1])
				{
					candidates.push_back({ fc1 });
					candidates.push_back({ fc1, fc0 });
					if (fieldCandsInCam.size() > 2)
					{
						for (auto& fc2 : fieldCandsInCam[2])
						{
							candidates.push_back({ fc2 });
							candidates.push_back({ fc2, fc0 });
							candidates.push_back({ fc2, fc1 });
							candidates.push_back({ fc2, fc1, fc0 });
							if (fieldCandsInCam.size() > 3)
							{
								for (auto& fc3 : fieldCandsInCam[3])
								{
									candidates.push_back({ fc3 });
									candidates.push_back({ fc3, fc0 });
									candidates.push_back({ fc3, fc1 });
									candidates.push_back({ fc3, fc2 });
									candidates.push_back({ fc3, fc2, fc1 });
									candidates.push_back({ fc3, fc2, fc0 });
									candidates.push_back({ fc3, fc1, fc0 });
									candidates.push_back({ fc3, fc2, fc1, fc0 });
								}
							}
						}
					}
				}
			}
		}
		for (int i = 0; i < jobs.size(); ++i)
		{
			if (fieldCandsInCam[i].size() > 0)
			{
				f.numObservations[fieldCandsInCam[i][0].camIndex] = static_cast<int>(fieldCandsInCam[i].size());
			}
		}
		std::sort(candidates.begin(), candidates.end(), [](const std::vector<Det2Dinfo>& l, const std::vector<Det2Dinfo>& r)
		{
			return l.size() > r.size();
		});
		std::vector<Det3Dinfo> triangulationCandidates;
		int triangulateSuccess = 0;
		for (int i = 0; i < candidates.size(); ++i)
		{
			auto& c = candidates[i];
			clear();
			cv::Point3d pt3;

			if (static_cast<int>(c.size()) < triangulateSuccess)
			{
				break;
			}

			std::vector<PreviousBallInCam> observations;
			int numDetected = 0;
			if (c.size() > 1)
			{
				for (auto& cc : c)
				{
					add(cc.camPos, cc.bestDet->pos3d());
					observations.push_back({ cc.camPos, cc.camID, cc.prevLocation });
					numDetected += cc.bestDet->isDetected();
				}
				pt3 = triangulate();
			}
			else
			{
				pt3 = c[0].bestDet->pos3d();
				err = 0;
				observations.push_back({ c[0].camPos, c[0].camID, c[0].prevLocation });
				numDetected += c[0].bestDet->isDetected();
			}
#ifndef SPS_DEBUG_TRIANGULATION
			if (getError() <= reconstructionThreshold[static_cast<int>(c.size())] && pt3.z >= 0)
#endif
			{
				triangulationCandidates.push_back(Det3Dinfo(c, getError(), numDetected, static_cast<int>(c.size()), observations, pt3));
#ifdef SPS_DEBUG_TRIANGULATION
				if (getError() <= reconstructionThreshold[static_cast<int>(c.size())] && pt3.z >= 0)
#endif
				{
					triangulateSuccess = static_cast<int>(c.size());
				}
			}
		}

		std::sort(triangulationCandidates.begin(), triangulationCandidates.end(), [](const Det3Dinfo& l, const Det3Dinfo& r)
		{
			if (l.det2d->size() > r.det2d->size())
				return true;
			if (l.det2d->size() < r.det2d->size())
				return false;
			return l.error < r.error;
		});
		std::vector<bool> triangulationCandidatesValid(triangulationCandidates.size(), true);
		for (int i = 0; i < triangulationCandidates.size(); ++i)
		{
			if (triangulationCandidatesValid[i])
			{
#ifdef SPS_DEBUG_TRIANGULATION
				if(triangulationCandidates[i].error <= reconstructionThreshold[triangulationCandidates[i].numCameras] && triangulationCandidates[i].pos3d.z >= 0)
#endif
				{
					for (int j = i + 1; j < triangulationCandidates.size(); ++j)
					{
						if (triangulationCandidatesValid[j])
						{
#ifdef SPS_DEBUG_TRIANGULATION
							if(triangulationCandidates[j].error <= reconstructionThreshold[triangulationCandidates[j].numCameras] && triangulationCandidates[j].pos3d.z >= 0)
#endif
							{
								bool valid = true;
								for (int k = static_cast<int>(triangulationCandidates[i].det2d->size()) - 1; k >= 0; --k)
								{
									int trackID = (*triangulationCandidates[i].det2d)[k].trackID;
									auto foundItem = std::find_if(triangulationCandidates[j].det2d->rbegin(), triangulationCandidates[j].det2d->rend(),
										[&trackID](Det2Dinfo const& d)
									{
										return d.trackID == trackID;
									});
									valid = foundItem == triangulationCandidates[j].det2d->rend();
									if (!valid)
										break;
								}
								triangulationCandidatesValid[j] = valid;
							}
#ifdef SPS_DEBUG_TRIANGULATION
							else
							{
								triangulationCandidatesValid[j] = false;
							}
#endif
						}
					}
				}
#ifdef SPS_DEBUG_TRIANGULATION
				else
				{
					triangulationCandidatesValid[i] = false;
				}
#endif
			}
		}
		for (int j = 0; j < triangulationCandidates.size(); ++j)
		{
#ifdef SPS_DEBUG_TRIANGULATION
			for(auto& d : (*triangulationCandidates[j].det2d))
			{
				std::cout << d.trackID << " [" << d.camID << "] ";
			}
			std::cout << triangulationCandidates[j].error << " -> " << triangulationCandidates[j].pos3d << " -> " << (triangulationCandidatesValid[j]?"pass":"reject") << std::endl;
#endif
			if (triangulationCandidatesValid[j])
			{
				auto& r = triangulationCandidates[j];

				std::unordered_map<std::string, int> tracks2dIDs;
				for(const auto& d2 : *r.det2d)
				{
					tracks2dIDs[d2.camID] = d2.trackID;
				}
				std::shared_ptr<DetectedBall> ball = std::make_shared<DetectedBall>(IDFactory::generateID(ID_DETECTION3D), "", r.pos3d, r.numCameras, r.numDetected, static_cast<float>(r.error), tracks2dIDs, r.observations);
				if (ball)
				{
					CV_Assert(r.numCameras > 0);
					ball->field() = fields.inField(ball->pos3d());

					if (ball->field())
					{
						std::sort(r.det2d->begin(), r.det2d->end(), [](Det2Dinfo& l, Det2Dinfo& r)
						{
							return l.bestDet->getProbability() > r.bestDet->getProbability();
						});
						for (int i = 0; i < std::min(2, r.numCameras); ++i)
						{
						//	ball->assignPropabilityS((*r.det2d)[i].bestDet->getProbability(), "p" + (*r.det2d)[i].camID);
						}
						ball->assignPropabilityS(static_cast<double>(r.numCameras) / static_cast<double>(jobs.size()), "po");

						double errorVariance = 2.0 * reconstructionThreshold[r.numCameras] * reconstructionThreshold[r.numCameras];
						ball->assignPropabilityS(cv::exp(-r.error*r.error / errorVariance), "pe");

						results.push_back(ball);
					}
				}
			}
		}
	}
#ifdef SPS_DEBUG_TRIANGULATION
	std::cout << std::endl;
#endif
	return results;
}
cv::Point3d pt3aa;
void TriangulationNViews::triangulate(std::vector<std::shared_ptr<Worker>>& jobs, std::deque<std::shared_ptr<KFTrack3d>>& tracks)
{
	for (unsigned int y = 0; y < tracks.size(); ++y)
	{
		if (tracks[y]->isVisible() && !tracks[y]->isExtended())
		{
			std::unordered_map<std::size_t, std::unordered_map<std::string, std::tuple<cv::Point3d, double, std::shared_ptr<MovingObject>>>> pastLocs;
			for (int k = 0; k < tracks[y]->showTraceAfter; ++k)
			{
				const auto& det = std::static_pointer_cast<BallObject>(tracks[y]->getDet(k));
				//CV_Assert(det->getPrevLocs().size() >= 1);
				CV_Assert(det->getPrevLocs().size() <= jobs.size());

				for (std::size_t j = 0; j < det->getPrevLocs().size(); ++j)
				{
					CV_Assert(static_cast<int>(det->getPrevLocs()[j].locs.size()) <= tracks[y]->showTraceAfter);
					for (int i = 0; i < det->getPrevLocs()[j].locs.size(); ++i)
					{
						if (static_cast<int64_t>(det->getPrevLocs()[j].locs[i].frameNum) < tracks[y]->getStartTimestamp() && det->getPrevLocs()[j].locs[i].det != nullptr)
						{
							if (k == 0 && j == 0)
							{
								pastLocs.insert({ det->getPrevLocs()[j].locs[i].frameNum, std::unordered_map<std::string, std::tuple<cv::Point3d, double, std::shared_ptr<MovingObject>>>() });
							}
							if (pastLocs[det->getPrevLocs()[j].locs[i].frameNum].find(det->getPrevLocs()[j].camID) == pastLocs[det->getPrevLocs()[j].locs[i].frameNum].end())
							{
								pastLocs[det->getPrevLocs()[j].locs[i].frameNum].insert({ det->getPrevLocs()[j].camID, std::make_tuple(det->getPrevLocs()[j].camPos, det->recError(), det->getPrevLocs()[j].locs[i].det) });
							}
							else if (std::get<1>(pastLocs[det->getPrevLocs()[j].locs[i].frameNum][det->getPrevLocs()[j].camID]) > det->recError())
							{
								pastLocs[det->getPrevLocs()[j].locs[i].frameNum][det->getPrevLocs()[j].camID] = std::make_tuple(det->getPrevLocs()[j].camPos, det->recError(), det->getPrevLocs()[j].locs[i].det);
							}
							CV_Assert(pastLocs[det->getPrevLocs()[j].locs[i].frameNum].size() <= jobs.size());
						}
					}
				}
			}
			for (const auto& locFrame : pastLocs)
			{
				clear();
				cv::Point3d pt3;
				for (const auto& loc : locFrame.second)
				{
					add(std::get<0>(loc.second), std::get<2>(loc.second)->pos3d());
					pt3 = std::get<2>(loc.second)->pos3d();
				}
				if (views.size() > 1)
				{
					pt3 = triangulate();
				}
				else
				{
					err = 0;
				}
				if (getError() <= reconstructionThreshold[static_cast<int>(views.size())])
				{
					std::shared_ptr<MovingObject> ball = std::make_shared<BacktrackBall>(IDFactory::generateID(ID_DETECTION3D), "", pt3, static_cast<int>(views.size()), static_cast<float>(getError()));
					if (ball)
					{
						if (tracks[y]->field()->fieldExtended.contains(cv::Point(static_cast<int>(pt3.x), static_cast<int>(pt3.y))))
						{
							ball->field() = tracks[y]->field();
							if (ball->field())
							{
								CV_Assert(views.size() == locFrame.second.size());
								ball->assignPropabilityS(static_cast<double>(locFrame.second.size()) / static_cast<double>(jobs.size()), "po");

								double errorVariance = 2.0 * reconstructionThreshold[static_cast<int>(locFrame.second.size())] * reconstructionThreshold[static_cast<int>(locFrame.second.size())];
								ball->assignPropabilityS(cv::exp(-getError()*getError() / errorVariance), "pe");

								tracks[y]->extendTrace(ball);
								tracks[y]->assignProbability(ball);
							}
						}
					}
				}
			}
			bool furtherExtend = true;
			for (int k = static_cast<int>(tracks[y]->trace().size()) - 1; k >= 0 ; --k)
			{
				const auto& det = std::static_pointer_cast<BallObject>(tracks[y]->getDet(k));
				if(tracks[y]->field()->isInPlayerZone(det->pos3d()))
				{
					furtherExtend = false;
					break;
				}
			}
			if(furtherExtend)
			{
				auto det1 = std::static_pointer_cast<BallObject>(tracks[y]->getDet(0));
				if(tracks[y]->field()->zoneGame.contains(cv::Point(static_cast<int>(det1->pos3d().x), static_cast<int>(det1->pos3d().y))))
				{
					const auto& det2 = std::static_pointer_cast<BallObject>(tracks[y]->getDet(1));
					std::pair<cv::Point3d, cv::Rect> playerZone = tracks[y]->field()->closestPlayerZone(det1->pos3d());
					cv::Vec3d velocity = det1->pos3d() - det2->pos3d();
					cv::Vec3d direction = playerZone.first - det1->pos3d();
					
					if(velocity.dot(direction) > 0)
					{
						double speed = abs(velocity.dot(direction)) / (cv::norm(direction)*cv::norm(direction));
						cv::Vec3d velocityTowards;

						if (speed>0.0)
						{
							velocityTowards = direction * speed;
						}
						else
						{
							velocityTowards = direction;
						}

						int count = 2 * tracks[y]->showTraceAfter;
						while (count > 0)
						{
							cv::Point3d pt3 = det1->pos3d() + cv::Point3d(velocityTowards);
							if (pt3.z < ballRadius)
								pt3.z = ballRadius;

							if (tracks[y]->field()->field.contains(cv::Point(static_cast<int>(pt3.x), static_cast<int>(pt3.y))))
							{
								std::shared_ptr<BallObject> ball = std::make_shared<BacktrackBall>(IDFactory::generateID(ID_DETECTION3D), "", pt3, 0, 0.0f);
								if (ball)
								{
									ball->field() = tracks[y]->field();
									if (ball->field())
									{
										tracks[y]->extendTrace(ball);
										if (playerZone.second.contains(cv::Point(static_cast<int>(pt3.x), static_cast<int>(pt3.y))))
										{
											break;
										}
										det1 = ball;
									}
								}
							}
							else
							{
								break;
							}
							count--;
						}
					}
				}
			}
			tracks[y]->setExtended();
		}
	}
}
void TriangulationNViews::clear()
{
	views.clear();
	err = -1.0;
}