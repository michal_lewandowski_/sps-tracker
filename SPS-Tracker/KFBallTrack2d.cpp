#include "stdafx.h"
#include "KFBallTrack2d.h"
#include "PredictedBall.h"
#include <boost/lexical_cast.hpp>
#include "IDFactory.h"

extern double ballRadius;

KFBallTrack2d::KFBallTrack2d(const size_t iTrackID, const CameraCalibration& calib, const std::shared_ptr<MovingObject> obj, const int iStateParams, const int iMesaureParams, const int64_t dStartTS, cv::Size imgSize, int detectionWindow, double& maxAbsVelocity, double& maxBallProbability)
	: KFTrack2d(iTrackID, calib, obj, iStateParams, iMesaureParams, dStartTS, maxAbsVelocity, maxBallProbability), detectionUncertainness(detectionWindow, detectionWindow), imageSize(imgSize)
{
	distCostThreshold = 240;
	goodFramesThreshold = 10;
	ballProbailityThr =       -cv::log(0.0001);
	goodNegLogProbThreshold = -cv::log(0.0025);
	computeTresholds();

	setROI(obj);
	assignProbability();

	cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
	file["ballMaxDiameterMM"] >> ballMaxDiameterMM;
	file.release();
}

KFBallTrack2d::KFBallTrack2d(const KFBallTrack2d& other)
	: KFTrack2d(other)
	, m_roi(other.m_roi)
	, imageSize(other.imageSize)
	, detectionUncertainness(other.detectionUncertainness)
{

}
KFBallTrack2d& KFBallTrack2d::operator=(const KFBallTrack2d& other)
{
	if (this != &other)
	{
		this->KFTrack2d::operator=(other);
		this->m_roi = other.m_roi;
		this->imageSize = other.imageSize;
		this->detectionUncertainness = other.detectionUncertainness;
	}
	return *this;
}

KFBallTrack2d::~KFBallTrack2d()
{
}

std::shared_ptr<MovingObject> KFBallTrack2d::missFrame(const CameraCalibration* calib)
{
	m_iMissedFrames++;

	cv::Rect box(cv::Point2d(m_estimation.at<double>(2), m_estimation.at<double>(3)), cv::Point2d(m_estimation.at<double>(4), m_estimation.at<double>(5)));
	box &= cv::Rect(cv::Point(0, 0), calib->getImageSize());

	std::shared_ptr<PredictedBall> bo = std::make_shared<PredictedBall>(IDFactory::generateID(ID_DETECTION2D), getCurrentDet()->camID(), box);
	bo->field() = field();

	double dWidth = box.width * calib->getXunit(static_cast<int>(box.y + box.height), static_cast<int>(box.x + box.width / 2));
	double dHeight = box.height * calib->getYunit(static_cast<int>(box.y + box.height), static_cast<int>(box.x + box.width / 2));
	bo->computeSizeProbability(dWidth, dHeight, ballMaxDiameterMM);

	addTrace(calib, bo);

	return bo;
}
void KFBallTrack2d::addTrace(const CameraCalibration* calib, const std::shared_ptr<MovingObject> obj)
{
	KFTrack2d::addTrace(calib, obj);
	setROI(obj);
}
bool KFBallTrack2d::isDead(const std::vector<cv::Point>& mask, const FieldList& fields)
{
	CV_DbgAssert(field());

	/*if (!verifyVelocity() && m_trace.size() > 0)
	{
		m_trace.erase(--m_trace.end());
		m_dAbsVelocity = getAbsoluteVelocity();
	}*/

	return (m_iMissedFrames == maxMissedFrames)
		|| !verifyVelocity()
		|| m_bKillTrack
		|| !getCurrentDet()->inField(*field())
		|| !getCurrentDet()->inMask(mask)
		|| field()->detectionWindow.area() > 0 && !field()->detectionWindow.contains(getCurrentDet()->pos2d())
		|| getAverageProbability() > getProbabilityThreshold()
		|| getCurrentDet()->getNegLogProbability() > getProbabilityThreshold();
}	
bool KFBallTrack2d::isInROI(cv::Rect& bb) const
{
	return m_roi.contains(bb.tl() + 0.5 * cv::Point(bb.size()));
}
void KFBallTrack2d::setROI(const std::shared_ptr<MovingObject> obj)
{
	m_roi = cv::Rect(obj->pos2d() - detectionUncertainness, obj->pos2d() + detectionUncertainness);
	m_roi = m_roi & cv::Rect(cv::Point(0, 0), imageSize);
}
void KFBallTrack2d::printInfo(const std::vector<cv::Point>& mask, const FieldList& fields) const
{
#ifndef SPS_PRINT_INFO_2D_DISABLE_ISVIS
	if (isVisible())
#endif
	{
		std::cout << getCurrentDet()->camID() << " [" << fieldID() << "] - TI " << m_iTrackID << ": len " << getLength() << " miss " << m_iMissedFrames << " lb " << (isGoodEnoughFor()?1:0) << " (" << (getAverageProbability() <= goodNegLogProbThreshold ? 1 : 0) << ") sp " << m_dAbsVelocity << " (" << std::setprecision(5) << maxRecorderBallSpeedMMperFrame << " " << maxAbsVelocityGlobal << ") ";
		std::cout << "np " << std::setprecision(5) << getNegLogProbability() << " " << getAverageProbability() << " (" << getProbabilityThreshold() << "," << goodNegLogProbThreshold << "," << -cv::log(0.005) << ") ";
	
		if (isNew())
		{
			std::cout << "new track ";
		}
		if (updateFrom3D)
		{
			std::cout << "corrected ";
		}
		if (m_iMissedFrames == maxMissedFrames)
		{
			std::cout << "dead track [missed]";
		}
		else if (m_bKillTrack)
		{
			std::cout << "dead track [killed]";
		}
		else if (!getCurrentDet()->inField(*field()))
		{
			std::cout << "dead track [not in field]";
		}
		else if (!getCurrentDet()->inMask(mask))
		{
			std::cout << "dead track [not in mask]";
		}
		else if (!verifyVelocity())
		{
			std::cout << "dead track [too fast]";
		}
		else if(getAverageProbability() > getProbabilityThreshold())
		{
			std::cout << "dead track [too low prob]";
		}
		else if (getCurrentDet()->getNegLogProbability() > getProbabilityThreshold())
		{
			std::cout << "dead track [too low prob for last one]";
		}
		else if (field()->detectionWindow.area() > 0 && !field()->detectionWindow.contains(getCurrentDet()->pos2d()))
		{
			std::cout << "dead track [out of detection window]";
		}
		//std::cout << " " << getCurrentDet()->pos2d() << " " << getCurrentDet()->pos3d() << " " << getVelocity() << " " << getCurrentDet()->isDetected();
		std::cout << std::endl;
		std::cout << "\t\t" << getCurrentDet()->printInfo2d() << std::endl;
	}
}
void KFBallTrack2d::show(cv::Mat frame, cv::Scalar color) const
{
#ifndef SPS_PRINT_INFO_2D_DISABLE_ISVIS
	if(isVisible())
#endif
	{
		std::deque<std::shared_ptr<MovingObject>>::const_iterator itPrev = m_trace.begin();
		std::deque<std::shared_ptr<MovingObject>>::const_iterator itCurr = itPrev + 1;
		for (; itCurr != m_trace.end(); itPrev = itCurr, ++itCurr)
		{
			cv::line(frame, itPrev->get()->pos2d(), itCurr->get()->pos2d(), itPrev->get()->isDetected() ? color : cv::Scalar(0, 150, 255), 2, CV_AA);
		}
		cv::putText(frame, std::to_string(m_iTrackID), getBox().br() + cv::Point(7, 7), CV_FONT_HERSHEY_PLAIN, 1, color);

		getCurrentDet()->show(frame, color);

		if(updateFrom3D)
		{
			cv::RotatedRect rr(getCurrentDet()->pos2d(), cv::Size2f(getCurrentDet()->box().size()) * 3.0f, 45);
			cv::Point2f vertices[4];
			rr.points(vertices);
			for (int i = 0; i < 4; ++i)
			{
				cv::line(frame, vertices[i], vertices[(i + 1) % 4], cv::Scalar(0,255,0), 1);
			}
		}
	}
}

void KFBallTrack2d::show(cv::Mat frame, double windowScale, cv::Scalar color) const
{
	if (isVisible())
	{
		/*std::deque<std::shared_ptr<MovingObject>>::const_iterator itPrev = m_trace.begin();
		std::deque<std::shared_ptr<MovingObject>>::const_iterator itCurr = itPrev + 1;
		for (; itCurr != m_trace.end(); itPrev = itCurr, ++itCurr)
		{
			cv::line(frame, cv::Point(itPrev->get()->pos3d().x / windowScale, frame.rows - itPrev->get()->pos3d().y / windowScale), cv::Point(itCurr->get()->pos3d().x / windowScale, frame.rows - itCurr->get()->pos3d().y / windowScale), itPrev->get()->isDetected()?color: cv::Scalar(0, 150, 255), 2, CV_AA);
		}
		*/
		getCurrentDet()->show3d(frame, color, windowScale);
		cv::putText(frame, boost::lexical_cast<std::string>(getTID()), cv::Point(cvRound(m_trace.back()->pos3dEst().x / windowScale), cvRound(frame.rows - m_trace.back()->pos3dEst().y / windowScale)) + cv::Point(20, -20), CV_FONT_HERSHEY_PLAIN, 1, color);
	}
}
void KFBallTrack2d::show(cv::Mat frame, const CameraCalibration* calib, cv::Scalar color) const
{

}
bool KFBallTrack2d::assignProbability()
{
	assignProbability(getCurrentDet());

	if (getCurrentDet()->getNegLogProbability() < maxBallProbabilityGlobal)
	{
		maxBallProbabilityGlobal = getCurrentDet()->getNegLogProbability();
		return true;
	}
	return false;
}
void KFBallTrack2d::assignProbability(std::shared_ptr<MovingObject> obj)
{
	CV_Assert(obj);
	CV_Assert(obj->getNegLogProbability() >= 0.0);
	CV_Assert(obj->countProb() > 0);

	//velocity probability
	if (getLength() > 1)
	{
		if(m_dAbsVelocity > maxAbsVelocityGlobal)
		{
			m_dAbsVelocity = maxAbsVelocityGlobal;
		}
		obj->assignPropabilityD(m_dAbsVelocity / maxAbsVelocityGlobal, "pv");
	}
		

	//history probability
	obj->assignPropabilityD(1 - cv::exp(-probImportanceMap["pl"] * getLength()), "pl");
	//loosing evidence probability
	obj->assignPropabilityD(cv::exp(-probImportanceMap["pm"] * m_iMissedFrames), "pm");

	probability.value *= obj->getProbability();
	probability.log += -obj->getNegLogProbability();
	probability.count++;
}
void KFBallTrack2d::removeProbability(std::shared_ptr<MovingObject> obj)
{
	CV_Assert(obj);
	CV_Assert(obj->getNegLogProbability() >= 0.0);
	CV_Assert(obj->countProb() > 0);

	probability.value /= obj->getProbability();
	probability.log -= -obj->getNegLogProbability();
	probability.count--;

	obj->removePropabilityD("pv");
	obj->removePropabilityD("pl");
	obj->removePropabilityD("pm");
}
bool KFBallTrack2d::isGoodEnoughFor() const
{
	return  isVisible()
			&& getMissedFrames() == 0
			&& getNumDetected() > goodFramesThreshold
			&& getCurrentDet()->isDetected()
			&& getAverageProbability() < goodNegLogProbThreshold;
}