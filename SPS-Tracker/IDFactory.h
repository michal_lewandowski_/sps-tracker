#pragma once

#include <opencv2/opencv.hpp>
#include <map>

#define ID_DETECTION2D 1
#define ID_DETECTION3D 2
#define ID_TRACK2D 3
#define ID_TRACK3D 4


class IDFactory
{
public:
	static size_t generateID(int key);
	static void reset(void);

private:
	static std::map<int, size_t> s_nextIDs;
	static const size_t s_maxID2d;
	static const size_t s_maxID3d;
};
