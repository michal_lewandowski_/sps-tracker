#include "BgsPAWCS.h"
#include "Utils.h"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <iomanip>

#define FEEDBACK_R_VAR (0.01f)
#define FEEDBACK_V_INCR  (1.000f)
#define FEEDBACK_V_DECR  (0.100f)
#define FEEDBACK_T_DECR  (0.2500f)
#define FEEDBACK_T_INCR  (0.5000f)
#define FEEDBACK_T_LOWER (1.0000f)
#define FEEDBACK_T_UPPER (256.00f)
#define UNSTABLE_REG_RATIO_MIN (0.100f)
#define UNSTABLE_REG_RDIST_MIN (3.000f)
#define LBSPDESC_RATIO_MIN (0.100f)
#define LBSPDESC_RATIO_MAX (0.500f)
#define FRAMELEVEL_MIN_L1DIST_THRES (45)
#define FRAMELEVEL_MIN_CDIST_THRES (FRAMELEVEL_MIN_L1DIST_THRES/10)
#define FRAMELEVEL_DOWNSAMPLE_RATIO (8)
#define GWORD_LOOKUP_MAPS_DOWNSAMPLE_RATIO (2)
#define GWORD_DEFAULT_NB_INIT_SAMPL_PASSES (2)
#define GWORD_DESC_THRES_BITS_MATCH_FACTOR (4)

#define DEFAULT_FRAME_SIZE cv::Size(320,240)
#define DEFAULT_RESAMPLING_RATE (16)
#define DEFAULT_BOOTSTRAP_WIN_SIZE (500)
#define DEFAULT_LWORD_WEIGHT_OFFSET (DEFAULT_BOOTSTRAP_WIN_SIZE*2)
#define DEFAULT_LWORD_OCC_INCR 1
#define DEFAULT_LWORD_MAX_WEIGHT (1.0f)
#define DEFAULT_LWORD_INIT_WEIGHT (1.0f/nLocalWordWeightOffset)
#define UNSTAB_DESC_DIST_OFFSET (nDescDistThresholdOffset)
#define FLAT_REGION_BIT_COUNT (s_nDescMaxDataRange_1ch/8)
#define DEFAULT_MEDIAN_BLUR_KERNEL_SIZE (9)

static const size_t s_nColorMaxDataRange_1ch = UCHAR_MAX;
static const size_t s_nDescMaxDataRange_1ch = LBSP::DESC_SIZE * 8;
static const size_t s_nColorMaxDataRange_3ch = s_nColorMaxDataRange_1ch * 3;
static const size_t s_nDescMaxDataRange_3ch = s_nDescMaxDataRange_1ch * 3;

BgsPAWCS::BgsPAWCS(float fRelLBSPThreshold_
	, size_t nDescDistThresholdOffset_
	, size_t nMinColorDistThreshold_
	, size_t nMaxNbWords_
	, size_t nSamplesForMovingAvgs_)
	: nMinColorDistThreshold(nMinColorDistThreshold_)
	, nDescDistThresholdOffset(nDescDistThresholdOffset_)
	, nMaxLocalWords(nMaxNbWords_)
	, nCurrLocalWords(0)
	, nMaxGlobalWords(nMaxNbWords_ / 2)
	, nCurrGlobalWords(0)
	, nSamplesForMovingAvgs(nSamplesForMovingAvgs_)
	, fLastNonFlatRegionRatio(0.0f)
	, nMedianBlurKernelSize(fRelLBSPThreshold_)
	, nDownSampledROIPxCount(0)
	, nLocalWordWeightOffset(DEFAULT_LWORD_WEIGHT_OFFSET)
	, apLocalWordDict(nullptr)
	, aLocalWordList_1ch(nullptr)
	, pLocalWordListIter_1ch(nullptr)
	, aLocalWordList_3ch(nullptr)
	, pLocalWordListIter_3ch(nullptr)
	, apGlobalWordDict(nullptr)
	, aGlobalWordList_1ch(nullptr)
	, pGlobalWordListIter_1ch(nullptr)
	, aGlobalWordList_3ch(nullptr)
	, pGlobalWordListIter_3ch(nullptr)
	, aPxInfoLUT_PAWCS(nullptr)
	, nImgType(0)
	, nLBSPThresholdOffset(fRelLBSPThreshold_)
	, fRelLBSPThreshold(fRelLBSPThreshold_)
	, nTotPxCount(0)
	, nTotRelevantPxCount(0)
	, nFrameIndex(SIZE_MAX)
	, nFramesSinceLastReset(0)
	, nModelResetCooldown(0)
	, aPxIdxLUT(nullptr)
	, aPxInfoLUT(nullptr)
	, nDefaultMedianBlurKernelSize(DEFAULT_MEDIAN_BLUR_KERNEL_SIZE)
	, bInitialized(false)
	, bAutoModelResetEnabled(true)
	, bUsingMovingCamera(false)
{
	CV_Assert(fRelLBSPThreshold >= 0);
	CV_Assert(nMaxLocalWords > 0 && nMaxGlobalWords > 0);
}

BgsPAWCS::~BgsPAWCS()
{
	CleanupDictionaries();
}

void BgsPAWCS::init(const cv::Mat& oInitImg, const cv::Mat& oROI_)
{
	CV_Assert(oInitImg.channels() == 1);
	CV_Assert(!oInitImg.empty() && oInitImg.cols > 0 && oInitImg.rows > 0);
	CV_Assert(oInitImg.isContinuous());
	CV_Assert(oInitImg.type() == CV_8UC3 || oInitImg.type() == CV_8UC1);

	int closingSize = 2;
	closingElement = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(closingSize * 2 + 1, closingSize * 2 + 1), cv::Point(closingSize, closingSize));

	if (oInitImg.type() == CV_8UC3)
	{
		std::vector<cv::Mat> voInitImgChannels;
		cv::split(oInitImg, voInitImgChannels);
		if (!cv::countNonZero((voInitImgChannels[0] != voInitImgChannels[1]) | (voInitImgChannels[2] != voInitImgChannels[1])))
			std::cout << std::endl << "\tBgsPAWCS : Warning, grayscale images should always be passed in CV_8UC1 format for optimal performance." << std::endl;
	}
	cv::Mat oNewBGROI;
	if (oROI_.empty() && (oROI.empty() || oROI_.size() != oInitImg.size()))
	{
		oNewBGROI.create(oInitImg.size(), CV_8UC1);
		oNewBGROI = cv::Scalar_<uchar>(UCHAR_MAX);
	}
	else if (oROI_.empty())
		oNewBGROI = oROI;
	else
	{
		CV_Assert(oROI_.size() == oInitImg.size() && oROI_.type() == CV_8UC1);
		CV_Assert(cv::countNonZero((oROI_ < UCHAR_MAX)&(oROI_ > 0)) == 0);
		cv::threshold(oROI_, oNewBGROI, 125, 255, CV_THRESH_BINARY_INV);
		cv::Mat oTempROI;
		cv::dilate(oNewBGROI, oTempROI, cv::Mat(), cv::Point(-1, -1), LBSP::PATCH_SIZE / 2);
		cv::bitwise_or(oNewBGROI, oTempROI / 2, oNewBGROI);
	}
	const size_t nOrigROIPxCount = (size_t)cv::countNonZero(oNewBGROI);
	CV_Assert(nOrigROIPxCount > 0);
	LBSP::validateROI(oNewBGROI);
	const size_t nFinalROIPxCount = (size_t)cv::countNonZero(oNewBGROI);
	CV_Assert(nFinalROIPxCount > 0);
	CleanupDictionaries();
	oROI = oNewBGROI;
	oImgSize = oInitImg.size();
	nImgType = oInitImg.type();
	nImgChannels = oInitImg.channels();
	nTotPxCount = oImgSize.area();
	nTotRelevantPxCount = nFinalROIPxCount;
	nFrameIndex = 0;
	nFramesSinceLastReset = 0;
	nModelResetCooldown = 0;
	bUsingMovingCamera = false;
	oDownSampledFrameSize_MotionAnalysis = cv::Size(oImgSize.width / FRAMELEVEL_DOWNSAMPLE_RATIO, oImgSize.height / FRAMELEVEL_DOWNSAMPLE_RATIO);
	oDownSampledFrameSize_GlobalWordLookup = cv::Size(oImgSize.width / GWORD_LOOKUP_MAPS_DOWNSAMPLE_RATIO, oImgSize.height / GWORD_LOOKUP_MAPS_DOWNSAMPLE_RATIO);
	cv::resize(oROI, oDownSampledROI_MotionAnalysis, oDownSampledFrameSize_MotionAnalysis, 0, 0, cv::INTER_AREA);
	fLastNonFlatRegionRatio = 0.0f;
	nCurrLocalWords = nMaxLocalWords;
	if (nOrigROIPxCount >= nTotPxCount / 2 && (int)nTotPxCount >= DEFAULT_FRAME_SIZE.area())
	{
		const float fRegionSizeScaleFactor = (float)nTotPxCount / DEFAULT_FRAME_SIZE.area();
		const int nRawMedianBlurKernelSize = std::min((int)floor(0.5f + fRegionSizeScaleFactor) + nDefaultMedianBlurKernelSize, nDefaultMedianBlurKernelSize + 4);
		nMedianBlurKernelSize = (nRawMedianBlurKernelSize % 2) ? nRawMedianBlurKernelSize : nRawMedianBlurKernelSize - 1;
		nCurrGlobalWords = nMaxGlobalWords;
		oDownSampledROI_MotionAnalysis |= UCHAR_MAX / 2;
	}
	else
	{
		const float fRegionSizeScaleFactor = (float)nOrigROIPxCount / DEFAULT_FRAME_SIZE.area();
		const int nRawMedianBlurKernelSize = std::min((int)floor(0.5f + nDefaultMedianBlurKernelSize*fRegionSizeScaleFactor * 2) + (nDefaultMedianBlurKernelSize - 4), nDefaultMedianBlurKernelSize);
		nMedianBlurKernelSize = (nRawMedianBlurKernelSize % 2) ? nRawMedianBlurKernelSize : nRawMedianBlurKernelSize - 1;
		nCurrGlobalWords = std::min((size_t)std::pow(nMaxGlobalWords*fRegionSizeScaleFactor, 2) + 1, nMaxGlobalWords);
	}
	if (nImgChannels == 1)
	{
		nCurrLocalWords = std::max(nCurrLocalWords / 2, (size_t)1);
		nCurrGlobalWords = std::max(nCurrGlobalWords / 2, (size_t)1);
	}
	nDownSampledROIPxCount = (size_t)cv::countNonZero(oDownSampledROI_MotionAnalysis);
	nLocalWordWeightOffset = DEFAULT_LWORD_WEIGHT_OFFSET;
	oIllumUpdtRegionMask.create(oImgSize, CV_8UC1);
	oIllumUpdtRegionMask = cv::Scalar_<uchar>(0);
	oUpdateRateFrame.create(oImgSize, CV_32FC1);
	oUpdateRateFrame = cv::Scalar(FEEDBACK_T_LOWER);
	oDistThresholdFrame.create(oImgSize, CV_32FC1);
	oDistThresholdFrame = cv::Scalar(2.0f);
	oDistThresholdVariationFrame.create(oImgSize, CV_32FC1);
	oDistThresholdVariationFrame = cv::Scalar(FEEDBACK_V_INCR * 10);
	oMeanMinDistFrame_LT.create(oImgSize, CV_32FC1);
	oMeanMinDistFrame_LT = cv::Scalar(0.0f);
	oMeanMinDistFrame_ST.create(oImgSize, CV_32FC1);
	oMeanMinDistFrame_ST = cv::Scalar(0.0f);
	oMeanDownSampledLastDistFrame_LT.create(oDownSampledFrameSize_MotionAnalysis, CV_32FC((int)nImgChannels));
	oMeanDownSampledLastDistFrame_LT = cv::Scalar(0.0f);
	oMeanDownSampledLastDistFrame_ST.create(oDownSampledFrameSize_MotionAnalysis, CV_32FC((int)nImgChannels));
	oMeanDownSampledLastDistFrame_ST = cv::Scalar(0.0f);
	oMeanRawSegmResFrame_LT.create(oImgSize, CV_32FC1);
	oMeanRawSegmResFrame_LT = cv::Scalar(0.0f);
	oMeanRawSegmResFrame_ST.create(oImgSize, CV_32FC1);
	oMeanRawSegmResFrame_ST = cv::Scalar(0.0f);
	oMeanFinalSegmResFrame_LT.create(oImgSize, CV_32FC1);
	oMeanFinalSegmResFrame_LT = cv::Scalar(0.0f);
	oMeanFinalSegmResFrame_ST.create(oImgSize, CV_32FC1);
	oMeanFinalSegmResFrame_ST = cv::Scalar(0.0f);
	oUnstableRegionMask.create(oImgSize, CV_8UC1);
	oUnstableRegionMask = cv::Scalar_<uchar>(0);
	oBlinksFrame.create(oImgSize, CV_8UC1);
	oBlinksFrame = cv::Scalar_<uchar>(0);
	oDownSampledFrame_MotionAnalysis.create(oDownSampledFrameSize_MotionAnalysis, CV_8UC((int)nImgChannels));
	oDownSampledFrame_MotionAnalysis = cv::Scalar_<uchar>::all(0);
	oLastColorFrame.create(oImgSize, CV_8UC((int)nImgChannels));
	oLastColorFrame = cv::Scalar_<uchar>::all(0);
	oLastDescFrame.create(oImgSize, CV_16UC((int)nImgChannels));
	oLastDescFrame = cv::Scalar_<ushort>::all(0);
	oLastRawFGMask.create(oImgSize, CV_8UC1);
	oLastRawFGMask = cv::Scalar_<uchar>(0);
	oLastFGMask.create(oImgSize, CV_8UC1);
	oLastFGMask = cv::Scalar_<uchar>(0);
	oLastFGMask_dilated.create(oImgSize, CV_8UC1);
	oLastFGMask_dilated = cv::Scalar_<uchar>(0);
	oLastFGMask_dilated_inverted.create(oImgSize, CV_8UC1);
	oLastFGMask_dilated_inverted = cv::Scalar_<uchar>(0);
	oFGMask_FloodedHoles.create(oImgSize, CV_8UC1);
	oFGMask_FloodedHoles = cv::Scalar_<uchar>(0);
	oFGMask_PreFlood.create(oImgSize, CV_8UC1);
	oFGMask_PreFlood = cv::Scalar_<uchar>(0);
	oCurrRawFGBlinkMask.create(oImgSize, CV_8UC1);
	oCurrRawFGBlinkMask = cv::Scalar_<uchar>(0);
	oLastRawFGBlinkMask.create(oImgSize, CV_8UC1);
	oLastRawFGBlinkMask = cv::Scalar_<uchar>(0);
	oTempGlobalWordWeightDiffFactor.create(oDownSampledFrameSize_GlobalWordLookup, CV_32FC1);
	oTempGlobalWordWeightDiffFactor = cv::Scalar(-0.1f);
	oMorphExStructElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
	aPxIdxLUT = new size_t[nTotRelevantPxCount];
	memset(aPxIdxLUT, 0, sizeof(size_t)*nTotRelevantPxCount);
	aPxInfoLUT_PAWCS = new PxInfo_PAWCS[nTotPxCount];
	memset(aPxInfoLUT_PAWCS, 0, sizeof(PxInfo_PAWCS)*nTotPxCount);
	aPxInfoLUT = aPxInfoLUT_PAWCS;
	apLocalWordDict = new LocalWordBase*[nTotRelevantPxCount*nCurrLocalWords];
	memset(apLocalWordDict, 0, sizeof(LocalWordBase*)*nTotRelevantPxCount*nCurrLocalWords);
	apGlobalWordDict = new GlobalWordBase*[nCurrGlobalWords];
	memset(apGlobalWordDict, 0, sizeof(GlobalWordBase*)*nCurrGlobalWords);
	if (nImgChannels == 1)
	{
		CV_DbgAssert(oLastColorFrame.step.p[0] == (size_t)oImgSize.width && oLastColorFrame.step.p[1] == 1);
		CV_DbgAssert(oLastDescFrame.step.p[0] == oLastColorFrame.step.p[0] * 2 && oLastDescFrame.step.p[1] == oLastColorFrame.step.p[1] * 2);
		aLocalWordList_1ch = new LocalWord_1ch[nTotRelevantPxCount*nCurrLocalWords];
		memset(aLocalWordList_1ch, 0, sizeof(LocalWord_1ch)*nTotRelevantPxCount*nCurrLocalWords);
		pLocalWordListIter_1ch = aLocalWordList_1ch;
		aGlobalWordList_1ch = new GlobalWord_1ch[nCurrGlobalWords];
		pGlobalWordListIter_1ch = aGlobalWordList_1ch;
		for (size_t t = 0; t <= UCHAR_MAX; ++t)
			anLBSPThreshold_8bitLUT[t] = cv::saturate_cast<uchar>((nLBSPThresholdOffset + t*fRelLBSPThreshold) / 3);
		for (size_t nPxIter = 0, nModelIter = 0; nPxIter < nTotPxCount; ++nPxIter)
		{
			if (oROI.data[nPxIter])
			{
				aPxIdxLUT[nModelIter] = nPxIter;
				aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y = (int)nPxIter / oImgSize.width;
				aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X = (int)nPxIter%oImgSize.width;
				aPxInfoLUT_PAWCS[nPxIter].nModelIdx = nModelIter;
				aPxInfoLUT_PAWCS[nPxIter].nGlobalWordMapLookupIdx = (size_t)((aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y / GWORD_LOOKUP_MAPS_DOWNSAMPLE_RATIO)*oDownSampledFrameSize_GlobalWordLookup.width + (aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X / GWORD_LOOKUP_MAPS_DOWNSAMPLE_RATIO)) * 4;
				aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT = new GlobalWordBase*[nCurrGlobalWords];
				for (size_t nGlobalWordIdxIter = 0; nGlobalWordIdxIter < nCurrGlobalWords; ++nGlobalWordIdxIter)
					aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordIdxIter] = &(aGlobalWordList_1ch[nGlobalWordIdxIter]);
				oLastColorFrame.data[nPxIter] = oInitImg.data[nPxIter];
				const size_t nDescIter = nPxIter * 2;
				LBSP::computeGrayscaleDescriptor(oInitImg, oInitImg.data[nPxIter], aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X, aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y, anLBSPThreshold_8bitLUT[oInitImg.data[nPxIter]], *((ushort*)(oLastDescFrame.data + nDescIter)));
				++nModelIter;
			}
		}
	}
	else
	{ //nImgChannels==3
		CV_DbgAssert(oLastColorFrame.step.p[0] == (size_t)oImgSize.width * 3 && oLastColorFrame.step.p[1] == 3);
		CV_DbgAssert(oLastDescFrame.step.p[0] == oLastColorFrame.step.p[0] * 2 && oLastDescFrame.step.p[1] == oLastColorFrame.step.p[1] * 2);
		aLocalWordList_3ch = new LocalWord_3ch[nTotRelevantPxCount*nCurrLocalWords];
		memset(aLocalWordList_3ch, 0, sizeof(LocalWord_3ch)*nTotRelevantPxCount*nCurrLocalWords);
		pLocalWordListIter_3ch = aLocalWordList_3ch;
		aGlobalWordList_3ch = new GlobalWord_3ch[nCurrGlobalWords];
		pGlobalWordListIter_3ch = aGlobalWordList_3ch;
		for (size_t t = 0; t <= UCHAR_MAX; ++t)
			anLBSPThreshold_8bitLUT[t] = cv::saturate_cast<uchar>(nLBSPThresholdOffset + t*fRelLBSPThreshold);
		for (size_t nPxIter = 0, nModelIter = 0; nPxIter < nTotPxCount; ++nPxIter)
		{
			if (oROI.data[nPxIter])
			{
				aPxIdxLUT[nModelIter] = nPxIter;
				aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y = (int)nPxIter / oImgSize.width;
				aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X = (int)nPxIter%oImgSize.width;
				aPxInfoLUT_PAWCS[nPxIter].nModelIdx = nModelIter;
				aPxInfoLUT_PAWCS[nPxIter].nGlobalWordMapLookupIdx = (size_t)((aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y / GWORD_LOOKUP_MAPS_DOWNSAMPLE_RATIO)*oDownSampledFrameSize_GlobalWordLookup.width + (aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X / GWORD_LOOKUP_MAPS_DOWNSAMPLE_RATIO)) * 4;
				aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT = new GlobalWordBase*[nCurrGlobalWords];
				for (size_t nGlobalWordIdxIter = 0; nGlobalWordIdxIter < nCurrGlobalWords; ++nGlobalWordIdxIter)
					aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordIdxIter] = &(aGlobalWordList_3ch[nGlobalWordIdxIter]);
				const size_t nPxRGBIter = nPxIter * 3;
				const size_t nDescRGBIter = nPxRGBIter * 2;
				for (size_t c = 0; c < 3; ++c)
				{
					oLastColorFrame.data[nPxRGBIter + c] = oInitImg.data[nPxRGBIter + c];
					LBSP::computeSingleRGBDescriptor(oInitImg, oInitImg.data[nPxRGBIter + c], aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X, aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y, c, anLBSPThreshold_8bitLUT[oInitImg.data[nPxRGBIter + c]], ((ushort*)(oLastDescFrame.data + nDescRGBIter))[c]);
				}
				++nModelIter;
			}
		}
	}
	bInitialized = true;
	refreshModel(1, 0);
}

void BgsPAWCS::preprocess(const cv::Mat& iframe, const cv::Size& size, cv::Mat& pframe, int code)
{
	cv::Mat frameBGR;
	cv::demosaicing(iframe, frameBGR, code);
	if (frameBGR.size() != size)
	{
		cv::resize(frameBGR, pframe, size, 0.0, 0.0, cv::INTER_LINEAR);
	}
	else
	{
		pframe = frameBGR;
	}
}

void BgsPAWCS::refreshModel(size_t nBaseOccCount, float fOccDecrFrac, bool bForceFGUpdate)
{
	// == refresh
	CV_Assert(bInitialized);
	CV_Assert(fOccDecrFrac >= 0.0f && fOccDecrFrac <= 1.0f);
	if (nImgChannels == 1)
	{
		for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			if (bForceFGUpdate || !oLastFGMask_dilated.data[nPxIter])
			{
				const size_t nLocalDictIdx = nModelIter*nCurrLocalWords;
				const size_t nFloatIter = nPxIter * 4;
				uchar& bCurrRegionIsUnstable = oUnstableRegionMask.data[nPxIter];
				const float fCurrDistThresholdFactor = *(float*)(oDistThresholdFrame.data + nFloatIter);
				const size_t nCurrColorDistThreshold = (size_t)(sqrt(fCurrDistThresholdFactor)*nMinColorDistThreshold) / 2;
				const size_t nCurrDescDistThreshold = ((size_t)1 << ((size_t)floor(fCurrDistThresholdFactor + 0.5f))) + nDescDistThresholdOffset + (bCurrRegionIsUnstable*UNSTAB_DESC_DIST_OFFSET);
				// == refresh: local decr
				if (fOccDecrFrac > 0.0f)
				{
					for (size_t nLocalWordIdx = 0; nLocalWordIdx < nCurrLocalWords; ++nLocalWordIdx)
					{
						LocalWord_1ch* pCurrLocalWord = (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
						if (pCurrLocalWord)
							pCurrLocalWord->nOccurrences -= (size_t)(fOccDecrFrac*pCurrLocalWord->nOccurrences);
					}
				}
				const size_t nCurrWordOccIncr = DEFAULT_LWORD_OCC_INCR;
				const size_t nTotLocalSamplingIterCount = (s_nSamplesInitPatternWidth*s_nSamplesInitPatternHeight) * 2;
				for (size_t nLocalSamplingIter = 0; nLocalSamplingIter < nTotLocalSamplingIterCount; ++nLocalSamplingIter)
				{
					// == refresh: local resampling
					int nSampleImgCoord_Y, nSampleImgCoord_X;
					getRandSamplePosition(nSampleImgCoord_X, nSampleImgCoord_Y, aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X, aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
					const size_t nSamplePxIdx = oImgSize.width*nSampleImgCoord_Y + nSampleImgCoord_X;
					if (bForceFGUpdate || !oLastFGMask_dilated.data[nSamplePxIdx])
					{
						const uchar nSampleColor = oLastColorFrame.data[nSamplePxIdx];
						const size_t nSampleDescIdx = nSamplePxIdx * 2;
						const ushort nSampleIntraDesc = *((ushort*)(oLastDescFrame.data + nSampleDescIdx));
						bool bFoundUninitd = false;
						size_t nLocalWordIdx;
						for (nLocalWordIdx = 0; nLocalWordIdx < nCurrLocalWords; ++nLocalWordIdx)
						{
							LocalWord_1ch* pCurrLocalWord = (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
							if (pCurrLocalWord
								&& L1dist(nSampleColor, pCurrLocalWord->oFeature.anColor[0]) <= nCurrColorDistThreshold
								&& hdist(nSampleIntraDesc, pCurrLocalWord->oFeature.anDesc[0]) <= nCurrDescDistThreshold)
							{
								pCurrLocalWord->nOccurrences += nCurrWordOccIncr;
								pCurrLocalWord->nLastOcc = nFrameIndex;
								break;
							}
							else if (!pCurrLocalWord)
								bFoundUninitd = true;
						}
						if (nLocalWordIdx == nCurrLocalWords)
						{
							nLocalWordIdx = nCurrLocalWords - 1;
							LocalWord_1ch* pCurrLocalWord = bFoundUninitd ? pLocalWordListIter_1ch++ : (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
							pCurrLocalWord->oFeature.anColor[0] = nSampleColor;
							pCurrLocalWord->oFeature.anDesc[0] = nSampleIntraDesc;
							pCurrLocalWord->nOccurrences = nBaseOccCount;
							pCurrLocalWord->nFirstOcc = nFrameIndex;
							pCurrLocalWord->nLastOcc = nFrameIndex;
							apLocalWordDict[nLocalDictIdx + nLocalWordIdx] = pCurrLocalWord;
						}
						while (nLocalWordIdx > 0 && (!apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1] || GetLocalWordWeight(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], nFrameIndex, nLocalWordWeightOffset) > GetLocalWordWeight(apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1], nFrameIndex, nLocalWordWeightOffset)))
						{
							std::swap(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1]);
							--nLocalWordIdx;
						}
					}
				}
				CV_Assert(apLocalWordDict[nLocalDictIdx]);
				for (size_t nLocalWordIdx = 1; nLocalWordIdx < nCurrLocalWords; ++nLocalWordIdx)
				{
					// == refresh: local random resampling
					LocalWord_1ch* pCurrLocalWord = (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
					if (!pCurrLocalWord)
					{
						const size_t nRandLocalWordIdx = (rand() % nLocalWordIdx);
						const LocalWord_1ch* pRefLocalWord = (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx + nRandLocalWordIdx];
						const int nRandColorOffset = (rand() % (nCurrColorDistThreshold + 1)) - (int)nCurrColorDistThreshold / 2;
						pCurrLocalWord = pLocalWordListIter_1ch++;
						pCurrLocalWord->oFeature.anColor[0] = cv::saturate_cast<uchar>((int)pRefLocalWord->oFeature.anColor[0] + nRandColorOffset);
						pCurrLocalWord->oFeature.anDesc[0] = pRefLocalWord->oFeature.anDesc[0];
						pCurrLocalWord->nOccurrences = std::max((size_t)(pRefLocalWord->nOccurrences*((float)(nCurrLocalWords - nLocalWordIdx) / nCurrLocalWords)), (size_t)1);
						pCurrLocalWord->nFirstOcc = nFrameIndex;
						pCurrLocalWord->nLastOcc = nFrameIndex;
						apLocalWordDict[nLocalDictIdx + nLocalWordIdx] = pCurrLocalWord;
					}
				}
			}
		}
		CV_Assert(aLocalWordList_1ch == (pLocalWordListIter_1ch - nTotRelevantPxCount*nCurrLocalWords));
		cv::Mat oGlobalDictPresenceLookupMap(oImgSize, CV_8UC1, cv::Scalar_<uchar>(0));
		size_t nPxIterIncr = std::max(nTotPxCount / nCurrGlobalWords, (size_t)1);
		for (size_t nSamplingPasses = 0; nSamplingPasses < GWORD_DEFAULT_NB_INIT_SAMPL_PASSES; ++nSamplingPasses)
		{
			for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
			{
				// == refresh: global resampling
				const size_t nPxIter = aPxIdxLUT[nModelIter];
				if ((nPxIter%nPxIterIncr) == 0)
				{ // <=(nCurrGlobalWords) gwords from (nCurrGlobalWords) equally spaced pixels
					if (bForceFGUpdate || !oLastFGMask_dilated.data[nPxIter])
					{
						const size_t nLocalDictIdx = nModelIter*nCurrLocalWords;
						const size_t nGlobalWordMapLookupIdx = aPxInfoLUT_PAWCS[nPxIter].nGlobalWordMapLookupIdx;
						const size_t nFloatIter = nPxIter * 4;
						uchar& bCurrRegionIsUnstable = oUnstableRegionMask.data[nPxIter];
						const float fCurrDistThresholdFactor = *(float*)(oDistThresholdFrame.data + nFloatIter);
						const size_t nCurrColorDistThreshold = (size_t)(sqrt(fCurrDistThresholdFactor)*nMinColorDistThreshold) / 2;
						const size_t nCurrDescDistThreshold = ((size_t)1 << ((size_t)floor(fCurrDistThresholdFactor + 0.5f))) + nDescDistThresholdOffset + (bCurrRegionIsUnstable*UNSTAB_DESC_DIST_OFFSET);
						CV_Assert(apLocalWordDict[nLocalDictIdx]);
						const LocalWord_1ch* pRefBestLocalWord = (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx];
						const float fRefBestLocalWordWeight = GetLocalWordWeight(pRefBestLocalWord, nFrameIndex, nLocalWordWeightOffset);
						const uchar nRefBestLocalWordDescBITS = (uchar)popcount(pRefBestLocalWord->oFeature.anDesc[0]);
						bool bFoundUninitd = false;
						size_t nGlobalWordIdx;
						for (nGlobalWordIdx = 0; nGlobalWordIdx < nCurrGlobalWords; ++nGlobalWordIdx)
						{
							GlobalWord_1ch* pCurrGlobalWord = (GlobalWord_1ch*)apGlobalWordDict[nGlobalWordIdx];
							if (pCurrGlobalWord
								&& L1dist(pCurrGlobalWord->oFeature.anColor[0], pRefBestLocalWord->oFeature.anColor[0]) <= nCurrColorDistThreshold
								&& L1dist(nRefBestLocalWordDescBITS, pCurrGlobalWord->nDescBITS) <= nCurrDescDistThreshold / GWORD_DESC_THRES_BITS_MATCH_FACTOR)
								break;
							else if (!pCurrGlobalWord)
								bFoundUninitd = true;
						}
						if (nGlobalWordIdx == nCurrGlobalWords)
						{
							nGlobalWordIdx = nCurrGlobalWords - 1;
							GlobalWord_1ch* pCurrGlobalWord = bFoundUninitd ? pGlobalWordListIter_1ch++ : (GlobalWord_1ch*)apGlobalWordDict[nGlobalWordIdx];
							pCurrGlobalWord->oFeature.anColor[0] = pRefBestLocalWord->oFeature.anColor[0];
							pCurrGlobalWord->oFeature.anDesc[0] = pRefBestLocalWord->oFeature.anDesc[0];
							pCurrGlobalWord->nDescBITS = nRefBestLocalWordDescBITS;
							pCurrGlobalWord->oSpatioOccMap.create(oDownSampledFrameSize_GlobalWordLookup, CV_32FC1);
							pCurrGlobalWord->oSpatioOccMap = cv::Scalar(0.0f);
							pCurrGlobalWord->fLatestWeight = 0.0f;
							apGlobalWordDict[nGlobalWordIdx] = pCurrGlobalWord;
						}
						float& fCurrGlobalWordLocalWeight = *(float*)(apGlobalWordDict[nGlobalWordIdx]->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
						if (fCurrGlobalWordLocalWeight < fRefBestLocalWordWeight)
						{
							apGlobalWordDict[nGlobalWordIdx]->fLatestWeight += fRefBestLocalWordWeight;
							fCurrGlobalWordLocalWeight += fRefBestLocalWordWeight;
						}
						oGlobalDictPresenceLookupMap.data[nPxIter] = UCHAR_MAX;
						while (nGlobalWordIdx > 0 && (!apGlobalWordDict[nGlobalWordIdx - 1] || apGlobalWordDict[nGlobalWordIdx]->fLatestWeight > apGlobalWordDict[nGlobalWordIdx - 1]->fLatestWeight))
						{
							std::swap(apGlobalWordDict[nGlobalWordIdx], apGlobalWordDict[nGlobalWordIdx - 1]);
							--nGlobalWordIdx;
						}
					}
				}
			}
			nPxIterIncr = std::max(nPxIterIncr / 3, (size_t)1);
		}
		for (size_t nGlobalWordIdx = 0; nGlobalWordIdx < nCurrGlobalWords; ++nGlobalWordIdx)
		{
			GlobalWord_1ch* pCurrGlobalWord = (GlobalWord_1ch*)apGlobalWordDict[nGlobalWordIdx];
			if (!pCurrGlobalWord)
			{
				pCurrGlobalWord = pGlobalWordListIter_1ch++;
				pCurrGlobalWord->oFeature.anColor[0] = 0;
				pCurrGlobalWord->oFeature.anDesc[0] = 0;
				pCurrGlobalWord->nDescBITS = 0;
				pCurrGlobalWord->oSpatioOccMap.create(oDownSampledFrameSize_GlobalWordLookup, CV_32FC1);
				pCurrGlobalWord->oSpatioOccMap = cv::Scalar(0.0f);
				pCurrGlobalWord->fLatestWeight = 0.0f;
				apGlobalWordDict[nGlobalWordIdx] = pCurrGlobalWord;
			}
		}
		CV_Assert((size_t)(pGlobalWordListIter_1ch - aGlobalWordList_1ch) == nCurrGlobalWords && aGlobalWordList_1ch == (pGlobalWordListIter_1ch - nCurrGlobalWords));
	}
	else
	{ //nImgChannels==3
		for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			if (bForceFGUpdate || !oLastFGMask_dilated.data[nPxIter])
			{
				const size_t nLocalDictIdx = nModelIter*nCurrLocalWords;
				const size_t nFloatIter = nPxIter * 4;
				uchar& bCurrRegionIsUnstable = oUnstableRegionMask.data[nPxIter];
				const float fCurrDistThresholdFactor = *(float*)(oDistThresholdFrame.data + nFloatIter);
				const size_t nCurrTotColorDistThreshold = (size_t)(sqrt(fCurrDistThresholdFactor)*nMinColorDistThreshold) * 3;
				const size_t nCurrTotDescDistThreshold = (((size_t)1 << ((size_t)floor(fCurrDistThresholdFactor + 0.5f))) + nDescDistThresholdOffset + (bCurrRegionIsUnstable*UNSTAB_DESC_DIST_OFFSET)) * 3;
				// == refresh: local decr
				if (fOccDecrFrac > 0.0f)
				{
					for (size_t nLocalWordIdx = 0; nLocalWordIdx < nCurrLocalWords; ++nLocalWordIdx)
					{
						LocalWord_3ch* pCurrLocalWord = (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
						if (pCurrLocalWord)
							pCurrLocalWord->nOccurrences -= (size_t)(fOccDecrFrac*pCurrLocalWord->nOccurrences);
					}
				}
				const size_t nCurrWordOccIncr = DEFAULT_LWORD_OCC_INCR;
				const size_t nTotLocalSamplingIterCount = (s_nSamplesInitPatternWidth*s_nSamplesInitPatternHeight) * 2;
				for (size_t nLocalSamplingIter = 0; nLocalSamplingIter < nTotLocalSamplingIterCount; ++nLocalSamplingIter)
				{
					// == refresh: local resampling
					int nSampleImgCoord_Y, nSampleImgCoord_X;
					getRandSamplePosition(nSampleImgCoord_X, nSampleImgCoord_Y, aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X, aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
					const size_t nSamplePxIdx = oImgSize.width*nSampleImgCoord_Y + nSampleImgCoord_X;
					if (bForceFGUpdate || !oLastFGMask_dilated.data[nSamplePxIdx])
					{
						const size_t nSamplePxRGBIdx = nSamplePxIdx * 3;
						const size_t nSampleDescRGBIdx = nSamplePxRGBIdx * 2;
						const uchar* const anSampleColor = oLastColorFrame.data + nSamplePxRGBIdx;
						const ushort* const anSampleIntraDesc = ((ushort*)(oLastDescFrame.data + nSampleDescRGBIdx));
						bool bFoundUninitd = false;
						size_t nLocalWordIdx;
						for (nLocalWordIdx = 0; nLocalWordIdx < nCurrLocalWords; ++nLocalWordIdx)
						{
							LocalWord_3ch* pCurrLocalWord = (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
							if (pCurrLocalWord
								&& cmixdist<3>(anSampleColor, pCurrLocalWord->oFeature.anColor) <= nCurrTotColorDistThreshold
								&& hdist<3>(anSampleIntraDesc, pCurrLocalWord->oFeature.anDesc) <= nCurrTotDescDistThreshold)
							{
								pCurrLocalWord->nOccurrences += nCurrWordOccIncr;
								pCurrLocalWord->nLastOcc = nFrameIndex;
								break;
							}
							else if (!pCurrLocalWord)
								bFoundUninitd = true;
						}
						if (nLocalWordIdx == nCurrLocalWords)
						{
							nLocalWordIdx = nCurrLocalWords - 1;
							LocalWord_3ch* pCurrLocalWord = bFoundUninitd ? pLocalWordListIter_3ch++ : (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
							for (size_t c = 0; c < 3; ++c)
							{
								pCurrLocalWord->oFeature.anColor[c] = anSampleColor[c];
								pCurrLocalWord->oFeature.anDesc[c] = anSampleIntraDesc[c];
							}
							pCurrLocalWord->nOccurrences = nBaseOccCount;
							pCurrLocalWord->nFirstOcc = nFrameIndex;
							pCurrLocalWord->nLastOcc = nFrameIndex;
							apLocalWordDict[nLocalDictIdx + nLocalWordIdx] = pCurrLocalWord;
						}
						while (nLocalWordIdx > 0 && (!apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1] || GetLocalWordWeight(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], nFrameIndex, nLocalWordWeightOffset) > GetLocalWordWeight(apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1], nFrameIndex, nLocalWordWeightOffset)))
						{
							std::swap(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1]);
							--nLocalWordIdx;
						}
					}
				}
				CV_Assert(apLocalWordDict[nLocalDictIdx]);
				for (size_t nLocalWordIdx = 1; nLocalWordIdx < nCurrLocalWords; ++nLocalWordIdx)
				{
					// == refresh: local random resampling
					LocalWord_3ch* pCurrLocalWord = (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
					if (!pCurrLocalWord)
					{
						const size_t nRandLocalWordIdx = (rand() % nLocalWordIdx);
						const LocalWord_3ch* pRefLocalWord = (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx + nRandLocalWordIdx];
						const int nRandColorOffset = (rand() % (nCurrTotColorDistThreshold / 3 + 1)) - (int)(nCurrTotColorDistThreshold / 6);
						pCurrLocalWord = pLocalWordListIter_3ch++;
						for (size_t c = 0; c < 3; ++c)
						{
							pCurrLocalWord->oFeature.anColor[c] = cv::saturate_cast<uchar>((int)pRefLocalWord->oFeature.anColor[c] + nRandColorOffset);
							pCurrLocalWord->oFeature.anDesc[c] = pRefLocalWord->oFeature.anDesc[c];
						}
						pCurrLocalWord->nOccurrences = std::max((size_t)(pRefLocalWord->nOccurrences*((float)(nCurrLocalWords - nLocalWordIdx) / nCurrLocalWords)), (size_t)1);
						pCurrLocalWord->nFirstOcc = nFrameIndex;
						pCurrLocalWord->nLastOcc = nFrameIndex;
						apLocalWordDict[nLocalDictIdx + nLocalWordIdx] = pCurrLocalWord;
					}
				}
			}
		}
		CV_Assert(aLocalWordList_3ch == (pLocalWordListIter_3ch - nTotRelevantPxCount*nCurrLocalWords));
		cv::Mat oGlobalDictPresenceLookupMap(oImgSize, CV_8UC1, cv::Scalar_<uchar>(0));
		size_t nPxIterIncr = std::max(nTotPxCount / nCurrGlobalWords, (size_t)1);
		for (size_t nSamplingPasses = 0; nSamplingPasses < GWORD_DEFAULT_NB_INIT_SAMPL_PASSES; ++nSamplingPasses)
		{
			for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
			{
				// == refresh: global resampling
				const size_t nPxIter = aPxIdxLUT[nModelIter];
				if ((nPxIter%nPxIterIncr) == 0)
				{ // <=(nCurrGlobalWords) gwords from (nCurrGlobalWords) equally spaced pixels
					if (bForceFGUpdate || !oLastFGMask_dilated.data[nPxIter])
					{
						const size_t nLocalDictIdx = nModelIter*nCurrLocalWords;
						const size_t nGlobalWordMapLookupIdx = aPxInfoLUT_PAWCS[nPxIter].nGlobalWordMapLookupIdx;
						const size_t nFloatIter = nPxIter * 4;
						uchar& bCurrRegionIsUnstable = oUnstableRegionMask.data[nPxIter];
						const float fCurrDistThresholdFactor = *(float*)(oDistThresholdFrame.data + nFloatIter);
						const size_t nCurrTotColorDistThreshold = (size_t)(sqrt(fCurrDistThresholdFactor)*nMinColorDistThreshold) * 3;
						const size_t nCurrTotDescDistThreshold = (((size_t)1 << ((size_t)floor(fCurrDistThresholdFactor + 0.5f))) + nDescDistThresholdOffset + (bCurrRegionIsUnstable*UNSTAB_DESC_DIST_OFFSET)) * 3;
						CV_Assert(apLocalWordDict[nLocalDictIdx]);
						const LocalWord_3ch* pRefBestLocalWord = (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx];
						const float fRefBestLocalWordWeight = GetLocalWordWeight(pRefBestLocalWord, nFrameIndex, nLocalWordWeightOffset);
						const uchar nRefBestLocalWordDescBITS = (uchar)popcount<3>(pRefBestLocalWord->oFeature.anDesc);
						bool bFoundUninitd = false;
						size_t nGlobalWordIdx;
						for (nGlobalWordIdx = 0; nGlobalWordIdx < nCurrGlobalWords; ++nGlobalWordIdx)
						{
							GlobalWord_3ch* pCurrGlobalWord = (GlobalWord_3ch*)apGlobalWordDict[nGlobalWordIdx];
							if (pCurrGlobalWord
								&& L1dist(nRefBestLocalWordDescBITS, pCurrGlobalWord->nDescBITS) <= nCurrTotDescDistThreshold / GWORD_DESC_THRES_BITS_MATCH_FACTOR
								&& cmixdist<3>(pRefBestLocalWord->oFeature.anColor, pCurrGlobalWord->oFeature.anColor) <= nCurrTotColorDistThreshold)
								break;
							else if (!pCurrGlobalWord)
								bFoundUninitd = true;
						}
						if (nGlobalWordIdx == nCurrGlobalWords)
						{
							nGlobalWordIdx = nCurrGlobalWords - 1;
							GlobalWord_3ch* pCurrGlobalWord = bFoundUninitd ? pGlobalWordListIter_3ch++ : (GlobalWord_3ch*)apGlobalWordDict[nGlobalWordIdx];
							for (size_t c = 0; c < 3; ++c)
							{
								pCurrGlobalWord->oFeature.anColor[c] = pRefBestLocalWord->oFeature.anColor[c];
								pCurrGlobalWord->oFeature.anDesc[c] = pRefBestLocalWord->oFeature.anDesc[c];
							}
							pCurrGlobalWord->nDescBITS = nRefBestLocalWordDescBITS;
							pCurrGlobalWord->oSpatioOccMap.create(oDownSampledFrameSize_GlobalWordLookup, CV_32FC1);
							pCurrGlobalWord->oSpatioOccMap = cv::Scalar(0.0f);
							pCurrGlobalWord->fLatestWeight = 0.0f;
							apGlobalWordDict[nGlobalWordIdx] = pCurrGlobalWord;
						}
						float& fCurrGlobalWordLocalWeight = *(float*)(apGlobalWordDict[nGlobalWordIdx]->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
						if (fCurrGlobalWordLocalWeight < fRefBestLocalWordWeight)
						{
							apGlobalWordDict[nGlobalWordIdx]->fLatestWeight += fRefBestLocalWordWeight;
							fCurrGlobalWordLocalWeight += fRefBestLocalWordWeight;
						}
						oGlobalDictPresenceLookupMap.data[nPxIter] = UCHAR_MAX;
						while (nGlobalWordIdx > 0 && (!apGlobalWordDict[nGlobalWordIdx - 1] || apGlobalWordDict[nGlobalWordIdx]->fLatestWeight > apGlobalWordDict[nGlobalWordIdx - 1]->fLatestWeight))
						{
							std::swap(apGlobalWordDict[nGlobalWordIdx], apGlobalWordDict[nGlobalWordIdx - 1]);
							--nGlobalWordIdx;
						}
					}
				}
			}
			nPxIterIncr = std::max(nPxIterIncr / 3, (size_t)1);
		}
		for (size_t nGlobalWordIdx = 0; nGlobalWordIdx < nCurrGlobalWords; ++nGlobalWordIdx)
		{
			GlobalWord_3ch* pCurrGlobalWord = (GlobalWord_3ch*)apGlobalWordDict[nGlobalWordIdx];
			if (!pCurrGlobalWord)
			{
				pCurrGlobalWord = pGlobalWordListIter_3ch++;
				for (size_t c = 0; c < 3; ++c)
				{
					pCurrGlobalWord->oFeature.anColor[c] = 0;
					pCurrGlobalWord->oFeature.anDesc[c] = 0;
				}
				pCurrGlobalWord->nDescBITS = 0;
				pCurrGlobalWord->oSpatioOccMap.create(oDownSampledFrameSize_GlobalWordLookup, CV_32FC1);
				pCurrGlobalWord->oSpatioOccMap = cv::Scalar(0.0f);
				pCurrGlobalWord->fLatestWeight = 0.0f;
				apGlobalWordDict[nGlobalWordIdx] = pCurrGlobalWord;
			}
		}
		CV_Assert((size_t)(pGlobalWordListIter_3ch - aGlobalWordList_3ch) == nCurrGlobalWords && aGlobalWordList_3ch == (pGlobalWordListIter_3ch - nCurrGlobalWords));
	}
	for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
	{
		// == refresh: per-px global word sort
		const size_t nPxIter = aPxIdxLUT[nModelIter];
		const size_t nGlobalWordMapLookupIdx = aPxInfoLUT_PAWCS[nPxIter].nGlobalWordMapLookupIdx;
		float fLastGlobalWordLocalWeight = *(float*)(aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[0]->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
		for (size_t nGlobalWordLUTIdx = 1; nGlobalWordLUTIdx < nCurrGlobalWords; ++nGlobalWordLUTIdx)
		{
			const float fCurrGlobalWordLocalWeight = *(float*)(aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx]->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
			if (fCurrGlobalWordLocalWeight > fLastGlobalWordLocalWeight)
				std::swap(aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx], aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx - 1]);
			else
				fLastGlobalWordLocalWeight = fCurrGlobalWordLocalWeight;
		}
	}
}

void BgsPAWCS::apply(const cv::Mat& image, cv::Mat& fgmask, double learningRateOverride)
{
	CV_Assert(image.channels() == 1);

	startApply();

	CV_Assert(bInitialized);

	cv::Mat oInputImg;
	if (isHSV)
	{
		cv::cvtColor(image, oInputImg, CV_BGR2HSV);
	}
	else
	{
		oInputImg = image.clone();
	}

	CV_Assert(oInputImg.type() == nImgType && oInputImg.size() == oImgSize);
	CV_Assert(oInputImg.isContinuous());
	fgmask.create(oImgSize, CV_8UC1);
	cv::Mat oCurrFGMask = fgmask;
	memset(oCurrFGMask.data, 0, oCurrFGMask.cols*oCurrFGMask.rows);
	const bool bBootstrapping = ++nFrameIndex <= DEFAULT_BOOTSTRAP_WIN_SIZE;
	const size_t nCurrSamplesForMovingAvg_LT = bBootstrapping ? nSamplesForMovingAvgs / 2 : nSamplesForMovingAvgs;
	const size_t nCurrSamplesForMovingAvg_ST = nCurrSamplesForMovingAvg_LT / 4;
	const float fRollAvgFactor_LT = 1.0f / std::min(nFrameIndex, nCurrSamplesForMovingAvg_LT);
	const float fRollAvgFactor_ST = 1.0f / std::min(nFrameIndex, nCurrSamplesForMovingAvg_ST);
	const size_t nCurrGlobalWordUpdateRate = bBootstrapping ? DEFAULT_RESAMPLING_RATE / 2 : DEFAULT_RESAMPLING_RATE;
	size_t nFlatRegionCount = 0;
	if (nImgChannels == 1)
	{
		for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			const size_t nDescIter = nPxIter * 2;
			const size_t nFloatIter = nPxIter * 4;
			const size_t nLocalDictIdx = nModelIter*nCurrLocalWords;
			const size_t nGlobalWordMapLookupIdx = aPxInfoLUT_PAWCS[nPxIter].nGlobalWordMapLookupIdx;
			const uchar nCurrColor = oInputImg.data[nPxIter];
			uchar& nLastColor = oLastColorFrame.data[nPxIter];
			ushort& nLastIntraDesc = *((ushort*)(oLastDescFrame.data + nDescIter));
			size_t nMinColorDist = s_nColorMaxDataRange_1ch;
			size_t nMinDescDist = s_nDescMaxDataRange_1ch;
			float& fCurrMeanRawSegmRes_LT = *(float*)(oMeanRawSegmResFrame_LT.data + nFloatIter);
			float& fCurrMeanRawSegmRes_ST = *(float*)(oMeanRawSegmResFrame_ST.data + nFloatIter);
			float& fCurrMeanFinalSegmRes_LT = *(float*)(oMeanFinalSegmResFrame_LT.data + nFloatIter);
			float& fCurrMeanFinalSegmRes_ST = *(float*)(oMeanFinalSegmResFrame_ST.data + nFloatIter);
			float& fCurrDistThresholdFactor = *(float*)(oDistThresholdFrame.data + nFloatIter);
			float& fCurrDistThresholdVariationFactor = *(float*)(oDistThresholdVariationFrame.data + nFloatIter);
			float& fCurrLearningRate = *(float*)(oUpdateRateFrame.data + nFloatIter);
			float& fCurrMeanMinDist_LT = *(float*)(oMeanMinDistFrame_LT.data + nFloatIter);
			float& fCurrMeanMinDist_ST = *(float*)(oMeanMinDistFrame_ST.data + nFloatIter);
			const float fBestLocalWordWeight = GetLocalWordWeight(apLocalWordDict[nLocalDictIdx], nFrameIndex, nLocalWordWeightOffset);
			const float fLocalWordsWeightSumThreshold = fBestLocalWordWeight / (fCurrDistThresholdFactor * 2);
			uchar& bCurrRegionIsUnstable = oUnstableRegionMask.data[nPxIter];
			uchar& nCurrRegionIllumUpdtVal = oIllumUpdtRegionMask.data[nPxIter];
			uchar& nCurrRegionSegmVal = oCurrFGMask.data[nPxIter];
			const bool bCurrRegionIsROIBorder = oROI.data[nPxIter] < UCHAR_MAX;
			const int nCurrImgCoord_X = aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X;
			const int nCurrImgCoord_Y = aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y;
			ushort nCurrInterDesc, nCurrIntraDesc;
			LBSP::computeGrayscaleDescriptor(oInputImg, nCurrColor, nCurrImgCoord_X, nCurrImgCoord_Y, anLBSPThreshold_8bitLUT[nCurrColor], nCurrIntraDesc);
			const uchar nCurrIntraDescBITS = (uchar)popcount(nCurrIntraDesc);
			const bool bCurrRegionIsFlat = nCurrIntraDescBITS < FLAT_REGION_BIT_COUNT;
			if (bCurrRegionIsFlat)
				++nFlatRegionCount;
			const size_t nCurrWordOccIncr = (DEFAULT_LWORD_OCC_INCR + nModelResetCooldown) << int(bCurrRegionIsFlat || bBootstrapping);
			const size_t nCurrLocalWordUpdateRate = learningRateOverride > 0 ? (size_t)ceil(learningRateOverride) : bCurrRegionIsFlat ? (size_t)ceil(fCurrLearningRate + FEEDBACK_T_LOWER) / 2 : (size_t)ceil(fCurrLearningRate);
			const size_t nCurrColorDistThreshold = (size_t)(sqrt(fCurrDistThresholdFactor)*nMinColorDistThreshold) / 2;
			const size_t nCurrDescDistThreshold = ((size_t)1 << ((size_t)floor(fCurrDistThresholdFactor + 0.5f))) + nDescDistThresholdOffset + (bCurrRegionIsUnstable*UNSTAB_DESC_DIST_OFFSET);
			size_t nLocalWordIdx = 0;
			float fPotentialLocalWordsWeightSum = 0.0f;
			float fLastLocalWordWeight = FLT_MAX;
			while (nLocalWordIdx < nCurrLocalWords && fPotentialLocalWordsWeightSum < fLocalWordsWeightSumThreshold)
			{
				LocalWord_1ch* pCurrLocalWord = (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
				const float fCurrLocalWordWeight = GetLocalWordWeight(pCurrLocalWord, nFrameIndex, nLocalWordWeightOffset);
				{
					const size_t nColorDist = L1dist(nCurrColor, pCurrLocalWord->oFeature.anColor[0]);
					const size_t nIntraDescDist = hdist(nCurrIntraDesc, pCurrLocalWord->oFeature.anDesc[0]);
					LBSP::computeGrayscaleDescriptor(oInputImg, pCurrLocalWord->oFeature.anColor[0], nCurrImgCoord_X, nCurrImgCoord_Y, anLBSPThreshold_8bitLUT[pCurrLocalWord->oFeature.anColor[0]], nCurrInterDesc);
					const size_t nInterDescDist = hdist(nCurrInterDesc, pCurrLocalWord->oFeature.anDesc[0]);
					const size_t nDescDist = (nIntraDescDist + nInterDescDist) / 2;
					if ((!bCurrRegionIsUnstable || bCurrRegionIsFlat || bCurrRegionIsROIBorder)
						&& nColorDist <= nCurrColorDistThreshold
						&& nColorDist >= nCurrColorDistThreshold / 2
						&& nIntraDescDist <= nCurrDescDistThreshold / 2
						&& (rand() % (nCurrRegionIllumUpdtVal ? (nCurrLocalWordUpdateRate / 2 + 1) : nCurrLocalWordUpdateRate)) == 0)
					{
						// == illum updt
						pCurrLocalWord->oFeature.anColor[0] = nCurrColor;
						pCurrLocalWord->oFeature.anDesc[0] = nCurrIntraDesc;
						oIllumUpdtRegionMask.data[nPxIter - 1] = 1 & oROI.data[nPxIter - 1];
						oIllumUpdtRegionMask.data[nPxIter + 1] = 1 & oROI.data[nPxIter + 1];
						oIllumUpdtRegionMask.data[nPxIter] = 2;
					}
					if (nDescDist <= nCurrDescDistThreshold && nColorDist <= nCurrColorDistThreshold)
					{
						fPotentialLocalWordsWeightSum += fCurrLocalWordWeight;
						pCurrLocalWord->nLastOcc = nFrameIndex;
						if ((!oLastFGMask.data[nPxIter] || bUsingMovingCamera) && fCurrLocalWordWeight < DEFAULT_LWORD_MAX_WEIGHT)
							pCurrLocalWord->nOccurrences += nCurrWordOccIncr;
						nMinColorDist = std::min(nMinColorDist, nColorDist);
						nMinDescDist = std::min(nMinDescDist, nDescDist);
					}
				}
				if (fCurrLocalWordWeight > fLastLocalWordWeight)
				{
					std::swap(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1]);
				}
				else
					fLastLocalWordWeight = fCurrLocalWordWeight;
				++nLocalWordIdx;
			}
			while (nLocalWordIdx < nCurrLocalWords)
			{
				const float fCurrLocalWordWeight = GetLocalWordWeight(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], nFrameIndex, nLocalWordWeightOffset);
				if (fCurrLocalWordWeight > fLastLocalWordWeight)
				{
					std::swap(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1]);
				}
				else
					fLastLocalWordWeight = fCurrLocalWordWeight;
				++nLocalWordIdx;
			}
			if (fPotentialLocalWordsWeightSum >= fLocalWordsWeightSumThreshold || bCurrRegionIsROIBorder)
			{
				// == background
				const float fNormalizedMinDist = std::max((float)nMinColorDist / s_nColorMaxDataRange_1ch, (float)nMinDescDist / s_nDescMaxDataRange_1ch);
				fCurrMeanMinDist_LT = fCurrMeanMinDist_LT*(1.0f - fRollAvgFactor_LT) + fNormalizedMinDist*fRollAvgFactor_LT;
				fCurrMeanMinDist_ST = fCurrMeanMinDist_ST*(1.0f - fRollAvgFactor_ST) + fNormalizedMinDist*fRollAvgFactor_ST;
				fCurrMeanRawSegmRes_LT = fCurrMeanRawSegmRes_LT*(1.0f - fRollAvgFactor_LT);
				fCurrMeanRawSegmRes_ST = fCurrMeanRawSegmRes_ST*(1.0f - fRollAvgFactor_ST);
				if ((rand() % nCurrLocalWordUpdateRate) == 0)
				{
					size_t nGlobalWordLUTIdx;
					GlobalWord_1ch* pCurrGlobalWord = nullptr;
					for (nGlobalWordLUTIdx = 0; nGlobalWordLUTIdx < nCurrGlobalWords; ++nGlobalWordLUTIdx)
					{
						pCurrGlobalWord = (GlobalWord_1ch*)aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx];
						if (L1dist(pCurrGlobalWord->oFeature.anColor[0], nCurrColor) <= nCurrColorDistThreshold
							&& L1dist(nCurrIntraDescBITS, pCurrGlobalWord->nDescBITS) <= nCurrDescDistThreshold / GWORD_DESC_THRES_BITS_MATCH_FACTOR)
							break;
					}
					if (nGlobalWordLUTIdx != nCurrGlobalWords || (rand() % (nCurrLocalWordUpdateRate * 2)) == 0)
					{
						if (nGlobalWordLUTIdx == nCurrGlobalWords)
						{
							pCurrGlobalWord = (GlobalWord_1ch*)apGlobalWordDict[nCurrGlobalWords - 1];
							pCurrGlobalWord->oFeature.anColor[0] = nCurrColor;
							pCurrGlobalWord->oFeature.anDesc[0] = nCurrIntraDesc;
							pCurrGlobalWord->nDescBITS = nCurrIntraDescBITS;
							pCurrGlobalWord->oSpatioOccMap = cv::Scalar(0.0f);
							pCurrGlobalWord->fLatestWeight = 0.0f;
						}
						float& fCurrGlobalWordLocalWeight = *(float*)(pCurrGlobalWord->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
						if (fCurrGlobalWordLocalWeight < fPotentialLocalWordsWeightSum)
						{
							pCurrGlobalWord->fLatestWeight += fPotentialLocalWordsWeightSum;
							fCurrGlobalWordLocalWeight += fPotentialLocalWordsWeightSum;
						}
					}
				}
			}
			else
			{
				// == foreground
				const float fNormalizedMinDist = std::max(std::max((float)nMinColorDist / s_nColorMaxDataRange_1ch, (float)nMinDescDist / s_nDescMaxDataRange_1ch), (fLocalWordsWeightSumThreshold - fPotentialLocalWordsWeightSum) / fLocalWordsWeightSumThreshold);
				fCurrMeanMinDist_LT = fCurrMeanMinDist_LT*(1.0f - fRollAvgFactor_LT) + fNormalizedMinDist*fRollAvgFactor_LT;
				fCurrMeanMinDist_ST = fCurrMeanMinDist_ST*(1.0f - fRollAvgFactor_ST) + fNormalizedMinDist*fRollAvgFactor_ST;
				fCurrMeanRawSegmRes_LT = fCurrMeanRawSegmRes_LT*(1.0f - fRollAvgFactor_LT) + fRollAvgFactor_LT;
				fCurrMeanRawSegmRes_ST = fCurrMeanRawSegmRes_ST*(1.0f - fRollAvgFactor_ST) + fRollAvgFactor_ST;
				if (bCurrRegionIsFlat || (rand() % nCurrLocalWordUpdateRate) == 0)
				{
					size_t nGlobalWordLUTIdx;
					GlobalWord_1ch* pCurrGlobalWord;
					for (nGlobalWordLUTIdx = 0; nGlobalWordLUTIdx < nCurrGlobalWords; ++nGlobalWordLUTIdx)
					{
						pCurrGlobalWord = (GlobalWord_1ch*)aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx];
						if (L1dist(pCurrGlobalWord->oFeature.anColor[0], nCurrColor) <= nCurrColorDistThreshold
							&& L1dist(nCurrIntraDescBITS, pCurrGlobalWord->nDescBITS) <= nCurrDescDistThreshold / GWORD_DESC_THRES_BITS_MATCH_FACTOR)
							break;
					}
					if (nGlobalWordLUTIdx == nCurrGlobalWords)
						nCurrRegionSegmVal = UCHAR_MAX;
					else
					{
						const float fGlobalWordLocalizedWeight = *(float*)(pCurrGlobalWord->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
						if (fPotentialLocalWordsWeightSum + fGlobalWordLocalizedWeight / (bCurrRegionIsFlat ? 2 : 4) < fLocalWordsWeightSumThreshold)
							nCurrRegionSegmVal = UCHAR_MAX;
					}
				}
				else
					nCurrRegionSegmVal = UCHAR_MAX;
				if (fPotentialLocalWordsWeightSum < DEFAULT_LWORD_INIT_WEIGHT)
				{
					const size_t nNewLocalWordIdx = nCurrLocalWords - 1;
					LocalWord_1ch* pNewLocalWord = (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx + nNewLocalWordIdx];
					pNewLocalWord->oFeature.anColor[0] = nCurrColor;
					pNewLocalWord->oFeature.anDesc[0] = nCurrIntraDesc;
					pNewLocalWord->nOccurrences = nCurrWordOccIncr;
					pNewLocalWord->nFirstOcc = nFrameIndex;
					pNewLocalWord->nLastOcc = nFrameIndex;
				}
			}
			// == neighb updt
			if ((!nCurrRegionSegmVal && (rand() % nCurrLocalWordUpdateRate) == 0) || bCurrRegionIsROIBorder || bUsingMovingCamera)
			{
				//if((!nCurrRegionSegmVal && (rand()%(nCurrRegionIllumUpdtVal?(nCurrLocalWordUpdateRate/2+1):nCurrLocalWordUpdateRate))==0) || bCurrRegionIsROIBorder) {
				int nSampleImgCoord_Y, nSampleImgCoord_X;
				if (bCurrRegionIsFlat || bCurrRegionIsROIBorder || bUsingMovingCamera)
					getRandNeighborPosition_5x5(nSampleImgCoord_X, nSampleImgCoord_Y, nCurrImgCoord_X, nCurrImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
				else
					getRandNeighborPosition_3x3(nSampleImgCoord_X, nSampleImgCoord_Y, nCurrImgCoord_X, nCurrImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
				const size_t nSamplePxIdx = oImgSize.width*nSampleImgCoord_Y + nSampleImgCoord_X;
				if (oROI.data[nSamplePxIdx])
				{
					const size_t nNeighborLocalDictIdx = aPxInfoLUT_PAWCS[nSamplePxIdx].nModelIdx*nCurrLocalWords;
					size_t nNeighborLocalWordIdx = 0;
					float fNeighborPotentialLocalWordsWeightSum = 0.0f;
					while (nNeighborLocalWordIdx < nCurrLocalWords && fNeighborPotentialLocalWordsWeightSum < fLocalWordsWeightSumThreshold)
					{
						LocalWord_1ch* pNeighborLocalWord = (LocalWord_1ch*)apLocalWordDict[nNeighborLocalDictIdx + nNeighborLocalWordIdx];
						const size_t nNeighborColorDist = L1dist(nCurrColor, pNeighborLocalWord->oFeature.anColor[0]);
						const size_t nNeighborIntraDescDist = hdist(nCurrIntraDesc, pNeighborLocalWord->oFeature.anDesc[0]);
						const bool bNeighborRegionIsFlat = popcount(pNeighborLocalWord->oFeature.anDesc[0]) < FLAT_REGION_BIT_COUNT;
						const size_t nNeighborWordOccIncr = bNeighborRegionIsFlat ? nCurrWordOccIncr * 2 : nCurrWordOccIncr;
						if (nNeighborColorDist <= nCurrColorDistThreshold && nNeighborIntraDescDist <= nCurrDescDistThreshold)
						{
							const float fNeighborLocalWordWeight = GetLocalWordWeight(pNeighborLocalWord, nFrameIndex, nLocalWordWeightOffset);
							fNeighborPotentialLocalWordsWeightSum += fNeighborLocalWordWeight;
							pNeighborLocalWord->nLastOcc = nFrameIndex;
							if (fNeighborLocalWordWeight < DEFAULT_LWORD_MAX_WEIGHT)
								pNeighborLocalWord->nOccurrences += nNeighborWordOccIncr;
						}
						else if (!oCurrFGMask.data[nSamplePxIdx] && bCurrRegionIsFlat && (bBootstrapping || (rand() % nCurrLocalWordUpdateRate) == 0))
						{
							const size_t nSampleDescIdx = nSamplePxIdx * 2;
							ushort& nNeighborLastIntraDesc = *((ushort*)(oLastDescFrame.data + nSampleDescIdx));
							const size_t nNeighborLastIntraDescDist = hdist(nCurrIntraDesc, nNeighborLastIntraDesc);
							if (nNeighborColorDist <= nCurrColorDistThreshold && nNeighborLastIntraDescDist <= nCurrDescDistThreshold / 2)
							{
								const float fNeighborLocalWordWeight = GetLocalWordWeight(pNeighborLocalWord, nFrameIndex, nLocalWordWeightOffset);
								fNeighborPotentialLocalWordsWeightSum += fNeighborLocalWordWeight;
								pNeighborLocalWord->nLastOcc = nFrameIndex;
								if (fNeighborLocalWordWeight < DEFAULT_LWORD_MAX_WEIGHT)
									pNeighborLocalWord->nOccurrences += nNeighborWordOccIncr;
								pNeighborLocalWord->oFeature.anDesc[0] = nCurrIntraDesc;
							}
						}
						++nNeighborLocalWordIdx;
					}
					if (fNeighborPotentialLocalWordsWeightSum < DEFAULT_LWORD_INIT_WEIGHT)
					{
						nNeighborLocalWordIdx = nCurrLocalWords - 1;
						LocalWord_1ch* pNeighborLocalWord = (LocalWord_1ch*)apLocalWordDict[nNeighborLocalDictIdx + nNeighborLocalWordIdx];
						pNeighborLocalWord->oFeature.anColor[0] = nCurrColor;
						pNeighborLocalWord->oFeature.anDesc[0] = nCurrIntraDesc;
						pNeighborLocalWord->nOccurrences = nCurrWordOccIncr;
						pNeighborLocalWord->nFirstOcc = nFrameIndex;
						pNeighborLocalWord->nLastOcc = nFrameIndex;
					}
				}
			}
			if (nCurrRegionIllumUpdtVal)
				nCurrRegionIllumUpdtVal -= 1;
			// == feedback adj
			bCurrRegionIsUnstable = fCurrDistThresholdFactor > UNSTABLE_REG_RDIST_MIN || (fCurrMeanRawSegmRes_LT - fCurrMeanFinalSegmRes_LT) > UNSTABLE_REG_RATIO_MIN || (fCurrMeanRawSegmRes_ST - fCurrMeanFinalSegmRes_ST) > UNSTABLE_REG_RATIO_MIN;
			if (oLastFGMask.data[nPxIter] || (std::min(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST) < UNSTABLE_REG_RATIO_MIN && nCurrRegionSegmVal))
				fCurrLearningRate = std::min(fCurrLearningRate + FEEDBACK_T_INCR / (std::max(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST)*fCurrDistThresholdVariationFactor), FEEDBACK_T_UPPER);
			else
				fCurrLearningRate = std::max(fCurrLearningRate - FEEDBACK_T_DECR*fCurrDistThresholdVariationFactor / std::max(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST), FEEDBACK_T_LOWER);
			if (std::max(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST) > UNSTABLE_REG_RATIO_MIN && oBlinksFrame.data[nPxIter])
				(fCurrDistThresholdVariationFactor) += bBootstrapping ? FEEDBACK_V_INCR * 2 : FEEDBACK_V_INCR;
			else
				fCurrDistThresholdVariationFactor = std::max(fCurrDistThresholdVariationFactor - FEEDBACK_V_DECR*((bBootstrapping || bCurrRegionIsFlat) ? 2 : oLastFGMask.data[nPxIter] ? 0.5f : 1), FEEDBACK_V_DECR);
			if (fCurrDistThresholdFactor < std::pow(1.0f + std::min(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST) * 2, 2))
				fCurrDistThresholdFactor += FEEDBACK_R_VAR*(fCurrDistThresholdVariationFactor - FEEDBACK_V_DECR);
			else
				fCurrDistThresholdFactor = std::max(fCurrDistThresholdFactor - FEEDBACK_R_VAR / fCurrDistThresholdVariationFactor, 1.0f);
			nLastIntraDesc = nCurrIntraDesc;
			nLastColor = nCurrColor;
		}
	}
	else
	{ //nImgChannels==3
		for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			const size_t nPxRGBIter = nPxIter * 3;
			const size_t nDescRGBIter = nPxRGBIter * 2;
			const size_t nFloatIter = nPxIter * 4;
			const size_t nLocalDictIdx = nModelIter*nCurrLocalWords;
			const size_t nGlobalWordMapLookupIdx = aPxInfoLUT_PAWCS[nPxIter].nGlobalWordMapLookupIdx;
			const uchar* const anCurrColor = oInputImg.data + nPxRGBIter;
			uchar* anLastColor = oLastColorFrame.data + nPxRGBIter;
			ushort* anLastIntraDesc = ((ushort*)(oLastDescFrame.data + nDescRGBIter));
			size_t nMinTotColorDist = s_nColorMaxDataRange_3ch;
			size_t nMinTotDescDist = s_nDescMaxDataRange_3ch;
			float& fCurrMeanRawSegmRes_LT = *(float*)(oMeanRawSegmResFrame_LT.data + nFloatIter);
			float& fCurrMeanRawSegmRes_ST = *(float*)(oMeanRawSegmResFrame_ST.data + nFloatIter);
			float& fCurrMeanFinalSegmRes_LT = *(float*)(oMeanFinalSegmResFrame_LT.data + nFloatIter);
			float& fCurrMeanFinalSegmRes_ST = *(float*)(oMeanFinalSegmResFrame_ST.data + nFloatIter);
			float& fCurrDistThresholdFactor = *(float*)(oDistThresholdFrame.data + nFloatIter);
			float& fCurrDistThresholdVariationFactor = *(float*)(oDistThresholdVariationFrame.data + nFloatIter);
			float& fCurrLearningRate = *(float*)(oUpdateRateFrame.data + nFloatIter);
			float& fCurrMeanMinDist_LT = *(float*)(oMeanMinDistFrame_LT.data + nFloatIter);
			float& fCurrMeanMinDist_ST = *(float*)(oMeanMinDistFrame_ST.data + nFloatIter);
			const float fBestLocalWordWeight = GetLocalWordWeight(apLocalWordDict[nLocalDictIdx], nFrameIndex, nLocalWordWeightOffset);
			const float fLocalWordsWeightSumThreshold = fBestLocalWordWeight / (fCurrDistThresholdFactor * 2);
			uchar& bCurrRegionIsUnstable = oUnstableRegionMask.data[nPxIter];
			uchar& nCurrRegionIllumUpdtVal = oIllumUpdtRegionMask.data[nPxIter];
			uchar& nCurrRegionSegmVal = oCurrFGMask.data[nPxIter];
			const bool bCurrRegionIsROIBorder = oROI.data[nPxIter] < UCHAR_MAX;
			const int nCurrImgCoord_X = aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X;
			const int nCurrImgCoord_Y = aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y;
			ushort anCurrInterDesc[3], anCurrIntraDesc[3];
			const size_t anCurrIntraLBSPThresholds[3] = { anLBSPThreshold_8bitLUT[anCurrColor[0]],anLBSPThreshold_8bitLUT[anCurrColor[1]],anLBSPThreshold_8bitLUT[anCurrColor[2]] };
			LBSP::computeRGBDescriptor(oInputImg, anCurrColor, nCurrImgCoord_X, nCurrImgCoord_Y, anCurrIntraLBSPThresholds, anCurrIntraDesc);
			const uchar nCurrIntraDescBITS = (uchar)popcount<3>(anCurrIntraDesc);
			const bool bCurrRegionIsFlat = nCurrIntraDescBITS < FLAT_REGION_BIT_COUNT * 2;
			if (bCurrRegionIsFlat)
				++nFlatRegionCount;
			const size_t nCurrWordOccIncr = (DEFAULT_LWORD_OCC_INCR + nModelResetCooldown) << int(bCurrRegionIsFlat || bBootstrapping);
			const size_t nCurrLocalWordUpdateRate = learningRateOverride > 0 ? (size_t)ceil(learningRateOverride) : bCurrRegionIsFlat ? (size_t)ceil(fCurrLearningRate + FEEDBACK_T_LOWER) / 2 : (size_t)ceil(fCurrLearningRate);
			const size_t nCurrTotColorDistThreshold = (size_t)(sqrt(fCurrDistThresholdFactor)*nMinColorDistThreshold) * 3;
			const size_t nCurrTotDescDistThreshold = (((size_t)1 << ((size_t)floor(fCurrDistThresholdFactor + 0.5f))) + nDescDistThresholdOffset + (bCurrRegionIsUnstable*UNSTAB_DESC_DIST_OFFSET)) * 3;
			size_t nLocalWordIdx = 0;
			float fPotentialLocalWordsWeightSum = 0.0f;
			float fLastLocalWordWeight = FLT_MAX;
			while (nLocalWordIdx < nCurrLocalWords && fPotentialLocalWordsWeightSum < fLocalWordsWeightSumThreshold)
			{
				LocalWord_3ch* pCurrLocalWord = (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
				const float fCurrLocalWordWeight = GetLocalWordWeight(pCurrLocalWord, nFrameIndex, nLocalWordWeightOffset);
				{
					const size_t nTotColorL1Dist = L1dist<3>(anCurrColor, pCurrLocalWord->oFeature.anColor);
					const size_t nColorDistortion = cdist<3>(anCurrColor, pCurrLocalWord->oFeature.anColor);
					const size_t nTotColorMixDist = cmixdist(nTotColorL1Dist, nColorDistortion);
					const size_t nTotIntraDescDist = hdist<3>(anCurrIntraDesc, pCurrLocalWord->oFeature.anDesc);
					const size_t anCurrInterLBSPThresholds[3] = { anLBSPThreshold_8bitLUT[pCurrLocalWord->oFeature.anColor[0]],anLBSPThreshold_8bitLUT[pCurrLocalWord->oFeature.anColor[1]],anLBSPThreshold_8bitLUT[pCurrLocalWord->oFeature.anColor[2]] };
					LBSP::computeRGBDescriptor(oInputImg, pCurrLocalWord->oFeature.anColor, nCurrImgCoord_X, nCurrImgCoord_Y, anCurrInterLBSPThresholds, anCurrInterDesc);
					const size_t nTotInterDescDist = hdist<3>(anCurrInterDesc, pCurrLocalWord->oFeature.anDesc);
					const size_t nTotDescDist = (nTotIntraDescDist + nTotInterDescDist) / 2;
					if ((!bCurrRegionIsUnstable || bCurrRegionIsFlat || bCurrRegionIsROIBorder)
						&& nTotColorMixDist <= nCurrTotColorDistThreshold
						&& nTotColorL1Dist >= nCurrTotColorDistThreshold / 2
						&& nTotIntraDescDist <= nCurrTotDescDistThreshold / 2
						&& (rand() % (nCurrRegionIllumUpdtVal ? (nCurrLocalWordUpdateRate / 2 + 1) : nCurrLocalWordUpdateRate)) == 0)
					{
						// == illum updt
						for (size_t c = 0; c < 3; ++c)
						{
							pCurrLocalWord->oFeature.anColor[c] = anCurrColor[c];
							pCurrLocalWord->oFeature.anDesc[c] = anCurrIntraDesc[c];
						}
						oIllumUpdtRegionMask.data[nPxIter - 1] = 1 & oROI.data[nPxIter - 1];
						oIllumUpdtRegionMask.data[nPxIter + 1] = 1 & oROI.data[nPxIter + 1];
						oIllumUpdtRegionMask.data[nPxIter] = 2;
					}
					if (nTotDescDist <= nCurrTotDescDistThreshold && nTotColorMixDist <= nCurrTotColorDistThreshold)
					{
						fPotentialLocalWordsWeightSum += fCurrLocalWordWeight;
						pCurrLocalWord->nLastOcc = nFrameIndex;
						if ((!oLastFGMask.data[nPxIter] || bUsingMovingCamera) && fCurrLocalWordWeight < DEFAULT_LWORD_MAX_WEIGHT)
							pCurrLocalWord->nOccurrences += nCurrWordOccIncr;
						nMinTotColorDist = std::min(nMinTotColorDist, nTotColorMixDist);
						nMinTotDescDist = std::min(nMinTotDescDist, nTotDescDist);
					}
				}
				if (fCurrLocalWordWeight > fLastLocalWordWeight)
				{
					std::swap(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1]);
				}
				else
					fLastLocalWordWeight = fCurrLocalWordWeight;
				++nLocalWordIdx;
			}
			while (nLocalWordIdx < nCurrLocalWords)
			{
				const float fCurrLocalWordWeight = GetLocalWordWeight(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], nFrameIndex, nLocalWordWeightOffset);
				if (fCurrLocalWordWeight > fLastLocalWordWeight)
				{
					std::swap(apLocalWordDict[nLocalDictIdx + nLocalWordIdx], apLocalWordDict[nLocalDictIdx + nLocalWordIdx - 1]);
				}
				else
					fLastLocalWordWeight = fCurrLocalWordWeight;
				++nLocalWordIdx;
			}
			if (fPotentialLocalWordsWeightSum >= fLocalWordsWeightSumThreshold || bCurrRegionIsROIBorder)
			{
				// == background
				const float fNormalizedMinDist = std::max((float)nMinTotColorDist / s_nColorMaxDataRange_3ch, (float)nMinTotDescDist / s_nDescMaxDataRange_3ch);
				fCurrMeanMinDist_LT = fCurrMeanMinDist_LT*(1.0f - fRollAvgFactor_LT) + fNormalizedMinDist*fRollAvgFactor_LT;
				fCurrMeanMinDist_ST = fCurrMeanMinDist_ST*(1.0f - fRollAvgFactor_ST) + fNormalizedMinDist*fRollAvgFactor_ST;
				fCurrMeanRawSegmRes_LT = fCurrMeanRawSegmRes_LT*(1.0f - fRollAvgFactor_LT);
				fCurrMeanRawSegmRes_ST = fCurrMeanRawSegmRes_ST*(1.0f - fRollAvgFactor_ST);
				if ((rand() % nCurrLocalWordUpdateRate) == 0)
				{
					size_t nGlobalWordLUTIdx;
					GlobalWord_3ch* pCurrGlobalWord = nullptr;
					for (nGlobalWordLUTIdx = 0; nGlobalWordLUTIdx < nCurrGlobalWords; ++nGlobalWordLUTIdx)
					{
						pCurrGlobalWord = (GlobalWord_3ch*)aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx];
						if (L1dist(nCurrIntraDescBITS, pCurrGlobalWord->nDescBITS) <= nCurrTotDescDistThreshold / GWORD_DESC_THRES_BITS_MATCH_FACTOR
							&& cmixdist<3>(anCurrColor, pCurrGlobalWord->oFeature.anColor) <= nCurrTotColorDistThreshold)
							break;
					}
					if (nGlobalWordLUTIdx != nCurrGlobalWords || (rand() % (nCurrLocalWordUpdateRate * 2)) == 0)
					{
						if (nGlobalWordLUTIdx == nCurrGlobalWords)
						{
							pCurrGlobalWord = (GlobalWord_3ch*)apGlobalWordDict[nCurrGlobalWords - 1];
							for (size_t c = 0; c < 3; ++c)
							{
								pCurrGlobalWord->oFeature.anColor[c] = anCurrColor[c];
								pCurrGlobalWord->oFeature.anDesc[c] = anCurrIntraDesc[c];
							}
							pCurrGlobalWord->nDescBITS = nCurrIntraDescBITS;
							pCurrGlobalWord->oSpatioOccMap = cv::Scalar(0.0f);
							pCurrGlobalWord->fLatestWeight = 0.0f;
						}
						float& fCurrGlobalWordLocalWeight = *(float*)(pCurrGlobalWord->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
						if (fCurrGlobalWordLocalWeight < fPotentialLocalWordsWeightSum)
						{
							pCurrGlobalWord->fLatestWeight += fPotentialLocalWordsWeightSum;
							fCurrGlobalWordLocalWeight += fPotentialLocalWordsWeightSum;
						}
					}
				}
			}
			else
			{
				// == foreground
				const float fNormalizedMinDist = std::max(std::max((float)nMinTotColorDist / s_nColorMaxDataRange_3ch, (float)nMinTotDescDist / s_nDescMaxDataRange_3ch), (fLocalWordsWeightSumThreshold - fPotentialLocalWordsWeightSum) / fLocalWordsWeightSumThreshold);
				fCurrMeanMinDist_LT = fCurrMeanMinDist_LT*(1.0f - fRollAvgFactor_LT) + fNormalizedMinDist*fRollAvgFactor_LT;
				fCurrMeanMinDist_ST = fCurrMeanMinDist_ST*(1.0f - fRollAvgFactor_ST) + fNormalizedMinDist*fRollAvgFactor_ST;
				fCurrMeanRawSegmRes_LT = fCurrMeanRawSegmRes_LT*(1.0f - fRollAvgFactor_LT) + fRollAvgFactor_LT;
				fCurrMeanRawSegmRes_ST = fCurrMeanRawSegmRes_ST*(1.0f - fRollAvgFactor_ST) + fRollAvgFactor_ST;
				if (bCurrRegionIsFlat || (rand() % nCurrLocalWordUpdateRate) == 0)
				{
					size_t nGlobalWordLUTIdx;
					GlobalWord_3ch* pCurrGlobalWord;
					for (nGlobalWordLUTIdx = 0; nGlobalWordLUTIdx < nCurrGlobalWords; ++nGlobalWordLUTIdx)
					{
						pCurrGlobalWord = (GlobalWord_3ch*)aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx];
						if (L1dist(nCurrIntraDescBITS, pCurrGlobalWord->nDescBITS) <= nCurrTotDescDistThreshold / GWORD_DESC_THRES_BITS_MATCH_FACTOR
							&& cmixdist<3>(anCurrColor, pCurrGlobalWord->oFeature.anColor) <= nCurrTotColorDistThreshold)
							break;
					}
					if (nGlobalWordLUTIdx == nCurrGlobalWords)
						nCurrRegionSegmVal = UCHAR_MAX;
					else
					{
						const float fGlobalWordLocalizedWeight = *(float*)(pCurrGlobalWord->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
						if (fPotentialLocalWordsWeightSum + fGlobalWordLocalizedWeight / (bCurrRegionIsFlat ? 2 : 4) < fLocalWordsWeightSumThreshold)
							nCurrRegionSegmVal = UCHAR_MAX;
					}
				}
				else
					nCurrRegionSegmVal = UCHAR_MAX;
				if (fPotentialLocalWordsWeightSum < DEFAULT_LWORD_INIT_WEIGHT)
				{
					const size_t nNewLocalWordIdx = nCurrLocalWords - 1;
					LocalWord_3ch* pNewLocalWord = (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx + nNewLocalWordIdx];
					for (size_t c = 0; c < 3; ++c)
					{
						pNewLocalWord->oFeature.anColor[c] = anCurrColor[c];
						pNewLocalWord->oFeature.anDesc[c] = anCurrIntraDesc[c];
					}
					pNewLocalWord->nOccurrences = nCurrWordOccIncr;
					pNewLocalWord->nFirstOcc = nFrameIndex;
					pNewLocalWord->nLastOcc = nFrameIndex;
				}
			}
			// == neighb updt
			if ((!nCurrRegionSegmVal && (rand() % nCurrLocalWordUpdateRate) == 0) || bCurrRegionIsROIBorder || bUsingMovingCamera)
			{
				//if((!nCurrRegionSegmVal && (rand()%(nCurrRegionIllumUpdtVal?(nCurrLocalWordUpdateRate/2+1):nCurrLocalWordUpdateRate))==0) || bCurrRegionIsROIBorder) {
				int nSampleImgCoord_Y, nSampleImgCoord_X;
				if (bCurrRegionIsFlat || bCurrRegionIsROIBorder || bUsingMovingCamera)
					getRandNeighborPosition_5x5(nSampleImgCoord_X, nSampleImgCoord_Y, nCurrImgCoord_X, nCurrImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
				else
					getRandNeighborPosition_3x3(nSampleImgCoord_X, nSampleImgCoord_Y, nCurrImgCoord_X, nCurrImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
				const size_t nSamplePxIdx = oImgSize.width*nSampleImgCoord_Y + nSampleImgCoord_X;
				if (oROI.data[nSamplePxIdx])
				{
					const size_t nNeighborLocalDictIdx = aPxInfoLUT_PAWCS[nSamplePxIdx].nModelIdx*nCurrLocalWords;
					size_t nNeighborLocalWordIdx = 0;
					float fNeighborPotentialLocalWordsWeightSum = 0.0f;
					while (nNeighborLocalWordIdx < nCurrLocalWords && fNeighborPotentialLocalWordsWeightSum < fLocalWordsWeightSumThreshold)
					{
						LocalWord_3ch* pNeighborLocalWord = (LocalWord_3ch*)apLocalWordDict[nNeighborLocalDictIdx + nNeighborLocalWordIdx];
						const size_t nNeighborTotColorL1Dist = L1dist<3>(anCurrColor, pNeighborLocalWord->oFeature.anColor);
						const size_t nNeighborColorDistortion = cdist<3>(anCurrColor, pNeighborLocalWord->oFeature.anColor);
						const size_t nNeighborTotColorMixDist = cmixdist(nNeighborTotColorL1Dist, nNeighborColorDistortion);
						const size_t nNeighborTotIntraDescDist = hdist<3>(anCurrIntraDesc, pNeighborLocalWord->oFeature.anDesc);
						const bool bNeighborRegionIsFlat = popcount<3>(pNeighborLocalWord->oFeature.anDesc) < FLAT_REGION_BIT_COUNT * 2;
						const size_t nNeighborWordOccIncr = bNeighborRegionIsFlat ? nCurrWordOccIncr * 2 : nCurrWordOccIncr;
						if (nNeighborTotColorMixDist <= nCurrTotColorDistThreshold && nNeighborTotIntraDescDist <= nCurrTotDescDistThreshold)
						{
							const float fNeighborLocalWordWeight = GetLocalWordWeight(pNeighborLocalWord, nFrameIndex, nLocalWordWeightOffset);
							fNeighborPotentialLocalWordsWeightSum += fNeighborLocalWordWeight;
							pNeighborLocalWord->nLastOcc = nFrameIndex;
							if (fNeighborLocalWordWeight < DEFAULT_LWORD_MAX_WEIGHT)
								pNeighborLocalWord->nOccurrences += nNeighborWordOccIncr;
						}
						else if (!oCurrFGMask.data[nSamplePxIdx] && bCurrRegionIsFlat && (bBootstrapping || (rand() % nCurrLocalWordUpdateRate) == 0))
						{
							const size_t nSamplePxRGBIdx = nSamplePxIdx * 3;
							const size_t nSampleDescRGBIdx = nSamplePxRGBIdx * 2;
							ushort* anNeighborLastIntraDesc = ((ushort*)(oLastDescFrame.data + nSampleDescRGBIdx));
							const size_t nNeighborTotLastIntraDescDist = hdist<3>(anCurrIntraDesc, anNeighborLastIntraDesc);
							if (nNeighborTotColorMixDist <= nCurrTotColorDistThreshold && nNeighborTotLastIntraDescDist <= nCurrTotDescDistThreshold / 2)
							{
								const float fNeighborLocalWordWeight = GetLocalWordWeight(pNeighborLocalWord, nFrameIndex, nLocalWordWeightOffset);
								fNeighborPotentialLocalWordsWeightSum += fNeighborLocalWordWeight;
								pNeighborLocalWord->nLastOcc = nFrameIndex;
								if (fNeighborLocalWordWeight < DEFAULT_LWORD_MAX_WEIGHT)
									pNeighborLocalWord->nOccurrences += nNeighborWordOccIncr;
								for (size_t c = 0; c < 3; ++c)
									pNeighborLocalWord->oFeature.anDesc[c] = anCurrIntraDesc[c];
							}
							else
							{
								const bool bNeighborLastRegionIsFlat = popcount<3>(anNeighborLastIntraDesc) < FLAT_REGION_BIT_COUNT * 2;
								if (bNeighborLastRegionIsFlat && bCurrRegionIsFlat &&
									nNeighborTotLastIntraDescDist + nNeighborTotIntraDescDist <= nCurrTotDescDistThreshold &&
									nNeighborColorDistortion <= nCurrTotColorDistThreshold / 4)
								{
									const float fNeighborLocalWordWeight = GetLocalWordWeight(pNeighborLocalWord, nFrameIndex, nLocalWordWeightOffset);
									fNeighborPotentialLocalWordsWeightSum += fNeighborLocalWordWeight;
									pNeighborLocalWord->nLastOcc = nFrameIndex;
									if (fNeighborLocalWordWeight < DEFAULT_LWORD_MAX_WEIGHT)
										pNeighborLocalWord->nOccurrences += nNeighborWordOccIncr;
									for (size_t c = 0; c < 3; ++c)
										pNeighborLocalWord->oFeature.anColor[c] = anCurrColor[c];
								}
							}
						}
						++nNeighborLocalWordIdx;
					}
					if (fNeighborPotentialLocalWordsWeightSum < DEFAULT_LWORD_INIT_WEIGHT)
					{
						nNeighborLocalWordIdx = nCurrLocalWords - 1;
						LocalWord_3ch* pNeighborLocalWord = (LocalWord_3ch*)apLocalWordDict[nNeighborLocalDictIdx + nNeighborLocalWordIdx];
						for (size_t c = 0; c < 3; ++c)
						{
							pNeighborLocalWord->oFeature.anColor[c] = anCurrColor[c];
							pNeighborLocalWord->oFeature.anDesc[c] = anCurrIntraDesc[c];
						}
						pNeighborLocalWord->nOccurrences = nCurrWordOccIncr;
						pNeighborLocalWord->nFirstOcc = nFrameIndex;
						pNeighborLocalWord->nLastOcc = nFrameIndex;
					}
				}
			}
			if (nCurrRegionIllumUpdtVal)
				nCurrRegionIllumUpdtVal -= 1;
			// == feedback adj
			bCurrRegionIsUnstable = fCurrDistThresholdFactor > UNSTABLE_REG_RDIST_MIN || (fCurrMeanRawSegmRes_LT - fCurrMeanFinalSegmRes_LT) > UNSTABLE_REG_RATIO_MIN || (fCurrMeanRawSegmRes_ST - fCurrMeanFinalSegmRes_ST) > UNSTABLE_REG_RATIO_MIN;
			if (oLastFGMask.data[nPxIter] || (std::min(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST) < UNSTABLE_REG_RATIO_MIN && nCurrRegionSegmVal))
				fCurrLearningRate = std::min(fCurrLearningRate + FEEDBACK_T_INCR / (std::max(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST)*fCurrDistThresholdVariationFactor), FEEDBACK_T_UPPER);
			else
				fCurrLearningRate = std::max(fCurrLearningRate - FEEDBACK_T_DECR*fCurrDistThresholdVariationFactor / std::max(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST), FEEDBACK_T_LOWER);
			if (std::max(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST) > UNSTABLE_REG_RATIO_MIN && oBlinksFrame.data[nPxIter])
				(fCurrDistThresholdVariationFactor) += bBootstrapping ? FEEDBACK_V_INCR * 2 : FEEDBACK_V_INCR;
			else
				fCurrDistThresholdVariationFactor = std::max(fCurrDistThresholdVariationFactor - FEEDBACK_V_DECR*((bBootstrapping || bCurrRegionIsFlat) ? 2 : oLastFGMask.data[nPxIter] ? 0.5f : 1), FEEDBACK_V_DECR);
			if (fCurrDistThresholdFactor < std::pow(1.0f + std::min(fCurrMeanMinDist_LT, fCurrMeanMinDist_ST) * 2, 2))
				fCurrDistThresholdFactor += FEEDBACK_R_VAR*(fCurrDistThresholdVariationFactor - FEEDBACK_V_DECR);
			else
				fCurrDistThresholdFactor = std::max(fCurrDistThresholdFactor - FEEDBACK_R_VAR / fCurrDistThresholdVariationFactor, 1.0f);
			for (size_t c = 0; c < 3; ++c)
			{
				anLastIntraDesc[c] = anCurrIntraDesc[c];
				anLastColor[c] = anCurrColor[c];
			}
		}
	}
	const bool bRecalcGlobalWords = !(nFrameIndex % (nCurrGlobalWordUpdateRate << 5));
	const bool bUpdateGlobalWords = !(nFrameIndex % (nCurrGlobalWordUpdateRate));
	cv::Mat oLastFGMask_dilated_inverted_downscaled;
	if (bUpdateGlobalWords)
		cv::resize(oLastFGMask_dilated_inverted, oLastFGMask_dilated_inverted_downscaled, oDownSampledFrameSize_GlobalWordLookup, 0, 0, cv::INTER_NEAREST);
	for (size_t nGlobalWordIdx = 0; nGlobalWordIdx < nCurrGlobalWords; ++nGlobalWordIdx)
	{
		if (bRecalcGlobalWords && apGlobalWordDict[nGlobalWordIdx]->fLatestWeight > 0.0f)
		{
			apGlobalWordDict[nGlobalWordIdx]->fLatestWeight = GetGlobalWordWeight(apGlobalWordDict[nGlobalWordIdx]);
			if (apGlobalWordDict[nGlobalWordIdx]->fLatestWeight < 1.0f)
			{
				apGlobalWordDict[nGlobalWordIdx]->fLatestWeight = 0.0f;
				apGlobalWordDict[nGlobalWordIdx]->oSpatioOccMap = cv::Scalar(0.0f);
			}
		}
		if (bUpdateGlobalWords && apGlobalWordDict[nGlobalWordIdx]->fLatestWeight > 0.0f)
		{
			cv::accumulateProduct(apGlobalWordDict[nGlobalWordIdx]->oSpatioOccMap, oTempGlobalWordWeightDiffFactor, apGlobalWordDict[nGlobalWordIdx]->oSpatioOccMap, oLastFGMask_dilated_inverted_downscaled);
			apGlobalWordDict[nGlobalWordIdx]->fLatestWeight *= 0.9f;
			cv::blur(apGlobalWordDict[nGlobalWordIdx]->oSpatioOccMap, apGlobalWordDict[nGlobalWordIdx]->oSpatioOccMap, cv::Size(3, 3), cv::Point(-1, -1), cv::BORDER_REPLICATE);
		}
		if (nGlobalWordIdx > 0 && apGlobalWordDict[nGlobalWordIdx]->fLatestWeight > apGlobalWordDict[nGlobalWordIdx - 1]->fLatestWeight)
			std::swap(apGlobalWordDict[nGlobalWordIdx], apGlobalWordDict[nGlobalWordIdx - 1]);
	}
	if (bUpdateGlobalWords)
	{
		for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			const size_t nGlobalWordMapLookupIdx = aPxInfoLUT_PAWCS[nPxIter].nGlobalWordMapLookupIdx;
			float fLastGlobalWordLocalWeight = *(float*)(aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[0]->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
			for (size_t nGlobalWordLUTIdx = 1; nGlobalWordLUTIdx < nCurrGlobalWords; ++nGlobalWordLUTIdx)
			{
				const float fCurrGlobalWordLocalWeight = *(float*)(aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx]->oSpatioOccMap.data + nGlobalWordMapLookupIdx);
				if (fCurrGlobalWordLocalWeight > fLastGlobalWordLocalWeight)
					std::swap(aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx], aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT[nGlobalWordLUTIdx - 1]);
				else
					fLastGlobalWordLocalWeight = fCurrGlobalWordLocalWeight;
			}
		}
	}
	cv::bitwise_xor(oCurrFGMask, oLastRawFGMask, oCurrRawFGBlinkMask);
	cv::bitwise_or(oCurrRawFGBlinkMask, oLastRawFGBlinkMask, oBlinksFrame);
	oCurrRawFGBlinkMask.copyTo(oLastRawFGBlinkMask);
	oCurrFGMask.copyTo(oLastRawFGMask);
	cv::morphologyEx(oCurrFGMask, oFGMask_PreFlood, cv::MORPH_CLOSE, oMorphExStructElement);
	oFGMask_PreFlood.copyTo(oFGMask_FloodedHoles);
	cv::floodFill(oFGMask_FloodedHoles, cv::Point(0, 0), UCHAR_MAX);
	cv::bitwise_not(oFGMask_FloodedHoles, oFGMask_FloodedHoles);
	cv::erode(oFGMask_PreFlood, oFGMask_PreFlood, cv::Mat(), cv::Point(-1, -1), 3);
	cv::bitwise_or(oCurrFGMask, oFGMask_FloodedHoles, oCurrFGMask);
	cv::bitwise_or(oCurrFGMask, oFGMask_PreFlood, oCurrFGMask);
	cv::medianBlur(oCurrFGMask, oLastFGMask, nMedianBlurKernelSize);
	cv::dilate(oLastFGMask, oLastFGMask_dilated, cv::Mat(), cv::Point(-1, -1), 3);
	cv::bitwise_and(oBlinksFrame, oLastFGMask_dilated_inverted, oBlinksFrame);
	cv::bitwise_not(oLastFGMask_dilated, oLastFGMask_dilated_inverted);
	cv::bitwise_and(oBlinksFrame, oLastFGMask_dilated_inverted, oBlinksFrame);
	oLastFGMask.copyTo(oCurrFGMask);
	cv::addWeighted(oMeanFinalSegmResFrame_LT, (1.0f - fRollAvgFactor_LT), oLastFGMask, (1.0 / UCHAR_MAX)*fRollAvgFactor_LT, 0, oMeanFinalSegmResFrame_LT, CV_32F);
	cv::addWeighted(oMeanFinalSegmResFrame_ST, (1.0f - fRollAvgFactor_ST), oLastFGMask, (1.0 / UCHAR_MAX)*fRollAvgFactor_ST, 0, oMeanFinalSegmResFrame_ST, CV_32F);
	const float fCurrNonFlatRegionRatio = (float)(nTotRelevantPxCount - nFlatRegionCount) / nTotRelevantPxCount;
	if (fCurrNonFlatRegionRatio < LBSPDESC_RATIO_MIN && fLastNonFlatRegionRatio < LBSPDESC_RATIO_MIN)
	{
		for (size_t t = 0; t <= UCHAR_MAX; ++t)
			if (anLBSPThreshold_8bitLUT[t] > cv::saturate_cast<uchar>((nLBSPThresholdOffset + t*fRelLBSPThreshold) / 4))
				--anLBSPThreshold_8bitLUT[t];
	}
	else if (fCurrNonFlatRegionRatio > LBSPDESC_RATIO_MAX && fLastNonFlatRegionRatio > LBSPDESC_RATIO_MAX)
	{
		for (size_t t = 0; t <= UCHAR_MAX; ++t)
			if (anLBSPThreshold_8bitLUT[t] < cv::saturate_cast<uchar>(nLBSPThresholdOffset + UCHAR_MAX*fRelLBSPThreshold))
				++anLBSPThreshold_8bitLUT[t];
	}
	fLastNonFlatRegionRatio = fCurrNonFlatRegionRatio;
	cv::resize(oInputImg, oDownSampledFrame_MotionAnalysis, oDownSampledFrameSize_MotionAnalysis, 0, 0, cv::INTER_AREA);
	cv::accumulateWeighted(oDownSampledFrame_MotionAnalysis, oMeanDownSampledLastDistFrame_LT, fRollAvgFactor_LT);
	cv::accumulateWeighted(oDownSampledFrame_MotionAnalysis, oMeanDownSampledLastDistFrame_ST, fRollAvgFactor_ST);
	const float fCurrMeanL1DistRatio = L1dist((float*)oMeanDownSampledLastDistFrame_LT.data, (float*)oMeanDownSampledLastDistFrame_ST.data, oMeanDownSampledLastDistFrame_LT.total(), nImgChannels, oDownSampledROI_MotionAnalysis.data) / nDownSampledROIPxCount;
	if (!bAutoModelResetEnabled && fCurrMeanL1DistRatio >= FRAMELEVEL_MIN_L1DIST_THRES * 2)
		bAutoModelResetEnabled = true;
	if (bAutoModelResetEnabled || bUsingMovingCamera)
	{
		if ((nFrameIndex%DEFAULT_BOOTSTRAP_WIN_SIZE) == 0)
		{
			cv::Mat oCurrBackgroundImg, oDownSampledBackgroundImg;
			getBackgroundImage(oCurrBackgroundImg);
			cv::resize(oCurrBackgroundImg, oDownSampledBackgroundImg, oDownSampledFrameSize_MotionAnalysis, 0, 0, cv::INTER_AREA);
			cv::Mat oDownSampledBackgroundImg_32F; oDownSampledBackgroundImg.convertTo(oDownSampledBackgroundImg_32F, CV_32F);
			const float fCurrModelL1DistRatio = L1dist((float*)oMeanDownSampledLastDistFrame_LT.data, (float*)oDownSampledBackgroundImg_32F.data, oMeanDownSampledLastDistFrame_LT.total(), nImgChannels, cv::Mat(oDownSampledROI_MotionAnalysis == UCHAR_MAX).data) / nDownSampledROIPxCount;
			const float fCurrModelCDistRatio = cdist((float*)oMeanDownSampledLastDistFrame_LT.data, (float*)oDownSampledBackgroundImg_32F.data, oMeanDownSampledLastDistFrame_LT.total(), nImgChannels, cv::Mat(oDownSampledROI_MotionAnalysis == UCHAR_MAX).data) / nDownSampledROIPxCount;
			if (bUsingMovingCamera && fCurrModelL1DistRatio < FRAMELEVEL_MIN_L1DIST_THRES / 4 && fCurrModelCDistRatio < FRAMELEVEL_MIN_CDIST_THRES / 4)
			{
				nLocalWordWeightOffset = DEFAULT_LWORD_WEIGHT_OFFSET;
				bUsingMovingCamera = false;
				refreshModel(1, 1, true);
			}
			else if (bBootstrapping && !bUsingMovingCamera && (fCurrModelL1DistRatio >= FRAMELEVEL_MIN_L1DIST_THRES || fCurrModelCDistRatio >= FRAMELEVEL_MIN_CDIST_THRES))
			{
				nLocalWordWeightOffset = 5;
				bUsingMovingCamera = true;
				refreshModel(1, 1, true);
			}
		}
		if (nFramesSinceLastReset > DEFAULT_BOOTSTRAP_WIN_SIZE * 2)
			bAutoModelResetEnabled = false;
		else if (fCurrMeanL1DistRatio >= FRAMELEVEL_MIN_L1DIST_THRES && nModelResetCooldown == 0)
		{
			nFramesSinceLastReset = 0;
			refreshModel(nLocalWordWeightOffset / 8, 0, true);
			nModelResetCooldown = nCurrSamplesForMovingAvg_ST;
			oUpdateRateFrame = cv::Scalar(1.0f);
		}
		else if (!bBootstrapping)
			++nFramesSinceLastReset;
	}
	if (nModelResetCooldown > 0)
		--nModelResetCooldown;

	//filterMaskByColor(oCurrFGMask, oInputImg);
	//cv::morphologyEx(oCurrFGMask, oCurrFGMask, CV_MOP_CLOSE, closingElement, cv::Point(-1, -1), 1, cv::BORDER_CONSTANT);

	stopApply();
}

void BgsPAWCS::CleanupDictionaries()
{
	if (aLocalWordList_1ch)
	{
		delete[] aLocalWordList_1ch;
		aLocalWordList_1ch = nullptr;
		pLocalWordListIter_1ch = nullptr;
	}
	else if (aLocalWordList_3ch)
	{
		delete[] aLocalWordList_3ch;
		aLocalWordList_3ch = nullptr;
		pLocalWordListIter_3ch = nullptr;
	}
	if (apLocalWordDict)
	{
		delete[] apLocalWordDict;
		apLocalWordDict = nullptr;
	}
	if (aGlobalWordList_1ch)
	{
		delete[] aGlobalWordList_1ch;
		aGlobalWordList_1ch = nullptr;
		pGlobalWordListIter_1ch = nullptr;
	}
	else if (aGlobalWordList_3ch)
	{
		delete[] aGlobalWordList_3ch;
		aGlobalWordList_3ch = nullptr;
		pGlobalWordListIter_3ch = nullptr;
	}
	if (apGlobalWordDict)
	{
		delete[] apGlobalWordDict;
		apGlobalWordDict = nullptr;
	}
	if (aPxInfoLUT_PAWCS)
	{
		for (size_t nPxIter = 0; nPxIter < nTotPxCount; ++nPxIter)
			delete[] aPxInfoLUT_PAWCS[nPxIter].apGlobalDictSortLUT;
		delete[] aPxInfoLUT_PAWCS;
		aPxInfoLUT = nullptr;
		aPxInfoLUT_PAWCS = nullptr;
	}
	if (aPxIdxLUT)
	{
		delete[] aPxIdxLUT;
		aPxIdxLUT = nullptr;
	}
}

float BgsPAWCS::GetLocalWordWeight(const LocalWordBase* w, size_t nCurrFrame, size_t nOffset)
{
	return (float)(w->nOccurrences) / ((w->nLastOcc - w->nFirstOcc) + (nCurrFrame - w->nLastOcc) * 2 + nOffset);
}

float BgsPAWCS::GetGlobalWordWeight(const GlobalWordBase* w)
{
	return (float)cv::sum(w->oSpatioOccMap).val[0];
}

void BgsPAWCS::setAutomaticModelReset(bool bVal)
{
	bAutoModelResetEnabled = bVal;
}
void BgsPAWCS::getBackgroundImage(cv::OutputArray backgroundImage) const
{ 
	CV_Assert(bInitialized);
	cv::Mat oAvgBGImg = cv::Mat::zeros(oImgSize, CV_32FC((int)nImgChannels));
	for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
	{
		const size_t nPxIter = aPxIdxLUT[nModelIter];
		const size_t nLocalDictIdx = nModelIter*nCurrLocalWords;
		const int nCurrImgCoord_X = aPxInfoLUT_PAWCS[nPxIter].nImgCoord_X;
		const int nCurrImgCoord_Y = aPxInfoLUT_PAWCS[nPxIter].nImgCoord_Y;
		if (nImgChannels == 1)
		{
			float fTotWeight = 0.0f;
			float fTotColor = 0.0f;
			for (size_t nLocalWordIdx = 0; nLocalWordIdx < nCurrLocalWords; ++nLocalWordIdx)
			{
				LocalWord_1ch* pCurrLocalWord = (LocalWord_1ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
				float fCurrWeight = GetLocalWordWeight(pCurrLocalWord, nFrameIndex, nLocalWordWeightOffset);
				fTotColor += (float)pCurrLocalWord->oFeature.anColor[0] * fCurrWeight;
				fTotWeight += fCurrWeight;
			}
			oAvgBGImg.at<float>(nCurrImgCoord_Y, nCurrImgCoord_X) = fTotColor / fTotWeight;
		}
		else
		{ 
			float fTotWeight = 0.0f;
			float fTotColor[3] = { 0.0f,0.0f,0.0f };
			for (size_t nLocalWordIdx = 0; nLocalWordIdx < nCurrLocalWords; ++nLocalWordIdx)
			{
				LocalWord_3ch* pCurrLocalWord = (LocalWord_3ch*)apLocalWordDict[nLocalDictIdx + nLocalWordIdx];
				float fCurrWeight = GetLocalWordWeight(pCurrLocalWord, nFrameIndex, nLocalWordWeightOffset);
				for (size_t c = 0; c < 3; ++c)
					fTotColor[c] += (float)pCurrLocalWord->oFeature.anColor[c] * fCurrWeight;
				fTotWeight += fCurrWeight;
			}
			oAvgBGImg.at<cv::Vec3f>(nCurrImgCoord_Y, nCurrImgCoord_X) = cv::Vec3f(fTotColor[0] / fTotWeight, fTotColor[1] / fTotWeight, fTotColor[2] / fTotWeight);
		}
	}
	oAvgBGImg.convertTo(backgroundImage, CV_8U);
}
void BgsPAWCS::loadParams()
{
	{
		int vali;
		float valf;
		cv::FileStorage file(".\\BGS.xml", cv::FileStorage::READ);

		file["isHSV"] >> isHSV;

		file.release();
	}

	{
		cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
		file["colorMinBGRA"] >> colorMin;
		file["colorMaxBGRA"] >> colorMax;
		file.release();
	}

	if (showParams)
	{
		std::cout << "Color filter min [BGRA]: " << colorMin << std::endl;
		std::cout << "Color filter max [BGRA]: " << colorMax << std::endl << std::endl;

		std::cout << "PAWCS -> \t" << std::endl;

		showParams = false;
	}
}