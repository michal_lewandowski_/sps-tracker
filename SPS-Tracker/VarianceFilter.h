#pragma once

#include "ICascadeFilter.h"

class DetectionResult;

class VarianceFilter : public ICascadeFilter
{
public:
	VarianceFilter();
	virtual ~VarianceFilter();

	virtual void init();
	virtual void release();

	virtual bool filter(int idx, FieldList& fields);

	float calcVariance(int* win);
	float calcVariance(cv::Rect* win);
};

