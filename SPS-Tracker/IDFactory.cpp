#include "stdafx.h"
#include "IDFactory.h"

std::map<int, size_t> IDFactory::s_nextIDs;
const size_t IDFactory::s_maxID2d = 999;
const size_t IDFactory::s_maxID3d = 100;

size_t IDFactory::generateID(int key)
{
	const auto& it = s_nextIDs.find(key);
	if(it == s_nextIDs.end())
	{
		s_nextIDs.insert(std::make_pair(key, 1));
	}
	++s_nextIDs[key];
	
	if ((key & 1) == 1 && s_nextIDs[key] > s_maxID2d)
		s_nextIDs[key] = 1;
	if ((key & 4) == 4 && s_nextIDs[key] > s_maxID3d)
		s_nextIDs[key] = 1;

	return s_nextIDs[key];
}
void IDFactory::reset(void)
{
	s_nextIDs.clear();
}