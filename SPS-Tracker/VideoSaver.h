#pragma once
#include <string>
#include <vector>
#include <core/mat.hpp>
#include "CameraCalibration.h"
#include "TimeWatch.hpp"
#include "IBGS.h"
#include "Field.h"

class VideoSaver
{
public:
	VideoSaver();
	virtual ~VideoSaver();

	void addJob(std::string name, std::string videoFile, std::string configFile, std::string inputDir, std::string outputFile);
	void process();
	void setFields(const std::vector<std::pair<cv::Rect, std::string>>& f, int margin, int numCameras);
	void init();
	void handleKey();

	inline int64_t time() { return systemTime.getFrameNum(); };
private:
	TimeWatch systemTime;
	FieldList fields;

	std::vector<std::string> names;
	std::vector<std::string> videoFiles;
	std::vector<std::string> configFiles;
	std::vector<std::string> outputFiles;
	std::string inputDir;

	std::vector<CameraCalibration> calibs;
	std::vector<cv::VideoCapture> caps;

	std::vector<cv::VideoWriter> vid1;
	std::vector<cv::VideoWriter> vid2;

	bool stop;
	bool step;

	std::array<std::unique_ptr<IBGS>, 4> bgs;

	std::vector<cv::Mat> masks;
	cv::Mat closingElement;
	float factorH;
	float factorS;
	float factorV;
	bool factor;

	cv::Scalar colorMin;
	cv::Scalar colorMax;
	int code;
};

