#pragma once
#include <opencv.hpp>
#include <map>
#include <functional>
#include "MovingObject.h"
#include <memory>
#include "Track.h"
#include "KFTrack2d.h"

class Worker;

class Detector
{
public:
	Detector(Worker* w);
	virtual ~Detector();

	void detect(const cv::Mat& frame, const cv::Mat& mask, const std::deque<std::shared_ptr<KFTrack2d>>& tracks, std::vector<std::shared_ptr<MovingObject>>& dets);
	inline double sizeRatioThreshold(void) const { return ballSizeRatioThr; };

protected:
	double calcDistance(double u, double v, double x0, double y0, double w, double h) const;

protected:
	Worker* worker;
	cv::Mat maskProc;
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;

public:
	double ballSizeRatioThr;
	double ballMinDiameterMM;
	double ballMaxDiameterMM;
	static double ballProbailityThr;
	std::map<double, double, std::greater<double>> errorThresholds;
};
