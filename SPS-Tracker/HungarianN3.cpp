#include "stdafx.h"
#include "HungarianN3.h"
#include <assert.h>
#include "iPrimal.h"

namespace cv
{
	class Mat;
}

using namespace xray;

size_t EXPOSED = std::numeric_limits<size_t>::max();
size_t MINUS_1 = std::numeric_limits<size_t>::max() - 1;
size_t MINUS_2 = std::numeric_limits<size_t>::max() - 2;

HungarianN3::HungarianN3(const std::vector<std::vector<double>>& utility)
	: costMatrix()
	, n(0)
	, max_match(0) //number of vertices in current matching
	, lx()
	, ly()
	, xy()
	, yx()
	, S()
	, T()
	, slack()
	, slackx()
	, prev()
	, originalWorkers(utility.size())
	, originalJobs(utility[0].size())
	, cost(0)
{
	assert(utility.size() > 0);
	size_t row_size = utility.size();
	size_t col_size = utility[0].size();
	costMatrix.resize(row_size);
	for (size_t i = 0; i<row_size; ++i)
	{
		std::vector<double>& costMatrix_i_ = costMatrix[i];
		costMatrix_i_.resize(col_size);
		for (size_t j = 0; j < col_size; ++j)
		{
			costMatrix_i_[j] = -utility[i][j];
		}
	}

	if (col_size < row_size)
	{
		double min = -utility[0][0];
		for (size_t i = 0; i<row_size; ++i)
		{
			for (size_t j = 0; j < col_size; ++j)
			{
				if (-utility[i][j] < min)
				{
					min = -utility[i][j];
				}
			}
		}
		//extend by min columns
		for (size_t i = 0; i<row_size; ++i)
		{
			costMatrix[i].resize(row_size);
			for (size_t j = col_size; j<row_size; ++j)
			{
				costMatrix[i][j] = min;
			}
		}
		col_size = row_size;
		cost += min * (row_size - col_size);
	}
	if (row_size < col_size)
	{
		double min = -utility[0][0];
		for (size_t i = 0; i<row_size; ++i)
		{
			for (size_t j = 0; j < col_size; ++j)
			{
				if (-utility[i][j] < min)
				{
					min = -utility[i][j];
				}
			}
		}
		//extend by min rows
		costMatrix.resize(col_size);
		for (size_t i = row_size; i<col_size; ++i)
		{
			costMatrix[i].resize(col_size);
			for (size_t j = 0; j<col_size; ++j)
			{
				costMatrix[i][j] = min;
			}
		}
		row_size = col_size;
	}
	n = row_size;
	lx.resize(n);
	ly.resize(n);
	xy.resize(n);
	yx.resize(n);
	S.resize(n);
	T.resize(n);
	slack.resize(n);
	slackx.resize(n);
	prev.resize(n);

#ifndef NDEBUG
	for (double l : ly)
	{
		assert(l == 0);
	}
#endif
	for (size_t x = 0; x < n; ++x)
	{
		const std::vector<double>& cost_x_ = costMatrix[x];
		double max = cost_x_[0];
		for (size_t y = 1; y < n; ++y)
		{
			if (max < cost_x_[y])
			{
				max = cost_x_[y];
			}
		}
		lx[x] = max;
	}
	for (size_t x = 0; x < n; ++x)
	{
		xy[x] = EXPOSED;
	}
	for (size_t y = 0; y < n; ++y)
	{
		yx[y] = EXPOSED;
	}
}

HungarianN3::~HungarianN3()
{
}

void HungarianN3::update_labels()
{
	double delta = std::numeric_limits<double>::max(); //init delta as infinity
	for (size_t y = 0; y < n; ++y)
	{           //calculate delta using slack
		if (!T[y] && delta > slack[y])
		{
			delta = slack[y];
		}
	}
	for (size_t x = 0; x < n; ++x)
	{            //update X labels
		if (S[x])
		{
			lx[x] -= delta;
		}
	}
	for (size_t y = 0; y < n; ++y)
	{
		if (T[y])
		{
			//update Y labels
			ly[y] += delta;
		}
		else
		{
			//update slack array
			slack[y] -= delta;
		}
	}
}

void HungarianN3::add_to_tree(size_t x, size_t prevx)
//x - current vertex,prevx - vertex from X before x in the alternating path,
//so we add edges (prevx, xy[x]), (xy[x], x)
{
	S[x] = true;                    //add x to S
	prev[x] = prevx;                //we need this when augmenting
	for (size_t y = 0; y < n; ++y)
	{   //update slacks, because we add new vertex to S
		if (lx[x] + ly[y] - costMatrix[x][y] < slack[y])
		{
			slack[y] = lx[x] + ly[y] - costMatrix[x][y];
			slackx[y] = x;
		}
	}
}

void HungarianN3::augment()                         //main function of the algorithm
{
	if (max_match == n)
	{
		return;        //check wether matching is already perfect
	}
	size_t x, y;
	{
		size_t root = 0;                    //just counters and root vertex
		std::vector<size_t> q(n);
		size_t wr = 0, rd = 0;          //q - queue for bfs, wr,rd - write and read
										//pos in queue
		for (size_t i = 0; i < S.size(); ++i)
		{
			S[i] = false;
		}
		for (size_t i = 0; i < T.size(); ++i)
		{
			T[i] = false;
		}
		for (size_t i = 0; i < T.size(); ++i)
		{
			prev[i] = MINUS_1;
		}

		for (x = 0; x < n; ++x)            //finding root of the tree
			if (xy[x] == EXPOSED)
			{
				q[wr++] = root = x;
				prev[x] = MINUS_2;
				S[x] = true;
				break;
			}

		for (y = 0; y < n; ++y)            //initializing slack array
		{
			slack[y] = lx[root] + ly[y] - costMatrix[root][y];
			slackx[y] = root;
		}

		//second part of augment() function
		for (;;)                                                        //main cycle
		{
			while (rd < wr)                                                 //building tree with bfs cycle
			{
				x = q[rd++];                                                //current vertex from X part
				for (y = 0; y < n; y++)                                     //iterate through all edges in equality graph
					if (costMatrix[x][y] == lx[x] + ly[y] && !T[y])
					{
						if (yx[y] == EXPOSED) break;                             //an exposed vertex in Y found, so
																				 //augmenting path exists!
						T[y] = true;                                        //else just add y to T,
						q[wr++] = yx[y];                                    //add vertex yx[y], which is matched
																			//with y, to the queue
						add_to_tree(yx[y], x);                              //add edges (x,y) and (y,yx[y]) to the tree
					}
				if (y < n) break;                                           //augmenting path found!
			}
			if (y < n) break;                                               //augmenting path found!

			update_labels();                                                //augmenting path not found, so improve labeling
			wr = rd = 0;
			for (y = 0; y < n; y++)
				//in this cycle we add edges that were added to the equality graph as a
				//result of improving the labeling, we add edge (slackx[y], y) to the tree if
				//and only if !T[y] &&  slack[y] == 0, also with this edge we add another one
				//(y, yx[y]) or augment the matching, if y was exposed
				if (!T[y] && slack[y] == 0)
				{
					if (yx[y] == EXPOSED)                                        //exposed vertex in Y found - augmenting path exists!
					{
						x = slackx[y];
						break;
					}
					else
					{
						T[y] = true;                                        //else just add y to T,
						if (!S[yx[y]])
						{
							q[wr++] = yx[y];                                //add vertex yx[y], which is matched with
																			//y, to the queue
							add_to_tree(yx[y], slackx[y]);                  //and add edges (x,y) and (y,
																			//yx[y]) to the tree
						}
					}
				}
			if (y < n) break;                                               //augmenting path found!
		}
	}
	if (y < n)                                                          //we found augmenting path!
	{
		max_match++;                                                    //increment matching
																		//in this cycle we inverse edges along augmenting path
		for (size_t cx = x, cy = y, ty; cx != MINUS_2; cx = prev[cx], cy = ty)
		{
			ty = xy[cx];
			yx[cy] = cx;
			xy[cx] = cy;
		}
		augment();                                                      //recall function, go to step 1 of the algorithm
	}
}//end of augment() function

double HungarianN3::solve(std::vector<size_t>& assignmentsOut)
{
	augment();                        //steps 1-3
	double ret = cost;                   //weight of the optimal matching
	assignmentsOut.resize(originalWorkers);
	for (size_t x = 0; x < originalWorkers; ++x)
	{     //forming answer there
		const std::vector<double>& cost_x_ = costMatrix[x];
		if (xy[x] < originalJobs)
		{
			ret -= cost_x_[xy[x]]; //flip sign since we solve the minimization problem
			assignmentsOut[x] = xy[x];
		}
		else
		{
			assignmentsOut[x] = EXPOSED;
		}
	}
	return ret;
}
void HungarianN3::assignmentSolve(const cv::Mat& costMatrixIn, std::vector<size_t>& assignmentOut)
{
	if (costMatrixIn.rows == 0)
	{
		assignmentOut.resize(0);
		return;
	}
	if (costMatrixIn.cols == 0)
	{
		assignmentOut.resize(static_cast<unsigned>(costMatrixIn.rows));
		for (unsigned i = 0; i < static_cast<unsigned>(costMatrixIn.rows); ++i)
		{
			assignmentOut[i] = NOT_ASSIGNED();
		}
		return;
	}
	assert(costMatrixIn.data);

	lantao_liu::mat m;
	m.resize(costMatrixIn.rows);
	for (int r = 0; r < costMatrixIn.rows; ++r)
	{
		const double* ptr = costMatrixIn.ptr<double>(r);
		m[r].resize(costMatrixIn.cols);
		for (int c = 0; c < costMatrixIn.cols; ++c)
		{
			m[r][c] = ptr[c];
		}
	}
	xray::HungarianN3 h(m);
	double cost = h.solve(assignmentOut);
	for (auto& ass : assignmentOut)
	{
		if (ass >= static_cast<unsigned int>(costMatrixIn.cols))
		{
			ass = NOT_ASSIGNED();
		}
	}
}