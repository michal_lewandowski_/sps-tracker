#pragma once

#include <random>
#include "ICascadeFilter.h"

class NormalizedPatch;
class Field;
class DetectionResult;

class NPClassifier : public ICascadeFilter
{
public:
	NPClassifier();
	virtual ~NPClassifier();

	virtual void init();
	virtual void release();

	virtual bool filter(int idx, FieldList& fields);

	float relativeSimilarity(Field& f, NormalizedPatch& patch, int& currLabel);
	void relativeSimilarity(Field& f, NormalizedPatch& patch, int j, float& coef);
	float conservativeSimilarity(Field& f, NormalizedPatch& patch, int& currLabel);
	float classifyBBrel(Field& f, cv::Rect bb, int& currLabel);
	float classifyBBcon(Field& f, cv::Rect bb, int& currLabel);

	void learn(Field& f, std::vector<NormalizedPatch> patches);
	void initLearn(Field& f, std::vector<NormalizedPatch> patches);
	void forget(Field& f);

	static int forgetThresholdP;
	static int forgetThresholdN;
	static int forgetThresholdF;
	static std::uniform_int_distribution<> forgetDistribution;
	
	int *windows;
	static const float thetaFP;
	static const float thetaTP;
};
