
///////////////////////////////////////////////////////////////////////////////
// File name: iPrimal.h
// This file defines the main algorithm of IMPROVED primal assignment Method.
// The time complexity is improved to O(n^3 lg n), via merging multiple steps
// into single stages of swap loop searching. 
// Min-heaps and trees are used to maintain searching results.
// Lantao Liu, Jan 17, 2011
///////////////////////////////////////////////////////////////////////////////

#ifndef PRIMAL_H
#define PRIMAL_H

#pragma warning(push, 0)

#include <set>
#include <vector>
#include <queue>
#include <list>
#include <algorithm>

#pragma warning(pop)

//! http://robotics.usc.edu/~lantao/code.html
namespace lantao_liu {

//#define DEBUG		// to toggle the debug mode (verbose info.)

#define USE_HEAP	// to toggle if use the min-heaps or not

#define USE_GREEDY	// whether to use greedy approx initial solution


#ifdef DEBUG
  #define _cout(expr) std::cout<<expr
#else
  #define _cout(expr)
#endif

#define DOUBLE_EPSILON 1e-7 //For double type comparison: <= as valid, > invalid

#define VERBOSE_LEVEL 1
#define DISPLAY_WIDTH 10

#define EXCEPTION_BROKEN -0x01
#define EXCEPTION_WRONG  -0x02

typedef size_t uint;

typedef std::vector<std::vector<double> > mat;

typedef struct _cell {
  uint row_idx;
  uint col_idx;
} cell;

typedef struct _tree_node {

  cell c;
  int asgn;		// -1: root, 0: new found asgn, 1: old asgn

  // n-ary tree is constructed with siblings instead of direct children
  _tree_node* child; 
  _tree_node* parent;
  _tree_node* next_sibling; 

  // constructor is a must!
  _tree_node(){ child= nullptr; parent= nullptr; next_sibling= nullptr; }
  ~_tree_node(){}

} tree_node;

template<typename T> class less_comp; //defined later


class Primal{
public:
  Primal(const mat& m);
  ~Primal(){}

  //some accessors
  inline void ClearExploited(void){ 
	exploited_rows.clear(); exploited_cols.clear(); }
  inline void ClearSwapLoop(void){
	swap_loop.clear(); }
  inline uint GetAssignedCol(uint _row_id){ return assignment[_row_id]; }
  inline uint GetAssignedRow(uint _col_id); 
  std::vector<uint> GetAsgnVec(void){ return assignment; }
  size_t GetSwapLoopLength(void){ return swap_loop.size(); }
  void InitAsgnPtrs(void){ asgn_ptrs.clear(); asgn_ptrs.resize(row_size);
	for(uint i=0; i<row_size; i++) asgn_ptrs[i] = nullptr; }
  void InitSibPtrs(void){ sib_ptrs.clear(); sib_ptrs.resize(row_size);
	for(uint i=0; i<row_size; i++) sib_ptrs[i] = nullptr; }

  // initiations
  void RandSolution(void);
  void InitSolution(const std::vector<uint>&);
  void InitStage(void);
  
  //approximations, remove these two does not hurt the algo(safely ignore them)
  void GreedyApprox1(std::vector<uint>&); //local greedy
  void GreedyApprox2(std::vector<uint>&); //global greedy

  // pre-process and create the heaps
  void Preprocess(void);

  // get the next starting cell, if returns (-1, -1), meaning all are >=0
  cell NextStartEntry() const;
  cell NextStartEntry(uint) const;
  //void UpdateDualVars(const vector<double>& _deltas);
  void UpdateReducedCostMatrix(const std::vector<double>& _dual_rows, const std::vector<double>& _dual_cols);

  // search a swap loop using BFS
  bool SearchSwapLoop(cell& _start);

  // dual updates, return: -1 if simply update; 0 if no loop but feasible; 1 if a swap loop is found during the update, ie, root is changed & already covered.
  int DualUpdates(cell&, std::queue<tree_node*>&);

  // given a swap loop, tasks are swapped, matrix & assign-vec are updated
  void SwapTasks(void);

  //primal algo containing all compoments to get the ultimate solution
  //must init a solution before calling it
  double PrimalAlgo(std::vector<uint>& assignementsOut);

  // get current sum of costs based on current assignemnt solution 
  double ComputeCostSum(const mat& _m, const std::vector<uint>& _as) const;

  // some display functions
  void DisplayMatrix(const mat&) const;
  void DisplayMatrix(const lantao_liu::mat&, const std::vector<uint>&) const;
  void DisplayAssignment(void) const;
  void DisplaySet(const std::set<uint>&) const;
  void DisplayTree(tree_node* _root);
  template<typename T>
  void DisplayVec(const std::vector<T>& _vec);

  // destroy the whole tree, return number of nodes;
  uint DeleteTree(tree_node* _root);

  // for testing & debugging, not used in final algorithm
  void PrimalTest(void);

  // double check if all values in reduced cost matrix have become feasible
  void DoubleCheck(void); 


private:
  //basic data members
  uint row_size;
  uint col_size;

  std::vector<double> dual_row_vars;	// dual row variables, ie, row labelling values 
  std::vector<double> dual_col_vars;	// dual col variables, ie, col labelling values 
  std::vector<double> deltas;	// vector to store accumulated updates

  std::set<uint> exploited_rows;	// rows traversed during searching loop
  std::set<uint> exploited_cols;	// cols traversed during searching loop

  mat orig_matrix;   		// a copy of original matrix
  mat oprt_matrix;   		// the so-called reduced cost matrix

  std::vector<
	std::priority_queue<
        	std::pair<uint, double>,
        	std::vector<std::pair<uint,double> >,
        	less_comp<uint> >
	> min_heaps;		// heaps to maintain searching status
 
  std::vector<uint> assignment;	// indices of vector <-> values in the vector
  std::list<cell> swap_loop;

  std::vector<tree_node*> asgn_ptrs;	// store the pointers only to assigned cells
  std::vector<tree_node*> sib_ptrs;  // store the pointers of last siblings each row 

  //std::set<uint> greedy_cols;	// store greedily selected starting columns

};


// for priority queue elements' comparison
template<typename T>
class less_comp{
public:
  less_comp(bool __switch = false){ _switch = __switch; };
  bool operator() (const std::pair<T, double>& a,
                const std::pair<T, double>& b) const {
  if(!_switch) {
	  // output in increasing order
	  return a.second > b.second;
  }
  //else: output in decreasing order
  return a.second < b.second;
  }

private:
  bool _switch;

};

}
#endif
