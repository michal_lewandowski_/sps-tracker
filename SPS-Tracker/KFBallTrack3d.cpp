#include "stdafx.h"
#include "KFBallTrack3d.h"
#include "PredictedBall.h"
#include <boost/lexical_cast.hpp>
#include "IDFactory.h"

extern double ballRadius;

KFBallTrack3d::KFBallTrack3d(const size_t iTrackID, const std::shared_ptr<MovingObject> obj, const int iStateParams, const int iMesaureParams, const int64_t dStartTS, double& maxAbsVelocity, double& maxBallProbability)
	: KFTrack3d(iTrackID, obj, iStateParams, iMesaureParams, dStartTS, maxAbsVelocity, maxBallProbability), detectionUncertainness(120, 120)
{
	distCostThreshold = 3000;
	goodFramesThreshold = 10;
	ballProbailityThr =       -cv::log(0.001);
	goodNegLogProbThreshold = -cv::log(0.01);
	goodDetectionsThreshold = 1;
	computeTresholds();

	setROI(obj);
	assignProbability();
}

KFBallTrack3d::KFBallTrack3d(const KFBallTrack3d& other)
	: KFTrack3d(other)
	, m_roi(other.m_roi)
	, detectionUncertainness(other.detectionUncertainness)
{

}
KFBallTrack3d& KFBallTrack3d::operator=(const KFBallTrack3d& other)
{
	if (this != &other)
	{
		this->KFTrack3d::operator=(other);
		this->m_roi = other.m_roi;
		this->detectionUncertainness = other.detectionUncertainness;
	}
	return *this;
}

KFBallTrack3d::~KFBallTrack3d()
{
}

std::shared_ptr<MovingObject> KFBallTrack3d::missFrame(const CameraCalibration*)
{
	m_iMissedFrames++;

	std::shared_ptr<MovingObject> bo = std::make_shared<PredictedBall>(IDFactory::generateID(ID_DETECTION3D), "", cv::Point3d(m_estimation.at<double>(0), m_estimation.at<double>(1), m_estimation.at<double>(2)));
	bo->field() = field();

	for(int i=1; i <= 4; ++i)
	{
		Probability prob;
		if(m_trace.back()->getProbability("pcam"+std::to_string(i), prob))
		{
			bo->assignPropabilityS(prob.value, prob.name);
		}
	}
	bo->assignPropabilityS(0.25, "po");
	bo->assignPropabilityS(cv::exp(-0.5), "pe");

	addTrace(bo);

	return bo;
}
void KFBallTrack3d::addTrace(const std::shared_ptr<MovingObject> obj)
{
	KFTrack3d::addTrace(obj);
	setROI(obj);
}
bool KFBallTrack3d::isDead(const std::vector<cv::Point>&, const FieldList& fields)
{
	CV_DbgAssert(field());

	if (!verifyVelocity() && m_trace.size() > 0)
	{
		m_trace.erase(--m_trace.end());
		m_dAbsVelocity = getAbsoluteVelocity();
		return true;
	}
	if(getCurrentDet()->getNegLogProbability() > getProbabilityThreshold())
	{
		m_iMissedFrames++;
	}
	return (m_iMissedFrames == maxMissedFrames)
		|| m_bKillTrack
		|| !getCurrentDet()->inField(*field())
		|| getAverageProbability() > getProbabilityThreshold()
		|| getCurrentDet()->getNegLogProbability() > getProbabilityThreshold();
}
bool KFBallTrack3d::isInROI(cv::Rect& bb) const
{
	return m_roi.contains(bb.tl() + 0.5 * cv::Point(bb.size()));
}
void KFBallTrack3d::setROI(const std::shared_ptr<MovingObject> obj)
{
	m_roi = cv::Rect(obj->pos2d() - detectionUncertainness, obj->pos2d() + detectionUncertainness);
//	m_roi = m_roi & cv::Rect(cv::Point(0, 0), imageSize);
}
bool KFBallTrack3d::verifyVelocity(void) const
{
	return (m_dAbsVelocity < maxRecorderBallSpeedMMperFrame);
}
void KFBallTrack3d::printInfo(const std::vector<cv::Point>&, const FieldList& fields) const
{
#ifndef SPS_PRINT_INFO_3D_DISABLE_ISVIS
	if (isVisible())
#endif
	{
		std::cout << "T3d" << " [" << fieldID() << "] - " << m_iTrackID << ": len " << getLength() << " miss " << m_iMissedFrames << " dw " << (getAverageProbability() <= getProbabilityThreshold()?1:0) << " fb " << (isGoodEnoughFor()?"correct":"no") << " (" << (getAverageProbability() <= goodNegLogProbThreshold ? 1 : 0) << ") sp " << m_dAbsVelocity << " (" << std::setprecision(5) << maxAbsVelocityGlobal << ") ";
		std::cout << "np " << std::setprecision(5) << getAverageProbability() << " (" << getProbabilityThreshold() << "," << goodNegLogProbThreshold << "," << -cv::log(0.033) << ") ";
		std::cout << "t2d [";
		for(const auto& id : getCurrentDet()->track2dID())
		{
			std::cout << id.first << "-" << id.second << " ";
		}
		std::cout << "] ";

		if (isNew())
		{
			std::cout << "new track";
		}
		if (m_iMissedFrames == maxMissedFrames)
		{
			std::cout << "dead track [missed]";
		}
		else if (m_bKillTrack)
		{
			std::cout << "dead track [killed]";
		}
		else if (!getCurrentDet()->inField(*field()))
		{
			std::cout << "dead track [not in field]";
		}
		else if (!verifyVelocity())
		{
			std::cout << "dead track [too fast]";
		}
		else if (getAverageProbability() > getProbabilityThreshold())
		{
			std::cout << "dead track [too low prob]";
		}
		else if (getCurrentDet()->getNegLogProbability() > getProbabilityThreshold())
		{
			std::cout << "dead track [too low prob for last one]";
		}

		//std::cout << " " << getCurrentDet()->pos2d() << " " << getCurrentDet()->pos3d() << " " << getVelocity() << " " << getCurrentDet()->isDetected();
		std::cout << std::endl;
		std::cout << "\t\t" << getCurrentDet()->printInfo3d(getProbabilityThreshold()) << std::endl;
	}
}
void KFBallTrack3d::show(cv::Mat frame, cv::Scalar color) const
{
	
}

void KFBallTrack3d::show(cv::Mat frame, double windowScale, cv::Scalar color) const
{
#ifndef SPS_PRINT_INFO_3D_DISABLE_ISVIS
	if (isVisible())
#endif
	{
		std::deque<std::shared_ptr<MovingObject>>::const_iterator itPrev = m_trace.begin();
		std::deque<std::shared_ptr<MovingObject>>::const_iterator itCurr = itPrev + 1;
		for (; itCurr != m_trace.end(); itPrev = itCurr, ++itCurr)
		{
			cv::line(frame, cv::Point(cvRound(itPrev->get()->pos3d().x / windowScale), frame.rows - cvRound(itPrev->get()->pos3d().y / windowScale)), cv::Point(cvRound(itCurr->get()->pos3d().x / windowScale), frame.rows - cvRound(itCurr->get()->pos3d().y / windowScale)), itPrev->get()->isDetected()?color: (itPrev->get()->isBacktracked()? (itPrev->get()->views()==0? cv::Scalar(0, 0, 255):cv::Scalar(0, 150, 255)):cv::Scalar(0, 150, 255)), 2, CV_AA);
			//(*itPrev)->show3d(frame, color, windowScale);
		}
		cv::Point latestPos(cvRound(m_trace.back()->pos3d().x / windowScale), frame.rows - cvRound(m_trace.back()->pos3d().y / windowScale));
		cv::putText(frame, std::to_string(getTID()), latestPos + cv::Point(20, 20), CV_FONT_HERSHEY_PLAIN, 1, color);

		getCurrentDet()->show3d(frame, color, windowScale);

		if(isGoodEnoughFor())
		{
			cv::RotatedRect rr(latestPos, cv::Size2f(25, 25), 45);
			cv::Point2f vertices[4];
			rr.points(vertices);
			for (int i = 0; i < 4; ++i)
			{
				cv::line(frame, vertices[i], vertices[(i + 1) % 4], color, 1);
			}
		}
	}
}
void KFBallTrack3d::show(cv::Mat frame, const CameraCalibration* calib, cv::Scalar color) const
{
	if (isVisible())
	{
		std::deque<std::shared_ptr<MovingObject>>::const_iterator itPrev = m_trace.begin();
		std::deque<std::shared_ptr<MovingObject>>::const_iterator itCurr = itPrev + 1;
		cv::Point2d ptPrev, ptCurr;
		for (; itCurr != m_trace.end(); itPrev = itCurr, ++itCurr)
		{
			calib->transformToPixel(itPrev->get()->pos3d(), ptPrev);
			calib->transformToPixel(itCurr->get()->pos3d(), ptCurr);

			cv::line(frame, ptPrev, ptCurr, itPrev->get()->isDetected() ? color : (itPrev->get()->isBacktracked() ? (itPrev->get()->views() == 0 ? cv::Scalar(0, 0, 255) : cv::Scalar(0, 150, 255)) : cv::Scalar(0, 150, 255)), 1, CV_AA);
		}

		cv::circle(frame, cv::Point(ptCurr), 3, color, -1);
		cv::circle(frame, cv::Point(ptCurr), 4, cv::Scalar(0, 255, 255));
	}
}
bool KFBallTrack3d::assignProbability()
{
	assignProbability(getCurrentDet());
	return true;
}
void KFBallTrack3d::assignProbability(std::shared_ptr<MovingObject> obj)
{
	CV_Assert(obj);
	CV_Assert(obj->getNegLogProbability() >= 0.0);
	CV_Assert(obj->countProb() > 0);

	//velocity probability
	if (getLength() > 1)
	{
		if (m_dAbsVelocity > maxAbsVelocityGlobal)
		{
			m_dAbsVelocity = maxAbsVelocityGlobal;
		}
		obj->assignPropabilityD(m_dAbsVelocity / maxAbsVelocityGlobal, "pv");
	}

	//history probability
	obj->assignPropabilityD(1 - cv::exp(-probImportanceMap["pl"] * getLength()), "pl");
	//loosing evidence probability
	obj->assignPropabilityD(cv::exp(-probImportanceMap["pm"] * m_iMissedFrames), "pm");

	probability.value *= obj->getProbability();
	probability.log += -obj->getNegLogProbability();
	probability.count++;
}
void KFBallTrack3d::removeProbability(std::shared_ptr<MovingObject> obj)
{
	CV_Assert(obj);
	CV_Assert(obj->getNegLogProbability() >= 0.0);
	CV_Assert(obj->countProb() > 0);

	probability.value /= obj->getProbability();
	probability.log -= -obj->getNegLogProbability();
	probability.count--;

	obj->removePropabilityD("pv");
	obj->removePropabilityD("pl");
	obj->removePropabilityD("pm");
}
bool KFBallTrack3d::isGoodEnoughFor() const
{
	return  isVisible()
			&& getMissedFrames() == 0
			&& getNumDetected() > goodFramesThreshold
			&& getCurrentDet()->isDetected()
			&& getAverageProbability() < goodNegLogProbThreshold
			&& getCurrentDet()->numDetected2d() > goodDetectionsThreshold;
	}