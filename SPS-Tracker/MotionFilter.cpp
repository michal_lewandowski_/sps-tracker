#include "MotionFilter.h"
#include "NormalisedPatch.h"
#include "Field.h"

MotionFilter::MotionFilter()
{
}

MotionFilter::~MotionFilter()
{

}
void MotionFilter::init()
{
	release();
}

void MotionFilter::release()
{
}

bool MotionFilter::filter(int idx, FieldList& fields)
{
	bool nextStage = false;
	for (auto& f : fields)
	{
		if (idx < f.numWindows)
		{
			int* box = &f.windows[TLD_WINDOW_SIZE * idx];
			if (img1.at<uchar>(cvRound(box[1] + 0.5 * box[3]), cvRound(box[0] + 0.5 * box[2])) == 0)
				//if (cv::countNonZero(img1(cv::Rect(box[0],box[1],box[2],box[3]))) == 0)
			{
				f.detectionResult.posteriors[idx] = 0;
				f.detectionResult.lastStageInvalid[idx] = 1;
			}
			else
			{
				nextStage = true;
			}
		}
	}
	return nextStage;
}