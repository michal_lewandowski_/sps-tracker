#pragma once
#include <opencv2/opencv.hpp>

class ColorQuantiziser
{
public:
	ColorQuantiziser();
	virtual ~ColorQuantiziser();
	
	void digest(const cv::Vec3b& color, size_t count = 1);
	void quantize(size_t maxColors, std::vector<cv::Vec3b>& out);
	void clear();

	cv::Mat showInputHist();
	cv::Mat showOutputHist();
	cv::Mat showHist(std::vector<std::vector<float>>& colors);
	cv::Mat showSortedHist(std::vector<std::vector<float>>& colors);
	cv::Mat getColorQuantizisationImage() const;

protected:

	std::vector<float> vectorise(const cv::Vec3b& color, size_t iCount) const;

	std::vector<std::vector<float>> inputColors;
	std::vector<std::vector<float>> outputColors;
	std::vector<cv::Point3f> m_colors;
	size_t m_iTotalCounts;

	cv::Mat imgHistI;
	cv::Mat imgHistO;
};

inline bool operator<(const cv::Vec3b& lhs, const cv::Vec3b& rhs)
{
	if (lhs[0] < rhs[0])
	{
		return true;
	}
	if (lhs[0] == rhs[0])
	{
		if (lhs[1] < rhs[1])
		{
			return true;
		}
		if (lhs[1] == rhs[1])
		{
			return lhs[2] < rhs[2];
		}
		return false;
	}
	return false;
}