#pragma once
#include <opencv.hpp>
#include "MovingObject.h"
#include "FixedBinHistogram.h"

enum class BallType : int
{
	UNKNOWN = 0,
	DETECTED = 1,
	PREDICTED = 2,
	BACKTRACK = 3,
	BACKTRACK_EX = 4,
	VISUAL = 5
};

class BallObject : public MovingObject
{
public:
	BallObject(const std::size_t id, std::string cid, cv::Rect b, cv::RotatedRect e, double br, cv::Mat im, cv::Mat mm);
	BallObject(const std::size_t id, std::string cid, cv::Point3d p, int views, int numDet, float error, const std::unordered_map<std::string, int>& tracks, const std::vector<PreviousBallInCam>& loc);

	virtual ~BallObject();
	BallObject(const BallObject& other);
	BallObject& operator=(const BallObject& other);

	virtual std::string printInfo2d(void) const override;
	virtual std::string printInfo3d(double ballProbailityThr) const override;
	virtual bool inMask(const std::vector<cv::Point>& contour) const override;
	virtual bool inField(const Field& field) const override;
	void computeSizeProbability(double dWidth, double dHeight, double maxSize);
	void computeProximityProbability(cv::Point2d pt, double distVar);

	inline const std::vector<PreviousBallInCam>& getPrevLocs() const { return prevLoc; }
	
	inline double& sizeRatio(void) { return ballRatio; };
	inline const double& sizeRatio(void) const { return ballRatio; };

	inline double& colorDist(void) { return ballColorDist; };
	inline const double& colorDist(void) const { return ballColorDist; };

	inline std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>>& colorHist(void) { return hist; };
	inline const std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>>& colorHist(void) const { return hist; };

	inline cv::Mat& image(void) { return img; };
	inline const cv::Mat& image(void) const { return img; };

	inline cv::Mat& mask(void) { return mimg; };
	inline const cv::Mat& mask(void) const { return mimg; };

	cv::Mat getHistImage(const Field& field) const;
	inline const BallType& ballType(void) const { return type; };

protected:
	cv::RotatedRect ebb;
	std::vector<PreviousBallInCam> prevLoc;
	double ballRatio;
	BallType type;

	double ballColorDist;
	std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> hist;
	cv::Mat img;
	cv::Mat mimg;
};

