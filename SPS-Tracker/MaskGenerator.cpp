#include "stdafx.h"
#include "MaskGenerator.h"
#include <tuple>
#include <videoio.hpp>
#include "CameraCalibration.h"
#include <boost/filesystem.hpp>

MaskGenerator::MaskGenerator()
{
	
}
MaskGenerator::~MaskGenerator()
{
	
}
void MaskGenerator::addJob(std::string name, std::string videoFile, std::string configFile)
{
	data.push_back(std::make_tuple(name, videoFile, configFile));
}
void MaskGenerator::setMasks(std::vector<cv::Point3d>& m)
{
	masks3d = m;
}
void MaskGenerator::init(std::vector<cv::Point3d> m)
{
	masks2d.clear();
	if(boost::filesystem::exists(maskFile + ".txt"))
	{
		std::ifstream f(maskFile + ".txt", std::ios_base::in);
		cv::Point3d pt;
		while(!f.eof())
		{
			double x, y;
			f >> x >> y;
			masks2d.push_back(cv::Point2d(x, y));
		}
		f.close();
	}
	else
	{
		masks2d.resize(m.size());
		for (int i = 0; i < m.size(); ++i)
		{
			calibration.transformToPixel(m[i], masks2d[i]);
		}
	}
}
void MaskGenerator::process()
{
	CV_Assert(data.size() > 0);
	for(auto& d : data)
	{
		camID = std::get<0>(d);
		maskFile = std::get<1>(d).substr(0, std::get<1>(d).length() - 3) + "mask";

		cv::VideoCapture cap(std::get<1>(d));
		if(cap.isOpened())
		{
			if(calibration.init(std::get<2>(d)))
			{
				cv::Mat frameBGR, frameTemp, frameGray;
				cap.read(frameTemp);
				cap.release();

				frameGray.create(frameTemp.rows, frameTemp.cols, CV_8UC1);

				static int from_to[] = { 0,0 };
				cv::mixChannels(&frameTemp, 1, &frameGray, 1, from_to, 1);

				cv::demosaicing(frameGray, frameBGR, cv::COLOR_BayerBG2BGR);

				if (frameBGR.size() != calibration.getImageSize())
				{
					cv::resize(frameBGR, frame, calibration.getImageSize(), 0.0, 0.0, cv::INTER_LINEAR);
				}
				else
				{
					frame = frameBGR;
				}

				init(masks3d);
				while (true)
				{
					drawOverlayMask(this, R);
					cv::setMouseCallback("Generate mask", MaskGenerator::CallBackFunc, reinterpret_cast<void *>(this));

					int k = cv::waitKey(1);
					if (k == 'g')
					{
						cv::Mat mask = generateMask(maskFile, frame.size());
						cv::imshow("mask", mask);
					}
					if (k == 'r')
					{
						if (boost::filesystem::exists(maskFile + ".txt"))
						{
							boost::filesystem::remove(maskFile + ".txt");
						}
						init(masks3d);
					}
					if (k == ' ')
					{
						cv::destroyWindow("mask");
						init(masks3d);
						break;
					}
					if (k == 'q')
					{
						return;
					}
				}
			}
			else
			{
				std::cout << "cannot init calibration " << std::get<2>(d) << std::endl;
			}
		}
		else
		{
			std::cout << "cannot open the file " << std::get<1>(d) << std::endl;
		}
	}
}
void MaskGenerator::CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
	MaskGenerator* mg = reinterpret_cast<MaskGenerator*>(userdata);
	flags = 0;

	if (event == cv::EVENT_LBUTTONDOWN)
	{
		mg->masks2d.push_back(cv::Point(x, y));
		drawOverlayMask(mg, mg->R);
	}
	if (event == cv::EVENT_RBUTTONDOWN)
	{
		bool change = false;
		for(int i=0; i < mg->masks2d.size(); ++i)
		{
			double dx = std::abs(x - mg->masks2d[i].x);
			double dy = std::abs(y - mg->masks2d[i].y);
			if(dx*dx + dy*dy <= static_cast<double>(mg->R*mg->R))
			{
				change = true;
				mg->masks2d.erase(mg->masks2d.begin() + i);
				break;
			}
		}
		if(change)
		{
			drawOverlayMask(mg, mg->R);
		}
	}
}
//void MaskGenerator::generateAllMask()
//{
//	for (auto& d : data)
//	{
//		camID = std::get<0>(d);
//		maskFile = std::get<1>(d).substr(0, std::get<1>(d).length() - 3) + "mask";
//
//		if (calibration.init(std::get<2>(d)))
//		{
//			init(masks3d);
//			generateMask(maskFile, calibration.getImageSize());
//		}
//	}
//}
cv::Mat MaskGenerator::generateMask(std::string maskFile, cv::Size s)
{
	cv::Mat mask(s, CV_8UC1);
	mask.setTo(255);

	std::ofstream f(maskFile + ".txt", std::ios_base::out | std::ios_base::trunc);
	std::vector<cv::Point> v(masks2d.size());
	for (int i = 0; i < masks2d.size(); ++i)
	{
		v[i] = cv::Point(cvRound(masks2d[i].x), cvRound(masks2d[i].y));
		f << masks2d[i].x << " " << masks2d[i].y << std::endl;
	}
	std::vector<std::vector<cv::Point>> vv;
	vv.push_back(v);
	cv::drawContours(mask, vv, -1, cv::Scalar(0), -1);
	cv::imwrite(maskFile + ".png", mask);
	std::cout << "Generate mask " << camID << " -> " << maskFile << std::endl;
	f.close();
	return mask;
}
void MaskGenerator::drawOverlayMask(MaskGenerator* mg, int R)
{
	cv::Mat frame = mg->frame.clone();
	cv::Mat overlay;
	frame.copyTo(overlay);

	cv::Point2d center(0, 0);
	for (auto& pt : mg->masks2d)
	{
		center += pt;
	}
	center *= 1.0 / static_cast<double>(mg->masks2d.size());

	std::sort(mg->masks2d.begin(), mg->masks2d.end(), [&center](const cv::Point2d& a, const cv::Point2d& b)
	{
		if (a.x - center.x >= 0 && b.x - center.x < 0)
			return true;
		if (a.x - center.x < 0 && b.x - center.x >= 0)
			return false;
		if (a.x - center.x == 0 && b.x - center.x == 0)
		{
			if (a.y - center.y >= 0 || b.y - center.y >= 0)
				return a.y > b.y;
			return b.y > a.y;
		}

		// compute the cross product of vectors (center -> a) x (center -> b)
		double det = (a.x - center.x) * (b.y - center.y) - (b.x - center.x) * (a.y - center.y);
		if (det < 0)
			return true;
		if (det > 0)
			return false;

		// points a and b are on the same line from the center
		// check which point is closer to the center
		double d1 = (a.x - center.x) * (a.x - center.x) + (a.y - center.y) * (a.y - center.y);
		double d2 = (b.x - center.x) * (b.x - center.x) + (b.y - center.y) * (b.y - center.y);
		return d1 > d2;
	});

	for (auto& pt : mg->masks2d)
	{
		cv::circle(frame, pt, R, cv::Scalar(0, 0, 255), -1);
	}
	if (mg->masks2d.size() >= 3)
	{
		std::vector<cv::Point> v(mg->masks2d.size());
		for (int i = 0; i < mg->masks2d.size(); ++i)
		{
			v[i] = cv::Point(cvRound(mg->masks2d[i].x), cvRound(mg->masks2d[i].y));
		}
		std::vector<std::vector<cv::Point>> vv;
		vv.push_back(v);
		cv::drawContours(overlay, vv, -1, cv::Scalar(255, 255, 255), -1);
	}
	
	double alpha = 0.3;
	cv::addWeighted(overlay, alpha, frame, 1 - alpha, 0, frame);
	cv::imshow("Generate mask", frame);
}