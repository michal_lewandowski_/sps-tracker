#pragma once

#include "ICascadeFilter.h"

class DetectionResult;

class MotionFilter : public ICascadeFilter
{
public:
	MotionFilter();
	virtual ~MotionFilter();

	virtual void init();
	virtual void release();

	virtual bool filter(int idx, FieldList& fields);
};

