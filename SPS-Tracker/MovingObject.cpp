#include "stdafx.h"
#include "MovingObject.h"

MovingObject::MovingObject(const std::size_t id, std::string cid, cv::Rect b)
	: detID(id), bb_(b), field_(nullptr), observedViews_(1), numDetected_(0), reconstructionError_(0.0), valid(true), pos3d_(0.0,0.0,0.0), pos3d_est(0.0, 0.0, 0.0), camID_(cid),
	  staticProb({ 1, cv::log(1), "", 0}),
	  dynamicProb({ 1, cv::log(1), "", 0 })
{
	pos2d() = cv::Point2d(box().tl().x + box().size().width / 2.0f, box().tl().y + box().size().height / 2.0f);
#ifdef SPS_DEBUB_DETS
	forceLearn = false;
#endif
}
MovingObject::MovingObject(const std::size_t id, std::string cid, cv::Point3d p, int views, int numDet, float error, const std::unordered_map<std::string, int>& tracks)
	: detID(id), pos3d_(p), pos3d_est(p), field_(nullptr), observedViews_(views), numDetected_(numDet), reconstructionError_(error), valid(true), tr2dIDs(tracks), camID_(cid),
		staticProb({ 1, cv::log(1), "", 0 }),
		dynamicProb({ 1, cv::log(1), "", 0 })
{
#ifdef SPS_DEBUB_DETS
	forceLearn = false;
#endif
}
MovingObject::~MovingObject()
{
}

MovingObject::MovingObject(const MovingObject& other)
	: detID(other.detID)
	, bb_(other.bb_)
	, pos2d_(other.pos2d_)
	, pos3d_(other.pos3d_)
	, pos3d_est(other.pos3d_est)
	, field_(other.field_)
	, camID_(other.camID_)
	, observedViews_(other.observedViews_)
	, numDetected_(other.numDetected_)
	, reconstructionError_(other.reconstructionError_)
	, staticProb(other.staticProb)
	, dynamicProb(other.dynamicProb)
	, probs(other.probs)
	, tr2dIDs(other.tr2dIDs)
{
	
}
MovingObject& MovingObject::operator=(const MovingObject& other)
{
	if(this != &other)
	{
		detID = other.detID;
		bb_ = other.bb_;
		pos2d_ = other.pos2d_;
		pos3d_ = other.pos3d_;
		pos3d_est = other.pos3d_est;
		field_ = other.field_;
		camID_ = other.camID_;
		observedViews_ = other.observedViews_;
		numDetected_ = other.numDetected_;
		reconstructionError_ = other.reconstructionError_;
		staticProb = other.staticProb;
		dynamicProb = other.dynamicProb;
		probs = other.probs;
		tr2dIDs = other.tr2dIDs;
	}
	return *this;
}
bool MovingObject::operator==(const MovingObject& rhs) const
{
	return pos2d() == rhs.pos2d() && pos3d() == rhs.pos3d();
}
double MovingObject::dist2d(const std::shared_ptr<MovingObject> obj) const
{
	return cv::norm(cv::Mat(pos2d() - obj->pos2d()), cv::NormTypes::NORM_L2);
}
double MovingObject::dist3d(const std::shared_ptr<MovingObject> obj) const
{
	return cv::norm(cv::Mat(pos3d() - obj->pos3d()), cv::NormTypes::NORM_L2);
}
std::string MovingObject::printInfo(void) const
{
	std::stringstream ss;
	ss << "DI " << detID << ":";
	return ss.str();
}
void MovingObject::assignPropabilityS(double value, std::string name)
{
	if (value <= 0.0)
	{
		value = std::numeric_limits<double>::epsilon();
	}
	CV_Assert(value > 0.0 && value <= 1.0);
	probs.insert(std::make_pair(name, Probability(value, cv::log(value), name, 1 )));
	staticProb.value *= probs[name].value;
	staticProb.log += probs[name].log;
	staticProb.count++;
}
void MovingObject::removePropabilityS(std::string name)
{
	staticProb.value /= probs[name].value;
	staticProb.log -= probs[name].log;
	staticProb.count--;
	probs.erase(name);
}
void MovingObject::assignPropabilityD(double value, std::string name)
{
	if (value <= 0.0)
	{
		value = std::numeric_limits<double>::epsilon();
	}
	CV_Assert(value > 0.0 && value <= 1.0);
	probs.insert(std::make_pair(name, Probability(value, cv::log(value), name, 1)));
	dynamicProb.value *= probs[name].value;
	dynamicProb.log += probs[name].log;
	dynamicProb.count++;
}
void MovingObject::removePropabilityD(std::string name)
{
	dynamicProb.value /= probs[name].value;
	dynamicProb.log -= probs[name].log;
	dynamicProb.count--;
	probs.erase(name);
}
bool MovingObject::getProbability(std::string name, Probability& prob) const
{
	auto it = probs.find(name);
	if(it != probs.end())
	{
		prob = it->second;
		return true;
	}
	return false;
}