#include "stdafx.h"
#include "ColorBallHistAdaptive.h"
#include "BallObject.h"

ColorBallHistAdaptive::ColorBallHistAdaptive(TimeWatch& sysTime, std::shared_ptr<AdaptiveBinning<cv::Vec3b>> adaptiveBinning)
	: systemTime(sysTime)
	, adaptiveBinningPtr(adaptiveBinning)
	, bIsReady(false)
	, isProbModelReady(false)
	, iSmallestIndex(-1)
	, dSmallestDist(0.0)
	, dNowLearningRate(0.1)
	, probModelWaitForNumSamples(10)
	, binsColour(16)
	, binsProb(20)
	, resetColorModelAfter(250)
{
	cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
	file["resetColorModelAfter"] >> resetColorModelAfter;
	file["colorLearningRate"] >> dNowLearningRate;
	file.release();

	ballHistogram = std::make_shared<FixedBinHistogram<cv::Vec3b, double>>(binsColour, adaptiveBinningPtr->getBucketIndexFunc());
	ballDists = std::make_shared<FixedBinHistogram<double, double>>(binsProb, getBucketIndexFunc());
	notBallDists = std::make_shared<FixedBinHistogram<double, double>>(binsProb, getBucketIndexFunc());
	ballDistsNorm = nullptr;
	notBallDistsNorm = nullptr;

	for (size_t i = 0; i < binsProb; ++i)
	{
		binning[(i + 1) / static_cast<double>(binsProb)] = i;
		*ballDists << ((i + 1) / static_cast<double>(binsProb));
		*notBallDists << ((i + 1) / static_cast<double>(binsProb));
	}
	lastUpdateTimestamp = systemTime.getFrameNum();
}

ColorBallHistAdaptive::~ColorBallHistAdaptive()
{
}
void ColorBallHistAdaptive::reset()
{
	bIsReady = false;
	//isProbModelReady = false;
	ballHistogram->reset();
}
int64_t ColorBallHistAdaptive::learn(const std::shared_ptr<MovingObject> obj)
{
	CV_Assert(adaptiveBinningPtr);

	std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(obj);
	std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> normHist = getNormalizedHistogram(ball->image(), ball->mask(), true);
	if (normHist)
	{
		ballHistogram->add(*normHist, dNowLearningRate);
		bIsReady = true;
		lastUpdateTimestamp = systemTime.getFrameNum();
	}

	//learn probability model of being ball
	if (iSmallestIndex != -1)
	{
		double dd = 0;
		int ind = -1;
		for (unsigned int i = 0; i < dists.size(); ++i)
		{
			if (dists[i]>dd && i!=static_cast<unsigned int>(iSmallestIndex))
			{
				dd = dists[i];
				ind = static_cast<int>(i);
			}
		}
		if (ind != -1)
			*notBallDists << dists[ind];
		*ballDists << dists[iSmallestIndex];
		
		ballDistsNorm = ballDists->createNormalized(std::numeric_limits<double>::epsilon());
		notBallDistsNorm = notBallDists->createNormalized(std::numeric_limits<double>::epsilon());
		if (!isProbModelReady)
			isProbModelReady = ballDists->count() > probModelWaitForNumSamples;
	}
	return lastUpdateTimestamp;
}

void ColorBallHistAdaptive::prepare(void)
{
	sumDistance = 0;
	dists.clear();
	dSmallestDist = std::numeric_limits<double>::max();
	iSmallestIndex = -1;
	
	if(bIsReady && (systemTime.getFrameNum() - lastUpdateTimestamp) > resetColorModelAfter)
	{
		reset();
	}
}
double ColorBallHistAdaptive::classify(std::shared_ptr<MovingObject> obj)
{
	double distance = 1.0;
	if (bIsReady)
	{
		std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(obj);
		CV_Assert(ball);

		ball->colorHist() = getNormalizedHistogram(ball->image(), ball->mask(), false);
		CV_Assert(ball->colorHist());
		if (!ball->colorHist())
			return distance;
	
		distance = ballHistogram->distance(*ball->colorHist());

		if (ball->isDetected())
		{
			sumDistance += distance;
			dists.push_back(distance);
			if (distance < dSmallestDist)
			{
				dSmallestDist = distance;
				iSmallestIndex = static_cast<int>(dists.size() - 1);
			}
		}
	}
	return distance;
}
void ColorBallHistAdaptive::assignProbability(std::shared_ptr<MovingObject> obj) const
{
	if (bIsReady)
	{
		std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(obj);
		CV_Assert(ball);

		//probability based on learned model what is the ball and what is not
		if (isProbModelReady)
		{
			const double pB = (*ballDistsNorm)[getBucketIndex(ball->colorDist())];
			const double pNB = (*notBallDistsNorm)[getBucketIndex(ball->colorDist())];

			ball->assignPropabilityS(pB / (pB + pNB), "pc");
		}
		else if(ball->isDetected())
		{
			//data driven probability estimation to initialise the model
			if (dists.size() > 1)
			{
				ball->assignPropabilityS(1.0 - ball->colorDist() / sumDistance, "pc");
			}
			else if (dists.size() == 1)
			{
				ball->assignPropabilityS(1.0, "pc");
			}
		}
	}
}

std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> ColorBallHistAdaptive::getHistogram() const
{
	return ballHistogram;
}
cv::Mat ColorBallHistAdaptive::getImage(int width, int height) const
{
	return getImage(adaptiveBinningPtr, *ballHistogram, width, height);
}
cv::Mat ColorBallHistAdaptive::getImage(std::shared_ptr<AdaptiveBinning<cv::Vec3b>> adaptiveBinning, FixedBinHistogram<cv::Vec3b, double>& hist, int width, int height)
{
	CV_Assert(adaptiveBinning);

	cv::Mat img(height, width, CV_8UC3);
	const double factor = img.cols / static_cast<double>(hist.count());
	img.setTo(0);
	
	int x = 0;
	std::vector<cv::Vec3b> binColors;
	adaptiveBinning->getBins(binColors);

	double start = 0;
	for (const cv::Vec3b& color : binColors)
	{
		const double hits = std::max(0., hist[adaptiveBinning->getBucketIndex(color)]);

		x = static_cast<int>(start + 0.5);
		start += factor * hits;
		int width = static_cast<int>(start - x + 0.5);
		if (width > 0)
		{
			cv::Mat roi(img, cv::Rect(x, 0, width, img.rows));
			cv::Scalar c = color;
			roi.setTo(c);
		}
	}
	return img;
}
cv::Mat ColorBallHistAdaptive::getProbImage(std::stringstream& ss)
{
	cv::Mat img(100, 380, CV_8UC3);
	img.setTo(0);

	const double factorX = img.cols / static_cast<double>(binning.size());
	double dOptimalHeightScale = 0.5;

	if (isProbModelReady && ballDistsNorm && notBallDistsNorm)
	{
		for (std::map<double, size_t>::iterator it = binning.begin(); it != binning.end(); ++it)
		{
			if ((*ballDistsNorm)[it->second] > 0.5 || (*notBallDistsNorm)[it->second] > 0.5)
			{
				dOptimalHeightScale = 1.0;
				break;
			}
		}
		const double factorY = img.rows / dOptimalHeightScale;
		int x = 0;
		double start = 0;
		for (std::map<double, size_t>::iterator it = binning.begin(); it != binning.end(); ++it)
		{
			double d = (*ballDistsNorm)[it->second] / ((*ballDistsNorm)[it->second] + (*notBallDistsNorm)[it->second]);
			ss << std::setprecision(2) << (it->first-0.05) << "-" << std::setprecision(2) << it->first << std::setprecision(4) << "\t" << (*ballDists)[it->second] << "\t" << (*notBallDists)[it->second] << "\t" << std::setprecision(4) << (d<0.000001 ? 0 : d) << "\t - \t" << (*ballDistsNorm)[it->second] << "\t" << (*notBallDistsNorm)[it->second] << std::endl;

			x = static_cast<int>(start + 0.5);
			start += factorX;
			int width = static_cast<int>(start - x + 0.5);
			double heightBall = (*ballDistsNorm)[it->second] * factorY;
			double heightNotBall = (*notBallDistsNorm)[it->second] * factorY;
			if (heightBall > heightNotBall)
			{
				cv::Mat roi1(img, cv::Rect(x, img.rows - static_cast<int>(heightBall + 0.5), width, static_cast<int>(heightBall + 0.5)));
				roi1.setTo(cv::Scalar(255, 0, 0));
				cv::Mat roi2(img, cv::Rect(x, img.rows - static_cast<int>(heightNotBall + 0.5), width, static_cast<int>(heightNotBall + 0.5)));
				roi2.setTo(cv::Scalar(0, 0, 255));
			}
			else
			{
				cv::Mat roi1(img, cv::Rect(x, img.rows - static_cast<int>(heightNotBall + 0.5), width, static_cast<int>(heightNotBall + 0.5)));
				roi1.setTo(cv::Scalar(0, 0, 255));
				cv::Mat roi2(img, cv::Rect(x, img.rows - static_cast<int>(heightBall + 0.5), width, static_cast<int>(heightBall + 0.5)));
				roi2.setTo(cv::Scalar(255, 0, 0));
			}
		}
	}
	return img;
}
std::function<size_t(const double&)> ColorBallHistAdaptive::getBucketIndexFunc() const
{
	return std::bind(&ColorBallHistAdaptive::getBucketIndex, this, std::placeholders::_1);
}

size_t ColorBallHistAdaptive::getBucketIndex(const double& d) const
{
	std::map<double, size_t>::const_iterator it = binning.lower_bound(d);
	size_t idx;
	if (it == binning.cend())
	{
		idx = binning.size() - 1;
	}
	else
	{
		idx = it->second;
	}
	return idx;
}

std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> ColorBallHistAdaptive::getNormalizedHistogram(cv::Rect const& boundingBox, cv::Mat const& imageBGR, cv::Mat const& foregroundMask, bool trainAdaptiveBinning) const
{
	cv::Rect cropped = cv::Rect(cv::Point(0,0), imageBGR.size()) & boundingBox;
	cv::Mat roi = imageBGR(cropped);
	cv::Mat roiFG = foregroundMask(cropped);
	
	if (!adaptiveBinningPtr->ready())
	{
		if (trainAdaptiveBinning)
		{
			FixedBinHistogram<cv::Vec3b, size_t> histogram(adaptiveBinningPtr->getNumberOfBins(), adaptiveBinningPtr->getBucketIndexFunc());
			for (int r = 0; r < roi.rows; ++r)
			{
				const uchar* roi_r_ = roi.ptr(r);
				const uchar* roiFG_r_ = roiFG.ptr(r);
				for (int c = 0; c < roi.cols; ++c)
				{
					if (roiFG_r_[c] != 0)
					{
						int x = c * 3;
						cv::Vec3b color(roi_r_[x], roi_r_[x + 1], roi_r_[x + 2]);
						*adaptiveBinningPtr << color;
					}
				}
			}
		}
		return nullptr;
	}
	else
	{
		FixedBinHistogram<cv::Vec3b, size_t> histogram(adaptiveBinningPtr->getNumberOfBins(), adaptiveBinningPtr->getBucketIndexFunc());
		for (int r = 0; r < roi.rows; ++r)
		{
			const uchar* roi_r_ = roi.ptr(r);
			const uchar* roiFG_r_ = roiFG.ptr(r);
			for (int c = 0; c < roi.cols; ++c)
			{
				if (roiFG_r_[c] != 0)
				{
					int x = c * 3;
					cv::Vec3b color(roi_r_[x], roi_r_[x + 1], roi_r_[x + 2]);
					histogram << color;

					if (trainAdaptiveBinning)
					{
						*adaptiveBinningPtr << color;
					}
				}
			}
		}
		return histogram.createNormalized();
	}
}

std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> ColorBallHistAdaptive::getNormalizedHistogram(cv::Mat const& imageBGR, cv::Mat const& foregroundMask, bool trainAdaptiveBinning) const
{
	cv::Mat roi = imageBGR;
	cv::Mat roiFG = foregroundMask;

	if (!adaptiveBinningPtr->ready())
	{
		if (trainAdaptiveBinning)
		{
			FixedBinHistogram<cv::Vec3b, size_t> histogram(adaptiveBinningPtr->getNumberOfBins(), adaptiveBinningPtr->getBucketIndexFunc());
			for (int r = 0; r < roi.rows; ++r)
			{
				const uchar* roi_r_ = roi.ptr(r);
				const uchar* roiFG_r_ = roiFG.ptr(r);
				for (int c = 0; c < roi.cols; ++c)
				{
					if (roiFG_r_[c] != 0)
					{
						int x = c * 3;
						cv::Vec3b color(roi_r_[x], roi_r_[x + 1], roi_r_[x + 2]);
						*adaptiveBinningPtr << color;
					}
				}
			}
		}
		return nullptr;
	}
	else
	{
		FixedBinHistogram<cv::Vec3b, size_t> histogram(adaptiveBinningPtr->getNumberOfBins(), adaptiveBinningPtr->getBucketIndexFunc());
		for (int r = 0; r < roi.rows; ++r)
		{
			const uchar* roi_r_ = roi.ptr(r);
			const uchar* roiFG_r_ = roiFG.ptr(r);
			for (int c = 0; c < roi.cols; ++c)
			{
				if (roiFG_r_[c] != 0)
				{
					int x = c * 3;
					cv::Vec3b color(roi_r_[x], roi_r_[x + 1], roi_r_[x + 2]);
					histogram << color;

					if (trainAdaptiveBinning)
					{
						*adaptiveBinningPtr << color;
					}
				}
			}
		}
		return histogram.createNormalized();
	}
}

bool ColorBallHistAdaptive::isReady() const
{ 
	return bIsReady && isProbModelReady;
}