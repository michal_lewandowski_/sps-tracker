#include "stdafx.h"
#include "KFTrack2d.h"
#include <boost/format.hpp>
#include "BallObject.h"

extern double ballRadius;

KFTrack2d::KFTrack2d(const size_t iTrackID, const CameraCalibration& calib, const std::shared_ptr<MovingObject> obj, const int iStateParams, const int iMesaureParams, const int64_t dStartTS, double& maxAbsVelocity, double& maxBallProbability)
	: Track(iTrackID, obj, dStartTS, maxAbsVelocity, maxBallProbability), updateFrom3D(false), learnIn2d(false), numContinous4observations(0)
{
	addTrace(&calib, obj);

	m_KF.init(iStateParams, iMesaureParams, 0, CV_64FC1);
	m_state.create(iStateParams, 1, CV_64FC1);
	m_trackParams.create(6, 1, CV_64FC1);

	cv::setIdentity(m_KF.processNoiseCov);
	cv::setIdentity(m_KF.measurementNoiseCov);
	cv::setIdentity(m_KF.errorCovPost);
	
	double dDeltaTime = 1.0;// / 25.0;
	initMotionModel(iStateParams, iMesaureParams, dDeltaTime);

	m_KF.statePost.create(iStateParams, 1, CV_64FC1);
	m_KF.statePost.setTo(0);
	m_KF.statePost.at<double>(0) = obj->pos2d().x;
	m_KF.statePost.at<double>(1) = obj->pos2d().y;
	if (iStateParams > 4)
	{
		m_KF.statePost.at<double>(2) = -obj->box().width / 2.0;
		m_KF.statePost.at<double>(3) = -obj->box().height / 2.0;
		m_KF.statePost.at<double>(4) = obj->box().width / 2.0;
		m_KF.statePost.at<double>(5) = obj->box().height / 2.0;
	}
	m_estimation = m_KF.measurementMatrix*m_KF.statePost;
	
	m_icov = m_KF.measurementMatrix*m_KF.processNoiseCov*m_KF.measurementMatrix.t() + m_KF.measurementNoiseCov;
	m_det = cv::log(cv::determinant(m_icov));
	m_icov = m_icov.inv();
}
void KFTrack2d::initMotionModel(int iStateParams, int iMesaureParams, double dDeltaTime)
{
	if (iStateParams == 4 && iMesaureParams == 2)
	{
		/* (x, y, Vx, Vy) const speed */
		m_KF.transitionMatrix = (cv::Mat_<double>(iStateParams, iStateParams) << 1, 0, dDeltaTime, 0,
																				  0, 1, 0, dDeltaTime,
																				  0, 0, 1, 0,
																				  0, 0, 0, 1);
		m_KF.measurementMatrix = (cv::Mat_<double>(iMesaureParams, iStateParams) << 1, 0, 0, 0,
																					 0, 1, 0, 0);
		return;
	}
	if (iStateParams == 6 && iMesaureParams == 2)
	{
		/* (x, y, Vx, Vy, Ax, Ay) const acceleration */
		m_KF.transitionMatrix = (cv::Mat_<double>(iStateParams, iStateParams) << 1, 0, dDeltaTime, 0, 0.5*dDeltaTime*dDeltaTime, 0,
																				  0, 1, 0, dDeltaTime, 0, 0.5*dDeltaTime*dDeltaTime,
																				  0, 0, 1, 0, dDeltaTime, 0,
																				  0, 0, 0, 1, 0, dDeltaTime,
																			      0, 0, 0, 0, 1, 0,
																				  0, 0, 0, 0, 0, 1);
		m_KF.measurementMatrix = (cv::Mat_<double>(iMesaureParams, iStateParams) << 1, 0, 0, 0, 0, 0,
																					 0, 1, 0, 0, 0, 0);
		return;
	}
	if (iStateParams == 8 && iMesaureParams == 6)
	{
		/*(x,y,dx1,dy1,dx2,dy2,Vx,Vy) constant speed */
		m_KF.transitionMatrix = (cv::Mat_<double>(iStateParams, iStateParams) << 1, 0, 0, 0, 0, 0, dDeltaTime, 0,
																				  0, 1, 0, 0, 0, 0, 0, dDeltaTime,
																				  0, 0, 1, 0, 0, 0, 0, 0,
																				  0, 0, 0, 1, 0, 0, 0, 0,
																				  0, 0, 0, 0, 1, 0, 0, 0,
																				  0, 0, 0, 0, 0, 1, 0, 0,
																				  0, 0, 0, 0, 0, 0, 1, 0,
																				  0, 0, 0, 0, 0, 0, 0, 1);
		m_KF.measurementMatrix = (cv::Mat_<double>(iMesaureParams, iStateParams) << 1, 0, 0, 0, 0, 0, 0, 0,
																					 0, 1, 0, 0, 0, 0, 0, 0,
																					 1, 0, 1, 0, 0, 0, 0, 0,
																					 0, 1, 0, 1, 0, 0, 0, 0,
																					 1, 0, 0, 0, 1, 0, 0, 0,
																					 0, 1, 0, 0, 0, 1, 0, 0);
		return;
	}
	CV_Error(CV_StsInternal, (boost::format("no motion model for %d states and %d measures") % iStateParams % iMesaureParams).str());
}
KFTrack2d::KFTrack2d(const KFTrack2d& other) 
	: Track(other)
	, updateFrom3D(other.updateFrom3D)
{
	this->m_KF = other.m_KF;
	this->m_state = other.m_state.clone();
	this->m_icov = other.m_icov.clone();
	this->m_det = other.m_det;
	this->m_trackParams = other.m_trackParams.clone();
	this->m_estimation = other.m_estimation.clone();
	this->numContinous4observations = other.numContinous4observations;
}
KFTrack2d& KFTrack2d::operator=(const KFTrack2d& other)
{ 
	if(this != &other)
	{
		this->Track::operator=(other);
		this->m_KF = other.m_KF;
		this->m_state = other.m_state.clone();
		this->m_icov = other.m_icov.clone();
		this->m_det = other.m_det;
		this->m_trackParams = other.m_trackParams.clone();
		this->m_estimation = other.m_estimation.clone();
		this->updateFrom3D = other.updateFrom3D;
		this->numContinous4observations = other.numContinous4observations;
	}
	return *this; 
}

KFTrack2d::~KFTrack2d()
{
}
void KFTrack2d::addTrace(const CameraCalibration* calib, const std::shared_ptr<MovingObject> obj)
{
	CV_Assert(m_trace.size() == 0 || (getCurrentDet()->field() != nullptr && getCurrentDet()->fieldID() == obj->fieldID()));

	obj->pos3d() = calib->transformToRealWorld(obj->pos2d(), ballRadius);
	obj->pos3dEst() = obj->pos3d();
	m_trace.push_back(obj);
	traceLength++;

	numDetected += static_cast<int>(obj->isDetected());
	if (obj->numDetected2d() == 4)
		numContinous4observations++;
	else numContinous4observations = 0;
	computeVelocity();
	
	if (m_trace.size() == maxTraceLength)
	{
		m_trace.pop_front();
	}
}
std::shared_ptr<MovingObject> KFTrack2d::predict(int64_t time)
{
	updateFrom3D = false;
	learnIn2d = false;

	m_dEndTimestamp = time;
	m_state = m_KF.predict();
	m_estimation = m_KF.measurementMatrix*m_state;// +m_KF.measurementNoiseCov.diag(); //m_KF.measurementNoiseCov is constant
	return nullptr;
}
void KFTrack2d::update(std::shared_ptr<MovingObject> obj)
{
	m_iMissedFrames = 0;

	m_trackParams.at<double>(0) = static_cast<double>(obj->pos2d().x);
	m_trackParams.at<double>(1) = static_cast<double>(obj->pos2d().y);
	m_trackParams.at<double>(2) = static_cast<double>(obj->box().tl().x);
	m_trackParams.at<double>(3) = static_cast<double>(obj->box().tl().y);
	m_trackParams.at<double>(4) = static_cast<double>(obj->box().br().x);
	m_trackParams.at<double>(5) = static_cast<double>(obj->box().br().y);
	m_estimation = m_KF.measurementMatrix * m_KF.correct(m_trackParams);

	obj->pos2d() = cv::Point(static_cast<int>(m_estimation.at<double>(0) + 0.5), static_cast<int>(m_estimation.at<double>(1) + 0.5));
	obj->box() = cv::Rect(cv::Point2d(m_estimation.at<double>(2), m_estimation.at<double>(3)), cv::Point2d(m_estimation.at<double>(4), m_estimation.at<double>(5)));
}
void KFTrack2d::update2d(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj)
{
	updateFrom3D = false;
	learnIn2d = false;

	update(obj);
	addTrace(calib, obj);
}
void KFTrack2d::update3d(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj)
{
	updateFrom3D = true;
	cv::Point2d pt;
	calib->transformToPixel(obj->pos3d(), pt);

	getCurrentDet()->pos3d() = obj->pos3d();
	getCurrentDet()->pos2d() = cv::Point2i(cvRound(pt.x), cvRound(pt.y));
	getCurrentDet()->box() = cv::Rect(cvRound(getCurrentDet()->pos2d().x - getCurrentDet()->box().width * 0.5), cvRound(getCurrentDet()->pos2d().y - getCurrentDet()->box().height * 0.5), getCurrentDet()->box().width, getCurrentDet()->box().height);
	update(getCurrentDet());
	computeVelocity();
	removeProbability(getCurrentDet());

	double p = getProb(pt, getCurrentDet()->box());
	getCurrentDet()->assignPropabilityD(p, "pf");
}
void KFTrack2d::assignProximityProbability(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj)
{
	cv::Point2d pt;
	calib->transformToPixel(obj->pos3d(), pt);
	cv::Rect r(cvRound(pt.x - getCurrentDet()->box().width * 0.5), cvRound(pt.y - getCurrentDet()->box().height * 0.5), getCurrentDet()->box().width, getCurrentDet()->box().height);
	computeVelocity();
	removeProbability(getCurrentDet());

	distVariance *= 0.2;
	double p = getProb(pt, r);
	getCurrentDet()->assignPropabilityD(p, "pf");
	distVariance *= 5.0;
}
double KFTrack2d::getDist(const cv::Point2d& p2, const cv::Rect& r) const
{
	//return cv::Mahalanobis(m_estimation, trackParams, m_icov) + m_det;
	//return cv::norm(m_estimation, obj->getTrackParams(m_state.rows - 2));
	//return cv::norm(getCurrentDet()->getTrackParams(), obj->getTrackParams());
	cv::Mat m1 = (cv::Mat_<double>(6, 1) << static_cast<double>(getCurrentDet()->pos2d().x), static_cast<double>(getCurrentDet()->pos2d().y), static_cast<double>(getCurrentDet()->box().tl().x), static_cast<double>(getCurrentDet()->box().tl().y), static_cast<double>(getCurrentDet()->box().br().x), static_cast<double>(getCurrentDet()->box().br().y));
	cv::Mat m2 = (cv::Mat_<double>(6, 1) << p2.x, p2.y, static_cast<double>(r.tl().x), static_cast<double>(r.tl().y), static_cast<double>(r.br().x), static_cast<double>(r.br().y));
	return cv::norm(m1, m2);
}
double KFTrack2d::getDist(const std::shared_ptr<MovingObject> obj) const
{
	return getDist(cv::Point2d(obj->pos2d()), obj->box());
}
double KFTrack2d::getProb(const cv::Point2d& p2, const cv::Rect& r) const
{
	double dd = getDist(p2, r);

	dd = cv::exp(-dd * dd / distVariance);

	return dd;
}
double KFTrack2d::getProb(const std::shared_ptr<MovingObject> obj) const
{
	return getProb(cv::Point2d(obj->pos2d()), obj->box());
}
double KFTrack2d::getCost(const std::shared_ptr<MovingObject> obj) const
{
	//return getDist(obj);
	return 1.0 - getProb(obj);
}
double KFTrack2d::getCostThreshold(void) const
{
	//return distCostThreshold;
	return 1.0 - probCostThreshold;
}
double KFTrack2d::getCostMax(void) const
{
	//return maxDist;
	return 1.0 - maxProb;
}
void KFTrack2d::fillPastLocations(std::vector<PreviousBallLoc>& locs)
{
	locs.clear();
	locs.resize(showTraceAfter, {0, nullptr});
	int last = std::max(static_cast<int>(m_trace.size()) - showTraceAfter - 1, 0);
	for (int i = static_cast<int>(m_trace.size()) - 2, j = 0; i >= last; --i, ++j)
	{
		locs[j].det = m_trace[i];
		locs[j].frameNum = getStartTimestamp() + getLength() - 2 - j;
	}
}
void KFTrack2d::computeVelocity()
{
	m_dAbsVelocity = getAbsoluteVelocity();
	if (isVisible() && m_dAbsVelocity == 0.0 && (updateFrom3D || (getNumDetected() > goodFramesThreshold && getAverageProbability() <= goodNegLogProbThreshold)))
	{
		m_dAbsVelocity = std::numeric_limits<double>::epsilon();
	}
	if (getLength() > 1 && m_dAbsVelocity > maxAbsVelocityGlobal)
	{
		if (verifyVelocity())
		{
			if (isVisible())
			{
				maxAbsVelocityGlobal = m_dAbsVelocity;
			}
		}
		else
		{
			m_dAbsVelocity = 0.0;
		}
	}
}