#include "BgsMOG2cpu.h"


BgsMOG2cpu::BgsMOG2cpu()
	: learningRate(0.005)
{
	MOG2 = cv::createBackgroundSubtractorMOG2();
}

BgsMOG2cpu::~BgsMOG2cpu()
{
}

void BgsMOG2cpu::init(const cv::Mat& oInitImg, const cv::Mat& oROI_)
{
	int closingSize = 2;
	closingElement = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(closingSize * 2 + 1, closingSize * 2 + 1), cv::Point(closingSize, closingSize));

	oROI = oROI_;
}
void BgsMOG2cpu::apply(const cv::Mat& image, cv::Mat& fgmask, double learningRateOverride)
{
	startApply();

	cv::Mat fr;
	if (isHSV)
	{
		cv::cvtColor(image, fr, CV_BGR2HSV);
	}
	else
	{
		fr = image.clone();
	}
	MOG2->apply(image, fgmask, learningRateOverride);
	fgmask.setTo(0, oROI);

	if (isHSV)
	{
		filterMaskByColor(fgmask, fr);
	}
	cv::morphologyEx(fgmask, fgmask, CV_MOP_CLOSE, closingElement, cv::Point(-1, -1), 1, cv::BORDER_CONSTANT);

	stopApply();
}
void BgsMOG2cpu::preprocess(const cv::Mat& iframe, const cv::Size& size, cv::Mat& pframe, int code)
{
	cv::Mat frameBGR;
	cv::demosaicing(iframe, frameBGR, code);
	if (frameBGR.size() != size)
	{
		cv::resize(frameBGR, pframe, size, 0.0, 0.0, cv::INTER_LINEAR);
	}
	else
	{
		pframe = frameBGR;
	}
}
void BgsMOG2cpu::getBackgroundImage(cv::OutputArray backgroundImage) const
{
	MOG2->getBackgroundImage(backgroundImage);
}
void BgsMOG2cpu::loadParams()
{
	{
		int vali;
		float valf;
		cv::FileStorage file(".\\BGS.xml", cv::FileStorage::READ);
		file["history"] >> vali;					MOG2->setHistory(vali);
		file["nmixtures"] >> vali;					MOG2->setNMixtures(vali);
		file["varThreshold"] >> valf;				MOG2->setVarThreshold(valf);
		file["VarInit"] >> valf;					MOG2->setVarInit(valf);
		file["VarMin"] >> valf;						MOG2->setVarMin(valf);
		file["VarMax"] >> valf;						MOG2->setVarMax(valf);
		file["backgroundRatio"] >> valf;			MOG2->setBackgroundRatio(valf);
		file["ComplexityReductionPrior"] >> valf;	MOG2->setComplexityReductionThreshold(valf);
		file["varThresholdGen"] >> valf;			MOG2->setVarThresholdGen(valf);

		file["learningRate"] >> learningRate;
		file["isHSV"] >> isHSV;

		MOG2->setDetectShadows(true);
		MOG2->setShadowValue(0);
		MOG2->setShadowThreshold(0.15);

		file.release();
	}

	{
		cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
		file["colorMinBGRA"] >> colorMin;
		file["colorMaxBGRA"] >> colorMax;
		file.release();
	}
	if (showParams)
	{
		std::cout << "Color filter min [BGRA]: " << colorMin << std::endl;
		std::cout << "Color filter max [BGRA]: " << colorMax << std::endl << std::endl;

		std::cout << "MOG cpu -> history\t" << MOG2->getHistory() << std::endl;
		std::cout << "MOG cpu -> nmixtures\t" << MOG2->getNMixtures() << std::endl;
		std::cout << "MOG cpu -> varThreshold\t" << MOG2->getVarThreshold() << std::endl;
		std::cout << "MOG cpu -> varThresholdGen\t" << MOG2->getVarThresholdGen() << std::endl;
		std::cout << "MOG cpu -> backgroundRatio\t" << MOG2->getBackgroundRatio() << std::endl;
		std::cout << "MOG cpu -> VarInit\t" << MOG2->getVarInit() << std::endl;
		std::cout << "MOG cpu -> VarMin\t" << MOG2->getVarMin() << std::endl;
		std::cout << "MOG cpu -> VarMax\t" << MOG2->getVarMax() << std::endl;
		std::cout << "MOG cpu -> ComplexityReductionPrior\t" << MOG2->getComplexityReductionThreshold() << std::endl;
		showParams = false;
	}
}