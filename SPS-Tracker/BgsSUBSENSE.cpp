#include "BgsSUBSENSE.h"
#include "Utils.h"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iomanip>


#define GHOSTDET_D_MAX (0.010f) 
#define GHOSTDET_S_MIN (0.995f) 
#define FEEDBACK_R_VAR (0.01f)
#define FEEDBACK_V_INCR  (1.000f)
#define FEEDBACK_V_DECR  (0.100f)
#define FEEDBACK_T_DECR  (0.2500f)
#define FEEDBACK_T_INCR  (0.5000f)
#define FEEDBACK_T_LOWER (2.0000f)
#define FEEDBACK_T_UPPER (256.00f)
#define UNSTABLE_REG_RATIO_MIN (0.100f)
#define UNSTABLE_REG_RDIST_MIN (3.000f)
#define LBSPDESC_NONZERO_RATIO_MIN (0.100f)
#define LBSPDESC_NONZERO_RATIO_MAX (0.500f)
#define FRAMELEVEL_MIN_COLOR_DIFF_THRESHOLD  (nMinColorDistThreshold/2)
#define FRAMELEVEL_ANALYSIS_DOWNSAMPLE_RATIO (8)

#define DISPLAY_SUBSENSE_DEBUG_INFO 0
#define DEFAULT_FRAME_SIZE cv::Size(1932,996)
#define STAB_COLOR_DIST_OFFSET (nMinColorDistThreshold/5)
#define UNSTAB_DESC_DIST_OFFSET (nDescDistThresholdOffset)
#define DEFAULT_MEDIAN_BLUR_KERNEL_SIZE (5)

static const size_t s_nColorMaxDataRange_1ch = UCHAR_MAX;
static const size_t s_nDescMaxDataRange_1ch = LBSP::DESC_SIZE * 8;
static const size_t s_nColorMaxDataRange_3ch = s_nColorMaxDataRange_1ch * 3;
static const size_t s_nDescMaxDataRange_3ch = s_nDescMaxDataRange_1ch * 3;

BgsSUBSENSE::BgsSUBSENSE(float fRelLBSPThreshold_
	, size_t nDescDistThresholdOffset_
	, size_t nMinColorDistThreshold_
	, size_t nBGSamples_
	, size_t nRequiredBGSamples_
	, size_t nSamplesForMovingAvgs_)
	: nMinColorDistThreshold(nMinColorDistThreshold_)
	, nDescDistThresholdOffset(nDescDistThresholdOffset_)
	, nBGSamples(nBGSamples_)
	, nRequiredBGSamples(nRequiredBGSamples_)
	, nSamplesForMovingAvgs(nSamplesForMovingAvgs_)
	, fLastNonZeroDescRatio(0.0f)
	, bLearningRateScalingEnabled(true)
	, fCurrLearningRateLowerCap(FEEDBACK_T_LOWER)
	, fCurrLearningRateUpperCap(FEEDBACK_T_UPPER)
	, nMedianBlurKernelSize(nDefaultMedianBlurKernelSize)
	, bUse3x3Spread(true)
	, nImgChannels(0)
	, nImgType(0)
	, nLBSPThresholdOffset(fRelLBSPThreshold_)
	, fRelLBSPThreshold(fRelLBSPThreshold_)
	, nTotPxCount(0)
	, nTotRelevantPxCount(0)
	, nFrameIndex(SIZE_MAX)
	, nFramesSinceLastReset(0)
	, nModelResetCooldown(0)
	, aPxIdxLUT(nullptr)
	, aPxInfoLUT(nullptr)
	, nDefaultMedianBlurKernelSize(DEFAULT_MEDIAN_BLUR_KERNEL_SIZE)
	, bInitialized(false)
	, bAutoModelResetEnabled(true)
	, bUsingMovingCamera(false)
{
	CV_Assert(fRelLBSPThreshold >= 0);
	CV_Assert(nBGSamples > 0 && nRequiredBGSamples <= nBGSamples);
	CV_Assert(nMinColorDistThreshold >= STAB_COLOR_DIST_OFFSET);
}

BgsSUBSENSE::~BgsSUBSENSE()
{
	if (aPxIdxLUT)
		delete[] aPxIdxLUT;
	if (aPxInfoLUT)
		delete[] aPxInfoLUT;
}

void BgsSUBSENSE::init(const cv::Mat& image, const cv::Mat& oROI_)
{
	CV_Assert(!image.empty() && image.cols > 0 && image.rows > 0);
	CV_Assert(image.isContinuous());
	CV_Assert(image.type() == CV_8UC3 || image.type() == CV_8UC1);

	cv::Mat oInitImg;
	if (isHSV)
	{
		cv::cvtColor(image, oInitImg, CV_BGR2GRAY);
		std::vector<cv::Mat> v;
		cv::split(oInitImg, v);
		//cv::resize(v[0], oInitImg, cv::Size(), 0.5, 0.5, cv::INTER_LINEAR);
		oInitImg = v[0];
	}
	else
	{
		//cv::resize(image, oInitImg, cv::Size(), 0.5, 0.5, cv::INTER_LINEAR);
		oInitImg = image;
	}

	int closingSize = 2;
	closingElement = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(closingSize * 2 + 1, closingSize * 2 + 1), cv::Point(closingSize, closingSize));

	if (oInitImg.type() == CV_8UC3)
	{
		std::vector<cv::Mat> voInitImgChannels;
		cv::split(oInitImg, voInitImgChannels);
		if (!cv::countNonZero((voInitImgChannels[0] != voInitImgChannels[1]) | (voInitImgChannels[2] != voInitImgChannels[1])))
			std::cout << std::endl << "\tBgsSUBSENSE : Warning, grayscale images should always be passed in CV_8UC1 format for optimal performance." << std::endl;
	}
	cv::Mat oNewBGROI;
	if (oROI_.empty() && (oROI.empty() || oROI_.size() != oInitImg.size()))
	{
		oNewBGROI.create(oInitImg.size(), CV_8UC1);
		oNewBGROI = cv::Scalar_<uchar>(UCHAR_MAX);
	}
	else if (oROI_.empty())
		oNewBGROI = oROI;
	else
	{
		CV_Assert(oROI_.size() == oInitImg.size() && oROI_.type() == CV_8UC1);
		CV_Assert(cv::countNonZero((oROI_ < UCHAR_MAX)&(oROI_ > 0)) == 0);
		cv::threshold(oROI_, oNewBGROI, 125, 255, CV_THRESH_BINARY_INV);
		cv::Mat oTempROI;
		cv::dilate(oNewBGROI, oTempROI, cv::Mat(), cv::Point(-1, -1), LBSP::PATCH_SIZE / 2);
		cv::bitwise_or(oNewBGROI, oTempROI / 2, oNewBGROI);
	}
	const size_t nOrigROIPxCount = (size_t)cv::countNonZero(oNewBGROI);
	CV_Assert(nOrigROIPxCount > 0);
	LBSP::validateROI(oNewBGROI);
	const size_t nFinalROIPxCount = (size_t)cv::countNonZero(oNewBGROI);
	CV_Assert(nFinalROIPxCount > 0);
	oROI = oNewBGROI;
	oImgSize = oInitImg.size();
	nImgType = oInitImg.type();
	nImgChannels = oInitImg.channels();
	nTotPxCount = oImgSize.area();
	nTotRelevantPxCount = nFinalROIPxCount;
	nFrameIndex = 0;
	nFramesSinceLastReset = 0;
	nModelResetCooldown = 0;
	fLastNonZeroDescRatio = 0.0f;
	const int nTotImgPixels = oImgSize.height*oImgSize.width;
	if (nOrigROIPxCount >= nTotPxCount / 2 && (int)nTotPxCount >= DEFAULT_FRAME_SIZE.area())
	{
		bLearningRateScalingEnabled = true;
		bAutoModelResetEnabled = true;
		bUse3x3Spread = !(nTotImgPixels > DEFAULT_FRAME_SIZE.area() * 2);
		const int nRawMedianBlurKernelSize = std::min((int)floor((float)nTotImgPixels / DEFAULT_FRAME_SIZE.area() + 0.5f) + nDefaultMedianBlurKernelSize, 14);
		nMedianBlurKernelSize = (nRawMedianBlurKernelSize % 2) ? nRawMedianBlurKernelSize : nRawMedianBlurKernelSize - 1;
		fCurrLearningRateLowerCap = FEEDBACK_T_LOWER;
		fCurrLearningRateUpperCap = FEEDBACK_T_UPPER;
	}
	else
	{
		bLearningRateScalingEnabled = false;
		bAutoModelResetEnabled = false;
		bUse3x3Spread = true;
		nMedianBlurKernelSize = nDefaultMedianBlurKernelSize;
		fCurrLearningRateLowerCap = FEEDBACK_T_LOWER * 2;
		fCurrLearningRateUpperCap = FEEDBACK_T_UPPER * 2;
	}
	oUpdateRateFrame.create(oImgSize, CV_32FC1);
	oUpdateRateFrame = cv::Scalar(fCurrLearningRateLowerCap);
	oDistThresholdFrame.create(oImgSize, CV_32FC1);
	oDistThresholdFrame = cv::Scalar(1.0f);
	oVariationModulatorFrame.create(oImgSize, CV_32FC1);
	oVariationModulatorFrame = cv::Scalar(10.0f); // should always be >= FEEDBACK_V_DECR
	oMeanLastDistFrame.create(oImgSize, CV_32FC1);
	oMeanLastDistFrame = cv::Scalar(0.0f);
	oMeanMinDistFrame_LT.create(oImgSize, CV_32FC1);
	oMeanMinDistFrame_LT = cv::Scalar(0.0f);
	oMeanMinDistFrame_ST.create(oImgSize, CV_32FC1);
	oMeanMinDistFrame_ST = cv::Scalar(0.0f);
	oDownSampledFrameSize = cv::Size(oImgSize.width / FRAMELEVEL_ANALYSIS_DOWNSAMPLE_RATIO, oImgSize.height / FRAMELEVEL_ANALYSIS_DOWNSAMPLE_RATIO);
	oMeanDownSampledLastDistFrame_LT.create(oDownSampledFrameSize, CV_32FC((int)nImgChannels));
	oMeanDownSampledLastDistFrame_LT = cv::Scalar(0.0f);
	oMeanDownSampledLastDistFrame_ST.create(oDownSampledFrameSize, CV_32FC((int)nImgChannels));
	oMeanDownSampledLastDistFrame_ST = cv::Scalar(0.0f);
	oMeanRawSegmResFrame_LT.create(oImgSize, CV_32FC1);
	oMeanRawSegmResFrame_LT = cv::Scalar(0.0f);
	oMeanRawSegmResFrame_ST.create(oImgSize, CV_32FC1);
	oMeanRawSegmResFrame_ST = cv::Scalar(0.0f);
	oMeanFinalSegmResFrame_LT.create(oImgSize, CV_32FC1);
	oMeanFinalSegmResFrame_LT = cv::Scalar(0.0f);
	oMeanFinalSegmResFrame_ST.create(oImgSize, CV_32FC1);
	oMeanFinalSegmResFrame_ST = cv::Scalar(0.0f);
	oUnstableRegionMask.create(oImgSize, CV_8UC1);
	oUnstableRegionMask = cv::Scalar_<uchar>(0);
	oBlinksFrame.create(oImgSize, CV_8UC1);
	oBlinksFrame = cv::Scalar_<uchar>(0);
	oDownSampledFrame_MotionAnalysis.create(oDownSampledFrameSize, CV_8UC((int)nImgChannels));
	oDownSampledFrame_MotionAnalysis = cv::Scalar_<uchar>::all(0);
	oLastColorFrame.create(oImgSize, CV_8UC((int)nImgChannels));
	oLastColorFrame = cv::Scalar_<uchar>::all(0);
	oLastDescFrame.create(oImgSize, CV_16UC((int)nImgChannels));
	oLastDescFrame = cv::Scalar_<ushort>::all(0);
	oLastRawFGMask.create(oImgSize, CV_8UC1);
	oLastRawFGMask = cv::Scalar_<uchar>(0);
	oLastFGMask.create(oImgSize, CV_8UC1);
	oLastFGMask = cv::Scalar_<uchar>(0);
	oLastFGMask_dilated.create(oImgSize, CV_8UC1);
	oLastFGMask_dilated = cv::Scalar_<uchar>(0);
	oLastFGMask_dilated_inverted.create(oImgSize, CV_8UC1);
	oLastFGMask_dilated_inverted = cv::Scalar_<uchar>(0);
	oFGMask_FloodedHoles.create(oImgSize, CV_8UC1);
	oFGMask_FloodedHoles = cv::Scalar_<uchar>(0);
	oFGMask_PreFlood.create(oImgSize, CV_8UC1);
	oFGMask_PreFlood = cv::Scalar_<uchar>(0);
	oCurrRawFGBlinkMask.create(oImgSize, CV_8UC1);
	oCurrRawFGBlinkMask = cv::Scalar_<uchar>(0);
	oLastRawFGBlinkMask.create(oImgSize, CV_8UC1);
	oLastRawFGBlinkMask = cv::Scalar_<uchar>(0);
	voBGColorSamples.resize(nBGSamples);
	voBGDescSamples.resize(nBGSamples);
	for (size_t s = 0; s < nBGSamples; ++s)
	{
		voBGColorSamples[s].create(oImgSize, CV_8UC((int)nImgChannels));
		voBGColorSamples[s] = cv::Scalar_<uchar>::all(0);
		voBGDescSamples[s].create(oImgSize, CV_16UC((int)nImgChannels));
		voBGDescSamples[s] = cv::Scalar_<ushort>::all(0);
	}
	if (aPxIdxLUT)
		delete[] aPxIdxLUT;
	if (aPxInfoLUT)
		delete[] aPxInfoLUT;
	aPxIdxLUT = new size_t[nTotRelevantPxCount];
	aPxInfoLUT = new PxInfoBase[nTotPxCount];
	if (nImgChannels == 1)
	{
		CV_Assert(oLastColorFrame.step.p[0] == (size_t)oImgSize.width && oLastColorFrame.step.p[1] == 1);
		CV_Assert(oLastDescFrame.step.p[0] == oLastColorFrame.step.p[0] * 2 && oLastDescFrame.step.p[1] == oLastColorFrame.step.p[1] * 2);
		for (size_t t = 0; t <= UCHAR_MAX; ++t)
			anLBSPThreshold_8bitLUT[t] = cv::saturate_cast<uchar>((nLBSPThresholdOffset + t*fRelLBSPThreshold) / 3);
		for (size_t nPxIter = 0, nModelIter = 0; nPxIter < nTotPxCount; ++nPxIter)
		{
			if (oROI.data[nPxIter])
			{
				aPxIdxLUT[nModelIter] = nPxIter;
				aPxInfoLUT[nPxIter].nImgCoord_Y = (int)nPxIter / oImgSize.width;
				aPxInfoLUT[nPxIter].nImgCoord_X = (int)nPxIter%oImgSize.width;
				aPxInfoLUT[nPxIter].nModelIdx = nModelIter;
				oLastColorFrame.data[nPxIter] = oInitImg.data[nPxIter];
				const size_t nDescIter = nPxIter * 2;
				LBSP::computeGrayscaleDescriptor(oInitImg, oInitImg.data[nPxIter], aPxInfoLUT[nPxIter].nImgCoord_X, aPxInfoLUT[nPxIter].nImgCoord_Y, anLBSPThreshold_8bitLUT[oInitImg.data[nPxIter]], *((ushort*)(oLastDescFrame.data + nDescIter)));
				++nModelIter;
			}
		}
	}
	else
	{ //nImgChannels==3
		CV_Assert(oLastColorFrame.step.p[0] == (size_t)oImgSize.width * 3 && oLastColorFrame.step.p[1] == 3);
		CV_Assert(oLastDescFrame.step.p[0] == oLastColorFrame.step.p[0] * 2 && oLastDescFrame.step.p[1] == oLastColorFrame.step.p[1] * 2);
		for (size_t t = 0; t <= UCHAR_MAX; ++t)
			anLBSPThreshold_8bitLUT[t] = cv::saturate_cast<uchar>(nLBSPThresholdOffset + t*fRelLBSPThreshold);
		for (size_t nPxIter = 0, nModelIter = 0; nPxIter < nTotPxCount; ++nPxIter)
		{
			if (oROI.data[nPxIter])
			{
				aPxIdxLUT[nModelIter] = nPxIter;
				aPxInfoLUT[nPxIter].nImgCoord_Y = (int)nPxIter / oImgSize.width;
				aPxInfoLUT[nPxIter].nImgCoord_X = (int)nPxIter%oImgSize.width;
				aPxInfoLUT[nPxIter].nModelIdx = nModelIter;
				const size_t nPxRGBIter = nPxIter * 3;
				const size_t nDescRGBIter = nPxRGBIter * 2;
				for (size_t c = 0; c < 3; ++c)
				{
					oLastColorFrame.data[nPxRGBIter + c] = oInitImg.data[nPxRGBIter + c];
					LBSP::computeSingleRGBDescriptor(oInitImg, oInitImg.data[nPxRGBIter + c], aPxInfoLUT[nPxIter].nImgCoord_X, aPxInfoLUT[nPxIter].nImgCoord_Y, c, anLBSPThreshold_8bitLUT[oInitImg.data[nPxRGBIter + c]], ((ushort*)(oLastDescFrame.data + nDescRGBIter))[c]);
				}
				++nModelIter;
			}
		}
	}
	bInitialized = true;
	refreshModel(1.0f);
}

void BgsSUBSENSE::preprocess(const cv::Mat& iframe, const cv::Size& size, cv::Mat& pframe, int code)
{
	cv::Mat frameBGR;
	cv::demosaicing(iframe, frameBGR, code);
	if (frameBGR.size() != size)
	{
		cv::resize(frameBGR, pframe, size, 0.0, 0.0, cv::INTER_LINEAR);
	}
	else
	{
		pframe = frameBGR;
	}
}

void BgsSUBSENSE::refreshModel(float fSamplesRefreshFrac, bool bForceFGUpdate)
{
	// == refresh
	CV_Assert(bInitialized);
	CV_Assert(fSamplesRefreshFrac > 0.0f && fSamplesRefreshFrac <= 1.0f);
	const size_t nModelsToRefresh = fSamplesRefreshFrac < 1.0f ? (size_t)(fSamplesRefreshFrac*nBGSamples) : nBGSamples;
	const size_t nRefreshStartPos = fSamplesRefreshFrac < 1.0f ? rand() % nBGSamples : 0;
	if (nImgChannels == 1)
	{
		for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			if (bForceFGUpdate || !oLastFGMask.data[nPxIter])
			{
				for (size_t nCurrModelIdx = nRefreshStartPos; nCurrModelIdx < nRefreshStartPos + nModelsToRefresh; ++nCurrModelIdx)
				{
					int nSampleImgCoord_Y, nSampleImgCoord_X;
					getRandSamplePosition(nSampleImgCoord_X, nSampleImgCoord_Y, aPxInfoLUT[nPxIter].nImgCoord_X, aPxInfoLUT[nPxIter].nImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
					const size_t nSamplePxIdx = oImgSize.width*nSampleImgCoord_Y + nSampleImgCoord_X;
					if (bForceFGUpdate || !oLastFGMask.data[nSamplePxIdx])
					{
						const size_t nCurrRealModelIdx = nCurrModelIdx%nBGSamples;
						voBGColorSamples[nCurrRealModelIdx].data[nPxIter] = oLastColorFrame.data[nSamplePxIdx];
						*((ushort*)(voBGDescSamples[nCurrRealModelIdx].data + nPxIter * 2)) = *((ushort*)(oLastDescFrame.data + nSamplePxIdx * 2));
					}
				}
			}
		}
	}
	else
	{ //nImgChannels==3
		for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			if (bForceFGUpdate || !oLastFGMask.data[nPxIter])
			{
				for (size_t nCurrModelIdx = nRefreshStartPos; nCurrModelIdx < nRefreshStartPos + nModelsToRefresh; ++nCurrModelIdx)
				{
					int nSampleImgCoord_Y, nSampleImgCoord_X;
					getRandSamplePosition(nSampleImgCoord_X, nSampleImgCoord_Y, aPxInfoLUT[nPxIter].nImgCoord_X, aPxInfoLUT[nPxIter].nImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
					const size_t nSamplePxIdx = oImgSize.width*nSampleImgCoord_Y + nSampleImgCoord_X;
					if (bForceFGUpdate || !oLastFGMask.data[nSamplePxIdx])
					{
						const size_t nCurrRealModelIdx = nCurrModelIdx%nBGSamples;
						for (size_t c = 0; c < 3; ++c)
						{
							voBGColorSamples[nCurrRealModelIdx].data[nPxIter * 3 + c] = oLastColorFrame.data[nSamplePxIdx * 3 + c];
							*((ushort*)(voBGDescSamples[nCurrRealModelIdx].data + (nPxIter * 3 + c) * 2)) = *((ushort*)(oLastDescFrame.data + (nSamplePxIdx * 3 + c) * 2));
						}
					}
				}
			}
		}
	}
}
extern int frameGLOBAL;
void BgsSUBSENSE::apply(const cv::Mat& image, cv::Mat& fgmask, double learningRateOverride)
{
	//CV_Assert(image.channels() == 1);

	startApply();

	CV_Assert(bInitialized);
	cv::Mat oInputImg;
	if (isHSV)
	{
		cv::cvtColor(image, oInputImg, CV_BGR2GRAY);
		std::vector<cv::Mat> v;
		cv::split(oInputImg, v);
		//cv::resize(v[0], oInputImg, cv::Size(), 0.5, 0.5, cv::INTER_AREA);
		oInputImg = v[0];
	}
	else
	{
		//cv::resize(image, oInputImg, cv::Size(), 0.5, 0.5, cv::INTER_AREA);
		oInputImg = image;
	}

	//tw1.reset();
	CV_Assert(oInputImg.type() == nImgType && oInputImg.size() == oImgSize);
	CV_Assert(oInputImg.isContinuous());
	fgmask.create(oImgSize, CV_8UC1);
	cv::Mat oCurrFGMask = fgmask;
	memset(oCurrFGMask.data, 0, oCurrFGMask.cols*oCurrFGMask.rows);
	size_t nNonZeroDescCount = 0;
	const float fRollAvgFactor_LT = 1.0f / std::min(++nFrameIndex, nSamplesForMovingAvgs);
	const float fRollAvgFactor_ST = 1.0f / std::min(nFrameIndex, nSamplesForMovingAvgs / 4);
	if (nImgChannels == 1)
	{
		#pragma omp parallel for
		for (int nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			const size_t nDescIter = nPxIter * 2;
			const size_t nFloatIter = nPxIter * 4;
			const int nCurrImgCoord_X = aPxInfoLUT[nPxIter].nImgCoord_X;
			const int nCurrImgCoord_Y = aPxInfoLUT[nPxIter].nImgCoord_Y;
			const uchar nCurrColor = oInputImg.data[nPxIter];
			size_t nMinDescDist = s_nDescMaxDataRange_1ch;
			size_t nMinSumDist = s_nColorMaxDataRange_1ch;
			float* pfCurrDistThresholdFactor = (float*)(oDistThresholdFrame.data + nFloatIter);
			float* pfCurrVariationFactor = (float*)(oVariationModulatorFrame.data + nFloatIter);
			float* pfCurrLearningRate = ((float*)(oUpdateRateFrame.data + nFloatIter));
			float* pfCurrMeanLastDist = ((float*)(oMeanLastDistFrame.data + nFloatIter));
			float* pfCurrMeanMinDist_LT = ((float*)(oMeanMinDistFrame_LT.data + nFloatIter));
			float* pfCurrMeanMinDist_ST = ((float*)(oMeanMinDistFrame_ST.data + nFloatIter));
			float* pfCurrMeanRawSegmRes_LT = ((float*)(oMeanRawSegmResFrame_LT.data + nFloatIter));
			float* pfCurrMeanRawSegmRes_ST = ((float*)(oMeanRawSegmResFrame_ST.data + nFloatIter));
			float* pfCurrMeanFinalSegmRes_LT = ((float*)(oMeanFinalSegmResFrame_LT.data + nFloatIter));
			float* pfCurrMeanFinalSegmRes_ST = ((float*)(oMeanFinalSegmResFrame_ST.data + nFloatIter));
			ushort& nLastIntraDesc = *((ushort*)(oLastDescFrame.data + nDescIter));
			uchar& nLastColor = oLastColorFrame.data[nPxIter];
			const size_t nCurrColorDistThreshold = (size_t)(((*pfCurrDistThresholdFactor)*nMinColorDistThreshold) - ((!oUnstableRegionMask.data[nPxIter])*STAB_COLOR_DIST_OFFSET)) / 2;
			const size_t nCurrDescDistThreshold = ((size_t)1 << ((size_t)floor(*pfCurrDistThresholdFactor + 0.5f))) + nDescDistThresholdOffset + (oUnstableRegionMask.data[nPxIter] * UNSTAB_DESC_DIST_OFFSET);
			ushort nCurrInterDesc, nCurrIntraDesc;
			LBSP::computeGrayscaleDescriptor(oInputImg, nCurrColor, nCurrImgCoord_X, nCurrImgCoord_Y, anLBSPThreshold_8bitLUT[nCurrColor], nCurrIntraDesc);
			oUnstableRegionMask.data[nPxIter] = ((*pfCurrDistThresholdFactor) > UNSTABLE_REG_RDIST_MIN || (*pfCurrMeanRawSegmRes_LT - *pfCurrMeanFinalSegmRes_LT) > UNSTABLE_REG_RATIO_MIN || (*pfCurrMeanRawSegmRes_ST - *pfCurrMeanFinalSegmRes_ST) > UNSTABLE_REG_RATIO_MIN) ? 1 : 0;
			size_t nGoodSamplesCount = 0, nSampleIdx = 0;
			while (nGoodSamplesCount < nRequiredBGSamples && nSampleIdx < nBGSamples)
			{
				const uchar& nBGColor = voBGColorSamples[nSampleIdx].data[nPxIter];
				{
					const size_t nColorDist = L1dist(nCurrColor, nBGColor);
					if (nColorDist > nCurrColorDistThreshold)
						goto failedcheck1ch;
					const ushort& nBGIntraDesc = *((ushort*)(voBGDescSamples[nSampleIdx].data + nDescIter));
					const size_t nIntraDescDist = hdist(nCurrIntraDesc, nBGIntraDesc);
					LBSP::computeGrayscaleDescriptor(oInputImg, nBGColor, nCurrImgCoord_X, nCurrImgCoord_Y, anLBSPThreshold_8bitLUT[nBGColor], nCurrInterDesc);
					const size_t nInterDescDist = hdist(nCurrInterDesc, nBGIntraDesc);
					const size_t nDescDist = (nIntraDescDist + nInterDescDist) / 2;
					if (nDescDist > nCurrDescDistThreshold)
						goto failedcheck1ch;
					const size_t nSumDist = std::min((nDescDist / 4)*(s_nColorMaxDataRange_1ch / s_nDescMaxDataRange_1ch) + nColorDist, s_nColorMaxDataRange_1ch);
					if (nSumDist > nCurrColorDistThreshold)
						goto failedcheck1ch;
					if (nMinDescDist > nDescDist)
						nMinDescDist = nDescDist;
					if (nMinSumDist > nSumDist)
						nMinSumDist = nSumDist;
					nGoodSamplesCount++;
				}
				failedcheck1ch:
					nSampleIdx++;
			}
			const float fNormalizedLastDist = ((float)L1dist(nLastColor, nCurrColor) / s_nColorMaxDataRange_1ch + (float)hdist(nLastIntraDesc, nCurrIntraDesc) / s_nDescMaxDataRange_1ch) / 2;
			*pfCurrMeanLastDist = (*pfCurrMeanLastDist)*(1.0f - fRollAvgFactor_ST) + fNormalizedLastDist*fRollAvgFactor_ST;
			if (nGoodSamplesCount < nRequiredBGSamples)
			{
				// == foreground
				const float fNormalizedMinDist = std::min(1.0f, ((float)nMinSumDist / s_nColorMaxDataRange_1ch + (float)nMinDescDist / s_nDescMaxDataRange_1ch) / 2 + (float)(nRequiredBGSamples - nGoodSamplesCount) / nRequiredBGSamples);
				*pfCurrMeanMinDist_LT = (*pfCurrMeanMinDist_LT)*(1.0f - fRollAvgFactor_LT) + fNormalizedMinDist*fRollAvgFactor_LT;
				*pfCurrMeanMinDist_ST = (*pfCurrMeanMinDist_ST)*(1.0f - fRollAvgFactor_ST) + fNormalizedMinDist*fRollAvgFactor_ST;
				*pfCurrMeanRawSegmRes_LT = (*pfCurrMeanRawSegmRes_LT)*(1.0f - fRollAvgFactor_LT) + fRollAvgFactor_LT;
				*pfCurrMeanRawSegmRes_ST = (*pfCurrMeanRawSegmRes_ST)*(1.0f - fRollAvgFactor_ST) + fRollAvgFactor_ST;
				oCurrFGMask.data[nPxIter] = UCHAR_MAX;
				if (nModelResetCooldown && (rand() % (size_t)FEEDBACK_T_LOWER) == 0)
				{
					const size_t s_rand = rand() % nBGSamples;
					*((ushort*)(voBGDescSamples[s_rand].data + nDescIter)) = nCurrIntraDesc;
					voBGColorSamples[s_rand].data[nPxIter] = nCurrColor;
				}
			}
			else
			{
				// == background
				const float fNormalizedMinDist = ((float)nMinSumDist / s_nColorMaxDataRange_1ch + (float)nMinDescDist / s_nDescMaxDataRange_1ch) / 2;
				*pfCurrMeanMinDist_LT = (*pfCurrMeanMinDist_LT)*(1.0f - fRollAvgFactor_LT) + fNormalizedMinDist*fRollAvgFactor_LT;
				*pfCurrMeanMinDist_ST = (*pfCurrMeanMinDist_ST)*(1.0f - fRollAvgFactor_ST) + fNormalizedMinDist*fRollAvgFactor_ST;
				*pfCurrMeanRawSegmRes_LT = (*pfCurrMeanRawSegmRes_LT)*(1.0f - fRollAvgFactor_LT);
				*pfCurrMeanRawSegmRes_ST = (*pfCurrMeanRawSegmRes_ST)*(1.0f - fRollAvgFactor_ST);
				const size_t nLearningRate = learningRateOverride > 0 ? (size_t)ceil(learningRateOverride) : (size_t)ceil(*pfCurrLearningRate);
				if ((rand() % nLearningRate) == 0)
				{
					const size_t s_rand = rand() % nBGSamples;
					*((ushort*)(voBGDescSamples[s_rand].data + nDescIter)) = nCurrIntraDesc;
					voBGColorSamples[s_rand].data[nPxIter] = nCurrColor;
				}
				int nSampleImgCoord_Y, nSampleImgCoord_X;
				const bool bCurrUsing3x3Spread = bUse3x3Spread && !oUnstableRegionMask.data[nPxIter];
				if (bCurrUsing3x3Spread)
					getRandNeighborPosition_3x3(nSampleImgCoord_X, nSampleImgCoord_Y, nCurrImgCoord_X, nCurrImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
				else
					getRandNeighborPosition_5x5(nSampleImgCoord_X, nSampleImgCoord_Y, nCurrImgCoord_X, nCurrImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
				const size_t n_rand = rand();
				const size_t idx_rand_uchar = oImgSize.width*nSampleImgCoord_Y + nSampleImgCoord_X;
				const size_t idx_rand_flt32 = idx_rand_uchar * 4;
				const float fRandMeanLastDist = *((float*)(oMeanLastDistFrame.data + idx_rand_flt32));
				const float fRandMeanRawSegmRes = *((float*)(oMeanRawSegmResFrame_ST.data + idx_rand_flt32));
				if ((n_rand % (bCurrUsing3x3Spread ? nLearningRate : (nLearningRate / 2 + 1))) == 0
					|| (fRandMeanRawSegmRes > GHOSTDET_S_MIN && fRandMeanLastDist < GHOSTDET_D_MAX && (n_rand % ((size_t)fCurrLearningRateLowerCap)) == 0))
				{
					const size_t idx_rand_ushrt = idx_rand_uchar * 2;
					const size_t s_rand = rand() % nBGSamples;
					*((ushort*)(voBGDescSamples[s_rand].data + idx_rand_ushrt)) = nCurrIntraDesc;
					voBGColorSamples[s_rand].data[idx_rand_uchar] = nCurrColor;
				}
			}
			if (oLastFGMask.data[nPxIter] || (std::min(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST) < UNSTABLE_REG_RATIO_MIN && oCurrFGMask.data[nPxIter]))
			{
				if ((*pfCurrLearningRate) < fCurrLearningRateUpperCap)
					*pfCurrLearningRate += FEEDBACK_T_INCR / (std::max(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST)*(*pfCurrVariationFactor));
			}
			else if ((*pfCurrLearningRate) > fCurrLearningRateLowerCap)
				*pfCurrLearningRate -= FEEDBACK_T_DECR*(*pfCurrVariationFactor) / std::max(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST);
			if ((*pfCurrLearningRate) < fCurrLearningRateLowerCap)
				*pfCurrLearningRate = fCurrLearningRateLowerCap;
			else if ((*pfCurrLearningRate) > fCurrLearningRateUpperCap)
				*pfCurrLearningRate = fCurrLearningRateUpperCap;
			if (std::max(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST) > UNSTABLE_REG_RATIO_MIN && oBlinksFrame.data[nPxIter])
				(*pfCurrVariationFactor) += FEEDBACK_V_INCR;
			else if ((*pfCurrVariationFactor) > FEEDBACK_V_DECR)
			{
				(*pfCurrVariationFactor) -= oLastFGMask.data[nPxIter] ? FEEDBACK_V_DECR / 4 : oUnstableRegionMask.data[nPxIter] ? FEEDBACK_V_DECR / 2 : FEEDBACK_V_DECR;
				if ((*pfCurrVariationFactor) < FEEDBACK_V_DECR)
					(*pfCurrVariationFactor) = FEEDBACK_V_DECR;
			}
			if ((*pfCurrDistThresholdFactor) < std::pow(1.0f + std::min(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST) * 2, 2))
				(*pfCurrDistThresholdFactor) += FEEDBACK_R_VAR*(*pfCurrVariationFactor - FEEDBACK_V_DECR);
			else
			{
				(*pfCurrDistThresholdFactor) -= FEEDBACK_R_VAR / (*pfCurrVariationFactor);
				if ((*pfCurrDistThresholdFactor) < 1.0f)
					(*pfCurrDistThresholdFactor) = 1.0f;
			}
			if (popcount(nCurrIntraDesc) >= 2)
				++nNonZeroDescCount;
			nLastIntraDesc = nCurrIntraDesc;
			nLastColor = nCurrColor;
		}
	}
	else
	{ //nImgChannels==3
		for (size_t nModelIter = 0; nModelIter < nTotRelevantPxCount; ++nModelIter)
		{
			const size_t nPxIter = aPxIdxLUT[nModelIter];
			const int nCurrImgCoord_X = aPxInfoLUT[nPxIter].nImgCoord_X;
			const int nCurrImgCoord_Y = aPxInfoLUT[nPxIter].nImgCoord_Y;
			const size_t nPxIterRGB = nPxIter * 3;
			const size_t nDescIterRGB = nPxIterRGB * 2;
			const size_t nFloatIter = nPxIter * 4;
			const uchar* const anCurrColor = oInputImg.data + nPxIterRGB;
			size_t nMinTotDescDist = s_nDescMaxDataRange_3ch;
			size_t nMinTotSumDist = s_nColorMaxDataRange_3ch;
			float* pfCurrDistThresholdFactor = (float*)(oDistThresholdFrame.data + nFloatIter);
			float* pfCurrVariationFactor = (float*)(oVariationModulatorFrame.data + nFloatIter);
			float* pfCurrLearningRate = ((float*)(oUpdateRateFrame.data + nFloatIter));
			float* pfCurrMeanLastDist = ((float*)(oMeanLastDistFrame.data + nFloatIter));
			float* pfCurrMeanMinDist_LT = ((float*)(oMeanMinDistFrame_LT.data + nFloatIter));
			float* pfCurrMeanMinDist_ST = ((float*)(oMeanMinDistFrame_ST.data + nFloatIter));
			float* pfCurrMeanRawSegmRes_LT = ((float*)(oMeanRawSegmResFrame_LT.data + nFloatIter));
			float* pfCurrMeanRawSegmRes_ST = ((float*)(oMeanRawSegmResFrame_ST.data + nFloatIter));
			float* pfCurrMeanFinalSegmRes_LT = ((float*)(oMeanFinalSegmResFrame_LT.data + nFloatIter));
			float* pfCurrMeanFinalSegmRes_ST = ((float*)(oMeanFinalSegmResFrame_ST.data + nFloatIter));
			ushort* anLastIntraDesc = ((ushort*)(oLastDescFrame.data + nDescIterRGB));
			uchar* anLastColor = oLastColorFrame.data + nPxIterRGB;
			const size_t nCurrColorDistThreshold = (size_t)(((*pfCurrDistThresholdFactor)*nMinColorDistThreshold) - ((!oUnstableRegionMask.data[nPxIter])*STAB_COLOR_DIST_OFFSET));
			const size_t nCurrDescDistThreshold = ((size_t)1 << ((size_t)floor(*pfCurrDistThresholdFactor + 0.5f))) + nDescDistThresholdOffset + (oUnstableRegionMask.data[nPxIter] * UNSTAB_DESC_DIST_OFFSET);
			const size_t nCurrTotColorDistThreshold = nCurrColorDistThreshold * 3;
			const size_t nCurrTotDescDistThreshold = nCurrDescDistThreshold * 3;
			const size_t nCurrSCColorDistThreshold = nCurrTotColorDistThreshold / 2;
			ushort anCurrInterDesc[3], anCurrIntraDesc[3];
			const size_t anCurrIntraLBSPThresholds[3] = { anLBSPThreshold_8bitLUT[anCurrColor[0]],anLBSPThreshold_8bitLUT[anCurrColor[1]],anLBSPThreshold_8bitLUT[anCurrColor[2]] };
			LBSP::computeRGBDescriptor(oInputImg, anCurrColor, nCurrImgCoord_X, nCurrImgCoord_Y, anCurrIntraLBSPThresholds, anCurrIntraDesc);
			oUnstableRegionMask.data[nPxIter] = ((*pfCurrDistThresholdFactor) > UNSTABLE_REG_RDIST_MIN || (*pfCurrMeanRawSegmRes_LT - *pfCurrMeanFinalSegmRes_LT) > UNSTABLE_REG_RATIO_MIN || (*pfCurrMeanRawSegmRes_ST - *pfCurrMeanFinalSegmRes_ST) > UNSTABLE_REG_RATIO_MIN) ? 1 : 0;
			size_t nGoodSamplesCount = 0, nSampleIdx = 0;
			while (nGoodSamplesCount < nRequiredBGSamples && nSampleIdx < nBGSamples)
			{
				const ushort* const anBGIntraDesc = (ushort*)(voBGDescSamples[nSampleIdx].data + nDescIterRGB);
				const uchar* const anBGColor = voBGColorSamples[nSampleIdx].data + nPxIterRGB;
				size_t nTotDescDist = 0;
				size_t nTotSumDist = 0;
				for (size_t c = 0; c < 3; ++c)
				{
					const size_t nColorDist = L1dist(anCurrColor[c], anBGColor[c]);
					if (nColorDist > nCurrSCColorDistThreshold)
						goto failedcheck3ch;
					const size_t nIntraDescDist = hdist(anCurrIntraDesc[c], anBGIntraDesc[c]);
					LBSP::computeSingleRGBDescriptor(oInputImg, anBGColor[c], nCurrImgCoord_X, nCurrImgCoord_Y, c, anLBSPThreshold_8bitLUT[anBGColor[c]], anCurrInterDesc[c]);
					const size_t nInterDescDist = hdist(anCurrInterDesc[c], anBGIntraDesc[c]);
					const size_t nDescDist = (nIntraDescDist + nInterDescDist) / 2;
					const size_t nSumDist = std::min((nDescDist / 2)*(s_nColorMaxDataRange_1ch / s_nDescMaxDataRange_1ch) + nColorDist, s_nColorMaxDataRange_1ch);
					if (nSumDist > nCurrSCColorDistThreshold)
						goto failedcheck3ch;
					nTotDescDist += nDescDist;
					nTotSumDist += nSumDist;
				}
				if (nTotDescDist > nCurrTotDescDistThreshold || nTotSumDist > nCurrTotColorDistThreshold)
					goto failedcheck3ch;
				if (nMinTotDescDist > nTotDescDist)
					nMinTotDescDist = nTotDescDist;
				if (nMinTotSumDist > nTotSumDist)
					nMinTotSumDist = nTotSumDist;
				nGoodSamplesCount++;
			failedcheck3ch:
				nSampleIdx++;
			}
			const float fNormalizedLastDist = ((float)L1dist<3>(anLastColor, anCurrColor) / s_nColorMaxDataRange_3ch + (float)hdist<3>(anLastIntraDesc, anCurrIntraDesc) / s_nDescMaxDataRange_3ch) / 2;
			*pfCurrMeanLastDist = (*pfCurrMeanLastDist)*(1.0f - fRollAvgFactor_ST) + fNormalizedLastDist*fRollAvgFactor_ST;
			if (nGoodSamplesCount < nRequiredBGSamples)
			{
				// == foreground
				const float fNormalizedMinDist = std::min(1.0f, ((float)nMinTotSumDist / s_nColorMaxDataRange_3ch + (float)nMinTotDescDist / s_nDescMaxDataRange_3ch) / 2 + (float)(nRequiredBGSamples - nGoodSamplesCount) / nRequiredBGSamples);
				*pfCurrMeanMinDist_LT = (*pfCurrMeanMinDist_LT)*(1.0f - fRollAvgFactor_LT) + fNormalizedMinDist*fRollAvgFactor_LT;
				*pfCurrMeanMinDist_ST = (*pfCurrMeanMinDist_ST)*(1.0f - fRollAvgFactor_ST) + fNormalizedMinDist*fRollAvgFactor_ST;
				*pfCurrMeanRawSegmRes_LT = (*pfCurrMeanRawSegmRes_LT)*(1.0f - fRollAvgFactor_LT) + fRollAvgFactor_LT;
				*pfCurrMeanRawSegmRes_ST = (*pfCurrMeanRawSegmRes_ST)*(1.0f - fRollAvgFactor_ST) + fRollAvgFactor_ST;
				oCurrFGMask.data[nPxIter] = UCHAR_MAX;
				if (nModelResetCooldown && (rand() % (size_t)FEEDBACK_T_LOWER) == 0)
				{
					const size_t s_rand = rand() % nBGSamples;
					for (size_t c = 0; c < 3; ++c)
					{
						*((ushort*)(voBGDescSamples[s_rand].data + nDescIterRGB + 2 * c)) = anCurrIntraDesc[c];
						*(voBGColorSamples[s_rand].data + nPxIterRGB + c) = anCurrColor[c];
					}
				}
			}
			else
			{
				// == background
				const float fNormalizedMinDist = ((float)nMinTotSumDist / s_nColorMaxDataRange_3ch + (float)nMinTotDescDist / s_nDescMaxDataRange_3ch) / 2;
				*pfCurrMeanMinDist_LT = (*pfCurrMeanMinDist_LT)*(1.0f - fRollAvgFactor_LT) + fNormalizedMinDist*fRollAvgFactor_LT;
				*pfCurrMeanMinDist_ST = (*pfCurrMeanMinDist_ST)*(1.0f - fRollAvgFactor_ST) + fNormalizedMinDist*fRollAvgFactor_ST;
				*pfCurrMeanRawSegmRes_LT = (*pfCurrMeanRawSegmRes_LT)*(1.0f - fRollAvgFactor_LT);
				*pfCurrMeanRawSegmRes_ST = (*pfCurrMeanRawSegmRes_ST)*(1.0f - fRollAvgFactor_ST);
				const size_t nLearningRate = learningRateOverride > 0 ? (size_t)ceil(learningRateOverride) : (size_t)ceil(*pfCurrLearningRate);
				if ((rand() % nLearningRate) == 0)
				{
					const size_t s_rand = rand() % nBGSamples;
					for (size_t c = 0; c < 3; ++c)
					{
						*((ushort*)(voBGDescSamples[s_rand].data + nDescIterRGB + 2 * c)) = anCurrIntraDesc[c];
						*(voBGColorSamples[s_rand].data + nPxIterRGB + c) = anCurrColor[c];
					}
				}
				int nSampleImgCoord_Y, nSampleImgCoord_X;
				const bool bCurrUsing3x3Spread = bUse3x3Spread && !oUnstableRegionMask.data[nPxIter];
				if (bCurrUsing3x3Spread)
					getRandNeighborPosition_3x3(nSampleImgCoord_X, nSampleImgCoord_Y, nCurrImgCoord_X, nCurrImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
				else
					getRandNeighborPosition_5x5(nSampleImgCoord_X, nSampleImgCoord_Y, nCurrImgCoord_X, nCurrImgCoord_Y, LBSP::PATCH_SIZE / 2, oImgSize);
				const size_t n_rand = rand();
				const size_t idx_rand_uchar = oImgSize.width*nSampleImgCoord_Y + nSampleImgCoord_X;
				const size_t idx_rand_flt32 = idx_rand_uchar * 4;
				const float fRandMeanLastDist = *((float*)(oMeanLastDistFrame.data + idx_rand_flt32));
				const float fRandMeanRawSegmRes = *((float*)(oMeanRawSegmResFrame_ST.data + idx_rand_flt32));
				if ((n_rand % (bCurrUsing3x3Spread ? nLearningRate : (nLearningRate / 2 + 1))) == 0
					|| (fRandMeanRawSegmRes > GHOSTDET_S_MIN && fRandMeanLastDist < GHOSTDET_D_MAX && (n_rand % ((size_t)fCurrLearningRateLowerCap)) == 0))
				{
					const size_t idx_rand_uchar_rgb = idx_rand_uchar * 3;
					const size_t idx_rand_ushrt_rgb = idx_rand_uchar_rgb * 2;
					const size_t s_rand = rand() % nBGSamples;
					for (size_t c = 0; c < 3; ++c)
					{
						*((ushort*)(voBGDescSamples[s_rand].data + idx_rand_ushrt_rgb + 2 * c)) = anCurrIntraDesc[c];
						*(voBGColorSamples[s_rand].data + idx_rand_uchar_rgb + c) = anCurrColor[c];
					}
				}
			}
			if (oLastFGMask.data[nPxIter] || (std::min(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST) < UNSTABLE_REG_RATIO_MIN && oCurrFGMask.data[nPxIter]))
			{
				if ((*pfCurrLearningRate) < fCurrLearningRateUpperCap)
					*pfCurrLearningRate += FEEDBACK_T_INCR / (std::max(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST)*(*pfCurrVariationFactor));
			}
			else if ((*pfCurrLearningRate) > fCurrLearningRateLowerCap)
				*pfCurrLearningRate -= FEEDBACK_T_DECR*(*pfCurrVariationFactor) / std::max(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST);
			if ((*pfCurrLearningRate) < fCurrLearningRateLowerCap)
				*pfCurrLearningRate = fCurrLearningRateLowerCap;
			else if ((*pfCurrLearningRate) > fCurrLearningRateUpperCap)
				*pfCurrLearningRate = fCurrLearningRateUpperCap;
			if (std::max(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST) > UNSTABLE_REG_RATIO_MIN && oBlinksFrame.data[nPxIter])
				(*pfCurrVariationFactor) += FEEDBACK_V_INCR;
			else if ((*pfCurrVariationFactor) > FEEDBACK_V_DECR)
			{
				(*pfCurrVariationFactor) -= oLastFGMask.data[nPxIter] ? FEEDBACK_V_DECR / 4 : oUnstableRegionMask.data[nPxIter] ? FEEDBACK_V_DECR / 2 : FEEDBACK_V_DECR;
				if ((*pfCurrVariationFactor) < FEEDBACK_V_DECR)
					(*pfCurrVariationFactor) = FEEDBACK_V_DECR;
			}
			if ((*pfCurrDistThresholdFactor) < std::pow(1.0f + std::min(*pfCurrMeanMinDist_LT, *pfCurrMeanMinDist_ST) * 2, 2))
				(*pfCurrDistThresholdFactor) += FEEDBACK_R_VAR*(*pfCurrVariationFactor - FEEDBACK_V_DECR);
			else
			{
				(*pfCurrDistThresholdFactor) -= FEEDBACK_R_VAR / (*pfCurrVariationFactor);
				if ((*pfCurrDistThresholdFactor) < 1.0f)
					(*pfCurrDistThresholdFactor) = 1.0f;
			}
			if (popcount<3>(anCurrIntraDesc) >= 4)
				++nNonZeroDescCount;
			for (size_t c = 0; c < 3; ++c)
			{
				anLastIntraDesc[c] = anCurrIntraDesc[c];
				anLastColor[c] = anCurrColor[c];
			}
		}
	}

	//std::cout << "Time first part " << tw1.measure().getMilliseconds() << " ms" << std::endl;;
	//tw2.reset();
	cv::bitwise_xor(oCurrFGMask, oLastRawFGMask, oCurrRawFGBlinkMask);
	cv::bitwise_or(oCurrRawFGBlinkMask, oLastRawFGBlinkMask, oBlinksFrame);
	oCurrRawFGBlinkMask.copyTo(oLastRawFGBlinkMask);
	oCurrFGMask.copyTo(oLastRawFGMask);
	cv::morphologyEx(oCurrFGMask, oFGMask_PreFlood, cv::MORPH_CLOSE, cv::Mat());
	oFGMask_PreFlood.copyTo(oFGMask_FloodedHoles);
	cv::floodFill(oFGMask_FloodedHoles, cv::Point(0, 0), UCHAR_MAX);
	cv::bitwise_not(oFGMask_FloodedHoles, oFGMask_FloodedHoles);
	cv::erode(oFGMask_PreFlood, oFGMask_PreFlood, cv::Mat(), cv::Point(-1, -1), 3);
	cv::bitwise_or(oCurrFGMask, oFGMask_FloodedHoles, oCurrFGMask);
	cv::bitwise_or(oCurrFGMask, oFGMask_PreFlood, oCurrFGMask);
	cv::medianBlur(oCurrFGMask, oLastFGMask, nMedianBlurKernelSize);
	cv::dilate(oLastFGMask, oLastFGMask_dilated, cv::Mat(), cv::Point(-1, -1), 3);
	cv::bitwise_and(oBlinksFrame, oLastFGMask_dilated_inverted, oBlinksFrame);
	cv::bitwise_not(oLastFGMask_dilated, oLastFGMask_dilated_inverted);
	cv::bitwise_and(oBlinksFrame, oLastFGMask_dilated_inverted, oBlinksFrame);
	oLastFGMask.copyTo(oCurrFGMask);
	cv::addWeighted(oMeanFinalSegmResFrame_LT, (1.0f - fRollAvgFactor_LT), oLastFGMask, (1.0 / UCHAR_MAX)*fRollAvgFactor_LT, 0, oMeanFinalSegmResFrame_LT, CV_32F);
	cv::addWeighted(oMeanFinalSegmResFrame_ST, (1.0f - fRollAvgFactor_ST), oLastFGMask, (1.0 / UCHAR_MAX)*fRollAvgFactor_ST, 0, oMeanFinalSegmResFrame_ST, CV_32F);
	const float fCurrNonZeroDescRatio = (float)nNonZeroDescCount / nTotRelevantPxCount;
	if (fCurrNonZeroDescRatio < LBSPDESC_NONZERO_RATIO_MIN && fLastNonZeroDescRatio < LBSPDESC_NONZERO_RATIO_MIN)
	{
		for (size_t t = 0; t <= UCHAR_MAX; ++t)
			if (anLBSPThreshold_8bitLUT[t] > cv::saturate_cast<uchar>(nLBSPThresholdOffset + ceil(t*fRelLBSPThreshold / 4)))
				--anLBSPThreshold_8bitLUT[t];
	}
	else if (fCurrNonZeroDescRatio > LBSPDESC_NONZERO_RATIO_MAX && fLastNonZeroDescRatio > LBSPDESC_NONZERO_RATIO_MAX)
	{
		for (size_t t = 0; t <= UCHAR_MAX; ++t)
			if (anLBSPThreshold_8bitLUT[t] < cv::saturate_cast<uchar>(nLBSPThresholdOffset + UCHAR_MAX*fRelLBSPThreshold))
				++anLBSPThreshold_8bitLUT[t];
	}
	fLastNonZeroDescRatio = fCurrNonZeroDescRatio;
	if (bLearningRateScalingEnabled)
	{
		cv::resize(oInputImg, oDownSampledFrame_MotionAnalysis, oDownSampledFrameSize, 0, 0, cv::INTER_AREA);
		cv::accumulateWeighted(oDownSampledFrame_MotionAnalysis, oMeanDownSampledLastDistFrame_LT, fRollAvgFactor_LT);
		cv::accumulateWeighted(oDownSampledFrame_MotionAnalysis, oMeanDownSampledLastDistFrame_ST, fRollAvgFactor_ST);
		size_t nTotColorDiff = 0;
		for (int i = 0; i < oMeanDownSampledLastDistFrame_ST.rows; ++i)
		{
			const size_t idx1 = oMeanDownSampledLastDistFrame_ST.step.p[0] * i;
			for (int j = 0; j < oMeanDownSampledLastDistFrame_ST.cols; ++j)
			{
				const size_t idx2 = idx1 + oMeanDownSampledLastDistFrame_ST.step.p[1] * j;
				nTotColorDiff += (nImgChannels == 1) ?
					(size_t)fabs((*(float*)(oMeanDownSampledLastDistFrame_ST.data + idx2)) - (*(float*)(oMeanDownSampledLastDistFrame_LT.data + idx2))) / 2
					:  //(nImgChannels==3)
					std::max((size_t)fabs((*(float*)(oMeanDownSampledLastDistFrame_ST.data + idx2)) - (*(float*)(oMeanDownSampledLastDistFrame_LT.data + idx2))),
						std::max((size_t)fabs((*(float*)(oMeanDownSampledLastDistFrame_ST.data + idx2 + 4)) - (*(float*)(oMeanDownSampledLastDistFrame_LT.data + idx2 + 4))),
						(size_t)fabs((*(float*)(oMeanDownSampledLastDistFrame_ST.data + idx2 + 8)) - (*(float*)(oMeanDownSampledLastDistFrame_LT.data + idx2 + 8)))));
			}
		}
		const float fCurrColorDiffRatio = (float)nTotColorDiff / (oMeanDownSampledLastDistFrame_ST.rows*oMeanDownSampledLastDistFrame_ST.cols);
		if (bAutoModelResetEnabled)
		{
			if (nFramesSinceLastReset > 1000)
				bAutoModelResetEnabled = false;
			else if (fCurrColorDiffRatio >= FRAMELEVEL_MIN_COLOR_DIFF_THRESHOLD && nModelResetCooldown == 0)
			{
				nFramesSinceLastReset = 0;
				refreshModel(0.1f); // reset 10% of the bg model
				nModelResetCooldown = nSamplesForMovingAvgs / 4;
				oUpdateRateFrame = cv::Scalar(1.0f);
			}
			else
				++nFramesSinceLastReset;
		}
		else if (fCurrColorDiffRatio >= FRAMELEVEL_MIN_COLOR_DIFF_THRESHOLD * 2)
		{
			nFramesSinceLastReset = 0;
			bAutoModelResetEnabled = true;
		}
		if (fCurrColorDiffRatio >= FRAMELEVEL_MIN_COLOR_DIFF_THRESHOLD / 2)
		{
			fCurrLearningRateLowerCap = (float)std::max((int)FEEDBACK_T_LOWER >> (int)(fCurrColorDiffRatio / 2), 1);
			fCurrLearningRateUpperCap = (float)std::max((int)FEEDBACK_T_UPPER >> (int)(fCurrColorDiffRatio / 2), 1);
		}
		else
		{
			fCurrLearningRateLowerCap = FEEDBACK_T_LOWER;
			fCurrLearningRateUpperCap = FEEDBACK_T_UPPER;
		}
		if (nModelResetCooldown > 0)
			--nModelResetCooldown;
	}
	//std::cout << "Time second part " << tw2.measure().getMilliseconds() << " ms" << std::endl;;
	
	//cv::Mat tmp;
	//getBackgroundImage(tmp);
	//{
	//	char filename[180];
	//	sprintf(filename, "D:\\aaa\\sub_background\\seq_%06d.jpg", frameGLOBAL);
	//	cv::imwrite(filename, tmp);
	//}

	cv::Mat test;
	cv::cvtColor(image, test, CV_BGR2HSV);
	filterMaskByColor(oCurrFGMask, test);

	//cv::Mat test;
	//cv::resize(oCurrFGMask, fgmask, cv::Size(), 2, 2, cv::INTER_LINEAR);
	//cv::threshold(fgmask, fgmask, 127, 255, cv::THRESH_BINARY);

	cv::morphologyEx(oCurrFGMask, fgmask, CV_MOP_CLOSE, closingElement, cv::Point(-1, -1), 1, cv::BORDER_CONSTANT);
	cv::medianBlur(fgmask, fgmask, 5);
	
	stopApply();
}

void BgsSUBSENSE::setAutomaticModelReset(bool bVal)
{
	bAutoModelResetEnabled = bVal;
}
void BgsSUBSENSE::loadParams()
{
	{
		int vali;
		float valf;
		cv::FileStorage file(".\\BGS.xml", cv::FileStorage::READ);

		file["isHSV"] >> isHSV;

		file.release();
	}

	{
		cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
		file["colorMinBGRA"] >> colorMin;
		file["colorMaxBGRA"] >> colorMax;
		file.release();
	}

	if (showParams)
	{
		std::cout << "Color filter min [BGRA]: " << colorMin << std::endl;
		std::cout << "Color filter max [BGRA]: " << colorMax << std::endl << std::endl;

		std::cout << "SUBSENSE -> \t" << std::endl;

		showParams = false;
	}
}
void BgsSUBSENSE::getBackgroundImage(cv::OutputArray backgroundImage) const
{
	CV_Assert(bInitialized);
	cv::Mat oAvgBGImg = cv::Mat::zeros(oImgSize, CV_32FC((int)nImgChannels));
	for (size_t s = 0; s < nBGSamples; ++s)
	{
		for (int y = 0; y < oImgSize.height; ++y)
		{
			for (int x = 0; x < oImgSize.width; ++x)
			{
				const size_t idx_nimg = voBGColorSamples[s].step.p[0] * y + voBGColorSamples[s].step.p[1] * x;
				const size_t nFloatIter = idx_nimg * 4;
				float* oAvgBgImgPtr = (float*)(oAvgBGImg.data + nFloatIter);
				const uchar* const oBGImgPtr = voBGColorSamples[s].data + idx_nimg;
				for (size_t c = 0; c < nImgChannels; ++c)
					oAvgBgImgPtr[c] += ((float)oBGImgPtr[c]) / nBGSamples;
			}
		}
	}
	oAvgBGImg.convertTo(backgroundImage, CV_8U);
}