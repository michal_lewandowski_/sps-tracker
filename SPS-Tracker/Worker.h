#pragma once
#include <string>
#include "CameraCalibration.h"
#include "Detector.h"
#include "MovingObject.h"
#include "TimeWatch.hpp"
#include "KFTracker2d.h"
#include "KFTrack3d.h"
#include "ColorBallMap.h"
#include "IBGS.h"
#include "shadow/LrTextureShadRem.h"
#include "shadow/ChromacityShadRem.h"
#include "shadow/PhysicalShadRem.h"
#include "shadow/GeometryShadRem.h"
#include "shadow/SrTextureShadRem.h"
#include "DetectorCascade.h"
#include "MedianFlowTracker.h"

class Worker
{
public:
	Worker(TimeWatch& st, std::string name, std::string video, std::string config, const FieldList& f, cv::Scalar c);
	~Worker();

	bool init();
	bool process(std::deque<std::shared_ptr<KFTrack3d>>& tracks);
	void feedback(std::shared_ptr<KFTrack3d> tr3);
	std::pair<cv::Mat, cv::Mat> show(bool show, bool stop, const std::deque<std::shared_ptr<KFTrack3d>>& tracks3d, int fieldIDonly);
	void show3d(cv::Mat frame, double windowScale) const;

	double getFrameRate();
	inline cv::Size getImageSize() const { return calibration.getImageSize(); };
	//inline int64_t time() { return systemTime.measure().getMilliseconds(); };
	inline int64_t time() { return systemTime.getFrameNum(); };
	Field* inField(const std::shared_ptr<MovingObject> obj);
	inline const FieldList& getFields() const { return fields; };
	inline FieldList& getFields() { return fields; };
	inline const std::vector<cv::Point>& getMask() const { return mask; };
	inline const CameraCalibration& getCalibration() const { return calibration; };
	inline bool isSearchROI(void) const { return evidenceCount > evidenceThreshold; }
	inline std::string getCamID() const { return camID; }
	inline const std::deque<std::shared_ptr<KFTrack2d>>& tr2dlive() const { return tracks2dlive; }
	inline std::deque<std::shared_ptr<KFTrack2d>>& tr2dlive() { return tracks2dlive; }
	inline const std::deque<std::shared_ptr<KFTrack2d>>& tr2ddead() const { return tracks2ddead; }
	inline std::deque<std::shared_ptr<KFTrack2d>>& tr2ddead() { return tracks2ddead; }
	inline const std::vector<std::shared_ptr<MovingObject>>& dets() const { return detections; }
	inline std::vector<std::shared_ptr<MovingObject>>& dets() { return detections; }
	inline int getDetectionWindowSize() const { return detectionWindowSize; }
	inline double getDetectionWindowVar() const { return detectionWindowSizeVariance; }

	void updateEvidenceCount(std::size_t detected);
	void setROIT3d(double size);
	cv::Rect2d getROIT3d(const cv::Point3d pt) const;
	bool isGoodForLearn(const Field& f) const;
	bool isGoodForTLD(std::shared_ptr<KFTrack3d> ballTrack3d);

private:
	Worker(const Worker& other);
	Worker& operator=(const Worker& other);
	
protected:
	void preprocess();
	bool capture();
	void segment();
	void initDetectionWindows(std::deque<std::shared_ptr<KFTrack3d>>& tracks3d);
	void learnBall();

private:
	std::string camID;
	TimeWatch& systemTime;
	std::string videoPath;
	CameraCalibration calibration;
	cv::VideoCapture cap;
	std::unique_ptr<IBGS> bgs;

	FieldList fields;
	std::vector<cv::Point> mask;
	cv::Scalar color;
	cv::Point2d roiT3D;

	int detectionWindowSize;
	double detectionWindowSizeVariance;
	std::size_t evidenceThreshold;
	std::size_t evidenceCount;
	double detectionWindowProbThr;
	int64_t learnColorModelAfter;
	int learnFramesThreshold;
	double learnNegLogProbThreshold;
	int feedbackFramesThreshold;
	double feedbackNegLogProbThreshold;

	bool isTLDready;
	Detector detector;
	KFTracker2d tracker2d;
	std::vector<std::shared_ptr<MovingObject>> detections;
	std::deque<std::shared_ptr<KFTrack2d>> tracks2dlive;
	std::deque<std::shared_ptr<KFTrack2d>> tracks2ddead;
	std::string outputFile;
	DetectorCascade cascadeDetector;
	MedianFlowTracker opticalTracker;

	cv::Mat fieldMask;
	cv::Mat frameTemp;
	cv::Mat frameGray;
	cv::Mat frame;
	cv::Mat motionMask;
	cv::Mat dispFrame;
	cv::Mat dispMotion;
	cv::Mat bgImg;
	cv::Mat newmask;
	cv::Mat newmask1;
	cv::Mat newmask2;
	std::vector<cv::Mat> aa;
	std::vector<cv::Mat> bb;

	ChromacityShadRem chr;
	PhysicalShadRem phy;
	GeometryShadRem geo;
	SrTextureShadRem srTex;
	LrTextureShadRem lrTex;
};
