#include "Stats.h"
#include <numeric>
#include "Utils.h"
#include "CameraCalibration.h"
#include "Field.h"

Stats::Stats(int fid, std::string gtPath, Field* parent)
	: gtColor(50, 100, 155), field(parent)
{
	dataGT = Utils::readGT(gtPath);
	if (dataGT.size() > 0)
	{
		std::cout << "GroundTruth data loaded for field " << (fid) << " -> " << gtPath << std::endl;
		
		for(int i=0; i < numStats; ++i)
		{
			errorsAll[i].reserve(dataGT.size());
			errorsThresholded[i].reserve(dataGT.size());
		}
		
		falsePositivesAll.fill(0);
		falseNegativesAll.fill(0);
		trueNegativesAll.fill(0);
		truePositiveAll.fill(0);
		falsePositivesThresholded.fill(0);
		falseNegativesThresholded.fill(0);
		trueNegativesThresholded.fill(0);
		truePositiveThresholded.fill(0);
		totalFrames.fill(0);

		cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
		file["gtDistThresholdForAvailability"] >> distThreshold;
		file.release();
	}
}


Stats::~Stats()
{
}
void Stats::computeError(int numFrame, cv::Point3d ept)
{
	if (isLoad() && numFrame < dataGT.size())
	{
		cv::Point3d gpt = dataGT[numFrame];
		//double err = cv::norm(gpt - ept);
		double err = sqrt((gpt.x - ept.x)*(gpt.x - ept.x) + (gpt.y - ept.y)*(gpt.y - ept.y));
		computeError(0, gpt, ept, cv::Rect(), err);
		computeError(1, gpt, ept, field->zoneGame, err);
		computeError(2, gpt, ept, cv::Rect(field->zonePlayer1[0].tl(), field->zonePlayer1[2].br()), err);//lower player zone
		computeError(3, gpt, ept, cv::Rect(field->zonePlayer2[0].tl(), field->zonePlayer2[2].br()), err);//upper player zone
	}
}
void Stats::computeError(int index, cv::Point3d gpt, cv::Point3d ept, cv::Rect r, double err)
{
	if(isLoad() && (index ==0 || r.contains(cv::Point(cvRound(gpt.x), cvRound(gpt.y)))))
	{
		totalFrames[index]++;
		if(gpt == cv::Point3d(-1,-1,-1) && ept == cv::Point3d(-1, -1, -1))
		{
			trueNegativesAll[index]++;
			trueNegativesThresholded[index]++;
		}
		else if(ept == cv::Point3d(-1, -1, -1))
		{
			falseNegativesAll[index]++;
			falseNegativesThresholded[index]++;
		}
		else
		{
			if (index == 0 || r.contains(cv::Point(cvRound(ept.x), cvRound(ept.y))))
			{
				if (gpt == cv::Point3d(-1, -1, -1))
				{
					falsePositivesAll[index]++;
					falsePositivesThresholded[index]++;
				}
				else
				{
					truePositiveAll[index]++;
					errorsAll[index].push_back(err);
					if(err < distThreshold)
					{
						errorsThresholded[index].push_back(err);
						truePositiveThresholded[index]++;
					}
					else
					{
						falseNegativesThresholded[index]++;
					}
				}
			}
			else if(index > 0)
			{
				falseNegativesAll[index]++;
				falseNegativesThresholded[index]++;
			}
		}
	}
}
void Stats::computeStats(std::string outputDir, std::string outputName)
{
	if(isLoad())
	{
		std::vector<std::array<double, 3>> outErrors1;
		std::vector<std::array<double, 3>> outErrors2;
		std::vector<std::string> outNames = { "All zones", "Passing zone", "Player zone 1", "Player zone 2" };
		std::vector<std::vector<int>> outErrors3;
		for (int i = 0; i < numStats; ++i)
		{
			if (errorsAll[i].size() > 0)
			{
				std::array<double, 3> v1;
				std::array<double, 3> v2;

				CV_Assert((truePositiveAll[i] + trueNegativesAll[i] + falseNegativesAll[i] + falsePositivesAll[i]) == totalFrames[i]);
				double accuracyAll = 100.0 * static_cast<double>(truePositiveAll[i] + trueNegativesAll[i]) / static_cast<double>(totalFrames[i]);
				double accuracyThresholded = 100.0 * static_cast<double>(truePositiveThresholded[i] + trueNegativesThresholded[i]) / static_cast<double>(totalFrames[i]);

				double sumAll = std::accumulate(errorsAll[i].begin(), errorsAll[i].end(), 0.0, [](double s, const double& err) { return s + err; });
				double mAll = sumAll / static_cast<double>(errorsAll[i].size());
				double maxError = *std::max_element(errorsAll[i].begin(), errorsAll[i].end());
				int maxBound = ceil(maxError / 100.0);
				std::vector<int> histErr(maxBound, 0);

				double accumAll = 0.0;
				std::for_each(std::begin(errorsAll[i]), std::end(errorsAll[i]), [&accumAll, &mAll, &histErr](const double& err)
				{
					accumAll += (err - mAll) * (err - mAll);
					histErr[floor(err / 100.0)]++;
				});

				double stdevAll = sqrt(accumAll / static_cast<double>(errorsAll[i].size()));

				double sumThresholded = std::accumulate(errorsThresholded[i].begin(), errorsThresholded[i].end(), 0.0, [](double s, const double& err) { return s + err; });
				double mThresholded = sumThresholded / static_cast<double>(errorsThresholded[i].size());

				double accumThresholded = 0.0;
				std::for_each(std::begin(errorsThresholded[i]), std::end(errorsThresholded[i]), [&accumThresholded, &mThresholded](const double& err)
				{
					accumThresholded += (err - mThresholded) * (err - mThresholded);
				});

				double stdevThresholded = sqrt(accumThresholded / static_cast<double>(errorsThresholded[i].size()));

				v1[0] = accuracyAll;
				v2[0] = accuracyThresholded;
				v1[1] = cvRound(mAll / 10.0);
				v2[1] = cvRound(mThresholded / 10.0);
				v1[2] = cvRound(stdevAll / 10.0);
				v2[2] = cvRound(stdevThresholded / 10.0);

				std::cout << "Stats " << i << " data availability " << accuracyAll << " - " << accuracyThresholded << " with frames " << totalFrames[i] << " and distance error " << cvRound(mAll / 10.0) << "cm [+-" << cvRound(stdevAll / 10.0) << "cm] - " << cvRound(mThresholded / 10.0) << "cm [+-" << cvRound(stdevThresholded / 10.0) << "cm]" << std::endl;

				outErrors1.push_back(v1);
				outErrors2.push_back(v2);
				outErrors3.push_back(histErr);
			}
		}
		std::stringstream ss;
		ss << outputDir << "\\" << outputName << ".field";
		ss << field->fid;
		ss << ".stats.csv";

		std::ofstream outfile;
		outfile.open(ss.str(), std::ios_base::trunc | std::ios_base::out);
		if (outfile.is_open())
		{
			outfile << "All detected balls,,," << std::endl;
			outfile << ",Data Availability,Mean Error[cm],Std Dev Error[cm]" << std::endl;
			for (int i = 0; i < numStats; ++i)
			{
				outfile << outNames[i];
				for (int j = 0; j < outErrors1[i].size(); ++j)
				{
					outfile << "," << outErrors1[i][j];
				}
				outfile << std::endl;
			}
			outfile << std::endl;
			outfile << "All detected balls closer than " << cvRound(distThreshold/10.0) << " cm,,," << std::endl;
			outfile << ",Data Availability,Mean Error[cm],Std Dev Error[cm]" << std::endl;
			for (int i = 0; i < numStats; ++i)
			{
				outfile << outNames[i];
				for (int j = 0; j < outErrors2[i].size(); ++j)
				{
					outfile << "," << outErrors2[i][j];
				}
				outfile << std::endl;
			}
			outfile << std::endl;

			for (int i = 0; i < numStats; ++i)
			{
				if (outErrors3[i].size() > 0)
				{
					outfile << "All balls - ," << outNames[i] << ",," << std::endl;
					outfile << "Bins,Values,," << std::endl;
					int bin = 5;
					for (auto val : outErrors3[i])
					{
						if(val>1)
						{
							outfile << bin << "," << val << ",,," << std::endl;
						}
						bin += 5;
					}
					outfile << std::endl;
				}
			}

			outfile.close();
		}
	}
}
void Stats::show(int numFrame, cv::Mat frame, CameraCalibration& calibration) const
{
	if (isLoad() && numFrame > 1 && numFrame < dataGT.size())
	{
		std::vector<cv::Point3d>::const_iterator itCurr = ballGT().begin() + numFrame;
		std::vector<cv::Point3d>::const_iterator itPrev = itCurr - 1;
		int count = 0;

		if (itCurr != ballGT().end() && itCurr->x != -1 && itCurr->y != -1 && itCurr->z != -1 && itPrev->x != -1 && itPrev->y != -1 && itPrev->z != -1)
		{
			cv::Point2d pt;
			calibration.transformToPixel(*itCurr, pt);

			cv::circle(frame, pt, 10, gtColor, -1);

			for (; itPrev != ballGT().begin(); itCurr = itPrev, --itPrev)
			{
				if (itCurr->x != -1 && itCurr->y != -1 && itCurr->z != -1 && itPrev->x != -1 && itPrev->y != -1 && itPrev->z != -1)
				{
					cv::Point2d pt1;
					calibration.transformToPixel(*itCurr, pt1);
					cv::Point2d pt2;
					calibration.transformToPixel(*itPrev, pt2);

					cv::line(frame, pt1, pt2, gtColor, 5, CV_AA);
					count++;
					if (count > 25)
						break;
				}
				else
				{
					break;
				}
			}
		}
	}
}
void Stats::show3d(int numFrame, cv::Mat frame, double windowScale) const
{
	if (isLoad() && numFrame > 1 && numFrame < dataGT.size())
	{
		std::vector<cv::Point3d>::const_iterator itCurr = ballGT().begin() + numFrame;
		std::vector<cv::Point3d>::const_iterator itPrev = itCurr - 1;
		int count = 0;

		if (itCurr != ballGT().end() && itCurr->x != -1 && itCurr->y != -1 && itCurr->z != -1 && itPrev->x != -1 && itPrev->y != -1 && itPrev->z != -1)
		{
			cv::circle(frame, cv::Point(cvRound(itCurr->x / windowScale), frame.rows - cvRound(itCurr->y / windowScale)), 10, gtColor, -1);
			for (; itPrev != ballGT().begin(); itCurr = itPrev, --itPrev)
			{
				if (itCurr->x != -1 && itCurr->y != -1 && itCurr->z != -1 && itPrev->x != -1 && itPrev->y != -1 && itPrev->z != -1)
				{
					cv::line(frame, cv::Point(cvRound(itPrev->x / windowScale), frame.rows - cvRound(itPrev->y / windowScale)), cv::Point(cvRound(itCurr->x / windowScale), frame.rows - cvRound(itCurr->y / windowScale)), gtColor, 5, CV_AA);
					count++;
					if (count > 25)
						break;
				}
				else
				{
					break;
				}
			}
		}
	}
}
