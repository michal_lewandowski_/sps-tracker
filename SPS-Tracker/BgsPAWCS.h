#pragma once

#include "LBSP.h"
#include "IBGS.h"

#define BGSPAWCS_DEFAULT_LBSP_REL_SIMILARITY_THRESHOLD (0.333f)
#define BGSPAWCS_DEFAULT_DESC_DIST_THRESHOLD_OFFSET (2)
#define BGSPAWCS_DEFAULT_MIN_COLOR_DIST_THRESHOLD (20)
#define BGSPAWCS_DEFAULT_MAX_NB_WORDS (50)
#define BGSPAWCS_DEFAULT_N_SAMPLES_FOR_MV_AVGS (100)

class BgsPAWCS : public IBGS
{
public:
	BgsPAWCS(float fRelLBSPThreshold = BGSPAWCS_DEFAULT_LBSP_REL_SIMILARITY_THRESHOLD,
		size_t nDescDistThresholdOffset = BGSPAWCS_DEFAULT_DESC_DIST_THRESHOLD_OFFSET,
		size_t nMinColorDistThreshold = BGSPAWCS_DEFAULT_MIN_COLOR_DIST_THRESHOLD,
		size_t nMaxNbWords = BGSPAWCS_DEFAULT_MAX_NB_WORDS,
		size_t nSamplesForMovingAvgs = BGSPAWCS_DEFAULT_N_SAMPLES_FOR_MV_AVGS);
	virtual ~BgsPAWCS();
	virtual void init(const cv::Mat& oInitImg, const cv::Mat& oROI);
	virtual void refreshModel(size_t nBaseOccCount, float fOccDecrFrac, bool bForceFGUpdate = false);
	virtual void apply(const cv::Mat& image, cv::Mat& fgmask, double learningRateOverride = 0);
	virtual void preprocess(const cv::Mat& iframe, const cv::Size& size, cv::Mat& pframe, int code = cv::COLOR_BayerBG2BGR);
	virtual void loadParams();
	void setAutomaticModelReset(bool);
	void getBackgroundImage(cv::OutputArray backgroundImage) const;

protected:
	template<size_t nChannels>
	struct ColorLBSPFeature
	{
		uchar anColor[nChannels];
		ushort anDesc[nChannels];
	};
	struct LocalWordBase
	{
		size_t nFirstOcc;
		size_t nLastOcc;
		size_t nOccurrences;
	};
	template<typename T>
	struct LocalWord : LocalWordBase
	{
		T oFeature;
	};
	struct GlobalWordBase
	{
		float fLatestWeight;
		cv::Mat oSpatioOccMap;
		uchar nDescBITS;
	};
	template<typename T>
	struct GlobalWord : GlobalWordBase
	{
		T oFeature;
	};
	typedef LocalWord<ColorLBSPFeature<1>> LocalWord_1ch;
	typedef LocalWord<ColorLBSPFeature<3>> LocalWord_3ch;
	typedef GlobalWord<ColorLBSPFeature<1>> GlobalWord_1ch;
	typedef GlobalWord<ColorLBSPFeature<3>> GlobalWord_3ch;
	struct PxInfo_PAWCS
	{
		int nImgCoord_Y;
		int nImgCoord_X;
		size_t nModelIdx;
		size_t nGlobalWordMapLookupIdx;
		GlobalWordBase** apGlobalDictSortLUT;
	};
	cv::Size oImgSize;
	size_t nImgChannels;
	int nImgType;
	const size_t nLBSPThresholdOffset;
	const float fRelLBSPThreshold;
	size_t nTotPxCount, nTotRelevantPxCount;
	size_t nFrameIndex, nFramesSinceLastReset, nModelResetCooldown;
	size_t anLBSPThreshold_8bitLUT[UCHAR_MAX + 1];
	size_t* aPxIdxLUT;
	PxInfo_PAWCS* aPxInfoLUT;
	const int nDefaultMedianBlurKernelSize;
	bool bInitialized;
	bool bAutoModelResetEnabled;
	bool bUsingMovingCamera;
	cv::Mat oLastColorFrame;
	cv::Mat oLastDescFrame;
	cv::Mat oLastFGMask;
	const size_t nMinColorDistThreshold;
	const size_t nDescDistThresholdOffset;
	size_t nMaxLocalWords, nCurrLocalWords;
	size_t nMaxGlobalWords, nCurrGlobalWords;
	const size_t nSamplesForMovingAvgs;
	float fLastNonFlatRegionRatio;
	int nMedianBlurKernelSize;
	cv::Size oDownSampledFrameSize_MotionAnalysis, oDownSampledFrameSize_GlobalWordLookup;
	cv::Mat oDownSampledROI_MotionAnalysis;
	size_t nDownSampledROIPxCount;
	size_t nLocalWordWeightOffset;

	LocalWordBase** apLocalWordDict;
	LocalWord_1ch* aLocalWordList_1ch, *pLocalWordListIter_1ch;
	LocalWord_3ch* aLocalWordList_3ch, *pLocalWordListIter_3ch;
	GlobalWordBase** apGlobalWordDict;
	GlobalWord_1ch* aGlobalWordList_1ch, *pGlobalWordListIter_1ch;
	GlobalWord_3ch* aGlobalWordList_3ch, *pGlobalWordListIter_3ch;
	PxInfo_PAWCS* aPxInfoLUT_PAWCS;

	cv::Mat oIllumUpdtRegionMask;
	cv::Mat oUpdateRateFrame;
	cv::Mat oDistThresholdFrame;
	cv::Mat oDistThresholdVariationFrame;
	cv::Mat oMeanMinDistFrame_LT, oMeanMinDistFrame_ST;
	cv::Mat oMeanDownSampledLastDistFrame_LT, oMeanDownSampledLastDistFrame_ST;
	cv::Mat oMeanRawSegmResFrame_LT, oMeanRawSegmResFrame_ST;
	cv::Mat oMeanFinalSegmResFrame_LT, oMeanFinalSegmResFrame_ST;
	cv::Mat oUnstableRegionMask;
	cv::Mat oBlinksFrame;
	cv::Mat oDownSampledFrame_MotionAnalysis;
	cv::Mat oLastRawFGMask;

	cv::Mat oFGMask_PreFlood;
	cv::Mat oFGMask_FloodedHoles;
	cv::Mat oLastFGMask_dilated;
	cv::Mat oLastFGMask_dilated_inverted;
	cv::Mat oCurrRawFGBlinkMask;
	cv::Mat oLastRawFGBlinkMask;
	cv::Mat oTempGlobalWordWeightDiffFactor;
	cv::Mat oMorphExStructElement;

	void CleanupDictionaries();
	static float GetLocalWordWeight(const LocalWordBase* w, size_t nCurrFrame, size_t nOffset);
	static float GetGlobalWordWeight(const GlobalWordBase* w);
};
