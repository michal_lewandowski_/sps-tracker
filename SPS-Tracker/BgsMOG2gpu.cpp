#include "BgsMOG2gpu.h"


BgsMOG2gpu::BgsMOG2gpu()
	: learningRate(0.005)
{
#ifdef SPS_GPU_MODE
	MOG2 = cv::cuda::createBackgroundSubtractorMOG2();
#endif
}

BgsMOG2gpu::~BgsMOG2gpu()
{
}

void BgsMOG2gpu::init(const cv::Mat& oInitImg, const cv::Mat& oROI_)
{
#ifdef SPS_GPU_MODE
	int closingSize = 2;
	closingElement = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(closingSize * 2 + 1, closingSize * 2 + 1), cv::Point(closingSize, closingSize));

	oROI = oROI_;
	roiGPU.upload(oROI);

	frameGrayGPU.create(oInitImg.rows, oInitImg.cols, CV_8UC1);
	motionTempGPU.create(oInitImg.rows, oInitImg.cols, CV_8UC3);
#endif
}
void BgsMOG2gpu::apply(const cv::Mat& image, cv::Mat& fgmask, double learningRateOverride)
{
#ifdef SPS_GPU_MODE
	startApply();

	cv::Mat fr;
	if (isHSV)
	{
		cv::cvtColor(image, fr, CV_BGR2HSV);
	}
	else
	{
		fr = image.clone();
	}

	cv::cuda::GpuMat frGPU;
	frGPU.upload(fr);
	MOG2->apply(frGPU, motionTempGPU, learningRateOverride);
	motionTempGPU.setTo(0, roiGPU);
	motionTempGPU.download(fgmask);

	if (isHSV)
	{
		filterMaskByColor(fgmask, fr);
	}

	cv::morphologyEx(fgmask, fgmask, CV_MOP_CLOSE, closingElement, cv::Point(-1, -1), 1, cv::BORDER_CONSTANT);
	cv::medianBlur(fgmask, fgmask, 5);

	stopApply();
#endif
}
void BgsMOG2gpu::preprocess(const cv::Mat& iframe, const cv::Size& size, cv::Mat& pframe, int)
{
#ifdef SPS_GPU_MODE
	frameGrayGPU.upload(iframe);
	cv::cuda::demosaicing(frameGrayGPU, frameBGRGPU, cv::COLOR_BayerBG2RGB);

	if (frameBGRGPU.size() != size)
	{
		cv::cuda::resize(frameBGRGPU, frameGPU, size, 0.0, 0.0, cv::INTER_LINEAR);
	}
	else
	{
		frameGPU = frameBGRGPU;
	}
	frameGPU.download(pframe);
#endif
}
void BgsMOG2gpu::getBackgroundImage(cv::OutputArray backgroundImage) const
{
	MOG2->getBackgroundImage(backgroundImage);
}
void BgsMOG2gpu::loadParams()
{
	{
		int vali;
		float valf;
		cv::FileStorage file(".\\BGS.xml", cv::FileStorage::READ);
		file["history"] >> vali;					MOG2->setHistory(vali);
		file["nmixtures"] >> vali;					MOG2->setNMixtures(vali);
		file["varThreshold"] >> valf;				MOG2->setVarThreshold(valf);
		file["VarInit"] >> valf;					MOG2->setVarInit(valf);
		file["VarMin"] >> valf;						MOG2->setVarMin(valf);
		file["VarMax"] >> valf;						MOG2->setVarMax(valf);
		file["backgroundRatio"] >> valf;			MOG2->setBackgroundRatio(valf);
		file["ComplexityReductionPrior"] >> valf;	MOG2->setComplexityReductionThreshold(valf);
		file["varThresholdGen"] >> valf;			MOG2->setVarThresholdGen(valf);

		file["learningRate"] >> learningRate;
		file["isHSV"] >> isHSV;

		MOG2->setDetectShadows(true);
		MOG2->setShadowValue(0);
		MOG2->setShadowThreshold(0.15);

		file.release();
	}

	{
		cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
		file["colorMinBGRA"] >> colorMin;
		file["colorMaxBGRA"] >> colorMax;
		file.release();
	}

	if (showParams)
	{
		std::cout << "Color filter min [BGRA]: " << colorMin << std::endl;
		std::cout << "Color filter max [BGRA]: " << colorMax << std::endl << std::endl;

		std::cout << "MOG gpu -> history\t" << MOG2->getHistory() << std::endl;
		std::cout << "MOG gpu -> nmixtures\t" << MOG2->getNMixtures() << std::endl;
		std::cout << "MOG gpu -> varThreshold\t" << MOG2->getVarThreshold() << std::endl;
		std::cout << "MOG gpu -> varThresholdGen\t" << MOG2->getVarThresholdGen() << std::endl;
		std::cout << "MOG gpu -> backgroundRatio\t" << MOG2->getBackgroundRatio() << std::endl;
		std::cout << "MOG gpu -> VarInit\t" << MOG2->getVarInit() << std::endl;
		std::cout << "MOG gpu -> VarMin\t" << MOG2->getVarMin() << std::endl;
		std::cout << "MOG gpu -> VarMax\t" << MOG2->getVarMax() << std::endl;
		std::cout << "MOG gpu -> ComplexityReductionPrior\t" << MOG2->getComplexityReductionThreshold() << std::endl;
		showParams = false;
	}
}