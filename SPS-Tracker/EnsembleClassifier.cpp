#include "EnsembleClassifier.h"
#include "NormalisedPatch.h"
#include "Field.h"
#include "Utils.h"

int EnsembleClassifier::numIndices;
float *EnsembleClassifier::features;
const int EnsembleClassifier::numTrees = 10;
const int EnsembleClassifier::numFeatures = 5;

EnsembleClassifier::EnsembleClassifier()
{

}

EnsembleClassifier::~EnsembleClassifier()
{
    release();
}


void EnsembleClassifier::init()
{
	release();

    numIndices = static_cast<int>(pow(2, numFeatures));
    initFeatureLocations();
}

void EnsembleClassifier::release()
{
	if (features)
	{
		delete[] features;
		features = nullptr;
	}
}
void EnsembleClassifier::initFeatureLocations()
{
    int size = 2 * 2 * numFeatures * numTrees;
    features = new float[size];
	for (int i = 0; i < size; i++)
	{
		features[i] = rand() / (float) RAND_MAX;
	}
}

int EnsembleClassifier::calcFernFeature(Field& f, int windowIdx, int treeIdx)
{
	int index = 0;
	int *bbox = &f.windowOffsets[0] + windowIdx * TLD_WINDOW_OFFSET_SIZE;
	int *off = &f.featureOffsets[0] + bbox[4] + treeIdx * 2 * numFeatures;

	//CV_DbgAssert(windowIdx * TLD_WINDOW_OFFSET_SIZE < TLD_WINDOW_OFFSET_SIZE * detectionResult->numWindows);
	//CV_DbgAssert(bbox[4] + treeIdx * 2 * numFeatures < numScales * numTrees * numFeatures * 2);
	
    for(int i = 0; i < numFeatures; i++)
    {
        index <<= 1;

		int fp0 = img1.data[bbox[0] + off[0]];
		int fp1 = img1.data[bbox[0] + off[1]];

		if (fp0 > fp1)
		{
			index |= 1;
		}
        off += 2;
    }
    return index;
}

int EnsembleClassifier::calcFernFeature(Field& f, cv::Rect bb, int treeIdx)
{
	int index = 0;
	int *off = &f.featureOffsets[0] + bb.height + treeIdx * 2 * numFeatures;

	for (int i = 0; i < numFeatures; i++)
	{
		index <<= 1;

		int fp0 = img1.data[bb.x + off[0]];
		int fp1 = img1.data[bb.x + off[1]];

		if (fp0 > fp1)
		{
			index |= 1;
		}

		off += 2;
	}
	return index;
}

void EnsembleClassifier::calcFeatureVector(Field& f, int windowIdx, int *featureVector)
{
	for (int i = 0; i < numTrees; i++)
	{
		featureVector[i] = calcFernFeature(f, windowIdx, i);
	}
}
void EnsembleClassifier::calcFeatureVector(Field& f, cv::Rect bb, int* featureVector)
{
	for (int i = 0; i < numTrees; i++)
	{
		featureVector[i] = calcFernFeature(f, bb, i);
	}
}

float EnsembleClassifier::calcConfidence(Field& f, int *featureVector)
{
    float conf = 0.0;

    for(int i = 0; i < numTrees; i++)
    {
		CV_Assert(i * numIndices + featureVector[i] < numTrees * numIndices);
        conf += f.posteriors[i * numIndices + featureVector[i]];
    }

    return conf;
}
bool EnsembleClassifier::filter(int idx, FieldList& fields)
{
	//CV_DbgAssert(idx < detectionResult->numWindows);
	//CV_DbgAssert(idx < detectionResult->numWindows * numTrees);

	bool nextStage = false;
	for (auto& f : fields)
	{
		if (idx < f.numWindows && !f.detectionResult.lastStageInvalid[idx])
		{
			calcFeatureVector(f, idx, &f.detectionResult.featureVectors[numTrees * idx]);
			f.detectionResult.posteriors[idx] = calcConfidence(f, &f.detectionResult.featureVectors[numTrees * idx]);

			if (f.detectionResult.posteriors[idx] < 0.5)
			{
				f.detectionResult.lastStageInvalid[idx] = 1;
			}
			else
			{
				nextStage = true;
			}
		}
	}
	return nextStage;
}

void EnsembleClassifier::learn(Field& f, int positive, int *featureVector)
{
	float conf = calcConfidence(f, featureVector);
	if ((positive && conf < 0.5) || (!positive && conf > 0.5))
	{
		updatePosteriors(f, featureVector, positive, 1);
	}
}

void EnsembleClassifier::updatePosterior(Field& f, int treeIdx, int idx, int positive, int amount)
{
    int arrayIndex = treeIdx * numIndices + idx;
	(positive) ? f.positives[arrayIndex] += amount : f.negatives[arrayIndex] += amount;
	f.posteriors[arrayIndex] = static_cast<float>(f.positives[arrayIndex]) / (f.positives[arrayIndex] + f.negatives[arrayIndex]) / static_cast<float>(numTrees);
}

void EnsembleClassifier::updatePosteriors(Field& f, int *featureVector, int positive, int amount)
{
    for(int i = 0; i < numTrees; i++)
    {
        int idx = featureVector[i];
		updatePosterior(f, i, idx, positive, amount);
    }
}