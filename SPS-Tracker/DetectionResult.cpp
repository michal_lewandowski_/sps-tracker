#include "DetectionResult.h"
#include "EnsembleClassifier.h"

DetectionResult::DetectionResult()
{
    containsValidData = false;
    numClusters = 0;
    detectorBB = nullptr;

    variances = nullptr;
    posteriors = nullptr;
    featureVectors = nullptr;
	
	binaryP = nullptr;
	binaryN = nullptr;

	conf = 0.0f;
}

DetectionResult::~DetectionResult()
{
    release();
}

void DetectionResult::init(int nw)
{
	release();

	numWindows = nw;
    variances = new float[numWindows];
    posteriors = new float[numWindows];
	featureVectors = new int[numWindows * EnsembleClassifier::numTrees];
	binaryP = new int[numWindows];
	binaryN = new int[numWindows];
	confidentIndices.clear();
	confidentIndicesThread.resize(numWindows, 0);
	confidentIndices.reserve(numWindows);
	lastStageInvalid.resize(numWindows, 0);
	numClusters = 0;
	conf = 0.0f;

	memset(binaryP,0,numWindows*sizeof(int));
	memset(binaryN,0,numWindows*sizeof(int));
#ifdef SPS_LAB
	status.resize(numWindows, false);
#endif
}

void DetectionResult::reset()
{
    containsValidData = false;
    numClusters = 0;
	conf = 0.0f;
	if (detectorBB)
	{
		delete detectorBB;
		detectorBB = nullptr;
	}
	confidentIndices.clear();
	if(confidentIndicesThread.size() > 0)
	{
		memset(&confidentIndicesThread[0], 0, confidentIndicesThread.size() * sizeof(confidentIndicesThread[0]));
		memset(&lastStageInvalid[0], 0, lastStageInvalid.size() * sizeof(lastStageInvalid[0]));
#ifdef SPS_LAB
		memset(&status[0], 0, status.size() * sizeof(status[0]));
#endif	
	}
}

void DetectionResult::release()
{
	if (variances)
	{
		delete[] variances;
		variances = nullptr;
	}
	if (posteriors)
	{
		delete[] posteriors;
		posteriors = nullptr;
	}
	if (featureVectors)
	{
		delete[] featureVectors;
		featureVectors = nullptr;
	}
	if (binaryP)
	{
		delete[] binaryP;
		binaryP = nullptr;
	}
	if (binaryN)
	{
		delete[] binaryN;
		binaryN = nullptr;
	}
	reset();
}
TrackResult::TrackResult()
{
	trackerBB = nullptr;
	conf = 0.0f;
}

TrackResult::~TrackResult()
{
	release();
}

void TrackResult::init()
{
	release();
}

void TrackResult::reset()
{
	if (trackerBB)
	{
		delete trackerBB;
		trackerBB = nullptr;
	}
	conf = 0.0f;
}

void TrackResult::release()
{
	reset();
}