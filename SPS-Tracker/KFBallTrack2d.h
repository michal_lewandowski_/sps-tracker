#pragma once
#include <opencv2/opencv.hpp>
#include "KFTrack2d.h"

class KFBallTrack2d : public KFTrack2d
{
public:
	KFBallTrack2d(const size_t iTrackID, const CameraCalibration& calib, const std::shared_ptr<MovingObject> obj, const int iStateParams, const int iMesaureParams, const int64_t dStartTS, cv::Size imgSize, int detectionWindow, double& maxAbsVelocity, double& maxBallProbability);
	virtual ~KFBallTrack2d();

	virtual std::shared_ptr<MovingObject> missFrame(const CameraCalibration* calib) override;
	virtual void addTrace(const CameraCalibration* calib, const std::shared_ptr<MovingObject> obj) override;
	virtual bool isDead(const std::vector<cv::Point>& Mask, const FieldList& fields) override;
	virtual void printInfo(const std::vector<cv::Point>& Mask, const FieldList& fields) const override;
	virtual void show(cv::Mat frame, cv::Scalar color) const override;
	virtual void show(cv::Mat frame, double windowScale, cv::Scalar color) const;
	virtual void show(cv::Mat frame, const CameraCalibration* calib, cv::Scalar color) const;
	virtual bool assignProbability() override;
	virtual void assignProbability(std::shared_ptr<MovingObject> obj) override;
	virtual void removeProbability(std::shared_ptr<MovingObject> obj) override;
	virtual bool isGoodEnoughFor() const override;
	bool isInROI(cv::Rect& bb) const;
	void setROI(const std::shared_ptr<MovingObject> obj);
	inline const cv::Rect& roi() const { return m_roi; }
	
protected:
	KFBallTrack2d(const KFBallTrack2d& other);
	KFBallTrack2d& operator=(const KFBallTrack2d& other);

protected:
	cv::Point detectionUncertainness;
	cv::Rect m_roi;
	cv::Size imageSize;
	double ballMaxDiameterMM;
};
