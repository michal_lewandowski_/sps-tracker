#include "Clustering.h"
#include "NormalisedPatch.h"
#include "Field.h"
#include "DetectorCascade.h"
#include "Utils.h"

Clustering::Clustering()
{
    cutoff = 0.5;
}

Clustering::~Clustering()
{
}

void Clustering::release()
{

}

void Clustering::calcMeanRect(Field& f, const std::vector<int>& confidentIndices)
{
    float x, y, w, h;
    x = y = w = h = 0;

	int numIndices = static_cast<int>(confidentIndices.size());
    for(int i = 0; i < numIndices; i++)
    {
		int *bb = &f.windows[TLD_WINDOW_SIZE * confidentIndices[i]];
        x += bb[0];
        y += bb[1];
        w += bb[2];
        h += bb[3];
#ifdef SPS_LAB		
		//f.v.push_back(cv::Rect(bb[0], bb[1], bb[2], bb[3]));
#endif
    }

    x /= numIndices;
    y /= numIndices;
    w /= numIndices;
    h /= numIndices;

	cv::Rect *rect = new cv::Rect();
    f.detectionResult.detectorBB = rect;
    rect->x = cvFloor(x + 0.5);
	rect->y = cvFloor(y + 0.5);
	rect->width = cvFloor(w + 0.5);
	rect->height = cvFloor(h + 0.5);

}

void Clustering::calcMeanRect(Field& f, const std::vector<int>& clusterIndices, std::vector<cv::Rect>& dets)
{
	int numConfidentIndices = static_cast<int>(f.detectionResult.confidentIndices.size());
	int numClusterIndices = static_cast<int>(clusterIndices.size());
	float x, y, w, h;
	x = y = w = h = 0;

	std::unordered_map<int, int> unique;
	for (int i = 0; i < numClusterIndices; ++i)
	{
		unique[clusterIndices[i]]++;
	}
	for (auto& u : unique)
	{
		int numInCluster = 0;
		for (int j = 0; j < numConfidentIndices; ++j)
		{
			if (clusterIndices[j] == u.first)
			{
				int *bb = &f.windows[TLD_WINDOW_SIZE * f.detectionResult.confidentIndices[j]];
				x += bb[0];
				y += bb[1];
				w += bb[2];
				h += bb[3];
				numInCluster++;
			}
		}
		x /= static_cast<float>(numInCluster);
		y /= static_cast<float>(numInCluster);
		w /= static_cast<float>(numInCluster);
		h /= static_cast<float>(numInCluster);
		dets.push_back(cv::Rect(cvFloor(x + 0.5), cvFloor(y + 0.5), cvFloor(w + 0.5), cvFloor(h + 0.5)));
	}
}

void Clustering::clusterConfidentIndices(FieldList& fields, int globalNumWindows)
{
	for (auto& f : fields)
	{
		f.detectionResult.confidentIndices.clear();
	}
	for (size_t i = 0; i < globalNumWindows; ++i)
	{
		for (auto& f : fields)
		{
			if (i < f.numWindows && f.detectionResult.confidentIndicesThread[i] > 0)
			{
				f.detectionResult.confidentIndices.push_back(f.detectionResult.confidentIndicesThread[i] - 1);
			}
		}
	}
	for (auto& f : fields)
	{
		if (f.detectionResult.confidentIndices.size() > 0)
		{
			int numConfidentIndices = static_cast<int>(f.detectionResult.confidentIndices.size());
			std::vector<float> distances(numConfidentIndices * (numConfidentIndices - 1) / 2);
			std::vector<int> clusterIndices(numConfidentIndices);

			calcDistances(f, distances);
			cluster(f, distances, clusterIndices);

			if (f.detectionResult.numClusters == 1)
			{
				calcMeanRect(f, f.detectionResult.confidentIndices);
			}
			//std::cout << f.ID << " clusters " << f.detectionResult.numClusters << " " << f.v.size() << std::endl;
		}
		f.detectionResult.containsValidData = true;
	}
}
//distances must be of size n*(n+1)/2
void Clustering::calcDistances(Field& f, std::vector<float>& distances)
{
	if (distances.size()>0)
	{
		size_t distIndex = 0;
		std::vector<int> confidentIndices = f.detectionResult.confidentIndices;
		size_t indices_size = confidentIndices.size();
		
		for (size_t i = 0; i < confidentIndices.size(); i++)
		{
			int firstIndex = confidentIndices[0];
			confidentIndices.erase(confidentIndices.begin());
			tldOverlapOne(&f.windows[0], f.numWindows, firstIndex, &confidentIndices, &distances[distIndex]);
			distIndex += indices_size - i - 1;
		}
		for (size_t i = 0; i < indices_size * (indices_size - 1) / 2; i++)
		{
			distances[i] = 1 - distances[i];
		}
	}
}

void Clustering::cluster(Field& f, const std::vector<float>& distances, std::vector<int>& clusterIndices)
{
	int numConfidentIndices = static_cast<int>(f.detectionResult.confidentIndices.size());
    if(numConfidentIndices == 1)
    {
        clusterIndices[0] = 0;
		f.detectionResult.numClusters = 1;
        return;
    }

    int numDistances = numConfidentIndices * (numConfidentIndices - 1) / 2;
    std::vector<int> distUsed(numDistances, 0);


	std::fill(clusterIndices.begin(), clusterIndices.end(), -1);

    int newClusterIndex = 0;
    int numClusters = 0;

    while(true)
    {
        float shortestDist = -1;
        int shortestDistIndex = -1;
        int i1;
        int i2;
        int distIndex = 0;

        for(int i = 0; i < numConfidentIndices; i++)  
        {
            for(int j = i + 1; j < numConfidentIndices; j++) 
            {

                if(!distUsed[distIndex] && (shortestDistIndex == -1 || distances[distIndex] < shortestDist))
                {
                    shortestDist = distances[distIndex];
                    shortestDistIndex = distIndex;
                    i1 = i;
                    i2 = j;
                }

                distIndex++;
            }
        }

        if(shortestDistIndex == -1)
        {
            break; 
        }

        distUsed[shortestDistIndex] = 1;

        if(clusterIndices[i1] == -1 && clusterIndices[i2] == -1)
        {
           if(shortestDist < cutoff)
            {
                clusterIndices[i1] = clusterIndices[i2] = newClusterIndex;
                newClusterIndex++;
                numClusters++;
            }
            else    
            {
                clusterIndices[i1] = newClusterIndex;
                newClusterIndex++;
                numClusters++;
                clusterIndices[i2] = newClusterIndex;
                newClusterIndex++;
                numClusters++;
            }

        }
        else if(clusterIndices[i1] == -1 && clusterIndices[i2] != -1)
        {
            if(shortestDist < cutoff)
            {
                clusterIndices[i1] = clusterIndices[i2];
            }
            else   
            {
                clusterIndices[i1] = newClusterIndex;
                newClusterIndex++;
                numClusters++;
            }
        }
        else if(clusterIndices[i1] != -1 && clusterIndices[i2] == -1)
        {
            if(shortestDist < cutoff)
            {
                clusterIndices[i2] = clusterIndices[i1];
            }
            else  
            {
                clusterIndices[i2] = newClusterIndex;
                newClusterIndex++;
                numClusters++;
            }
        }
        else  
        {
            if(clusterIndices[i1] != clusterIndices[i2] && shortestDist < cutoff)
            {
             
                int oldClusterIndex = clusterIndices[i2];

                for(int i = 0; i < numConfidentIndices; i++)
                {
                    if(clusterIndices[i] == oldClusterIndex)
                    {
                        clusterIndices[i] = clusterIndices[i1];
                    }
                }

                numClusters--;
            }
        }
    }
	f.detectionResult.numClusters = numClusters;
}