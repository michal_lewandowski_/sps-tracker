#pragma once
#include <opencv.hpp>
#include "NormalisedPatch.h"

#define TLD_VIS_PATCH_SCALE 3

class Utils
{
public:
	
	static void parse_csint(const std::string& str, std::vector<float>& result);
	static std::vector<cv::Point3d> readGT(std::string filePath);
};

static const uchar popcount_LUT8[256] = {
	0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
	4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8,
};

static const int s_nSamplesInitPatternWidth = 7;
static const int s_nSamplesInitPatternHeight = 7;
static const int s_nSamplesInitPatternTot = 512;

static const int s_anSamplesInitPattern[s_nSamplesInitPatternHeight][s_nSamplesInitPatternWidth] = {
	{ 2,     4,     6,     7,     6,     4,     2, },
	{ 4,     8,    12,    14,    12,     8,     4, },
	{ 6,    12,    21,    25,    21,    12,     6, },
	{ 7,    14,    25,    28,    25,    14,     7, },
	{ 6,    12,    21,    25,    21,    12,     6, },
	{ 4,     8,    12,    14,    12,     8,     4, },
	{ 2,     4,     6,     7,     6,     4,     2, },
};

static const int s_anNeighborPatternSize_3x3 = 8;
static const int s_anNeighborPattern_3x3[8][2] = {
	{ -1, 1 },{ 0, 1 },{ 1, 1 },
	{ -1, 0 },{ 1, 0 },
	{ -1,-1 },{ 0,-1 },{ 1,-1 },
};

static const int s_anNeighborPatternSize_5x5 = 24;
static const int s_anNeighborPattern_5x5[24][2] = {
	{ -2, 2 },{ -1, 2 },{ 0, 2 },{ 1, 2 },{ 2, 2 },
	{ -2, 1 },{ -1, 1 },{ 0, 1 },{ 1, 1 },{ 2, 1 },
	{ -2, 0 },{ -1, 0 },{ 1, 0 },{ 2, 0 },
	{ -2,-1 },{ -1,-1 },{ 0,-1 },{ 1,-1 },{ 2,-1 },
	{ -2,-2 },{ -1,-2 },{ 0,-2 },{ 1,-2 },{ 2,-2 },
};
template<typename T> static inline typename std::enable_if<std::is_integral<T>::value, size_t>::type L1dist(T a, T b)
{
	return (size_t)abs((int)a - b);
}

template<typename T> static inline typename std::enable_if<std::is_floating_point<T>::value, float>::type L1dist(T a, T b)
{
	return fabs((float)a - (float)b);
}

template<size_t nChannels, typename T> static inline auto L1dist(const T* a, const T* b) -> decltype(L1dist(*a, *b))
{
	decltype(L1dist(*a, *b)) oResult = 0;
	for (size_t c = 0; c<nChannels; ++c)
		oResult += L1dist(a[c], b[c]);
	return oResult;
}

template<size_t nChannels, typename T> static inline auto L1dist(const T* a, const T* b, size_t nElements, const uchar* m = NULL) -> decltype(L1dist<nChannels>(a, b))
{
	decltype(L1dist<nChannels>(a, b)) oResult = 0;
	size_t nTotElements = nElements*nChannels;
	if (m)
	{
		for (size_t n = 0, i = 0; n<nTotElements; n += nChannels, ++i)
			if (m[i])
				oResult += L1dist<nChannels>(a + n, b + n);
	}
	else
	{
		for (size_t n = 0; n<nTotElements; n += nChannels)
			oResult += L1dist<nChannels>(a + n, b + n);
	}
	return oResult;
}

template<typename T> static inline auto L1dist(const T* a, const T* b, size_t nElements, size_t nChannels, const uchar* m = NULL) -> decltype(L1dist<3>(a, b, nElements, m))
{
	CV_Assert(nChannels>0 && nChannels <= 4);
	switch (nChannels)
	{
	case 1: return L1dist<1>(a, b, nElements, m);
	case 2: return L1dist<2>(a, b, nElements, m);
	case 3: return L1dist<3>(a, b, nElements, m);
	case 4: return L1dist<4>(a, b, nElements, m);
	default: return 0;
	}
}

template<size_t nChannels, typename T> static inline auto L1dist_(const cv::Vec<T, nChannels>& a, const cv::Vec<T, nChannels>& b) -> decltype(L1dist<nChannels, T>((T*)(0), (T*)(0)))
{
	T a_array[nChannels], b_array[nChannels];
	for (size_t c = 0; c<nChannels; ++c)
	{
		a_array[c] = a[(int)c];
		b_array[c] = b[(int)c];
	}
	return L1dist<nChannels>(a_array, b_array);
}

template<typename T> static inline auto L2sqrdist(T a, T b) -> decltype(L1dist(a, b))
{
	auto oResult = L1dist(a, b);
	return oResult*oResult;
}

template<size_t nChannels, typename T> static inline auto L2sqrdist(const T* a, const T* b) -> decltype(L2sqrdist(*a, *b))
{
	decltype(L2sqrdist(*a, *b)) oResult = 0;
	for (size_t c = 0; c<nChannels; ++c)
		oResult += L2sqrdist(a[c], b[c]);
	return oResult;
}

template<size_t nChannels, typename T> static inline auto L2sqrdist(const T* a, const T* b, size_t nElements, const uchar* m = NULL) -> decltype(L2sqrdist<nChannels>(a, b))
{
	decltype(L2sqrdist<nChannels>(a, b)) oResult = 0;
	size_t nTotElements = nElements*nChannels;
	if (m)
	{
		for (size_t n = 0, i = 0; n<nTotElements; n += nChannels, ++i)
			if (m[i])
				oResult += L2sqrdist<nChannels>(a + n, b + n);
	}
	else
	{
		for (size_t n = 0; n<nTotElements; n += nChannels)
			oResult += L2sqrdist<nChannels>(a + n, b + n);
	}
	return oResult;
}

template<typename T> static inline auto L2sqrdist(const T* a, const T* b, size_t nElements, size_t nChannels, const uchar* m = NULL) -> decltype(L2sqrdist<3>(a, b, nElements, m))
{
	CV_Assert(nChannels>0 && nChannels <= 4);
	switch (nChannels)
	{
	case 1: return L2sqrdist<1>(a, b, nElements, m);
	case 2: return L2sqrdist<2>(a, b, nElements, m);
	case 3: return L2sqrdist<3>(a, b, nElements, m);
	case 4: return L2sqrdist<4>(a, b, nElements, m);
	default: return 0;
	}
}

template<size_t nChannels, typename T> static inline auto L2sqrdist_(const cv::Vec<T, nChannels>& a, const cv::Vec<T, nChannels>& b) -> decltype(L2sqrdist<nChannels, T>((T*)(0), (T*)(0)))
{
	T a_array[nChannels], b_array[nChannels];
	for (size_t c = 0; c<nChannels; ++c)
	{
		a_array[c] = a[(int)c];
		b_array[c] = b[(int)c];
	}
	return L2sqrdist<nChannels>(a_array, b_array);
}

template<size_t nChannels, typename T> static inline float L2dist(const T* a, const T* b)
{
	decltype(L2sqrdist(*a, *b)) oResult = 0;
	for (size_t c = 0; c<nChannels; ++c)
		oResult += L2sqrdist(a[c], b[c]);
	return sqrt((float)oResult);
}

template<size_t nChannels, typename T> static inline float L2dist(const T* a, const T* b, size_t nElements, const uchar* m = NULL)
{
	decltype(L2sqrdist<nChannels>(a, b)) oResult = 0;
	size_t nTotElements = nElements*nChannels;
	if (m)
	{
		for (size_t n = 0, i = 0; n<nTotElements; n += nChannels, ++i)
			if (m[i])
				oResult += L2sqrdist<nChannels>(a + n, b + n);
	}
	else
	{
		for (size_t n = 0; n<nTotElements; n += nChannels)
			oResult += L2sqrdist<nChannels>(a + n, b + n);
	}
	return sqrt((float)oResult);
}

template<typename T> static inline float L2dist(const T* a, const T* b, size_t nElements, size_t nChannels, const uchar* m = NULL)
{
	CV_Assert(nChannels>0 && nChannels <= 4);
	switch (nChannels)
	{
	case 1: return L2dist<1>(a, b, nElements, m);
	case 2: return L2dist<2>(a, b, nElements, m);
	case 3: return L2dist<3>(a, b, nElements, m);
	case 4: return L2dist<4>(a, b, nElements, m);
	default: return 0;
	}
}

template<size_t nChannels, typename T> static inline float L2dist_(const cv::Vec<T, nChannels>& a, const cv::Vec<T, nChannels>& b)
{
	T a_array[nChannels], b_array[nChannels];
	for (size_t c = 0; c<nChannels; ++c)
	{
		a_array[c] = a[(int)c];
		b_array[c] = b[(int)c];
	}
	return L2dist<nChannels>(a_array, b_array);
}

template<size_t nChannels, typename T> static inline typename std::enable_if<std::is_integral<T>::value, size_t>::type cdist(const T* curr, const T* bg)
{
	static_assert(nChannels>1, "cdist: requires more than one channel");
	size_t curr_sqr = 0;
	bool bSkip = true;
	for (size_t c = 0; c<nChannels; ++c)
	{
		curr_sqr += curr[c] * curr[c];
		bSkip = bSkip&(bg[c] <= 0);
	}
	if (bSkip)
		return (size_t)sqrt((float)curr_sqr);
	size_t bg_sqr = 0;
	size_t mix = 0;
	for (size_t c = 0; c<nChannels; ++c)
	{
		bg_sqr += bg[c] * bg[c];
		mix += curr[c] * bg[c];
	}
	return (size_t)sqrt(curr_sqr - ((float)(mix*mix) / bg_sqr));
}

template<size_t nChannels, typename T> static inline typename std::enable_if<std::is_floating_point<T>::value, float>::type cdist(const T* curr, const T* bg)
{
	static_assert(nChannels>1, "cdist: requires more than one channel");
	float curr_sqr = 0;
	bool bSkip = true;
	for (size_t c = 0; c<nChannels; ++c)
	{
		curr_sqr += (float)curr[c] * curr[c];
		bSkip = bSkip&(bg[c] <= 0);
	}
	if (bSkip)
		return sqrt(curr_sqr);
	float bg_sqr = 0;
	float mix = 0;
	for (size_t c = 0; c<nChannels; ++c)
	{
		bg_sqr += (float)bg[c] * bg[c];
		mix += (float)curr[c] * bg[c];
	}
	return sqrt(curr_sqr - ((mix*mix) / bg_sqr));
}

template<size_t nChannels, typename T> static inline auto cdist(const T* a, const T* b, size_t nElements, const uchar* m = NULL) -> decltype(cdist<nChannels>(a, b))
{
	decltype(cdist<nChannels>(a, b)) oResult = 0;
	size_t nTotElements = nElements*nChannels;
	if (m)
	{
		for (size_t n = 0, i = 0; n<nTotElements; n += nChannels, ++i)
			if (m[i])
				oResult += cdist<nChannels>(a + n, b + n);
	}
	else
	{
		for (size_t n = 0; n<nTotElements; n += nChannels)
			oResult += cdist<nChannels>(a + n, b + n);
	}
	return oResult;
}

template<typename T> static inline auto cdist(const T* a, const T* b, size_t nElements, size_t nChannels, const uchar* m = NULL) -> decltype(cdist<3>(a, b, nElements, m))
{
	CV_Assert(nChannels>1 && nChannels <= 4);
	switch (nChannels)
	{
	case 2: return cdist<2>(a, b, nElements, m);
	case 3: return cdist<3>(a, b, nElements, m);
	case 4: return cdist<4>(a, b, nElements, m);
	default: return 0;
	}
}

template<size_t nChannels, typename T> static inline auto cdist_(const cv::Vec<T, nChannels>& a, const cv::Vec<T, nChannels>& b) -> decltype(cdist<nChannels, T>((T*)(0), (T*)(0)))
{
	T a_array[nChannels], b_array[nChannels];
	for (size_t c = 0; c<nChannels; ++c)
	{
		a_array[c] = a[(int)c];
		b_array[c] = b[(int)c];
	}
	return cdist<nChannels>(a_array, b_array);
}

template<typename T> static inline T cmixdist(T oL1Distance, T oCDistortion)
{
	return (oL1Distance / 2 + oCDistortion * 4);
}

template<size_t nChannels, typename T> static inline typename std::enable_if<std::is_integral<T>::value, size_t>::type cmixdist(const T* curr, const T* bg)
{
	return cmixdist(L1dist<nChannels>(curr, bg), cdist<nChannels>(curr, bg));
}

template<size_t nChannels, typename T> static inline typename std::enable_if<std::is_floating_point<T>::value, float>::type cmixdist(const T* curr, const T* bg)
{
	return cmixdist(L1dist<nChannels>(curr, bg), cdist<nChannels>(curr, bg));
}

template<typename T> static inline size_t popcount(T x)
{
	size_t nBytes = sizeof(T);
	size_t nResult = 0;
	for (size_t l = 0; l<nBytes; ++l)
		nResult += popcount_LUT8[(uchar)(x >> l * 8)];
	return nResult;
}

template<typename T> static inline size_t hdist(T a, T b)
{
	return popcount(a^b);
}

template<typename T> static inline size_t gdist(T a, T b)
{
	return L1dist(popcount(a), popcount(b));
}

template<size_t nChannels, typename T> static inline size_t popcount(const T* x)
{
	size_t nBytes = sizeof(T);
	size_t nResult = 0;
	for (size_t c = 0; c<nChannels; ++c)
		for (size_t l = 0; l<nBytes; ++l)
			nResult += popcount_LUT8[(uchar)(*(x + c) >> l * 8)];
	return nResult;
}

template<size_t nChannels, typename T> static inline size_t hdist(const T* a, const T* b)
{
	T xor_array[nChannels];
	for (size_t c = 0; c<nChannels; ++c)
		xor_array[c] = a[c] ^ b[c];
	return popcount<nChannels>(xor_array);
}

template<size_t nChannels, typename T> static inline size_t gdist(const T* a, const T* b)
{
	return L1dist(popcount<nChannels>(a), popcount<nChannels>(b));
}

static inline void getRandSamplePosition(int& x_sample, int& y_sample, const int x_orig, const int y_orig, const int border, const cv::Size& imgsize)
{
	int r = 1 + rand() % s_nSamplesInitPatternTot;
	for (x_sample = 0; x_sample<s_nSamplesInitPatternWidth; ++x_sample)
	{
		for (y_sample = 0; y_sample<s_nSamplesInitPatternHeight; ++y_sample)
		{
			r -= s_anSamplesInitPattern[y_sample][x_sample];
			if (r <= 0)
				goto stop;
		}
	}
stop:
	x_sample += x_orig - s_nSamplesInitPatternWidth / 2;
	y_sample += y_orig - s_nSamplesInitPatternHeight / 2;
	if (x_sample<border)
		x_sample = border;
	else if (x_sample >= imgsize.width - border)
		x_sample = imgsize.width - border - 1;
	if (y_sample<border)
		y_sample = border;
	else if (y_sample >= imgsize.height - border)
		y_sample = imgsize.height - border - 1;
}

static inline void getRandNeighborPosition_3x3(int& x_neighbor, int& y_neighbor, const int x_orig, const int y_orig, const int border, const cv::Size& imgsize)
{
	int r = rand() % s_anNeighborPatternSize_3x3;
	x_neighbor = x_orig + s_anNeighborPattern_3x3[r][0];
	y_neighbor = y_orig + s_anNeighborPattern_3x3[r][1];
	if (x_neighbor<border)
		x_neighbor = border;
	else if (x_neighbor >= imgsize.width - border)
		x_neighbor = imgsize.width - border - 1;
	if (y_neighbor<border)
		y_neighbor = border;
	else if (y_neighbor >= imgsize.height - border)
		y_neighbor = imgsize.height - border - 1;
}

static inline void getRandNeighborPosition_5x5(int& x_neighbor, int& y_neighbor, const int x_orig, const int y_orig, const int border, const cv::Size& imgsize)
{
	int r = rand() % s_anNeighborPatternSize_5x5;
	x_neighbor = x_orig + s_anNeighborPattern_5x5[r][0];
	y_neighbor = y_orig + s_anNeighborPattern_5x5[r][1];
	if (x_neighbor<border)
		x_neighbor = border;
	else if (x_neighbor >= imgsize.width - border)
		x_neighbor = imgsize.width - border - 1;
	if (y_neighbor<border)
		y_neighbor = border;
	else if (y_neighbor >= imgsize.height - border)
		y_neighbor = imgsize.height - border - 1;
}
template <class T1, class T2>
void tldConvertBB(T1 *src, T2 *dest)
{
	dest[0] = src[0];
	dest[1] = src[1];
	dest[2] = src[2];
	dest[3] = src[3];
}

template <class T>
void tldCopyBB(T *src, T *dest)
{
	tldConvertBB<T, T>(src, dest);
}

template <class T>
void tldCopyBoundaryToArray(T x, T y, T width, T height, T *array)
{
	array[0] = x;
	array[1] = y;
	array[2] = width;
	array[3] = height;
}

template <class T>
void tldExtractDimsFromArray(T *boundary, T *x, T *y, T *width, T *height)
{
	*x = boundary[0];
	*y = boundary[1];
	*width = boundary[2];
	*height = boundary[3];
}

template <class T>
void tldRectToArray(cv::Rect rect, T *boundary)
{
	boundary[0] = rect.x;
	boundary[1] = rect.y;
	boundary[2] = rect.width;
	boundary[3] = rect.height;
}

template <class T>
cv::Rect tldArrayToRect(T *boundary)
{
	cv::Rect rect;
	rect.x = boundary[0];
	rect.y = boundary[1];
	rect.width = boundary[2];
	rect.height = boundary[3];

	return rect;
}

unsigned int popcount32(unsigned int n);

int tldIsInside(int *bb1, int *bb2);
void tldRectToPoints(cv::Rect rect, cv::Point *p1, cv::Point *p2);
void tldBoundingBoxToPoints(int *bb, cv::Point *p1, cv::Point *p2);

cv::Rect tldBoundaryToRect(int *boundary);
void tldExtractSubImage(const cv::Mat &img, cv::Mat &subImage, cv::Rect rect);
float tldBBOverlap(int *bb1, int *bb2);
float tldBBOverlap(cv::Rect *bb1, int *bb2);

float tldCalcMean(const cv::Mat& intImg, int x, int y, int w, int h);
float tldCalcVariance(const cv::Mat& intImg, const cv::Mat& intSqImg, int x, int y, int w, int h);
void tldCalcBinary(const cv::Mat& intImg, const cv::Rect *bb, int& binaryP, int& binaryN);
void tldCalcBinary(const cv::Mat& intImg, const int *bb, int& binaryP, int& binaryN);
void tldCalcBinary(const cv::Mat& intImg, int x, int y, int w, int h, int& binaryP, int& binaryN);

bool tldSortByOverlapDesc(std::pair<int, float> bb1, std::pair<int, float> bb2);
bool tldSortByOverlapAsc(std::pair<int, float> bb1, std::pair<int, float> bb2);
cv::Rect *tldCopyRect(cv::Rect *r);

float tldOverlapRectRect(cv::Rect r1, cv::Rect r2);
void tldOverlapOne(int *windows, int numWindows, int index, std::vector<int> * indices, float *overlap);
void tldOverlap(int *windows, int numWindows, int *boundary, float *overlap);
void tldOverlapRect(int *windows, int numWindows, cv::Rect *boundary, float *overlap);

float tldGetMedian(float arr[], int n);
float tldGetMedianUnmanaged(float arr[], int n);

cv::Mat showCurrentPatch(cv::Mat frame, cv::Mat integral, cv::Rect r);
cv::Mat showDetectionModel(const std::deque<NormalizedPatch>& m);
cv::Mat tan_triggs_preprocessing(cv::InputArray src, float alpha = 0.1, float tau = 10.0, float gamma = 0.2, int sigma0 = 1, int sigma1 = 2);

cv::Point2f pointOnRect(cv::Rect bb, cv::Rect stopZone);