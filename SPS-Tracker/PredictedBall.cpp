#include "stdafx.h"
#include "PredictedBall.h"

PredictedBall::PredictedBall(const std::size_t id, std::string cid, cv::Rect b)
	: BallObject(id, cid, b, cv::RotatedRect(), 1.0, cv::Mat(), cv::Mat())
{
	ebb = cv::RotatedRect(pos2d(), box().size(), 0.0f);
	type = BallType::PREDICTED;
}
PredictedBall::PredictedBall(const std::size_t id, std::string cid, cv::Point3d p, int views, float error)
	: BallObject(id, cid, p, views, 0, error, std::unordered_map<std::string, int>(), std::vector<PreviousBallInCam>())
{
	type = BallType::PREDICTED;
}
std::shared_ptr<MovingObject> PredictedBall::clone() const
{
	return std::make_shared<PredictedBall>(*this);
}
PredictedBall::~PredictedBall()
{
}

PredictedBall::PredictedBall(const PredictedBall& other)
	: BallObject(other)
{
}

PredictedBall& PredictedBall::operator=(const PredictedBall& other)
{
	if(this != &other)
	{
		this->BallObject::operator=(other);
	}
	return *this;
}

void PredictedBall::show(cv::Mat frame, cv::Scalar color) const
{
	cv::ellipse(frame, ebb, color, -1);
	//cv::ellipse(frame, ebb, cv::Scalar(0, 150, 255), 3);
	//cv::putText(frame, boost::lexical_cast<std::string>(detID), cv::Point(bb_.br().x, bb_.tl().y) + cv::Point(7, -7), CV_FONT_HERSHEY_PLAIN, 1, color);
}
void PredictedBall::show3d(cv::Mat frame, cv::Scalar color, double windowScale) const
{
	cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 5, color, -1);
	//cv::putText(frame, boost::lexical_cast<std::string>(detID), cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - (cvRound(pos3dEst().y / windowScale)) + cv::Point(7, -7)), CV_FONT_HERSHEY_PLAIN, 1, color);
}
void PredictedBall::setImage(cv::Mat i, cv::Mat m)
{
	mimg.create(box().size(), CV_8UC1);
	mimg.setTo(0);
	cv::RotatedRect rr(cv::Point(cvFloor(box().width/2.0), cvFloor(box().height/2.0)), box().size(), 0.0f);
	cv::ellipse(mimg, rr, 255, -1);

	i(box()).copyTo(img, mimg);
}