#pragma once
#include "IBGS.h"

class BgsMOG2gpu : public IBGS
{
public:
	BgsMOG2gpu();
	virtual ~BgsMOG2gpu();

	virtual void init(const cv::Mat& oInitImg, const cv::Mat& oROI);
	virtual void apply(const cv::Mat& image, cv::Mat& fgmask, double learningRateOverride);
	virtual void preprocess(const cv::Mat& iframe, const cv::Size& size, cv::Mat& pframe, int code = cv::COLOR_BayerBG2BGR);
	virtual void loadParams();
	virtual void getBackgroundImage(cv::OutputArray backgroundImage) const;

protected:
	cv::Ptr<cv::BackgroundSubtractorMOG2> MOG2;
	//FDMOG2GPUVT_MASK mog2;
	double learningRate;

	cv::cuda::GpuMat frameGPU;
	cv::cuda::GpuMat motionTempGPU;
	cv::cuda::GpuMat frameGrayGPU;
	cv::cuda::GpuMat frameBGRGPU;
	cv::cuda::GpuMat roiGPU;
};

