#include "StdAfx.h"
#include "ncc.h"
#include "MedianFlowTracker.h"

#ifdef SPS_USE_SSE
float ncc(const NormalizedPatch& np1, const NormalizedPatch& np2)
{
	float score = 0.0f;
	__m128* a1f = (__m128*) np1.values;
	__m128* a2f = (__m128*) np2.values;
	__m128 sxy = _mm_setzero_ps();
	__m128 sx2 = _mm_set_ss(np1.norm);
	__m128 sy2 = _mm_set_ss(np2.norm);
	__m128 s1 = _mm_set_ss(1.0);
	__m128 s05 = _mm_set_ss(0.5);
	for (int i = 0; i < TLD_PATCH_SIZE * TLD_PATCH_SIZE / 4; ++i)
	{
		//sxy = _mm_add_ps(sxy, _mm_mul_ps(a1f[i], a2f[i]));
		sxy = _mm_fmadd_ps(a1f[i], a2f[i], sxy);
	}
	sxy = _mm_hadd_ps(sxy, sxy);
	sxy = _mm_hadd_ps(sxy, sxy);

	__m128 ns = _mm_mul_ss(sx2, sy2);
	ns = _mm_sqrt_ss(ns);
	ns = _mm_div_ss(sxy, ns);
	ns = _mm_add_ss(ns, s1);
	ns = _mm_mul_ss(ns, s05);
	_mm_store_ss(&score, ns);
	return score;
}
float ncc(const float *f1, const float *f2)
{
	float score = 0.0f;
	__m128* a1f = (__m128*) f1;
	__m128* a2f = (__m128*) f2;

	__m128 sxy = _mm_setzero_ps();
	__m128 sx2 = _mm_setzero_ps();
	__m128 sy2 = _mm_setzero_ps();
	__m128 mx = _mm_setzero_ps();
	__m128 my = _mm_setzero_ps();
	//__m128 s1 = _mm_set_ss(1.0);
	//__m128 s05 = _mm_set_ss(0.5);
	__m128 N = _mm_set_ss(TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC);
	for (int i = 0; i < TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC / 4; ++i)
	{
		mx = _mm_add_ps(mx, a1f[i]);
		my = _mm_add_ps(my, a2f[i]);
	}
	mx = _mm_hadd_ps(mx, mx);
	mx = _mm_hadd_ps(mx, mx);
	my = _mm_hadd_ps(my, my);
	my = _mm_hadd_ps(my, my);
	mx = _mm_div_ss(mx, N);
	my = _mm_div_ss(my, N);
	mx = _mm_shuffle_ps(mx, mx, _MM_SHUFFLE(0, 0, 0, 0));
	my = _mm_shuffle_ps(my, my, _MM_SHUFFLE(0, 0, 0, 0));
	for (int i = 0; i < TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC / 4; ++i)
	{
		a1f[i] = _mm_sub_ps(a1f[i], mx);
		a2f[i] = _mm_sub_ps(a2f[i], my);

		/*sxy = _mm_add_ps(sxy, _mm_mul_ps(a1f[i], a2f[i]));
		sx2 = _mm_add_ps(sx2, _mm_mul_ps(a1f[i], a1f[i]));
		sy2 = _mm_add_ps(sy2, _mm_mul_ps(a2f[i], a2f[i]));*/

		sxy = _mm_fmadd_ps(a1f[i], a2f[i], sxy);
		sx2 = _mm_fmadd_ps(a1f[i], a1f[i], sx2);
		sy2 = _mm_fmadd_ps(a2f[i], a2f[i], sy2);
	}
	sxy = _mm_hadd_ps(sxy, sxy);
	sxy = _mm_hadd_ps(sxy, sxy);
	sx2 = _mm_hadd_ps(sx2, sx2);
	sx2 = _mm_hadd_ps(sx2, sx2);
	sy2 = _mm_hadd_ps(sy2, sy2);
	sy2 = _mm_hadd_ps(sy2, sy2);
	__m128 ns = _mm_mul_ss(sx2, sy2);
	ns = _mm_sqrt_ss(ns);
	ns = _mm_div_ss(sxy, ns);
	//ns = _mm_add_ss(ns, s1);
	//ns = _mm_mul_ss(ns, s05);
	_mm_store_ss(&score, ns);

	return score;
}
void normCrossCorrelationError(const cv::Mat& pimg, const cv::Mat& cimg, const cv::Mat& pPatch, const cv::Mat& cPatch, CvPoint2D32f *points0, CvPoint2D32f *points1, int nPts, const char* status, float *match)
{
	for (int i = 0; i < nPts; i++)
	{
		if (status[i] == 1)
		{
			cv::getRectSubPix(pimg, MedianFlowTracker::winSizeNCC, cv::Point2f(points0[i].x, points0[i].y), pPatch, CV_32FC1);
			cv::getRectSubPix(cimg, MedianFlowTracker::winSizeNCC, cv::Point2f(points1[i].x, points1[i].y), cPatch, CV_32FC1);

			match[i] = ncc(reinterpret_cast<float*>(pPatch.data), reinterpret_cast<float*>(cPatch.data));
		}
		else
		{
			match[i] = 0.0;
		}
	}
}
#endif