#include "NPClassifier.h"
#include "NormalisedPatch.h"
#include "Utils.h"
#include "Field.h"
#include "ncc.h"

int NPClassifier::forgetThresholdP = TLD_FORGET_AFTER_SAMPLES;
int NPClassifier::forgetThresholdN = 2*TLD_FORGET_AFTER_SAMPLES;
std::uniform_int_distribution<> NPClassifier::forgetDistribution(0, static_cast<int>(forgetThresholdN / 2) - 1);
const float NPClassifier::thetaFP = 0.50f;
const float NPClassifier::thetaTP = 0.65f;
extern std::default_random_engine dre;

NPClassifier::NPClassifier()
{
}

NPClassifier::~NPClassifier()
{
}
void NPClassifier::init()
{
	release();
}

void NPClassifier::release()
{
}

float NPClassifier::relativeSimilarity(Field& f, NormalizedPatch& patch, int& currLabel)
{
	currLabel = 0;

	if (f.truePositives.empty())
    {
        return 0.0f;
    }

	if (f.falsePositives.empty())
    {
		return 1.0f;
    }

	float ccorr_max_p = 0.0f;
	float ccorr_max_n = 0.0f;
	int tpN = static_cast<int>(f.truePositives.size());
	int fpN = static_cast<int>(f.falsePositives.size());

	for (int i = tpN-1; i >= 0; --i)
    {
		float ccorr = ncc(f.truePositives[i], patch);
		ccorr_max_p = std::max(ccorr_max_p, ccorr);
		if (ccorr > TLD_THE_SAME_PATCH)
		{
			currLabel = 1;
			break;
		}
    }

	for (int i = fpN - 1; i >= 0; --i)
    {
		float ccorr = ncc(f.falsePositives[i], patch);
		ccorr_max_n = std::max(ccorr_max_n, ccorr);
		if (ccorr > TLD_THE_SAME_PATCH)
		{
			currLabel = -1;
			break;
		}
    }

    float dN = 1.0f - ccorr_max_n;
	float dP = 1.0f - ccorr_max_p;
	if (dN + dP == 0.0)
		return 0.0;
		
	return dN / (dN + dP);
}
void NPClassifier::relativeSimilarity(Field& f, NormalizedPatch& patch, int j, float& coef)
{
	if (f.truePositives.empty())
	{
		coef = 0.0f;
		return;
	}

	if (f.falsePositives.empty())
	{
		coef = 1.0f;
		return;
	}

	float ccorr_max_p = 0.0f;
	float ccorr_max_n = 0.0f;
	int tpN = static_cast<int>(f.truePositives.size());
	int fpN = static_cast<int>(f.falsePositives.size());

	for (int i = tpN - 1; i >= 0; --i)
	{
		if (i!=j)
		{
			float ccorr = ncc(f.truePositives[i], patch);
			ccorr_max_p = std::max(ccorr_max_p, ccorr);
		}
	}

	for (int i = fpN - 1; i >= 0; --i)
	{
		float ccorr = ncc(f.falsePositives[i], patch);
		ccorr_max_n = std::max(ccorr_max_n, ccorr);
	}
	
	float dN = 1.0f - ccorr_max_n;
	float dP = 1.0f - ccorr_max_p;
	if (dN + dP == 0.0)
	{
		coef = 0.0;
	}
	else
	{
		coef = dN / (dN + dP);
	}
}

float NPClassifier::conservativeSimilarity(Field& f, NormalizedPatch& patch, int& currLabel)
{
	currLabel = 0;

	if (f.truePositives.empty())
	{
		return 0.0f;
	}

	if (f.falsePositives.empty())
	{
		return 1.0f;
	}

	float ccorr_max_p = 0;
	float ccorr_max_n = 0;
	int tpN = static_cast<int>(f.truePositives.size());
	if (f.truePositives.size() >= 2)//mediana
		tpN = cvFloor(0.5 * f.truePositives.size());
	int fpN = static_cast<int>(f.falsePositives.size());

	for (int i = tpN - 1; i >= 0; --i)
	{
		float ccorr = ncc(f.truePositives[i], patch);
		ccorr_max_p = std::max(ccorr_max_p, ccorr);
		if (ccorr > TLD_THE_SAME_PATCH)
		{
			currLabel = 1;
			break;
		}
	}

	for (int i = fpN - 1; i >= 0; --i)
	{
		float ccorr = ncc(f.falsePositives[i], patch);
		ccorr_max_n = std::max(ccorr_max_n, ccorr);
		if (ccorr > TLD_THE_SAME_PATCH)
		{
			currLabel = -1;
			break;
		}
	}

	float dN = 1.0f - ccorr_max_n;
	float dP = 1.0f - ccorr_max_p;
	if (dN + dP == 0.0)
		return 0.0;

	return dN / (dN + dP);
}

float NPClassifier::classifyBBrel(Field& f, cv::Rect bb, int& currLabel)
{
    NormalizedPatch patch;
	patch.extract(img1, img2, bb);
	return relativeSimilarity(f, patch, currLabel);
}

float NPClassifier::classifyBBcon(Field& f, cv::Rect bb, int& currLabel)
{
	NormalizedPatch patch;
	patch.extract(img1, img2, bb);
	return conservativeSimilarity(f, patch, currLabel);
}

bool NPClassifier::filter(int idx, FieldList& fields)
{
	NormalizedPatch patch;
	int currLabel;

	bool nextStage = false;
	for (auto& f : fields)
	{
		if (idx < f.numWindows && !f.detectionResult.lastStageInvalid[idx])
		{
			int *bbox = &f.windows[TLD_WINDOW_SIZE * idx];
			patch.extract(img1, img2, bbox);

			float detConf = relativeSimilarity(f, patch, currLabel);

			if (detConf >= thetaTP || currLabel==1)
			{
				nextStage = true;
			}
			else
			{
				f.detectionResult.lastStageInvalid[idx] = 1;
			}
		}
	}
	return nextStage;
}

void NPClassifier::forget(Field& f)
{
	if (f.truePositives.size()>forgetThresholdP)
	{
		int index = forgetDistribution(dre);
		std::deque<NormalizedPatch>::iterator it = f.truePositives.begin();
		std::advance(it, index);
		f.truePositives.erase(it);
	}
	if (f.falsePositives.size() >= forgetThresholdN)
	{
		int index = forgetDistribution(dre);
		std::deque<NormalizedPatch>::iterator it = f.falsePositives.begin();
		std::advance(it, index);
		f.falsePositives.erase(it);
	}
}
void NPClassifier::learn(Field& f, std::vector<NormalizedPatch> patches)
{
    for(size_t i = 0; i < patches.size(); ++i)
    {
        NormalizedPatch patch = patches[i];
		int currLabel;
		float conf = relativeSimilarity(f, patch, currLabel);

		if (patch.positive && conf <= thetaTP && currLabel <= 0)
        {
			f.truePositives.push_back(patch);
        }
        if(!patch.positive && conf > thetaFP && currLabel >=0)
        {
			f.falsePositives.push_back(patch);
        }
    }
}
void NPClassifier::initLearn(Field& f, std::vector<NormalizedPatch> patches)
{
	for (size_t i = 0; i < patches.size(); ++i)
	{
		NormalizedPatch patch = patches[i];

		int currLabel;
		float conf = relativeSimilarity(f, patch, currLabel);

		if (patch.positive)
		{
			f.truePositives.push_back(patch);
		}
		if (!patch.positive && conf > thetaFP && currLabel >= 0)
		{
			f.falsePositives.push_back(patch);
		}
	}
}