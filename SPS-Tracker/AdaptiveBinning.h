#pragma once
#include <memory>
#include "FixedBinHistogram.h"
#include "ColorQuantiziser.h"
#include "MortonEncoding.h"

template <typename Data>
class AdaptiveBinning
{
public:
	AdaptiveBinning(size_t nBuckets, std::shared_ptr<ColorQuantiziser> pCQ);
	~AdaptiveBinning();

	std::function<size_t(const Data&)> getBucketIndexFunc() const;
	size_t getBucketIndex(const Data& d) const;
	AdaptiveBinning& operator<<(const Data& d);
	void rebin(std::vector<std::shared_ptr<FixedBinHistogram<Data, double>>>* histograms = nullptr);
	FixedBinHistogram<Data, size_t>& getHistogram();
	void getBins(std::vector<Data>& out) const;
	size_t getNumberOfBins() const;
	bool ready() const;

private:
	FixedBinHistogram<Data, size_t> motherHistogram;
	std::shared_ptr<ColorQuantiziser> colorQuantiziser;
	std::map<uint32_t, size_t> binning;
};

template <typename Data>
AdaptiveBinning<Data>::AdaptiveBinning(size_t nBuckets, std::shared_ptr<ColorQuantiziser> pCQ)
	: motherHistogram(nBuckets, getBucketIndexFunc())
	, colorQuantiziser(pCQ)
	, binning()
{
	CV_Assert(pCQ);
}


template <typename Data>
AdaptiveBinning<Data>::~AdaptiveBinning()
{
}

template <typename Data>
std::function<size_t(Data const&)> AdaptiveBinning<Data>::getBucketIndexFunc() const
{
	return std::bind(&AdaptiveBinning<Data>::getBucketIndex, this, std::placeholders::_1);
}

template <typename Data>
size_t AdaptiveBinning<Data>::getBucketIndex(Data const& d) const
{
	assert(!binning.empty());
	assert(binning.size() <= motherHistogram.size());
	uint32_t bitInterleaved = MortonEncoder::encode(d[0], d[1], d[2]);
	std::map<uint32_t, size_t>::const_iterator it = binning.lower_bound(bitInterleaved);
	size_t idx;
	if (it == binning.cend())
	{
		idx = binning.size() - 1;
	}
	else
	{
		idx = it->second;
	}
	return idx;
}

template <typename Data>
AdaptiveBinning<Data>& AdaptiveBinning<Data>::operator<<(Data const& d)
{
	if (!binning.empty())
	{
		motherHistogram << d;
	}
	colorQuantiziser->digest(d);
	return *this;
}

template <typename Data>
FixedBinHistogram<Data, size_t>& AdaptiveBinning<Data>::getHistogram()
{
	return motherHistogram;
}

template <typename Data>
void AdaptiveBinning<Data>::getBins(std::vector<Data>& out) const
{
	out.resize(binning.size());
	for (const std::pair<uint32_t, size_t>& bitInterleaved_index : binning)
	{
		Data color;
		MortonEncoder::decode(bitInterleaved_index.first, color[0], color[1], color[2]);
		out[bitInterleaved_index.second] = color;
	}
}

template <typename Data>
size_t AdaptiveBinning<Data>::getNumberOfBins() const
{
	return motherHistogram.size();
}

template <typename Data>
bool AdaptiveBinning<Data>::ready() const
{
	return !binning.empty();
}

template <typename Data>
void AdaptiveBinning<Data>::rebin(std::vector<std::shared_ptr<FixedBinHistogram<Data, double>>>* histograms)
{
	std::vector<Data> colors;
	colorQuantiziser->quantize(motherHistogram.size(), colors);
	if (!colors.empty())
	{
		std::map<uint32_t, size_t> newBinning;
		size_t i = 0;
		for (const Data& color : colors)
		{
			const uint32_t bitInterleaved = MortonEncoder::encode(color[0], color[1], color[2]);
			newBinning[bitInterleaved] = i;
			++i;
		}
		motherHistogram.rebin(binning, newBinning);
		if (histograms != nullptr)
		{
			for (std::shared_ptr<FixedBinHistogram<Data, double>>& hist : *histograms)
			{
				hist->rebin(binning, newBinning);
			}
		}
		binning = newBinning;
		colorQuantiziser->clear();
	}
}