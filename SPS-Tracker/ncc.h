#pragma once

#include <opencv.hpp>

class NormalizedPatch;
float ncc(const NormalizedPatch& np1, const NormalizedPatch& np2);
float ncc(const float *f1, const float *f2, float m1, float m2);
void normCrossCorrelationError(const cv::Mat& pimg, const cv::Mat& cimg, const cv::Mat& pPatch, const cv::Mat& cPatch, CvPoint2D32f *points0, CvPoint2D32f *points1, int nPts, const char* status, float *match);

float ncc1(const NormalizedPatch& np1, const NormalizedPatch& np2);
float ncc1(const float *f1, const float *f2, float m1, float m2);
void normCrossCorrelationError1(const cv::Mat& pimg, const cv::Mat& cimg, const cv::Mat& pPatch, const cv::Mat& cPatch, CvPoint2D32f *points0, CvPoint2D32f *points1, int nPts, const char* status, float *match);