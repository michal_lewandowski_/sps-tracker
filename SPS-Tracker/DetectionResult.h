#pragma once

#include <opencv.hpp>

class DetectionResult
{
public:
	DetectionResult();
	virtual ~DetectionResult();

	void init(int numWindows);

	void reset();
	void release();

	bool containsValidData;
	std::vector<int> confidentIndicesThread;
	std::vector<int> confidentIndices;
	float *posteriors;
	float *variances;
	int *featureVectors;
	int *binaryP;
	int *binaryN;
	int numClusters;
	int numWindows;
	std::vector<int> lastStageInvalid;
	cv::Rect *detectorBB;
	float conf;

#ifdef SPS_LAB
	std::vector<int> status;
#endif
};

class TrackResult
{
public:
	TrackResult();
	virtual ~TrackResult();

	void init();

	void reset();
	void release();

	cv::Rect *trackerBB;
	float conf;
};