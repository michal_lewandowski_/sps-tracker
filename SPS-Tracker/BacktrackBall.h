#pragma once
#include "BallObject.h"
#include <memory>

class BacktrackBall : public BallObject
{
public:
	BacktrackBall(const std::size_t id, std::string cid, cv::Rect b);
	BacktrackBall(const std::size_t id, std::string cid, cv::Point3d p, int views = 0, float error = 0.0f);
	virtual ~BacktrackBall();
	BacktrackBall(const BacktrackBall& other);
	BacktrackBall& operator=(const BacktrackBall& other);

	virtual std::shared_ptr<MovingObject> clone() const override;
	virtual bool isDetected(void) const override { return false; }
	virtual bool isBacktracked() const override { return true; }
	virtual void show(cv::Mat frame, cv::Scalar color) const override;
	virtual void show3d(cv::Mat frame, cv::Scalar color, double windowScale) const override;
};