#pragma once
#include <opencv.hpp>

class FieldList;

class ICascadeFilter
{
public:
	ICascadeFilter();
	virtual ~ICascadeFilter();

	virtual void init() = 0;
	virtual void release() = 0;

	virtual bool filter(int idx, FieldList& fields) = 0;

	void setImages(cv::Mat i1, cv::Mat i2 = cv::Mat());

	cv::Mat img1;
	cv::Mat img2;
};

