#pragma once
#include "BallObject.h"
#include <memory>

class VisualBall : public BallObject
{
public:
	VisualBall(const std::size_t id, std::string cid, cv::Rect b, cv::Mat im, cv::Mat mm);
	VisualBall(const std::size_t id, std::string cid, cv::Point3d p, int views, int numDet, float error, const std::unordered_map<std::string, int>& tracks, const std::vector<PreviousBallInCam>& prevLoc);
	virtual ~VisualBall();
	VisualBall(const VisualBall& other);
	VisualBall& operator=(const VisualBall& other);

	virtual std::shared_ptr<MovingObject> clone() const override;
	virtual bool isDetected(void) const override { return true; }
	virtual bool isBacktracked() const override { return false; }
	virtual void show(cv::Mat frame, cv::Scalar color) const override;
	virtual void show3d(cv::Mat frame, cv::Scalar color, double windowScale) const override;
	virtual std::string printInfo2d(void) const override;
};
