#include "stdafx.h"
#include "Detector.h"
#include "Worker.h"
#include "BallObject.h"
#include "DetectedBall.h"
#include "IDFactory.h"
#include "KFBallTrack2d.h"
#include "WorkManager.h"
#include "AdaptiveBinning.h"
#include "VisualBall.h"

extern double ballRadius;
double Detector::ballProbailityThr = (-cv::log(0.2) - cv::log(0.4) + cv::log(2));

bool showParams3 = true;

Detector::Detector(Worker* w)
	: worker(w)
	, ballSizeRatioThr(0.75f)
	, ballMinDiameterMM(0.8 * ballRadius) //80%
	, ballMaxDiameterMM(6.0 * ballRadius) //600%
{
	int sizeRatioColorNum = 2;
	std::vector<double> thrs{ 6.0, 4.0 };
	double fStep = (1.0f - ballSizeRatioThr) / static_cast<double>(sizeRatioColorNum);
	for (int i = 0; i < sizeRatioColorNum; ++i)
	{
		errorThresholds.insert(std::make_pair(ballSizeRatioThr + i*fStep, thrs[i]));
	}

	cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
	file["ballMinDiameterMM"] >> ballMinDiameterMM;
	file["ballMaxDiameterMM"] >> ballMaxDiameterMM;
	file["ballSizeRatio"] >> ballSizeRatioThr;
	file.release();

	if(showParams3)
	{
		std::cout << "Ball Min Diameter [MM]: " << ballMinDiameterMM << std::endl;
		std::cout << "Ball Max Diameter [MM]: " << ballMaxDiameterMM << std::endl;
		std::cout << "Ball Size Ratio [0-1]: " << ballSizeRatioThr << std::endl;
		showParams3 = false;
	}
}
Detector::~Detector()
{
	
}
void Detector::detect(const cv::Mat& frame, const cv::Mat& mask, const std::deque<std::shared_ptr<KFTrack2d>>& tracks, std::vector<std::shared_ptr<MovingObject>>& dets)
{
	dets.clear();

	mask.copyTo(maskProc);
	cv::findContours(maskProc, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_NONE);

	std::unordered_map<int,std::shared_ptr<KFTrack2d>> bestTracks;
	if (worker->isSearchROI())
	{
		for (std::deque<std::shared_ptr<KFTrack2d>>::const_iterator it = tracks.begin(); it != tracks.end(); ++it)
		{
			if((*it)->isVisible())
			{
				if (bestTracks.find((*it)->fieldID()) == bestTracks.end())
				{
					bestTracks[(*it)->fieldID()] = (*it);
				}
				else if ((*it)->getNegLogProbability() < bestTracks[(*it)->fieldID()]->getNegLogProbability())
				{
					bestTracks[(*it)->fieldID()] = (*it);
				}
			}
		}

	}

	if (!hierarchy.empty())
	{
		for (int i = 0; i >= 0; i = hierarchy[static_cast<size_t>(i)][0])
		{
			if (contours[i].size() >= 5)
			{
				cv::Rect bb = cv::boundingRect(contours[i]);

				if (bb.tl().x > 0 && bb.tl().y > 0 && bb.br().x <= worker->getImageSize().width && bb.br().y <= worker->getImageSize().height && static_cast<int>(bb.y + bb.height) < worker->getImageSize().height && static_cast<int>(bb.x + bb.width / 2) < worker->getImageSize().width)
				{
					double ratio = bb.width<bb.height ? static_cast<double>(bb.width) / static_cast<double>(bb.height) : static_cast<double>(bb.height) / static_cast<double>(bb.width);
					if (ratio >= ballSizeRatioThr)
					{
						double unitX = worker->getCalibration().getXunit(static_cast<int>(bb.y + bb.height), static_cast<int>(bb.x + bb.width / 2));
						double unitY = worker->getCalibration().getYunit(static_cast<int>(bb.y + bb.height), static_cast<int>(bb.x + bb.width / 2));

						double dWidth = bb.width * unitX;
						double dHeight = bb.height * unitY;

						if (dWidth >= ballMinDiameterMM && dWidth <= ballMaxDiameterMM && dHeight >= ballMinDiameterMM && dHeight <= ballMaxDiameterMM)
						{
							cv::RotatedRect e = cv::fitEllipse(cv::Mat(contours[i]));
							cv::Rect be = e.boundingRect();

							if (bb.contains(e.center) && be.tl().x >= 0 && be.tl().y >= 0 && be.br().x < worker->getImageSize().width && be.br().y < worker->getImageSize().height)
							{
								bb &= cv::Rect(cv::Point(0, 0), worker->getImageSize());
								std::shared_ptr<DetectedBall> ball = std::make_shared<DetectedBall>(IDFactory::generateID(ID_DETECTION2D), worker->getCamID(), bb, e, ratio, frame(bb), mask(bb));
								if (ball)
								{
									ball->pos3d() = worker->getCalibration().transformToRealWorld(ball->pos2d(), ballRadius);
									ball->pos3dEst() = ball->pos3d();
									ball->field() = worker->inField(ball);
									if (ball->field())
									{
										if (bestTracks.size() > 0)
										{
											auto it = bestTracks.find(ball->field()->fid);
											if(it!= bestTracks.end())
											{
												/*cv::Point2d speed = it->second->getDisplacement();
												dWidth = dWidth - speed.x;
												dHeight = dHeight - speed.y;*/

												/*cv::Point2d speed = it->second->getDisplacement();
												dWidth = dWidth - speed.x * unitX * 1 / 25.0;
												dHeight = dHeight - speed.y * unitY * 1 / 25.0;*/

												/*cv::Point3d speed = it->second->getVelocity() * 1 / 25.0;
												dWidth = dWidth - speed.x;
												dHeight = dHeight - speed.y;*/

												double unitXY = sqrt(unitX*unitX + unitY*unitY);
												cv::Point2d speed = it->second->getDisplacement();
												dWidth = dWidth - speed.x * unitXY * 1 / 25.0;
												dHeight = dHeight - speed.y * unitXY * 1 / 25.0;
											}
										}
										ball->isValid() = false;
										dets.push_back(ball);

										if (ball->field()->ballColorMap)
										{
											ball->field()->ballColorMap->classify(ball);
										}

										if (dWidth >= ballMinDiameterMM && dWidth <= ballMaxDiameterMM && dHeight >= ballMinDiameterMM && dHeight <= ballMaxDiameterMM)
										{
											std::map<double, double>::const_iterator it = errorThresholds.lower_bound(ratio);
											double distErr = 0;
											int k = 0;
											for (unsigned int j = 0; j < contours[i].size(); ++j)
											{
												double d = calcDistance(contours[i][j].x, contours[i][j].y, e.center.x, e.center.y, e.size.width / 2.0, e.size.height / 2.0);
												if (!(boost::math::isnan)(d))
												{
													distErr += d;
												}
												else
												{
													k++;
												}
											}
											distErr /= static_cast<double>(contours[i].size() - k);
#ifndef RELEASE
											CV_Assert(it != errorThresholds.end());
#endif
											if (it != errorThresholds.end() && distErr <= it->second)
											{
												if (ball->field()->detectionWindow.area() == 0 || ball->field()->detectionWindow.contains(ball->pos2d()))
												{
													ball->computeSizeProbability(dWidth, dHeight, ballMaxDiameterMM);
													if (ball->getNegLogProbability() <= ballProbailityThr)
													{
														ball->isValid() = true;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	//MICHOLO TLD - check overlap above 0.5 with standard detection and validate against template quality which one is better
	/*for (auto& f : worker->getFields())
	{
		if (f.currBB)
		{
			double unitX = worker->getCalibration().getXunit(static_cast<int>(f.currBB->y + f.currBB->height), static_cast<int>(f.currBB->x + f.currBB->width / 2));
			double unitY = worker->getCalibration().getYunit(static_cast<int>(f.currBB->y + f.currBB->height), static_cast<int>(f.currBB->x + f.currBB->width / 2));

			double dWidth = f.currBB->width * unitX;
			double dHeight = f.currBB->height * unitY;

			if (dWidth >= ballMinDiameterMM && dWidth <= ballMaxDiameterMM && dHeight >= ballMinDiameterMM && dHeight <= ballMaxDiameterMM)
			{
				std::shared_ptr<VisualBall> ball = std::make_shared<VisualBall>(IDFactory::generateID(ID_DETECTION2D), worker->getCamID(), *f.currBB, frame(*f.currBB), mask(*f.currBB));
				if (ball)
				{
					ball->pos3d() = worker->getCalibration().transformToRealWorld(ball->pos2d(), ballRadius);
					ball->pos3dEst() = ball->pos3d();
					ball->field() = worker->inField(ball);
					if (ball->field())
					{
						ball->isValid() = false;
						dets.push_back(ball);

						if (ball->field()->ballColorMap)
						{
							ball->field()->ballColorMap->classify(ball);
						}
						if (ball->field()->detectionWindow.area() == 0 || ball->field()->detectionWindow.contains(ball->pos2d()))
						{
							ball->computeSizeProbability(dWidth, dHeight, ballMaxDiameterMM);
							if (ball->getNegLogProbability() <= ballProbailityThr)
							{
								ball->isValid() = true;
							}
						}
					}
				}
			}
		}
	}*/

	for(auto& d : dets)
	{
		std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(d);
		if (ball->isValid() && ball->field()->ballColorMap)
		{
			ball->field()->ballColorMap->assignProbability(ball);
		}
		//if(fieldDet != worker->getFields()[ball->fieldID()].detectionWindows2d.end() && fieldDet->second.first.contains(ball->pos2d()))
		//{
		//	ball->computeProximityProbability(fieldDet->second.second, worker->getDetectionWindowVar());
		//}
		if (ball->isValid() && ball->getNegLogProbability() <= ballProbailityThr)
		{
			ball->isValid() = true;
		}
		else
		{
			ball->isValid() = false;
		}
	}
}
double Detector::calcDistance(double u, double v, double x0, double y0, double w, double h) const
{
	double a2_b2 = w*w - h*h;
	double u_x0 = u - x0;
	double v_y0 = v - y0;
	double theta = std::atan(v_y0 / u_x0);
	if (u_x0 < 0.0 && v_y0 >= 0.0)
	{
		theta = theta + CV_PI;
	}
	if (u_x0 < 0.0 && v_y0 < 0.0)
	{
		theta = theta - CV_PI;
	}

	// use Newton's method to find root
	double f = (a2_b2 * std::sin(theta) * std::cos(theta) - u_x0 * w * std::sin(theta) + v_y0 * h * std::cos(theta));
	double df = (a2_b2 * (std::cos(theta)*std::cos(theta) - std::sin(theta)*std::sin(theta)) - u_x0 * w * std::cos(theta) - v_y0 * h * std::sin(theta));

	for (int i = 0; i<5; i++)
	{
		theta = theta - f / df;
		f = (a2_b2 * std::sin(theta) * std::cos(theta) - u_x0 * w * std::sin(theta) + v_y0 * h * std::cos(theta));
		df = (a2_b2 * (std::cos(theta)*std::cos(theta) - std::sin(theta)*std::sin(theta)) - u_x0 * w * std::cos(theta) - v_y0 * h * std::sin(theta));
	}

	// calculate distance, |p-r|
	double dx = u - w * std::cos(theta) - x0;
	double dy = v - h * std::sin(theta) - y0;
	double distance = std::sqrt(dx*dx + dy*dy);
	return distance;
}