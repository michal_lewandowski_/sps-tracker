#include "MedianFlowTracker.h"
#include "NormalisedPatch.h"
#include "Utils.h"
#include "Field.h"
#include "ncc.h"

const cv::Size MedianFlowTracker::winSizeNCC = cv::Size(TLD_WIN_SIZE_NCC,TLD_WIN_SIZE_NCC);
float MedianFlowTracker::error_forwardBackward[trackPoints];
float MedianFlowTracker::error_ncc[trackPoints];
float MedianFlowTracker::error_forwardBackward1[trackPoints];
float MedianFlowTracker::error_ncc1[trackPoints];
char MedianFlowTracker::status[trackPoints];
char MedianFlowTracker::status1[trackPoints];
int MedianFlowTracker::minSize = -1;
int MedianFlowTracker::maxSize = -1;

MedianFlowTracker::MedianFlowTracker()
{
	PYR = nullptr;
	points[0] = nullptr;
	points[1] = nullptr;
	points[2] = nullptr;
	initImgs();

	currPatchData = static_cast<float*>(_aligned_malloc(sizeof(float) * TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC, TLD_MEM_ALIGNMENT));
	prevPatchData = static_cast<float*>(_aligned_malloc(sizeof(float) * TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC, TLD_MEM_ALIGNMENT));

	currPatch = cv::Mat(winSizeNCC.height, winSizeNCC.width, CV_32FC1, reinterpret_cast<void*>(currPatchData), sizeof(float) * TLD_WIN_SIZE_NCC);
	prevPatch = cv::Mat(winSizeNCC.height, winSizeNCC.width, CV_32FC1, reinterpret_cast<void*>(prevPatchData), sizeof(float) * TLD_WIN_SIZE_NCC);
}

MedianFlowTracker::~MedianFlowTracker()
{
	_aligned_free(currPatchData);
	_aligned_free(prevPatchData);
	release();
}
void MedianFlowTracker::setCurrImg(const cv::Mat &img)
{
	prevImg = currImg;
	currImg = img;
}

void MedianFlowTracker::track(FieldList& fields)
{
	bool init = false;
	for (auto& f : fields)
	{
		if (f.prevBB)
		{
			if (f.prevBB->width <= 0 || f.prevBB->height <= 0)
			{
				continue;
			}
			float bb_tracker[] = { static_cast<float>(f.prevBB->x), static_cast<float>(f.prevBB->y), static_cast<float>(f.prevBB->width + f.prevBB->x - 1), static_cast<float>(f.prevBB->height + f.prevBB->y - 1) };
			float scale;

			IplImage prevImg1 = prevImg;
			IplImage currImg1 = currImg;
			int success = fbtrack(&prevImg1, &currImg1, bb_tracker, bb_tracker, &scale);

			int x, y, w, h;
			x = cvFloor(bb_tracker[0] + 0.5f);
			y = cvFloor(bb_tracker[1] + 0.5f);
			w = cvFloor(bb_tracker[2] - bb_tracker[0] + 1.0f + 0.5f);
			h = cvFloor(bb_tracker[3] - bb_tracker[1] + 1.0f + 0.5f);

			if (w < minSize)
				w = minSize;
			if (h < minSize)
				h = minSize;
			if (w > maxSize)
				w = maxSize;
			if (h > maxSize)
				h = maxSize;

			if (!success || x < 0 || y < 0 || w < minSize || h < minSize || w > maxSize || h > maxSize || x + w >= currImg.cols || y + h >= currImg.rows || x != x || y != y || w != w || h != h) //x!=x is check for nan
			{
				f.trackResult.release();
			}
			else
			{
				f.trackResult.trackerBB = new cv::Rect(x, y, w, h);
			}
		}
	}	
}


int MedianFlowTracker::fbtrack(IplImage *imgI, IplImage *imgJ, float *bb, float *bbnew, float *scaleshift)
{
	const int sizePointsArray = trackPoints * 2;
	float pt[sizePointsArray];
	float ptTracked[sizePointsArray];
	int nlkPoints;
	CvPoint2D32f *startPoints;
	CvPoint2D32f *targetPoints;
	float *fbLkCleaned;
	float *nccLkCleaned;
	int i, M;
	int nRealPoints;
	float medFb;
	float medNcc;
	int nAfterFbUsage;

	getFilledBBPoints(bb, points_gridX, points_gridY, 5, pt);
	//getFilledBBPoints(bb, points_gridX, points_gridY, 5, &ptTracked);
	memcpy(ptTracked, pt, sizeof(float) * sizePointsArray);

	initImgs();
	trackLK(imgI, imgJ, pt, trackPoints, ptTracked, trackPoints, pyramidLevel, error_forwardBackward, error_ncc, status);
	initImgs();
	nlkPoints = 0;

	for (i = 0; i < trackPoints; i++)
	{
		nlkPoints += status[i];

	}
	startPoints = (CvPoint2D32f *) malloc(nlkPoints * sizeof(CvPoint2D32f));
	targetPoints = (CvPoint2D32f *) malloc(nlkPoints * sizeof(CvPoint2D32f));
	fbLkCleaned = (float *) malloc(nlkPoints * sizeof(float));
	nccLkCleaned = (float *) malloc(nlkPoints * sizeof(float));

	M = 2;
	nRealPoints = 0;

	for (i = 0; i < trackPoints; i++)
	{
		if (ptTracked[M * i] == -1)
		{
		}
		else
		{
			startPoints[nRealPoints].x = pt[2 * i];
			startPoints[nRealPoints].y = pt[2 * i + 1];
			targetPoints[nRealPoints].x = ptTracked[2 * i];
			targetPoints[nRealPoints].y = ptTracked[2 * i + 1];
			fbLkCleaned[nRealPoints] = error_forwardBackward[i];
			nccLkCleaned[nRealPoints] = error_ncc[i];
			nRealPoints++;
		}
	}

	medFb = tldGetMedian(fbLkCleaned, nlkPoints);
	medNcc = tldGetMedian(nccLkCleaned, nlkPoints);

	nAfterFbUsage = 0;

	for (i = 0; i < nlkPoints; i++)
	{
		if ((fbLkCleaned[i] <= medFb) & (nccLkCleaned[i] >= medNcc))
		{
			startPoints[nAfterFbUsage] = startPoints[i];
			targetPoints[nAfterFbUsage] = targetPoints[i];
			nAfterFbUsage++;
		}
	}

	predictbb(bb, startPoints, targetPoints, nAfterFbUsage, bbnew, scaleshift);

	free(startPoints);
	free(targetPoints);
	free(fbLkCleaned);
	free(nccLkCleaned);

	if (medFb > 10) 
		return 0;
	else return 1;

}

void MedianFlowTracker::euclideanDistance(CvPoint2D32f *point1, CvPoint2D32f *point2, float *match, int nPts)
{
	int i;

	for (i = 0; i < nPts; i++)
	{
		match[i] = sqrt((point1[i].x - point2[i].x) * (point1[i].x - point2[i].x) + (point1[i].y - point2[i].y) * (point1[i].y - point2[i].y));
	}
}
//void MedianFlowTracker::normCrossCorrelation(cv::Mat pimg, cv::Mat cimg, CvPoint2D32f *points0, CvPoint2D32f *points1, int nPts, char *status, float *match)
//{
//	for (int i = 0; i < nPts; i++)
//	{
//		if (status[i] == 1)
//		{
//			cv::getRectSubPix(pimg, winSizeNCC, cv::Point2f(points0[i].x, points0[i].y), prevPatch, CV_32FC1);
//			cv::getRectSubPix(cimg, winSizeNCC, cv::Point2f(points1[i].x, points1[i].y), currPatch, CV_32FC1);
//			CV_DbgAssert(currPatchData == reinterpret_cast<float*>(currPatch.data));
//			CV_DbgAssert(prevPatchData == reinterpret_cast<float*>(prevPatch.data));
//
//			cv::matchTemplate(prevPatch, currPatch, resultNCC, CV_TM_CCOEFF_NORMED);
//			match[i] = resultNCC.at<float>(0, 0);
//			std::cout<< (match[i]) <<" ";
//			
//			cv::Scalar p1 = cv::mean(prevPatch);
//			cv::Scalar c1 = cv::mean(currPatch);
//			match[i] = ncc_naive_(reinterpret_cast<float*>(prevPatch.data), reinterpret_cast<float*>(currPatch.data), p1[0], c1[0]);
//			std::cout << (match[i]) << " ";
//
//			//match[i] = ncc_sse_(reinterpret_cast<float*>(prevPatch.data), reinterpret_cast<float*>(currPatch.data));
//			std::cout << (match[i]) << " ";
//
//			//match[i] = ncc_avx_(reinterpret_cast<float*>(prevPatch.data), reinterpret_cast<float*>(currPatch.data));
//			std::cout << (match[i]) << " " << std::endl;
//		}
//		else
//		{
//			match[i] = 0.0;
//		}
//	}
//}

void MedianFlowTracker::release()
{
	if (PYR != 0)
	{
		int i;

		for (i = 0; i < 2; i++)
		{
			cvReleaseImage(&(PYR[i]));
			PYR[i] = 0;
		}

		free(PYR);
		PYR = 0;
	}
}

void MedianFlowTracker::initImgs()
{
	release();
	PYR = (IplImage **) calloc(2, sizeof(IplImage *));
}

int MedianFlowTracker::trackLK(IplImage *imgI, IplImage *imgJ, float ptsI[], int nPtsI, float ptsJ[], int nPtsJ, int level, float *fb, float *ncc, char *status)
{
	int I, J;
	CvSize pyr_sz;
	int i;

	if (level == -1)
	{
		level = 5;
	}

	I = 0;
	J = 1;

	pyr_sz = cvSize(imgI->width + 8, imgI->height / 3);
	PYR[I] = cvCreateImage(pyr_sz, IPL_DEPTH_32F, 1);
	PYR[J] = cvCreateImage(pyr_sz, IPL_DEPTH_32F, 1);

	// Points
	if (nPtsJ != nPtsI)
	{
		printf("Inconsistent input!\n");
		return 0;
	}

	points[0] = (CvPoint2D32f *) malloc(nPtsI * sizeof(CvPoint2D32f)); 
	points[1] = (CvPoint2D32f *) malloc(nPtsI * sizeof(CvPoint2D32f)); 
	points[2] = (CvPoint2D32f *) malloc(nPtsI * sizeof(CvPoint2D32f)); 

	char *statusBacktrack = (char *) malloc(nPtsI);

	for (i = 0; i < nPtsI; i++)
	{
		points[0][i].x = ptsI[2 * i];
		points[0][i].y = ptsI[2 * i + 1];
		points[1][i].x = ptsJ[2 * i];
		points[1][i].y = ptsJ[2 * i + 1];
		points[2][i].x = ptsI[2 * i];
		points[2][i].y = ptsI[2 * i + 1];
	}

	cvCalcOpticalFlowPyrLK(imgI, imgJ, PYR[I], PYR[J], points[0], points[1],
		nPtsI, cvSize(winSizeLK, winSizeLK), level, status, 0, cvTermCriteria(
		CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03),
		CV_LKFLOW_INITIAL_GUESSES);


	cvCalcOpticalFlowPyrLK(imgJ, imgI, PYR[J], PYR[I], points[1], points[2],
		nPtsI, cvSize(winSizeLK, winSizeLK), level, statusBacktrack, 0, cvTermCriteria(
		CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03),
		CV_LKFLOW_INITIAL_GUESSES | CV_LKFLOW_PYR_A_READY | CV_LKFLOW_PYR_B_READY);

	for (i = 0; i < nPtsI; i++)
	{
		if (status[i] && statusBacktrack[i])
		{
			status[i] = 1;
		}
		else
		{
			status[i] = 0;
		}
	}

	//normCrossCorrelation(cv::cvarrToMat(imgI), cv::cvarrToMat(imgJ), points[0], points[1], nPtsI, status, ncc);
	normCrossCorrelationError(prevImg, currImg, prevPatch, currPatch, points[0], points[1], nPtsI, status, error_ncc);

	euclideanDistance(points[0], points[2], fb, nPtsI);

	for (i = 0; i < nPtsI; i++)
	{
		if (status[i] == 1)
		{
			ptsJ[2 * i] = points[1][i].x;
			ptsJ[2 * i + 1] = points[1][i].y;
		}
		else 
		{
			ptsJ[2 * i] = -1.0f;
			ptsJ[2 * i + 1] = -1.0f;
			fb[i] = -1.0f;
			ncc[i] = -1.0f;
		}
	}

	for (i = 0; i < 3; i++)
	{
		free(points[i]);
		points[i] = 0;
	}

	free(statusBacktrack);
	return 1;
}

float MedianFlowTracker::getBbWidth(float *bb)
{
	return abs(bb[2] - bb[0] + 1);
}
float MedianFlowTracker::getBbHeight(float *bb)
{
	return abs(bb[3] - bb[1] + 1);
}

int MedianFlowTracker::predictbb(float *bb0, CvPoint2D32f *pt0, CvPoint2D32f *pt1, int nPts, float *bb1, float *shift)
{
	float *ofx = (float *) malloc(sizeof(float) * nPts);
	float *ofy = (float *) malloc(sizeof(float) * nPts);
	int i;
	int j;
	int d = 0;
	float dx, dy;
	int lenPdist;
	float *dist0;
	float *dist1;
	float s0, s1;

	for (i = 0; i < nPts; i++)
	{
		ofx[i] = pt1[i].x - pt0[i].x;
		ofy[i] = pt1[i].y - pt0[i].y;
	}

	dx = tldGetMedianUnmanaged(ofx, nPts);
	dy = tldGetMedianUnmanaged(ofy, nPts);
	free(ofx);
	ofx = 0;
	free(ofy);
	ofy = 0;
	//m(m-1)/2
	lenPdist = nPts * (nPts - 1) / 2;
	dist0 = (float *) malloc(sizeof(float) * lenPdist);
	dist1 = (float *) malloc(sizeof(float) * lenPdist);

	for (i = 0; i < nPts; i++)
	{
		for (j = i + 1; j < nPts; j++, d++)
		{
			dist0[d] = sqrt(pow(pt0[i].x - pt0[j].x, 2) + pow(pt0[i].y - pt0[j].y, 2));
			dist1[d] = sqrt(pow(pt1[i].x - pt1[j].x, 2) + pow(pt1[i].y - pt1[j].y, 2));
			dist0[d] = (dist0[d]==0.0f? 0.0f : dist1[d] / dist0[d]);
		}
	}

	*shift = tldGetMedianUnmanaged(dist0, lenPdist);

	free(dist0);
	dist0 = 0;
	free(dist1);
	dist1 = 0;
	s0 = 0.5f * (*shift - 1.0f) * getBbWidth(bb0);
	s1 = 0.5f * (*shift - 1.0f) * getBbHeight(bb0);
	
	bb1[0] = bb0[0] - s0 + dx;
	bb1[1] = bb0[1] - s1 + dy;
	bb1[2] = bb0[2] + s0 + dx;
	bb1[3] = bb0[3] + s1 + dy;

	return 1;
}

int MedianFlowTracker::getFilledBBPoints(float *bb, int numM, int numN, int margin, float *pts)
{
	int pointDim = 2;
	int i;
	int j;
	
	float divN;
	
	float divM;
	float bb_local[4];
	float center[2];
	float spaceN;
	float spaceM;
	
	bb_local[0] = bb[0] + margin;
	bb_local[1] = bb[1] + margin;
	bb_local[2] = bb[2] - margin;
	bb_local[3] = bb[3] - margin;

	
	if (numN == 1 && numM == 1)
	{
		calculateBBCenter(bb_local, pts);
		return 1;
	}
	else if (numN == 1 && numM > 1)
	{
		divM = numM - 1.0f;
		divN = 2.0f;
		
		spaceM = (bb_local[3] - bb_local[1]) / divM;
		calculateBBCenter(bb_local, center);

		for (i = 0; i < numN; i++)
		{
			for (j = 0; j < numM; j++)
			{
				pts[i * numM * pointDim + j * pointDim + 0] = center[0];
				pts[i * numM * pointDim + j * pointDim + 1] = bb_local[1] + j * spaceM;
			}
		}

		return 1;
	}
	else if (numN > 1 && numM == 1)
	{
		float center[2];
		float *cen;
		divM = 2.0f;
		divN = numN - 1.0f;
		
		spaceN = (bb_local[2] - bb_local[0]) / divN;
		cen = center;
		calculateBBCenter(bb_local, center);

		for (i = 0; i < numN; i++)
		{
			for (j = 0; j < numM; j++)
			{
				pts[i * numM * pointDim + j * pointDim + 0] = bb_local[0] + i * spaceN;
				pts[i * numM * pointDim + j * pointDim + 1] = cen[1];
			}
		}

		return 1;
	}
	else if (numN > 1 && numM > 1)
	{
		divM = numM - 1.0f;
		divN = numN - 1.0f;
	}

	spaceN = (bb_local[2] - bb_local[0]) / divN;
	spaceM = (bb_local[3] - bb_local[1]) / divM;

	for (i = 0; i < numN; i++)
	{
		for (j = 0; j < numM; j++)
		{
			pts[i * numM * pointDim + j * pointDim + 0] = bb_local[0] + i * spaceN;
			pts[i * numM * pointDim + j * pointDim + 1] = bb_local[1] + j * spaceM;
		}
	}

	return 1;
}

int MedianFlowTracker::calculateBBCenter(float bb[4], float center[2])
{
	if (bb == 0)
		return 0;

	center[0] = 0.5f * (bb[0] + bb[2]);
	center[1] = 0.5f * (bb[1] + bb[3]);
	return 1;
}
