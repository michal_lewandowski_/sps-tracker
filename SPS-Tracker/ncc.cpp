#include "NormalisedPatch.h"
#include "ncc.h"

#if !defined(SPS_USE_SSE) && !defined(SPS_USE_AVX)
float ncc(const NormalizedPatch& np1, const NormalizedPatch& np2)
{
	double corr = 0.0;

	for (int i = 0; i < TLD_PATCH_SIZE * TLD_PATCH_SIZE; ++i)
	{
		corr += np1.values[i] * np2.values[i];
	}

	return static_cast<float>(0.5f * (corr / sqrt(np1.norm * np2.norm) + 1.0));
}
float ncc(const float *f1, const float *f2, float m1, float m2)
{
	double corr = 0;
	double norm1 = 0;
	double norm2 = 0;

	for (int i = 0; i <TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC; ++i)
	{
		corr += (f1[i] - m1) * (f2[i] - m2);
		norm1 += (f1[i] - m1) * (f1[i] - m1);
		norm2 += (f2[i] - m2) * (f2[i] - m2);
	}

	//return static_cast<float>(0.5f * (corr / sqrt(norm1 * norm2) + 1.0f ));
	return static_cast<float>((corr / sqrt(norm1 * norm2)));
}
void normCrossCorrelationError(const cv::Mat& pimg, const cv::Mat& cimg, const cv::Mat& pPatch, const cv::Mat& cPatch, CvPoint2D32f *points0, CvPoint2D32f *points1, int nPts, const char* status, float *match)
{
	for (int i = 0; i < nPts; i++)
	{
		if (status[i] == 1)
		{
			cv::getRectSubPix(pimg, cv::Size(TLD_WIN_SIZE_NCC, TLD_WIN_SIZE_NCC), cv::Point2f(points0[i].x, points0[i].y), pPatch, CV_32FC1);
			cv::getRectSubPix(cimg, cv::Size(TLD_WIN_SIZE_NCC, TLD_WIN_SIZE_NCC), cv::Point2f(points1[i].x, points1[i].y), cPatch, CV_32FC1);

			cv::Scalar m1 = cv::mean(pPatch);
			cv::Scalar m2 = cv::mean(cPatch);
			match[i] = ncc(reinterpret_cast<float*>(pPatch.data), reinterpret_cast<float*>(cPatch.data), static_cast<float>(m1[0]), static_cast<float>(m2[0]));
		}
		else
		{
			match[i] = 0.0;
		}
	}
}
void normCrossCorrelationErrorTemplate(const cv::Mat& pimg, const cv::Mat& cimg, const cv::Mat& pPatch, const cv::Mat& cPatch, CvPoint2D32f *points0, CvPoint2D32f *points1, int nPts, const char* status, float *match)
{
	cv::Mat resultNCC;
	for (int i = 0; i < nPts; i++)
	{
		if (status[i] == 1)
		{
			cv::getRectSubPix(pimg, cv::Size(TLD_WIN_SIZE_NCC, TLD_WIN_SIZE_NCC), cv::Point2f(points0[i].x, points0[i].y), pPatch, CV_32FC1);
			cv::getRectSubPix(cimg, cv::Size(TLD_WIN_SIZE_NCC, TLD_WIN_SIZE_NCC), cv::Point2f(points1[i].x, points1[i].y), cPatch, CV_32FC1);

			cv::matchTemplate(pPatch, cPatch, resultNCC, CV_TM_CCOEFF_NORMED);
			match[i] = resultNCC.at<float>(0, 0);
		}
		else
		{
			match[i] = 0.0;
		}
	}
}
#endif