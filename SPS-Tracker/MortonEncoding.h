#pragma once
#include <cstdint>
#include <mutex>
#include <bitset>
#include <vector>

//Adapted from https://github.com/aavenel/FastMortonKeys

//! Encoder for bit interleaving by LUT
class MortonEncoder
{
public:
	/*! Encodes 3 bytes by bit interleaving
	\param x first byte  0x x7, x6, ..., x0;
	\param y second byte 0x y7, y6, ..., y0;
	\param z third byte  0x z7, z6, ..., z0;
	\return bit interleaved concatenation as 0x 0, 0, ..., 0, z7, y7, x7, ..., z0, y0, x0;
	*/
	inline static uint32_t encode(uint8_t x, uint8_t y, uint8_t z)
	{
		std::call_once(ofsp, []()
		{
			mortonEncoderPtr.reset(new MortonEncoder());
		});
		return mortonEncoderPtr->encodeMortonKey(x, y, z);
	}
	/*! Decodes a bit interleaved uint32_t into 3 bytes
	\param[in] bitInterleaved the bit interleaved uint32_t as 0x 0, 0, ..., 0, z7, y7, x7, ..., z0, y0, x0;
	\param[out] x first byte  0x x7, x6, ..., x0;
	\param[out] y second byte 0x y7, y6, ..., y0;
	\param[out] z third byte  0x z7, z6, ..., z0;
	*/
	inline static void decode(uint32_t bitInterleaved, uint8_t& x, uint8_t& y, uint8_t& z);
private:
	//! Singleton
	static std::shared_ptr<MortonEncoder> mortonEncoderPtr;
	//! Once flag to have a threadsafe singleton
	static std::once_flag ofsp;

	//! Copy ctor: not allowed
	MortonEncoder(MortonEncoder const&); // Don't Implement
	//! Assignment operator: not allowed
	void operator=(MortonEncoder const&); // Don't implement
	//! Ctor initialising the LUTs
	MortonEncoder()
	{
		morton<256, 0>::add_values(mortonkeyX);
		morton<256, 1>::add_values(mortonkeyY);
		morton<256, 2>::add_values(mortonkeyZ);
	}
	/*! Encodes 3 bytes by bit interleaving
	\param x first byte  0x x7, x6, ..., x0;
	\param y second byte 0x y7, y6, ..., y0;
	\param z third byte  0x z7, z6, ..., z0;
	\return bit interleaved concatenation as 0x 0, 0, ..., 0, z7, y7, x7, ..., z0, y0, x0;
	*/
	inline uint32_t encodeMortonKey(uint8_t x, uint8_t y, uint8_t z)
	{
		return mortonkeyZ[z] | mortonkeyY[y] | mortonkeyX[x];
	}

	// 2396745 is 00000000001001001001001001001001
#define MAXMORTONKEY 2396745
//! LUT for offset 0
	std::vector<uint32_t> mortonkeyX;
	//! LUT for offset 1
	std::vector<uint32_t> mortonkeyY;
	//! LUT for offset 2
	std::vector<uint32_t> mortonkeyZ;

	//*Compute a 256 array of morton keys at compile time
	template <uint32_t i, uint32_t offset>
	struct morton
	{
		enum
		{
			//Use a little trick to calculate next morton key
			//mortonkey(x+1) = (mortonkey(x) - MAXMORTONKEY) & MAXMORTONKEY
			value = (morton<i - 1, offset>::value - MAXMORTONKEY) & MAXMORTONKEY
		};
		static void add_values(std::vector<uint32_t>& v)
		{
			morton<i - 1, offset>::add_values(v);
			v.push_back(value << offset);
		}
	};

	//*Compute a 256 array of morton keys at compile time: initial value
	template <uint32_t offset>
	struct morton<0, offset>
	{
		enum
		{
			value = 0
		};
		static void add_values(std::vector<uint32_t>& v)
		{
			v.push_back(value);
		}
	};
};


void MortonEncoder::decode(uint32_t bitInterleaved, uint8_t& x, uint8_t& y, uint8_t& z)
{
	std::bitset<8> a, b, c;
	std::bitset<24> bits(bitInterleaved);
	for (unsigned i = 0; i < 24; ++i)
	{
		const unsigned j = i / 3;
		a[j] = bits[i];
		b[j] = bits[++i];
		c[j] = bits[++i];
	}
	x = static_cast<uint8_t>(a.to_ulong());
	y = static_cast<uint8_t>(b.to_ulong());
	z = static_cast<uint8_t>(c.to_ulong());
}
