#pragma once

#include <opencv.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/program_options.hpp>
#include "CameraCalibration.h"

class CameraCalibration
{
public:
	CameraCalibration();
	CameraCalibration(std::string configFile);
	virtual ~CameraCalibration() {}
	CameraCalibration(const CameraCalibration& other);
	CameraCalibration& operator=(const CameraCalibration& other);

	bool init(std::string configFile);

	const cv::Size& getImageSize() const;
	double getFocalLengthX() const;
	double getFocalLengthY() const;
	cv::Point2d getPrincipalPoint() const;
	double getTilt() const;
	double getPan() const;
	double getRoll() const;
	cv::Point3d getCameraPosition() const;
	cv::Vec3d getRotationVector() const;
	cv::Vec3d getTranslationVector() const;
	cv::Matx33d getIntrinsicMatrix() const;
	cv::Matx34d getExtrinsicMatrix() const;
	cv::Matx34d getCameraMatrix() const;
	void getDistortionCoefficients(double* k1, double* k2 = nullptr,double* p1 = nullptr, double* p2 = nullptr,double* k3 = nullptr, double* k4 = nullptr,double* k5 = nullptr, double* k6 = nullptr) const;
	cv::Mat getDistortionCoefficients(void) const;

	CameraCalibration& setFocalLengthX(double focalLengthX);
	CameraCalibration& setFocalLengthY(double focalLengthY);
	CameraCalibration& setPrincipalPoint(const cv::Point2f& principalPoint);
	CameraCalibration& setPosition(const cv::Point3d& cameraPosition);
	CameraCalibration& setRotation(double tiltInRad, double panInRad, double rollInRad, const cv::Point3d& cameraPosition);
	CameraCalibration& setDistortionCoefficients(double k1, double k2, double p1, double p2, double k3, double k4, double k5, double k6);

	bool transformToPixel(const cv::Point3d& realWorld,cv::Point2d& pixel) const;
	cv::Point3d transformToRealWorld(const cv::Point2d& pixel,double height = 0.0) const;
	cv::Point3d transformToRealWorld(const cv::Point2d& pixel,double height, std::vector<cv::Vec2d>& buffer, std::vector<cv::Vec2d>& outBuffer) const;
	cv::Point3d transformToRealWorld(const cv::Point& pixel,double height = 0.0) const;
	cv::Point3d transformToRealWorld(const cv::Point2f& pixel,double height = 0.0) const;

	double getApproximateUnit(const cv::Point3d pt1, const cv::Point3d pt2) const;

	void getVisiblePolygonOnGroundPlane(const cv::Point2f& minRealWorldCoordinateInMM, const cv::Point2f& maxRealWorldCoordinateInMM, std::vector<cv::Point2f>& polygonInMM) const;
	void getVisiblePolygonOnGroundPlane(const std::vector<cv::Point>& maskContour, std::vector<cv::Point2f>& polygonInMM) const;
	void getVisiblePolygonOnGroundPlane(const cv::Point2f& minRealWorldCoordinateInMM, const cv::Point2f& maxRealWorldCoordinateInMM, const std::vector<cv::Point>& maskContour, std::vector<cv::Point2f>& polygonInMM) const;

	inline double getXunit(int y, int x) const
	{
		return unitTemplateMapX.at<double>(y, x);
	}
	inline double getYunit(int y, int x) const
	{
		return unitTemplateMapY.at<double>(y, x);
	}
	inline cv::Mat& unitTempMapX() { return unitTemplateMapX; }
	inline cv::Mat& unitTempMapY() { return unitTemplateMapY; }
	inline cv::Mat& ballSizeMap() { return ballMap; }

	void showSizeMap(std::string camID, cv::Mat frame, cv::Mat mask);

private:
	bool isInit;
	cv::Matx33d m_intrinsicMatrix;
	cv::Mat m_distortionCoefficients;
	cv::Size m_imageSize;
	cv::Matx33d m_rotationMatrix;
	cv::Vec3d m_translationVector;
	cv::Point2d m_min;
	cv::Point2d m_max;
	//! Position of the camera in real world coordinates in mm = -R't
	cv::Point3d m_cameraPosition;
	cv::Mat unitTemplateMapX;
	cv::Mat unitTemplateMapY;
	cv::Mat ballMap;
	boost::property_tree::ptree m_configTree;
};