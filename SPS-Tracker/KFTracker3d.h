#pragma once

#include <opencv2/opencv.hpp>
#include "MovingObject.h"
#include "Track.h"
#include "KFTrack3d.h"

class WorkManager;
struct BallSaveData;

class KFTracker3d
{
public:
	KFTracker3d(WorkManager* w);
	virtual ~KFTracker3d();

	void track(const std::vector<std::shared_ptr<MovingObject>>& detections, std::deque<std::shared_ptr<KFTrack3d>>& liveTracks, std::deque<std::shared_ptr<KFTrack3d>>& deadTracks, std::vector<std::deque<BallSaveData>>& ballData);
	void saveData(std::shared_ptr<KFTrack3d> track, std::deque<BallSaveData>& ballData) const;

protected:
	std::shared_ptr<KFTrack3d> createTrack(const std::shared_ptr<MovingObject> obj, const int64_t dStartTS, const size_t iNumTracks);

private:
	KFTracker3d(const KFTracker3d& other);
	KFTracker3d& operator=(const KFTracker3d& other);

public:
	WorkManager* workerManager;
	int stateParams;
	int mesaureParams;
	int minTrackLength;
	std::size_t maxNumTracks;
	double distThreshold;
	double probThreshold;
	double distVariance;
	double maxDist;
	double maxProb;
};
