#include "stdafx.h"
#include "vt_bgfs_mog2.h"
#include <core/cuda_stream_accessor.hpp>
#include <iostream>

bool showParams1 = true;

namespace MOG2
{
	// default parameters of gaussian background detection algorithm
	const int defaultHistory = 150; // Learning rate; alpha = 1/defaultHistory2
	const float defaultVarThreshold = 4.0f * 4.0f;
	const int defaultNMixtures = 5; // maximal number of Gaussians in mixture
	const float defaultBackgroundRatio = 0.9f; // threshold sum of weights for background test
	const float defaultVarThresholdGen = 3.0f * 3.0f;
	const float defaultVarInit = 15.0f; // initial variance for new components
	const float defaultVarMax = 5.0f * defaultVarInit;
	const float defaultVarMin = 4.0f;

	// additional parameters
	const float defaultfCT = 0.05f; // complexity reduction prior constant 0 - no reduction of number of components
	const unsigned char defaultnShadowDetection = 127; // value to use in the segmentation mask for shadows, set 0 not to do shadow detection
	const float defaultfTau = 0.5f; // Tau - shadow threshold, see the paper for explanation
	const float defaultHSVFactorH = 1.0f;
	const float defaultHSVFactorS = 1.0f;
	const float defaultHSVFactorV = 1.0f;

	void setNumberOfGaussiansPerPixel(int nMixtures);
	void setVarianceThreshold(float varThreshold);
	void setBackgroundRatio(float backgroundRatio);
	void setVarianceThresholdGen(float varThresholdGen);
	void setInitialVariance(float initialVariance);
	void setMinVariance(float minVariance);
	void setMaxVariance(float maxVariance);
	void setShadowValue(unsigned char nShadowDetection);
	void setShadowThreshold(float tau);
	void setHfactor(float HSVFactorH);
	void setSfactor(float HSVFactorS);
	void setVfactor(float HSVFactorV);
	void setComplexityReductionPrior(float ct);
	void enableShadowDetection(bool shadowDetection);

	void digest(cv::cuda::PtrStepSzb frame, int cn, cv::cuda::PtrStepSzb validmask, cv::cuda::PtrStepSzb fgmask, cv::cuda::PtrStepSzb modesUsed, cv::cuda::PtrStepSzf weight, cv::cuda::PtrStepSzf variance, cv::cuda::PtrStepSzb mean, float alphaT, float prune, bool detectShadows, cudaStream_t stream);
	void getBackgroundImage(int cn, cv::cuda::PtrStepSzb modesUsed, cv::cuda::PtrStepSzf weight, cv::cuda::PtrStepSzb mean, cv::cuda::PtrStepSzb dst, cudaStream_t stream);
}


VT_MOG2_GPU::VT_MOG2_GPU(int nmixtures) 
	: frameSize_(0, 0), 
	  frameType_(0), 
	  nframes_(0)
{
    nmixtures_ = nmixtures > 0 ? nmixtures : MOG2::defaultNMixtures;

    history = MOG2::defaultHistory;
    varThreshold = MOG2::defaultVarThreshold;
    bShadowDetection = true;

    backgroundRatio = MOG2::defaultBackgroundRatio;
    fVarInit = MOG2::defaultVarInit;
    fVarMax  = MOG2::defaultVarMax;
    fVarMin = MOG2::defaultVarMin;

    varThresholdGen = MOG2::defaultVarThresholdGen;
    fCT = MOG2::defaultfCT;
    nShadowDetection =  MOG2::defaultnShadowDetection;
    fTau = MOG2::defaultfTau;
		
	fHSVFactorH = MOG2::defaultHSVFactorH;
	fHSVFactorS = MOG2::defaultHSVFactorS;
	fHSVFactorV = MOG2::defaultHSVFactorV;
}

void VT_MOG2_GPU::initialize(cv::Size frameSize, int frameType)
{
    CV_Assert(frameType == CV_8UC1 || frameType == CV_8UC3 || frameType == CV_8UC4);

    frameSize_ = frameSize;
    frameType_ = frameType;
    nframes_ = 0;

    int ch = CV_MAT_CN(frameType);
    int work_ch = ch;

    // for each gaussian mixture of each pixel bg model we store ...
    // the mixture weight (w),
    // the mean (nchannels values) and
    // the covariance
    weight_.create(frameSize.height * nmixtures_, frameSize_.width, CV_32FC1);
    variance_.create(frameSize.height * nmixtures_, frameSize_.width, CV_32FC1);
    mean_.create(frameSize.height * nmixtures_, frameSize_.width, CV_32FC(work_ch));

    //make the array for keeping track of the used modes per pixel - all zeros at start
    bgmodelUsedModes_.create(frameSize_, CV_8UC1);
    bgmodelUsedModes_.setTo(cv::Scalar::all(0));

	MOG2::setNumberOfGaussiansPerPixel(nmixtures_);
	MOG2::setVarianceThreshold(varThreshold);
	MOG2::setBackgroundRatio(backgroundRatio);
	MOG2::setVarianceThresholdGen(varThresholdGen);
	MOG2::setInitialVariance(fVarInit);
	MOG2::setMinVariance( fVarMin );
	MOG2::setMaxVariance( fVarMax );
	MOG2::setShadowThreshold(fTau);
	MOG2::setShadowValue(nShadowDetection);
	MOG2::setHfactor( fHSVFactorH );
	MOG2::setSfactor( fHSVFactorS );
	MOG2::setVfactor( fHSVFactorV );

	if (showParams1)
	{
		std::cout << "history\t" << history << std::endl;
		std::cout << "nmixtures\t" << nmixtures_ << std::endl;
		std::cout << "varThreshold\t" << varThreshold << std::endl;
		std::cout << "varThresholdGen\t" << varThresholdGen << std::endl;
		std::cout << "backgroundRatio\t" << backgroundRatio << std::endl;
		std::cout << "VarInit\t" << fVarInit << std::endl;
		std::cout << "VarMin\t" << fVarMin << std::endl;
		std::cout << "VarMax\t" << fVarMax << std::endl;
		std::cout << "ComplexityReductionPrior\t" << fCT << std::endl;
		std::cout << "Tau\t" << fTau << std::endl;
		std::cout << "HSVFactorH\t" << fHSVFactorH << std::endl;
		std::cout << "fHSVFactorS\t" << fHSVFactorS << std::endl;
		std::cout << "fHSVFactorV\t" << fHSVFactorV << std::endl;
		showParams1 = false;
	}
}

void VT_MOG2_GPU::setNumberOfGaussiansPerPixel(int nMixtures)
{
	if (nMixtures != nmixtures_)
	{
		MOG2::setNumberOfGaussiansPerPixel(nMixtures);
		nmixtures_ = nMixtures;
	}
}

void VT_MOG2_GPU::setVarianceThreshold(float newVarThreshold)
{
	if (varThreshold != newVarThreshold)
	{
		MOG2::setVarianceThreshold(newVarThreshold);		
		varThreshold = newVarThreshold;
	}
}

void VT_MOG2_GPU::setBackgroundRatio(float newBackgroundRatio)
{
	if (backgroundRatio != newBackgroundRatio)
	{
		MOG2::setBackgroundRatio(newBackgroundRatio);
		backgroundRatio = newBackgroundRatio;
	}
}

void VT_MOG2_GPU::setVarianceThresholdGen(float newVarThresholdGen)
{
	if (varThresholdGen != newVarThresholdGen)
	{
		MOG2::setVarianceThresholdGen(newVarThresholdGen);
		varThresholdGen = newVarThresholdGen;
	}
}

void VT_MOG2_GPU::setInitialVariance(float initialVariance)
{
	if (fVarInit != initialVariance)
	{
		MOG2::setInitialVariance(initialVariance);
		fVarInit = initialVariance;
	}
}

void VT_MOG2_GPU::setMinVariance(float minVariance)
{
	if (fVarMin != minVariance)
	{
		MOG2::setMinVariance( minVariance );
		fVarMin = minVariance;
	}
}

void VT_MOG2_GPU::setMaxVariance(float maxVariance)
{
	if (fVarMax != maxVariance)
	{
		MOG2::setMaxVariance( maxVariance );
		fVarMax = maxVariance;
	}
}

void VT_MOG2_GPU::setShadowValue(unsigned char newNShadowDetection)
{
	if (newNShadowDetection != nShadowDetection)
	{
		MOG2::setShadowValue( newNShadowDetection );
		nShadowDetection = newNShadowDetection;
	}
}

void VT_MOG2_GPU::setComplexityReductionPrior(float ct)
{
	fCT = ct;
}

void VT_MOG2_GPU::enableShadowDetection(bool shadowDetection)
{
	bShadowDetection = shadowDetection;
}

void VT_MOG2_GPU::setShadowThreshold(float tau)
{
	if (fTau != tau)
	{
		MOG2::setShadowThreshold(tau);
		fTau = tau;
	}
}

void VT_MOG2_GPU::setHfactor(float HSVFactorH)
{
	if (fHSVFactorH != HSVFactorH)
	{
		MOG2::setHfactor( HSVFactorH );
		fHSVFactorH = HSVFactorH;
	}
}

void VT_MOG2_GPU::setSfactor(float HSVFactorS)
{
	if (fHSVFactorS != HSVFactorS)
	{
		MOG2::setSfactor( HSVFactorS );
		fHSVFactorS = HSVFactorS;
	}
}

void VT_MOG2_GPU::setVfactor(float HSVFactorV)
{
	if (fHSVFactorV != HSVFactorV)
	{
		MOG2::setVfactor( HSVFactorV );
		fHSVFactorV = HSVFactorV;
	}
}

void VT_MOG2_GPU::operator()(const cv::cuda::GpuMat& frame, const cv::cuda::GpuMat& validmask, cv::cuda::GpuMat& fgmask, float learningRate, cv::cuda::Stream& stream)
{
    int ch = frame.channels();
    int work_ch = ch;

    if (nframes_ == 0 || learningRate >= 1.0f || frame.size() != frameSize_ || work_ch != mean_.channels())
        initialize(frame.size(), frame.type());

    fgmask.create(frameSize_, CV_8UC1);
    //All pixels will be set in MOG2::vt_mog2_gpu so no need to call fgmask.setTo(cv::Scalar::all(0));

    ++nframes_;
    learningRate = learningRate >= 0.0f && nframes_ > 1 ? learningRate : 1.0f / std::min(2 * nframes_, history);
    CV_Assert(learningRate >= 0.0f);

	MOG2::digest(frame, frame.channels(), validmask, fgmask, bgmodelUsedModes_, weight_, variance_, mean_, learningRate, -learningRate * fCT, bShadowDetection, cv::cuda::StreamAccessor::getStream(stream));
}

void VT_MOG2_GPU::getBackgroundImage(cv::cuda::GpuMat& backgroundImage, cv::cuda::Stream& stream) const
{
    backgroundImage.create(frameSize_, frameType_);

	MOG2::getBackgroundImage(backgroundImage.channels(), bgmodelUsedModes_, weight_, mean_, backgroundImage, cv::cuda::StreamAccessor::getStream(stream));
}

void VT_MOG2_GPU::release()
{
    frameSize_ = cv::Size(0, 0);
    frameType_ = 0;
    nframes_ = 0;

    weight_.release();
    variance_.release();
    mean_.release();

    bgmodelUsedModes_.release();
}
