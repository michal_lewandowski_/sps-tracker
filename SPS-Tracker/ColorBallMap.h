#pragma once
#include <map>
#include "AdaptiveBinning.h"
#include "KFTrack2d.h"
#include "ColorBallHistAdaptive.h"
#include "TimeWatch.hpp"

class ColorBallMap
{
public:
	ColorBallMap(TimeWatch& sysTime, double ratioThr);
	virtual ~ColorBallMap();

	bool learn(const std::shared_ptr<MovingObject> obj);
	void learn(const std::shared_ptr<KFTrack2d> trBall);
	void classify(std::shared_ptr<MovingObject> ball);
	bool isReady(const std::shared_ptr<MovingObject> ball);
	void prepare(void);
	void assignProbability(std::shared_ptr<MovingObject> ball);

	size_t getRebinTime(void);

	cv::Mat getImage(const std::shared_ptr<KFTrack2d> trBall, std::string& prefixStr) const;
	cv::Mat getProbImage(const std::shared_ptr<KFTrack2d> trBall, std::stringstream& ss, std::string& prefixStr);

	cv::Mat getImage(int width = 380, int height = 50) const;
	cv::Mat getProbImage(std::stringstream& ss);

	inline const std::shared_ptr<AdaptiveBinning<cv::Vec3b>>& adaptiveBin() const { return adaptiveBinning; };
	inline std::shared_ptr<AdaptiveBinning<cv::Vec3b>>& adaptiveBin() { return adaptiveBinning; }
	
	inline const std::shared_ptr<ColorQuantiziser>& quantiziser() const { return colorQuantiziser; };
	inline std::shared_ptr<ColorQuantiziser>& quantiziser() { return colorQuantiziser; };
	;
protected:
public:
	std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>> hists;
	std::map<double, std::vector<double>> dists;
	std::shared_ptr<AdaptiveBinning<cv::Vec3b>> adaptiveBinning;
	std::shared_ptr<ColorQuantiziser> colorQuantiziser;
	size_t time;
	size_t rebinEvery;
	size_t timeDelay;
	double ratioThr;
	TimeWatch& systemTime;
};
