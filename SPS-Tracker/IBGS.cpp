#include "IBGS.h"
#include "BgsMOG2gpu.h"
#include "BgsSUBSENSE.h"
#include "BgsPAWCS.h"
#include "BgsMOG2cpu.h"

bool IBGS::showParams = true;

IBGS::IBGS()
	: isHSV(false)
{
}


IBGS::~IBGS()
{
}

void IBGS::filterMaskByColor(cv::Mat& mask, cv::Mat img)
{
	/*for (int y = 0; y < mask.rows; ++y)
	{
		for (int x = 0; x < mask.cols; ++x)
		{
			if (mask.at<uchar>(y, x) > 0 && img.channels() == 3)
			{
				if (!(
					(img.at<cv::Vec3b>(y, x)[0] >= colorMin[0] && img.at<cv::Vec3b>(y, x)[0] <= colorMax[0])
					&& (img.at<cv::Vec3b>(y, x)[1] >= colorMin[1] && img.at<cv::Vec3b>(y, x)[1] <= colorMax[1])
					&& (img.at<cv::Vec3b>(y, x)[2] >= colorMin[2] && img.at<cv::Vec3b>(y, x)[2] <= colorMax[2])
					))
				{
					mask.at<uchar>(y, x) = 0;
				}
			}
			else if (mask.at<uchar>(y, x) > 0 && img.channels() == 1)
			{
				if (!(
					(img.at<uchar>(y, x) >= colorMin[0] && img.at<uchar>(y, x) <= colorMax[0])))
				{
					mask.at<uchar>(y, x) = 0;
				}
			}
		}
	}*/
}

std::unique_ptr<IBGS> IBGS::createBackgroundSegmentation()
{
	std::string alg;
	cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
	file["bgsAlgorithm"] >> alg;
	file.release();
	if(alg == "MOG")
	{
#ifdef SPS_GPU_MODE
		return std::make_unique<BgsMOG2gpu>();
#else
		return std::make_unique<BgsMOG2cpu>();
#endif
	}
	if (alg == "SUB")
	{
		return std::make_unique<BgsSUBSENSE>();
	}
	if (alg == "PAW")
	{
		return std::make_unique<BgsPAWCS>();
	}
	return nullptr;
}
void IBGS::startApply()
{
	tw.reset();
}
void IBGS::stopApply()
{
	//std::cout << "Time " << tw.measure().getMilliseconds() << " ms" << std::endl;;
}