#include "stdafx.h"
#include "BacktrackBall.h"

BacktrackBall::BacktrackBall(const std::size_t id, std::string cid, cv::Rect b)
	: BallObject(id, cid, b, cv::RotatedRect(), 1.0, cv::Mat(), cv::Mat())
{
	ebb = cv::RotatedRect(pos2d(), box().size(), 0.0f);
	type = BallType::BACKTRACK;
}
BacktrackBall::BacktrackBall(const std::size_t id, std::string cid, cv::Point3d p, int views, float error)
	: BallObject(id, cid, p, views, 0, error, std::unordered_map<std::string, int>(), std::vector<PreviousBallInCam>())
{
	if(views > 0)
	{
		type = BallType::BACKTRACK;
	}
	else
	{
		type = BallType::BACKTRACK_EX;
	}
}
std::shared_ptr<MovingObject> BacktrackBall::clone() const
{
	return std::make_shared<BacktrackBall>(*this);
}
BacktrackBall::~BacktrackBall()
{
}

BacktrackBall::BacktrackBall(const BacktrackBall& other)
	: BallObject(other)
{
}

BacktrackBall& BacktrackBall::operator=(const BacktrackBall& other)
{
	if (this != &other)
	{
		this->BallObject::operator=(other);
	}
	return *this;
}

void BacktrackBall::show(cv::Mat frame, cv::Scalar color) const
{
	cv::ellipse(frame, ebb, color, -1);
	//cv::ellipse(frame, ebb, cv::Scalar(0, 150, 255), 3);
	//cv::putText(frame, boost::lexical_cast<std::string>(detID), cv::Point(bb_.br().x, bb_.tl().y) + cv::Point(7, -7), CV_FONT_HERSHEY_PLAIN, 1, color);
}
void BacktrackBall::show3d(cv::Mat frame, cv::Scalar color, double windowScale) const
{
	if(views() > 0)
	{
		cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 5, color, -1);
	}
	else
	{
		cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 5, cv::Scalar(0, 0, 255), -1);
	}
	//cv::putText(frame, boost::lexical_cast<std::string>(detID), cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - (cvRound(pos3dEst().y / windowScale)) + cv::Point(7, -7)), CV_FONT_HERSHEY_PLAIN, 1, color);
}