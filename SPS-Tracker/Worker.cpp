#include "stdafx.h"
#include "Worker.h"
#include "Templates.h"
#include "KFBallTrack2d.h"
#include "Field.h"
#include "DetectedBall.h"
#include "VarianceFilter.h"
#include "BinaryClassifier.h"
#include "NPClassifier.h"
#include "EnsembleClassifier.h"
#include "MotionFilter.h"
#include "Utils.h"
#include "IDFactory.h"
#include "VisualBall.h"

extern double ballRadius;
bool showParams1 = true;

Worker::Worker(TimeWatch& st, std::string name, std::string video, std::string config, const FieldList& f, cv::Scalar c)
	: systemTime(st)
	, camID(name)
	, calibration(config)
	, videoPath(video)
	, detector(this)
	, fields(f)
	, tracker2d(this)
	, evidenceCount(0)
	, evidenceThreshold(7)
	, color(c)
	, detectionWindowSize(150)
	, detectionWindowProbThr(-cv::log(0.001))
	, learnColorModelAfter(0)
	, isTLDready(false)
{
	for(auto& ff : fields)
	{
		ff.ballColorMap = std::make_shared<ColorBallMap>(st, detector.sizeRatioThreshold());
	}
	detectionWindowSizeVariance = 2.0 * detectionWindowSize * detectionWindowSize;
}
Worker::~Worker()
{

}

bool Worker::init()
{
	if(!cap.open(videoPath))
	{
		std::cout << "Cannot open video file: " << videoPath << std::endl;
		return false;
	}
	//int i = 80;
	//while(i>0)
	{
		if (!cap.read(frameTemp))
		{
			std::cout << "Cannot read frame in file: " << videoPath << std::endl;
			return false;
		}
		//i--;
	}
	if(!calibration.init(""))
	{
		return false;
	}

	//std::cout << "fps " << cap.get(CV_CAP_PROP_FPS) << std::endl;

	std::string maskFile = videoPath.substr(0, videoPath.length() - 3) + "mask";
	std::string tempUnitXFile = videoPath.substr(0, videoPath.length() - 3) + "unitX.bin";
	std::string tempUnitYFile = videoPath.substr(0, videoPath.length() - 3) + "unitY.bin";
	std::string tempUnitOffsetFile = videoPath.substr(0, videoPath.length() - 3) + "size.bin";

	fieldMask = cv::imread(maskFile + ".png", cv::IMREAD_GRAYSCALE);
	std::ifstream f(maskFile + ".txt", std::ios_base::in);
	if(!f.is_open())
	{
		std::cout << "cannot find mask file " << maskFile << ".txt" << std::endl;
		exit(1);
	}
	cv::Point3d pt;
	while (!f.eof())
	{
		double x, y;
		f >> x >> y;
		mask.push_back(cv::Point(static_cast<int>(x), static_cast<int>(y)));
	}
	f.close();

	bool cache = true;
	calibration.unitTempMapX() = createUnitMapX(tempUnitXFile, calibration, cache);
	calibration.unitTempMapY() = createUnitMapY(tempUnitYFile, calibration, cache);
	calibration.ballSizeMap() = createBallTemplateSizeMap(tempUnitOffsetFile, calibration, getFields(), cache);

	frameGray.create(frameTemp.rows, frameTemp.cols, CV_8UC1);

	//cv::Mat test;
	//cv::resize(fieldMask, test, cv::Size(), 0.5, 0.5, cv::INTER_NEAREST);
	//fieldMask = test;

	bgs = IBGS::createBackgroundSegmentation();
	if(!bgs)
	{
		std::cout << "Cannot create foreground detector" << std::endl;
		return false;
	}
	preprocess();
	bgs->loadParams();
	bgs->init(frame, fieldMask);

	DetectorCascade::minSize = 20;
	DetectorCascade::maxSize = 50;
	MedianFlowTracker::minSize = 20;
	MedianFlowTracker::maxSize = 50;

	cv::Mat gframe;
	cv::cvtColor(frame, gframe, CV_BGR2GRAY);

	cascadeDetector.imgWidth = gframe.cols;
	cascadeDetector.imgHeight = gframe.rows;
	cascadeDetector.imgWidthStep = static_cast<int>(gframe.step);
	cascadeDetector.init(true);

	for (auto& f : fields)
	{
		f.generateMask(calibration);
		f.reinit(calibration.ballSizeMap(), fieldMask);
		cascadeDetector.updateGlobalWindows(f);
	}

	return true;
}
double Worker::getFrameRate()
{
	return 1000.0 / cap.get(CV_CAP_PROP_FPS);
}
bool Worker::capture()
{
	if (!cap.read(frameTemp))
	{
		return false;
	}
	return true;
}
bool Worker::process(std::deque<std::shared_ptr<KFTrack3d>>& tracks3d)
{
	for(auto& f : fields)
	{
		f.prepare();
	}

	if (!capture())
		return false;

	tr2ddead().clear();

	preprocess();
	segment();
	
	initDetectionWindows(tracks3d);
	detector.detect(frame, motionMask, tr2dlive(), detections);
	tracker2d.track(frame, motionMask, detections, tr2dlive(), tr2ddead());

	learnBall();

	return true;
}
void Worker::learnBall()
{
	if (time() >= learnColorModelAfter)
	{
		for (auto& f : fields)
		{
			if (isGoodForLearn(f))
			{
				f.ballColorMap->learn(f.ballTrack2d);
			}
		}
	}
}

void Worker::feedback(std::shared_ptr<KFTrack3d> tr3)
{
	if(tr3->isGoodEnoughFor())
	{
		int id = tr3->getCurrentDet()->track2dID(camID);
		if(id>=0)
		{
			Field* f = getFields().find(tr3->fieldID());
			CV_Assert(f);
			f->maxAbsVelocityGlobal = std::numeric_limits<double>::epsilon();
			f->maxBallProbabilityGlobal = std::numeric_limits<double>::max();
			for (auto& tr2 : tr2dlive())
			{
				if(tr2->field()->fid == tr3->field()->fid)
				{
					if(tr2->getTID() == id)
					{
						tr2->update3d(&calibration, tr3->getCurrentDet());
						if(tr2->field()->ballColorMap
							&& tr2->isVisible()
							&& tr2->getMissedFrames() == 0
							&& tr2->getNumDetected() > 10
							&& tr2->getCurrentDet()->isDetected()
							&& tr2->field()->zoneGame.contains(cv::Point(static_cast<int>(tr2->getCurrentDet()->pos3d().x), static_cast<int>(tr2->getCurrentDet()->pos3d().y))))
						{
							//tr2->field()->ballColorMap->learn(tr2);
						}
					}
					else
					{
						tr2->assignProximityProbability(&calibration, tr3->getCurrentDet());
					}
				}
			}
			for (unsigned int y = 0; y < tr2dlive().size(); ++y)
			{
				if (tr2dlive()[y]->field()->fid == tr3->field()->fid)
				{
					if (tr2dlive()[y]->assignProbability())
					{
						tr2dlive()[y]->field()->ballTrack2d = tr2dlive()[y];
					}
					if (tr2dlive()[y]->isDead(getMask(), getFields()))
					{
						tr2dlive()[y]->setEndTimestamp(time());
						tr2ddead().push_back(tr2dlive()[y]);

						tr2dlive().erase(tr2dlive().begin() + y);
						y--;
					}
				}
			}
		}
		else
		{
			cv::Point2d estpt;
			calibration.transformToPixel(tr3->getCurrentDet()->pos3d(), estpt);
			for(auto& d : detections)
			{
				if(d->isDetected() && d->fieldID() == tr3->fieldID() && d->box().contains(estpt))
				{
#ifdef SPS_DEBUB_DETS
					d->forceLearn = d->field()->ballColorMap->learn(d);
#else
					d->field()->ballColorMap->learn(d);
#endif					
					
				}
			}
			
		}
	}
}
bool Worker::isGoodForLearn(const Field& f) const
{
	/*if(f.ballColorMap != nullptr && f.ballTrack2d && f.ballTrack2d->isGoodEnoughFor())
	{
	if(f.ballColorMap->isReady(f.ballTrack2d->getCurrentDet()))
	{
	return true;
	}
	else
	{
	if(f.isInGameZone(f.ballTrack2d->getCurrentDet()->pos3d()))
	{
	return true;
	}
	}
	}
	return false;*/
	return f.ballColorMap != nullptr
			&& f.ballTrack2d
			&& f.ballTrack2d->isGoodEnoughFor();
}
bool Worker::isGoodForTLD(std::shared_ptr<KFTrack3d> ballTrack3d)
{
	if (ballTrack3d)
	{
		int id = ballTrack3d->getCurrentDet()->track2dID(camID);
		if (id >= 0)
		{
			Field* f = getFields().find(ballTrack3d->fieldID());
			CV_Assert(f);

			if (f->ballTrack2d && systemTime.getFrameNum() > 100)
			{
				if (ballTrack3d->isVisible() && f->ballTrack2d->isVisible())
				{
					if (f->ballTrack2d->getMissedFrames() == 0
						&& f->ballTrack2d->getNumDetected() > 10
						&& f->ballTrack2d->getCurrentDet()->isDetected()
						&& f->ballTrack2d->getAverageProbability() < -cv::log(0.005))
					{
					//	std::cout << "jest: " << f.ballTrack3d->getMissedFrames() << " " << f.ballTrack3d->getCurrentDet()->isDetected() << " " << (f.ballTrack3d->getAverageProbability() < -cv::log(0.033)) << " " << f.ballTrack3d->getCurrentDet()->numDetected2d() << " " << std::endl;
						if (ballTrack3d->getMissedFrames() == 0
							&& ballTrack3d->getNumDetected() > 15
							&& ballTrack3d->getCurrentDet()->isDetected()
							&& ballTrack3d->getAverageProbability() < -cv::log(0.033)
							&& ballTrack3d->getCurrentDet()->numDetected2d() > 3)
						{
							if (!f->isInitTLD)
							{
								f->currBB = new cv::Rect(f->ballTrack2d->getBox());
								f->currConf = 1.0;
								f->valid = 1.0;
								f->currState = VisualState::NEW;

								cv::Mat gframe, intImg, intSqImg, gaussImg;
								cv::cvtColor(frame, gframe, CV_BGR2GRAY);
								cv::integral(gframe, intImg, intSqImg);
								cv::GaussianBlur(gframe, gaussImg, cv::Size(3, 3), 0);

								cascadeDetector.motionFilter->setImages(motionMask);
								cascadeDetector.varianceFilter->setImages(intImg, intSqImg);
								cascadeDetector.binaryClassifier->setImages(intImg);
								cascadeDetector.ensembleClassifier->setImages(gaussImg);
								cascadeDetector.expertClassifier->setImages(frame, intImg);

								float initVar = tldCalcVariance(intImg, intSqImg, f->currBB->x, f->currBB->y, f->currBB->width, f->currBB->height);
								f->minVar = initVar / 2;
								tldCalcBinary(intImg, f->currBB, f->binaryP, f->binaryN);

								f->initialLearning(gframe, intImg, intSqImg, cascadeDetector);
								f->isInitTLD = true;
								isTLDready = true;
								return true;
							}
						}
					}
				}
			}
		}
	}
	return false;
}
void Worker::preprocess()
{
	static int from_to[] = { 0,0 };
	cv::mixChannels(&frameTemp, 1, &frameGray, 1, from_to, 1);
	bgs->preprocess(frameGray, calibration.getImageSize(), frame);
		
	for (auto& f : fields)
	{
		f.storePreviousData();
	}
}
void Worker::segment()
{
	bgs->apply(frame, motionMask);
	/*bgs->getBackgroundImage(bgImg);
	chr.removeShadows(frame, motionMask, bgImg, newmask2);
	cv::Mat m;
	cv::threshold(motionMask, m, 127, 255, cv::THRESH_BINARY_INV);
	cv::cvtColor(frame, newmask1, cv::COLOR_BGR2HSV);
	newmask1.setTo(0, m);
	cv::split(newmask1, aa);
	newmask = newmask1.clone();
	bb.resize(2);
	bb[0] = aa[0].clone();
	bb[1] = aa[1].clone();
	for (int y = 0; y < newmask1.rows; ++y)
	{
		for (int x = 0; x < newmask1.cols; ++x)
		{
			if (motionMask.at<uchar>(y, x) > 0 && newmask1.channels() == 3)
			{
				if (!(
					(newmask1.at<cv::Vec3b>(y, x)[0] >= 0 && newmask1.at<cv::Vec3b>(y, x)[0] <= 50)
					&& (newmask1.at<cv::Vec3b>(y, x)[1] >= 127 && newmask1.at<cv::Vec3b>(y, x)[1] <= 255)
					))
				{
					newmask.at<cv::Vec3b>(y, x) = cv::Vec3b(0,0,0);
				}
			}
			if (motionMask.at<uchar>(y, x) > 0 && aa[0].channels() == 1)
			{
				if (!(
					(aa[0].at<uchar>(y, x) >= 0 && aa[0].at<uchar>(y, x) <= 50)))
				{
					bb[0].at<uchar>(y, x) = 0;
				}
			}
			if (motionMask.at<uchar>(y, x) > 0 && aa[1].channels() == 1)
			{
				if (!(
					(aa[1].at<uchar>(y, x) >= 127 && aa[1].at<uchar>(y, x) <= 255)))
				{
					bb[1].at<uchar>(y, x) = 0;
				}
			}
		}
	}
	cv::Mat hsvImg;
	cv::cvtColor(frame, hsvImg, CV_BGR2HSV);
	cv::Mat channel[3];
	split(hsvImg, channel);
	cv::Mat channelbgs[3];
	cv::split(bgImg, channelbgs);
	cv::Mat motionmasknew(motionMask.size(), CV_8UC1); motionmasknew.setTo(0);
	cv::Mat motionmasknew1(motionMask.size(), CV_8UC1); motionmasknew1.setTo(0);
	for (int y = 0; y < newmask1.rows; ++y)
	{
		for (int x = 0; x < newmask1.cols; ++x)
		{
			if (motionMask.at<uchar>(y, x) > 0)
			{
				float ratioV = (float)channel[2].at<uchar>(y,x) / (float)channelbgs[2].at<uchar>(y, x);
				if(ratioV > 0.3 && ratioV <= 1.0)
				{
					int diffH = fabs(channel[0].at<uchar>(y, x) - channelbgs[0].at<uchar>(y, x));
					int diffS = fabs(channel[1].at<uchar>(y, x) - channelbgs[1].at<uchar>(y, x));
					if( diffH < 100 && diffS < 100)
					{
						motionmasknew.at<uchar>(y, x) = 255;
						motionMask.at<uchar>(y, x) = 0;
					}
					else
					{
						motionmasknew1.at<uchar>(y, x) = 255;
					}
				}
				else
				{
					motionmasknew1.at<uchar>(y, x) = 255;
				}
			}
		}
	}
	split(frame, channel);
	channel[2].setTo(255, motionmasknew);
	channel[0].setTo(255, motionmasknew1);
	cv::merge(channel, 3, motionmasknew);
	cv::imshow("moze tam", motionmasknew);
	split(frame, channel);
	channel[0].setTo(255, motionMask);
	cv::merge(channel, 3, motionmasknew);
	cv::imshow("final", motionmasknew);*/

	//MICHOLO TLD
	/*cv::Mat gframe;
	cv::cvtColor(frame, gframe, CV_BGR2GRAY);
	opticalTracker.setCurrImg(gframe);
	if(isTLDready)
	{
		cv::Mat gaussImg;
		cv::Mat intImg, intSqImg;

		cv::GaussianBlur(gframe, gaussImg, cv::Size(3, 3), 0);
		cv::integral(gframe, intImg, intSqImg);

		cascadeDetector.motionFilter->setImages(motionMask);
		cascadeDetector.varianceFilter->setImages(intImg, intSqImg);
		cascadeDetector.binaryClassifier->setImages(intImg);
		cascadeDetector.ensembleClassifier->setImages(gaussImg);
		cascadeDetector.expertClassifier->setImages(gframe, intImg);

		cascadeDetector.detect(fields);
		opticalTracker.track(fields);

		for (auto& f : fields)
		{
			f.fuseHypothesis(cascadeDetector.expertClassifier);
			f.learn(gframe, intImg, intSqImg, cascadeDetector);
		}
	}*/
}
void Worker::initDetectionWindows(std::deque<std::shared_ptr<KFTrack3d>>& tracks3d)
{
	for(const auto& t3 : tracks3d)
	{
		if(t3->isVisible() && t3->getAverageProbability() <= detectionWindowProbThr && t3->getCurrentDet()->isDetected())
		{
			cv::Point2d pt2;
			calibration.transformToPixel(t3->getCurrentDet()->pos3d(), pt2);
			Field* f = getFields().find(t3->fieldID());
			CV_Assert(f);
			f->detectionWindow = cv::Rect2d(pt2 - cv::Point2d(detectionWindowSize, detectionWindowSize), pt2 + cv::Point2d(detectionWindowSize, detectionWindowSize));
			f->detectionWindow &= cv::Rect(cv::Point(0, 0), frame.size());
		}
	}
}
std::pair<cv::Mat, cv::Mat> Worker::show(bool show, bool stop, const std::deque<std::shared_ptr<KFTrack3d>>& tracks3d, int fieldIDonly)
{
	frame.copyTo(dispFrame);
	cv::cvtColor(motionMask, dispMotion, CV_GRAY2BGR);
	
	cv::putText(dispFrame, camID, cv::Point(10, 30), CV_FONT_HERSHEY_PLAIN, 3, cv::Scalar(255,255,255), 3);
	cv::putText(dispMotion, camID, cv::Point(10, 30), CV_FONT_HERSHEY_PLAIN, 3, cv::Scalar(255, 255, 255), 3);

	std::unordered_map<int, cv::Mat> colorHist;
	std::vector<std::vector<cv::Point>> vv;
	std::vector<std::vector<cv::Point>> vvx;
	for (int j = 0; j < fields.size(); ++j)
	{
		if(fields[j].fid == fieldIDonly || fieldIDonly == -1)
		{
			fields[j].show(systemTime.getFrameNum(), dispFrame, dispMotion, camID, calibration, color, show);
			//fields[j].showSizeMap(camID, dispFrame, 0);
			//fields[j].showSizeMap(camID, dispFrame, 1);
			//fields[j].showSizeMap(camID, dispFrame, 2);
#ifdef SPS_DEBUB_DETS
			if (stop && fields[j].ballColorMap && show)
			{
				cv::Mat img(50, 50, CV_8UC3, cv::Scalar(200, 200, 200));
				cv::hconcat(img, fields[j].ballColorMap->getImage(800, 50), colorHist[fields[j].fid]);
		}
#endif	
		}
	}

	vv.push_back(mask);
	cv::drawContours(dispFrame, vv, -1, cv::Scalar(128, 128, 128), 1);
	cv::drawContours(dispMotion, vv, -1, cv::Scalar(128, 128, 128), 1);

	//cv::Point2d camPos;
	//calibration.transformToPixel(-calibration.getTranslationVector(), camPos);
	//std::cout << camPos << std::endl;
	//cv::circle(dispFrame, camPos, 20, cv::Scalar(255, 0, 255), -1);
	//cv::circle(dispMotion, camPos, 20, cv::Scalar(255, 0, 255), -1);
	
	for(auto& r : detections)
	{
		if(r->fieldID() == fieldIDonly || fieldIDonly == -1)
		{
			r->show(dispFrame, color);
			r->show(dispMotion, color);

#ifdef SPS_DEBUB_DETS
			if(show)
			{
				std::cout << camID << " [" << r->fieldID() << "] - " << r->printInfo2d() << std::endl;

				if (stop)
				{
					std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(r);
					if (ball->field()->ballColorMap && ball->colorHist())
					{
						cv::Mat img = ball->getHistImage(fields[r->fieldID()]);
						if (!img.empty())
						{
							cv::vconcat(colorHist[r->fieldID()], img, colorHist[r->fieldID()]);
						}
					}
				}
			}
#endif	
		}
	}
#ifdef SPS_DEBUB_DETS
	if(detections.size() > 0 && show)
	{
		std::cout << std::endl;
	}
#endif
	for (auto& t : tr2dlive())
	{
#ifdef SPS_DEBUB_DETS
		if (show)
		{
			if (stop && !t->getCurrentDet()->isDetected())
			{
				std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(t->getCurrentDet());
				if (ball->field()->ballColorMap && ball->colorHist())
				{
					cv::Mat img = ball->getHistImage(fields[t->fieldID()]);
					if (!img.empty())
					{
						cv::vconcat(colorHist[t->fieldID()], img, colorHist[t->fieldID()]);
					}
				}
			}
		}
#endif
		if (t->fieldID() == fieldIDonly || fieldIDonly == -1)
		{
			t->show(dispFrame, color);
			t->show(dispMotion, color);

#ifdef SPS_PRINT_INFO_2D
			if(show)
			{
				t->printInfo(getMask(), getFields());
			}
#endif
		}
	}
#ifdef SPS_PRINT_INFO_2D
	//for (auto& t : tr2ddead())
	//{
	//if (t->fieldID() == fieldIDonly || fieldIDonly == -1)
	//{
	//	//t->show(dispFrame, color);
	//	//t->show(dispMotion, color);

	//	t->printInfo(getCamID(), getMask(), getFields());
	//}
	//}
	if(show && (tr2dlive().size() > 0 || tr2ddead().size() > 0))
	{
		std::cout << std::endl;
	}
#endif
#ifdef SPS_DEBUB_DETS
	if (stop && show)
	{
		cv::Mat imgAllHist;
		for (auto& ch : colorHist)
		{
			if (imgAllHist.empty())
			{
				imgAllHist = ch.second;
			}
			else
			{
				cv::vconcat(imgAllHist, ch.second, imgAllHist);
			}
			cv::imshow("hist in " + camID, imgAllHist);
		}
	}
#endif
	/*if (isSearchROI())
	{
		for (auto& t : tr2dlive())
		{
			if(t->isVisible())
			{
				cv::rectangle(dispFrame, std::static_pointer_cast<KFBallTrack2d>(t)->roi().tl(), std::static_pointer_cast<KFBallTrack2d>(t)->roi().br(), cv::Scalar(128, 255, 0), 1, 8);
				cv::rectangle(dispMotion, std::static_pointer_cast<KFBallTrack2d>(t)->roi().tl(), std::static_pointer_cast<KFBallTrack2d>(t)->roi().br(), cv::Scalar(128, 255, 0), 1, 8);
			}
		}
	}*/
	for (auto& t : tracks3d)
	{
		if (t->fieldID() == fieldIDonly || fieldIDonly == -1)
		{
			t->show(dispFrame, &calibration, cv::Scalar(0, 255, 0));
			t->show(dispMotion, &calibration, cv::Scalar(0, 255, 0));
		}
	}

	for (int j = 0; j < fields.size(); ++j)
	{
		if(fields[j].fid == fieldIDonly || fieldIDonly == -1)
		{
			if(fieldIDonly == -1)
			{
				cv::rectangle(dispFrame, fields[j].detectionWindow.tl(), fields[j].detectionWindow.br(), cv::Scalar(0, 80, 255), 1);
				cv::rectangle(dispMotion, fields[j].detectionWindow.tl(), fields[j].detectionWindow.br(), cv::Scalar(0, 80, 255), 1);
			}
			if (isGoodForLearn(fields[j]))
			{
				cv::RotatedRect rr(fields[j].ballTrack2d->getCurrentDet()->pos2d(), cv::Size2f(fields[j].ballTrack2d->getCurrentDet()->box().size()) * 2.0f, 45);
				cv::Point2f vertices[4];
				rr.points(vertices);
				for (int i = 0; i < 4; ++i)
				{
					cv::line(dispFrame, vertices[i], vertices[(i + 1) % 4], cv::Scalar(0, 255, 255), 1);
					cv::line(dispMotion, vertices[i], vertices[(i + 1) % 4], cv::Scalar(0, 255, 255), 1);
				}
			}
		}
	}

	/*std::vector<cv::Mat> channels;
	cv::split(frame, channels);
	fieldMask.copyTo(channels[2]);
	cv::merge(channels, frame);*/

	

	if (show)
	{
		cv::imshow(camID + " - frame", dispFrame);
		cv::imshow(camID + " - mask", dispMotion);

		//calibration.showSizeMap(camID, frame, fieldMask);
	}

	return std::make_pair(dispFrame, dispMotion);
}
void Worker::show3d(cv::Mat frame, double windowScale) const
{
	for (auto& t : tr2dlive())
	{
		t->show(frame, windowScale, color);
	}
	

	//std::cout << calibration.getCameraPosition() << " " << cv::Point(calibration.getCameraPosition().x / windowScale, calibration.getCameraPosition().y / windowScale) << std::endl;
	//cv::circle(frame, cv::Point(calibration.getCameraPosition().x / windowScale, calibration.getCameraPosition().y / windowScale), 3, cv::Scalar(255, 255, 255), 2);

	/*for (auto& w : detectionWindows3d)
	{
		cv::rectangle(frame, cv::Point(cvRound(w.second.tl().x / windowScale), frame.rows - cvRound(w.second.tl().y / windowScale)), cv::Point(cvRound(w.second.br().x / windowScale), frame.rows - cvRound(w.second.br().y / windowScale)), cv::Scalar(0, 80, 255), 1);
	}*/
}
Field* Worker::inField(const std::shared_ptr<MovingObject> obj)
{
	return fields.inField(obj->pos3d());
}
void Worker::updateEvidenceCount(std::size_t detected)
{
	if(detected > evidenceCount)
	{
		evidenceCount = detected;
	}
}
void Worker::setROIT3d(double size)
{
	roiT3D = cv::Point2d(size, size);
}
cv::Rect2d Worker::getROIT3d(const cv::Point3d pt) const
{
	cv::Point2d pt2(pt.x, pt.y);
	return cv::Rect2d(pt2 - roiT3D, pt2 + roiT3D);
}