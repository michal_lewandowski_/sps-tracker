#include "stdafx.h"
#include "BallObject.h"
#include "Detector.h"
#include "ColorBallHistAdaptive.h"
#include "ColorBallMap.h"
#include "Field.h"

extern double ballRadius;

BallObject::BallObject(const std::size_t id, std::string cid, cv::Rect b, cv::RotatedRect e, double br, cv::Mat im, cv::Mat mm)
	: MovingObject(id, cid, b), ebb(e), ballRatio(br), ballColorDist(1.0), type(BallType::UNKNOWN)
{
	im.copyTo(img, mm);
	mm.copyTo(mimg);
}
BallObject::BallObject(const std::size_t id, std::string cid, cv::Point3d p, int views, int numDet, float error, const std::unordered_map<std::string, int>& tracks, const std::vector<PreviousBallInCam>& loc)
	: MovingObject(id, cid, p, views, numDet, error, tracks), prevLoc(loc), ballRatio(1.0), ballColorDist(1.0), type(BallType::UNKNOWN)
{
	
}
BallObject::~BallObject()
{
}

BallObject::BallObject(const BallObject& other)
	: MovingObject(other)
	, ebb(other.ebb)
	, prevLoc(other.prevLoc)
	, ballRatio(other.ballRatio)
	, ballColorDist(other.ballColorDist)
	, hist(other.hist)
	, img(other.img.clone())
{
	
}

BallObject& BallObject::operator=(const BallObject& other)
{
	if (this != &other)
	{
		this->MovingObject::operator=(other);
		this->ebb = other.ebb;
		this->prevLoc = other.prevLoc;
		this->ballRatio = other.ballRatio;
		this->ballColorDist = other.ballColorDist;
		this->hist = other.hist;
		this->img = other.img.clone();
	}
	return *this;
}

bool BallObject::inMask(const std::vector<cv::Point>& contour) const
{
	return contour.size()>2 && cv::pointPolygonTest(contour, pos2d(), false) >= 0;
}
bool BallObject::inField(const Field& field) const
{
	return field.fieldExtended.contains(cv::Point(static_cast<int>(pos3d().x), static_cast<int>(pos3d().y)));
}
std::string BallObject::printInfo2d(void) const
{
	std::stringstream ss;

	ss << MovingObject::printInfo();
	ss << " " << isValid();
	ss << " " << (isDetected() ? "d" : "p");
	ss << " " << pos2d();
	ss << " " << pos3d();
	ss << " " << std::setprecision(2) << ballRatio;
	ss << " <";
	for (const auto& p : probs)
	{
		ss << p.first << " " << std::setprecision(2) << p.second.value << " ";
	}
	ss << "-> " << std::setprecision(3) << getNegLogStaticProbability() << " (" << std::setprecision(3) << Detector::ballProbailityThr << ") p-> " << getProbability();
	ss << ">";
	return ss.str();
}
std::string BallObject::printInfo3d(double ballProbailityThr) const
{
	std::stringstream ss;

	ss << MovingObject::printInfo();
	ss << " " << (isDetected() ? "d" : "p");
	//ss << " " << pos2d();
	ss << " " << pos3d();
	ss << " in cams " << views();
	ss << " (" << numDetected2d() << ")";
	ss << " with err " << std::setprecision(2) << recError();
	ss << " <";
	for (const auto& p : probs)
	{
		ss << p.first << " " << std::setprecision(2) << p.second.value << " ";
	}
	ss << "-> " << std::setprecision(3) << getNegLogProbability() << " (" << std::setprecision(3) << ballProbailityThr << ") p-> " << getProbability();
	ss << ">";
	return ss.str();
}
void BallObject::computeSizeProbability(double dWidth, double dHeight, double maxSize)
{
	double probW = 1.0 - abs(dWidth - 2.0 * ballRadius) / (maxSize - 2.0 * ballRadius);
	double probH = 1.0 - abs(dHeight - 2.0 * ballRadius) / (maxSize - 2.0 * ballRadius);

	assignPropabilityS(probW * probH, "pS");

	/*assignPropabilityS(probW, "pW");
	assignPropabilityS(probH, "pH");*/
}
void BallObject::computeProximityProbability(cv::Point2d pt, double distVar)
{
	double dd = cv::norm(cv::Point2d(pos2d()) - pt);

	dd = cv::exp(-dd * dd / distVar);

	assignPropabilityS(dd, "pT3");
}
cv::Mat BallObject::getHistImage(const Field& field) const
{
	if (field.ballColorMap && field.ballColorMap->isReady(nullptr))
	{
		cv::Mat outimg = ColorBallHistAdaptive::getImage(field.ballColorMap->adaptiveBin(), *colorHist(), 800, 50);
		cv::Mat imgBall;
		cv::resize(image(), imgBall, cv::Size(50, 50), 0, 0, cv::INTER_AREA);
		if (isValid() && isDetected())
		{
			cv::rectangle(imgBall, cv::Point(0, 0), cv::Point(50, 50), cv::Scalar(0, 255, 0), 2);
		}
		if (isValid() && !isDetected())
		{
			cv::rectangle(imgBall, cv::Point(0, 0), cv::Point(50, 50), cv::Scalar(255, 0, 0), 2);
		}
		std::stringstream ss;
		ss << getDID() << "[" << fieldID() << "]: ";
		ss << (isDetected() ? "dt " : "pr ");
		ss << std::setprecision(2) << colorDist() << " ";
		ss << std::setprecision(2) << ballRatio << " ";
		ss << "<";

		Probability probS;
		if (getProbability("pS", probS))
		{
			ss << "pS " << std::setprecision(2) << probS.value << " ";
		}
		/*Probability probW;
		if (getProbability("pW", probW))
		{
			ss << "pW " << std::setprecision(2) << probW.value;
		}
		Probability probH;
		if (getProbability("pH", probH))
		{
			ss << "pH " << std::setprecision(2) << probH.value;
		}*/
		Probability probC;
		if (getProbability("pc", probC))
		{
			ss << "pc " << std::setprecision(2) << probC.value << " ";
		}
		Probability probT;
		if (getProbability("pT3", probT))
		{
			ss << "pT3 " << std::setprecision(2) << probT.value << " ";
		}

		ss << "-> " << std::setprecision(3) << getNegLogStaticProbability() << " (" << std::setprecision(3) << Detector::ballProbailityThr << ") p-> " << getProbability();
		ss << ">";
		cv::putText(outimg, ss.str(), cv::Point(1, 30), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(255, 255, 255), 1);
		cv::hconcat(imgBall, outimg, outimg);
		return outimg;
	}
	return cv::Mat();
}