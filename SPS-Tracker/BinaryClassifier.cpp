#include "BinaryClassifier.h"
#include "NormalisedPatch.h"
#include "Utils.h"
#include "Field.h"

unsigned int BinaryClassifier::diffAtMost = 1;

BinaryClassifier::BinaryClassifier()
{
}

BinaryClassifier::~BinaryClassifier()
{

}
void BinaryClassifier::init()
{
	release();
}

void BinaryClassifier::release()
{
}

bool BinaryClassifier::compareBinary(int a1, int a2, int b1, int b2)
{
	//CV_Assert((popcount32(a1 ^ a2) == diffAtMost) == (a1 == a2));
	//CV_Assert((popcount32(b1 ^ b2) == diffAtMost) == (b1 == b2));
	//return a1 == a2 && b1 == b2;
	return popcount32(a1 ^ a2) <= diffAtMost && popcount32(b1 ^ b2) <= diffAtMost;
}

bool BinaryClassifier::filter(int idx, FieldList& fields)
{
	bool nextStage = false;
	for (auto& f : fields)
	{
		if (idx < f.numWindows && !f.detectionResult.lastStageInvalid[idx])
		{
			int* box = &f.windows[TLD_WINDOW_SIZE * idx];
			int binaryP = 0;;
			int binaryN = 0;
			tldCalcBinary(img1, box, binaryP, binaryN);
			f.detectionResult.binaryP[idx] = binaryP;
			f.detectionResult.binaryN[idx] = binaryN;
			//if (binaryP != f.binaryP || binaryN != f.binary)
			if (!compareBinary(binaryP, f.binaryP, binaryN, f.binaryN))
			{
				f.detectionResult.posteriors[idx] = 0;
				f.detectionResult.lastStageInvalid[idx] = 1;
			}
			else
			{
				nextStage = true;
			}
		}
	}
	return nextStage;
}