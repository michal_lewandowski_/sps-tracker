#pragma once
#include <chrono>
#include <array>

enum class Timer : int
{
	ALL = 0,
	Count
};

class TimeWatch
{
public:
	inline TimeWatch();

	inline void reset();
	inline TimeWatch& measure();

	inline double getSeconds() const;
	inline int64_t getMilliseconds() const;
	inline void advance() { frameCount++; }
	inline int64_t getFrameNum() const;

private:
	std::chrono::high_resolution_clock::time_point start_;
	std::chrono::high_resolution_clock::time_point end_;
	int64_t frameCount;
};

class FrameTimers
{
public:
	inline int64_t operator[](Timer type) const;

	inline void reset(Timer type);
	inline void reset();

	inline void start(Timer type);
	inline void stop(Timer type);

private:
	static constexpr int NR_TIMERS = static_cast<int>(Timer::Count);
	std::array<TimeWatch, NR_TIMERS> watches;
	std::array<int64_t, NR_TIMERS> times;
};

inline TimeWatch::TimeWatch()
{
	reset();
}

inline void TimeWatch::reset()
{
	start_ = std::chrono::high_resolution_clock::now();
	frameCount = 1;
}

inline TimeWatch& TimeWatch::measure()
{
	end_ = std::chrono::high_resolution_clock::now();
	return *this;
}

inline double TimeWatch::getSeconds() const
{
	return std::chrono::duration<double>(end_ - start_).count();
}

inline int64_t TimeWatch::getMilliseconds() const
{
	return static_cast<int64_t>(std::chrono::duration_cast<std::chrono::milliseconds>(end_ - start_).count());
}
inline int64_t TimeWatch::getFrameNum() const
{
	return frameCount;
}


inline int64_t FrameTimers::operator[](Timer type) const
{
	return times[static_cast<int>(type)];
}

inline void FrameTimers::reset(Timer type)
{
	watches[static_cast<int>(type)].reset();
}

inline void FrameTimers::reset()
{
	for (auto& w : watches)
	{
		w.reset();
	}
}

inline void FrameTimers::start(Timer type)
{
	watches[static_cast<int>(type)].reset();
}

inline void FrameTimers::stop(Timer type)
{
	int t = static_cast<int>(type);
	times[t] = watches[t].measure().getMilliseconds();
}

