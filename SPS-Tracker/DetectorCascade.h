#pragma once

#include <opencv.hpp>
#include "TimeWatch.hpp"

class MotionFilter;
class VarianceFilter;
class EnsembleClassifier;
class BinaryClassifier;
class Clustering;
class NPClassifier;
class DetectionResult;
class NormalizedPatch;
class FieldList;
class Field;
class DetectorCascade
{
public:
	DetectorCascade();
	virtual ~DetectorCascade();

	void init(bool reserve);
	void reinit();

	void release();
	void detect(FieldList& fields);
	void updateGlobalWindows(const Field& f);

	//Configurable members
	static int minScale;
	static int maxScale;
	static float shift;
	static int minSize;
	static int maxSize;

	//Needed for init
	static int imgWidth;
	static int imgHeight;
	static int imgWidthStep;
	int globalNumWindows;


	//State data
	bool initialised;

	cv::Ptr<MotionFilter> motionFilter;
	cv::Ptr<VarianceFilter> varianceFilter;
	cv::Ptr<EnsembleClassifier> ensembleClassifier;
	cv::Ptr<BinaryClassifier> binaryClassifier;
	cv::Ptr<NPClassifier> expertClassifier;
	cv::Ptr<Clustering> clustering;

	cv::Mat img;
	cv::Mat integralImg;
	cv::Mat integralSqImg;

	TimeWatch tw;
};
