// SPS-Tracker.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <opencv.hpp>
#include <random>
#include<boost/program_options.hpp>
#include<boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include "WorkManager.h"
#include "MaskGenerator.h"
#include "Visualizer.h"
#include "VideoSaver.h"

#ifdef SPS_LAB
	std::default_random_engine dre(0);
#else
	std::default_random_engine dre(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));
#endif


WorkManager manager;
MaskGenerator generator;
Visualizer vis;
VideoSaver vidsav;
double ballRadius = 110.0f;

void saveStats(const FieldList& fields, std::string outputDir, std::string outputName, std::vector<int> stats);
void saveData(std::vector<std::deque<BallSaveData>>& ballData, const std::vector<std::vector<std::vector<int>>>& observationsPerCamera, const FieldList& fields, int64_t totalFrames, std::string outputDir, std::string outputName);

int main(int ac, char* av[])
{
	/*cv::FileStorage file("D:\\sps\\V012.A002.l.run1.cam1-4\\config.xml", cv::FileStorage::WRITE);
	file << "cam1" << "{";
	file << "video" << "V012.A002.l.run1.cam1.avi";
	file << "config" << "V012.A002.l.run1.cam1.cfg";
	file << "}";
	file << "cam2" << "{";
	file << "video" << "V012.A002.l.run1.cam2.avi";
	file << "config" << "V012.A002.l.run1.cam2.cfg";
	file << "}";
	file << "cam3" << "{";
	file << "video" << "V012.A002.l.run1.cam3.avi";
	file << "config" << "V012.A002.l.run1.cam3.cfg";
	file << "}";
	file << "cam4" << "{";
	file << "video" << "V012.A002.l.run1.cam4.avi";
	file << "config" << "V012.A002.l.run1.cam4.cfg";
	file << "}";
	file << "masks" << "{";
	file << "mask" << "{";
	file << "pt" << cv::Point(1000, 1000);
	file << "pt" << cv::Point(1000, 14000);
	file << "}";
	file << "}";
	file.release();*/

	boost::program_options::options_description appConfiguration("Input arguments");
	appConfiguration.add_options()
		("mode,m", boost::program_options::value<std::string>()->default_value("proc"), "Run mode [proc, mask, vis, vis_all, vid]")
		("dir,d", boost::program_options::value<std::string>(), "Folder with config file")
		("show,s", boost::program_options::value<bool>()->default_value(true), "Show output [1, 0]")
		("count,c", boost::program_options::value<bool>()->default_value(false), "Generate stats [1, 0]");

	boost::program_options::options_description cmdline_options;
	cmdline_options.add(appConfiguration);

	boost::program_options::variables_map vm;
	boost::program_options::command_line_parser parser(ac, av);
	store(parser.options(cmdline_options).run(), vm);

	boost::filesystem::path inputDir(vm["dir"].as<std::string>());
	if (!boost::filesystem::exists(inputDir))
	{
		std::cout << "Wrong input directory " << inputDir.string() << std::endl;
		return 1;
	}
	boost::filesystem::path configFile(inputDir.string() + std::string("\\config.xml"));
	if (!boost::filesystem::exists(configFile))
	{
		std::cout << "No config file " << configFile.string() << std::endl;
		return 1;
	}
	std::string vidFile;
	std::string conFile;
	std::string outFile;
	std::string outputName;
	std::string mode = vm["mode"].as<std::string>();
	try
	{
		cv::FileStorage fs(configFile.string(), cv::FileStorage::READ);
		if (fs.isOpened())
		{
			int numFields, numPoints, margin;
			cv::FileNode n = fs["fields"];
			n["margin"] >> margin;
			n["num"] >> numFields;
			
			std::vector<std::pair<cv::Rect,std::string>> fields(numFields);
			for (int i = 1; i <= numFields; ++i)
			{
				cv::Point tl;
				cv::Point br;
				std::string gtPath;
				cv::FileNode nn = n["field" + std::to_string(i)];
				nn["tl"] >> tl;
				nn["br"] >> br;
				nn["gt"] >> gtPath;
				fields[i - 1] = std::make_pair(cv::Rect(tl, br), inputDir.string() + "\\" + gtPath);
			}
			std::vector<cv::Point3d> mask;
			n = fs["mask"];
			n["num"] >> numPoints;
			mask.resize(numPoints);
			for (int j = 1; j <= numPoints; ++j)
			{
				n["pt" + std::to_string(j)] >> mask[j - 1];
			}

			int numCameras;
			n = fs["cameras"];
			n["num"] >> numCameras;

			if (mode == "mask")
			{
				generator.setMasks(mask);
			}
			if (mode == "proc")
			{
				manager.setFields(fields, margin, numCameras);
			}
			if (mode == "vis")
			{
				vis.setFields(fields, margin, numCameras);
			}
			if (mode == "vid")
			{
				vidsav.setFields(fields, margin, numCameras);
			}
			for (int i = 1; i <= numCameras; ++i)
			{
				std::string workerID = "cam" + std::to_string(i);

				cv::FileNode nn = n[workerID];
				nn["video"] >> vidFile;
				nn["config"] >> conFile;
				nn["output"] >> outFile;

				std::vector<std::string> strs;
				boost::algorithm::split(strs, vidFile, boost::is_any_of("."));
				CV_Assert(strs.size() == 6);
				outputName = strs[0] + "." + strs[1] + "." + strs[2] + "." + strs[3];

				vidFile = inputDir.string() + std::string("\\") + vidFile;
				conFile = inputDir.string() + std::string("\\") + conFile;

				if (!boost::filesystem::exists(vidFile))
				{
					std::cout << "Video file doesnt exist: " << vidFile << std::endl;
					return 1;
				}
				if (!boost::filesystem::exists(conFile))
				{
					std::cout << "Config file doesnt exist: " << conFile << std::endl;
					return 1;
				}
				if (mode == "mask")
				{
					generator.addJob(workerID, vidFile, conFile);
				}
				if (mode == "proc")
				{
					manager.addJob(workerID, vidFile, conFile);
				}
				if (mode == "vis")
				{
					vis.addJob(workerID, vidFile, conFile, inputDir.string());
				}
				if (mode == "vid")
				{
					vidsav.addJob(workerID, vidFile, conFile, inputDir.string(), outFile);
				}
				//std::cout << "Add job '" << workerID << "' for '" << vidFile << "' with config '" << conFile << "," << std::endl;
			}
			fs.release();
		}
		else
		{
			std::cout << "Cannot open config file " << configFile << std::endl;
			return 1;
		}
	}
	catch(cv::Exception& ex)
	{
		std::cout << "Config processing exception: " << ex.msg << std::endl;
		return 1;
	}
	if (mode == "proc")
	{
		bool show = vm["show"].as<bool>();
		std::vector<std::deque<BallSaveData>> ballData;
		std::vector<std::vector<std::vector<int>>> observationsPerCamera;
		observationsPerCamera.reserve(10000);
		if(manager.init(show))
		{
			manager.process(ballData, observationsPerCamera);
			saveData(ballData, observationsPerCamera, manager.getFields(), manager.time(), inputDir.string(), outputName);
		}
	}
	else if (mode == "mask")
	{
		generator.process();
	}
	else if (mode == "vis")
	{
		bool count = vm["count"].as<bool>();
		if(vis.init(false, outputName, count))
		{
			vis.process();
			saveStats(manager.getFields(), inputDir.string(), outputName, vis.getCountData());
		}
	}
	else if (mode == "vid")
	{
		vidsav.init();
		vidsav.process();
	}
	return 0;
}
void saveStats(const FieldList& fields, std::string outputDir, std::string outputName, std::vector<int> stats)
{
	for (std::size_t f = 0; f < fields.size(); ++f)
	{
		std::stringstream ss;
		ss << outputDir << "\\" << outputName << ".field";
		ss << fields[f].fid;
		ss << ".stats.txt";
		std::ofstream outfile;
		outfile.open(ss.str(), std::ios_base::trunc | std::ios_base::out);
		if (outfile.is_open())
		{
			int totalPasses = 0;
			int goodPasses = 0;
			for (int i = 0; i < stats.size(); ++i)
			{
				totalPasses += stats[i];
				if (i<4)
				{
					goodPasses += stats[i];
				}
			}
			outfile << "Total zone crossings: " << totalPasses << std::endl;
			outfile << "Detected zone crossings: " << goodPasses << " [" << (goodPasses*100.0) / static_cast<double>(totalPasses) << "]" << std::endl;
			outfile << "Missed zone crossings: " << (totalPasses - goodPasses) << " [" << ((totalPasses - goodPasses)*100.0) / static_cast<double>(totalPasses) << "]" << std::endl;
			outfile.close();
		}
	}
}
void saveData(std::vector<std::deque<BallSaveData>>& ballData, const std::vector<std::vector<std::vector<int>>>& observationsPerCamera, const FieldList& fields, int64_t totalFrames, std::string outputDir, std::string outputName)
{
	for (std::size_t f = 0; f < fields.size(); ++f)
	{
		std::vector<std::vector<BallSaveData>> vv(totalFrames);
		std::sort(ballData[fields[f].fid].begin(), ballData[fields[f].fid].end(), [](BallSaveData& l, BallSaveData& r)
		{
			return l.frameNum < r.frameNum;
		});
		
		for (std::size_t j = 0; j < ballData[fields[f].fid].size(); ++j)
		{
			std::vector<BallSaveData> v;
			v.push_back(ballData[fields[f].fid][j]);
			for (std::size_t i = j+1; i < ballData[fields[f].fid].size(); ++i)
			{
				if(ballData[fields[f].fid][i].frameNum != v[0].frameNum)
				{
					break;
				}
				v.push_back(ballData[fields[f].fid][i]);
				j++;
			}
			std::sort(v.begin(), v.end(), [](BallSaveData& l, BallSaveData& r)
			{
				return l.trackNegLogProb < r.trackNegLogProb;
			});
			vv[v[0].frameNum] = v;
		}

		std::stringstream ss;
		ss << outputDir << "\\" << outputName << ".field";
		ss << fields[f].fid;
		ss << ".csv";

		std::ofstream outfile;
		outfile.open(ss.str(), std::ios_base::trunc | std::ios_base::out);
		if(outfile.is_open())
		{
			outfile << "frame,x1,y1,z1,d1,o1,t1,i1,x2,y2,z2,d2,o2,t2,i2,x3,y3,z3,d3,o3,t3,i3,x4,y4,z4,d4,o4,t4,i4,x5,y5,z5,d5,o5,t5,i5,error code,num 3D det,cam1 2d det,cam2 2d det,cam3 2d det,cam4 2d det" << std::endl;
			outfile << "1,-1,-1,-1,0,0,-1,-1,-1,0,0,-1,-1,-1,0,0,-1,-1,-1,0,0,-1,-1,-1,0,0,2,0,0,0,0,0" << std::endl;
			for (std::size_t j = 1; j < vv.size(); ++j)
			{
				if (vv[j].size() > 0 && vv[j][0].frameNum > 0)
				{
					outfile << vv[j][0].frameNum+1 << ",";
					for (std::size_t i = 0; i < std::min(static_cast<int>(vv[j].size()), 5); ++i)
					{
						CV_Assert(vv[j][i].frameNum == j);
						outfile << vv[j][i].pos.x << "," << vv[j][i].pos.y << "," << vv[j][i].pos.z << "," << vv[j][i].detected << "," << vv[j][i].observations << "," << static_cast<int>(vv[j][i].type) << "," << vv[j][i].trackID << ",";
					}
					if (vv[j].size() < 5)
					{
						for (std::size_t i = vv[j].size(); i < 5; ++i)
						{
							outfile << "-1,-1,-1,0,0,-1,0,";
						}
					}
					if (vv[j].size() == 1)
						outfile << "0,";
					else if (vv[j].size() >= 5)
						outfile << "3,";
					else
						outfile << "1,";
					outfile << vv[j].size() << ",";
					outfile << observationsPerCamera[j - 1][f][0] << "," << observationsPerCamera[j - 1][f][1] << "," << observationsPerCamera[j - 1][f][2] << "," << observationsPerCamera[j - 1][f][3] << std::endl;
				}
				else
				{
					outfile << j+1 << ",-1,-1,-1,0,0,-1,0,-1,-1,-1,0,0,-1,0,-1,-1,-1,0,0,-1,0,-1,-1,-1,0,0,-1,0,-1,-1,-1,0,0,-1,0,2,0,0,0,0,0" << std::endl;
				}
			}
			outfile.close();
		}
		else
		{
			std::cout << "CANNOT OPEN OUTPUT FILE!" << std::endl;
		}
	}
}
//void saveData(std::vector<std::deque<BallSaveData>>& ballData, const std::vector<std::vector<std::vector<int>>>& observationsPerCamera, const FieldList& fields, int64_t totalFrames, std::string outputDir)
//{
//	for (std::size_t f = 0; f < fields.size(); ++f)
//	{
//		std::vector<std::vector<BallSaveData>> vv(totalFrames);
//		std::sort(ballData[fields[f].fid-1].begin(), ballData[fields[f].fid-1].end(), [](BallSaveData& l, BallSaveData& r)
//		{
//			return l.frameNum < r.frameNum;
//		});
//
//		for (std::size_t j = 0; j < ballData[fields[f].fid-1].size(); ++j)
//		{
//			std::vector<BallSaveData> v;
//			v.push_back(ballData[fields[f].fid-1][j]);
//			for (std::size_t i = j + 1; i < ballData[fields[f].fid-1].size(); ++i)
//			{
//				if (ballData[fields[f].fid-1][i].frameNum != v[0].frameNum)
//				{
//					break;
//				}
//				v.push_back(ballData[fields[f].fid-1][i]);
//				j++;
//			}
//			std::sort(v.begin(), v.end(), [](BallSaveData& l, BallSaveData& r)
//			{
//				return l.observations > r.observations;
//			});
//			CV_Assert(v.size() > 0);
//			for(std::size_t i = 0; i < v.size() - 1; ++i)
//			{
//				CV_Assert(v[i].frameNum == v[i + 1].frameNum);
//			}
//			vv[v[0].frameNum] = v;
//		}
//
//		std::stringstream ss;
//		ss << outputDir << "\\field_" << (f+1);
//		ss << "_" << fields[f].field.tl().x;
//		ss << "_" << fields[f].field.tl().y;
//		ss << "_" << fields[f].field.br().x;
//		ss << "_" << fields[f].field.br().y;
//		ss << ".csv";
//
//		std::ofstream outfile;
//		outfile.open(ss.str(), std::ios_base::trunc | std::ios_base::out);
//		outfile << "frame,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,x5,y5,z5,error code,cameras used,3D det,cam1 det,cam2 det,cam3 det,cam4 det" << std::endl;
//		outfile << "1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,2,0,0,0,0,0,0" << std::endl;
//		for (std::size_t j = 1; j < vv.size(); ++j)
//		{
//			std::vector<std::vector<BallSaveData>> vvv(5);
//			for (auto v : vv[j])
//			{
//				vvv[v.observations].push_back(v);
//			}
//			int k = 4;
//			for (; k >= 0; --k)
//			{
//				if (vvv[k].size() > 5)
//				{
//					outfile << j+1 << ",-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,2," << k << "," << vvv[k].size();
//					break;
//				}
//				else if (vvv[k].size() > 0 && vvv[k][0].trackLength > 0)
//				{
//					outfile << vvv[k][0].frameNum << ",";
//					for(std::size_t i=0; i < vvv[k].size(); ++i)
//					{
//						CV_Assert(vvv[k][i].observations == k);
//						CV_Assert(vvv[k][i].frameNum == j);
//						outfile << vvv[k][i].pos.x << "," << vvv[k][i].pos.y << "," << vvv[k][i].pos.z << ",";
//					}
//					for (std::size_t i = vvv[k].size(); i < 5; ++i)
//					{
//						outfile << "-1,-1,-1,";
//					}
//					if (vvv[k].size() == 1)
//						outfile << "0,";
//					else
//						outfile << "1,";
//					
//					outfile << k << "," << vvv[k].size();
//					break;
//				}
//				else if (k == 0)
//				{
//					outfile << j << ",-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,2," << k << ",0";
//					break;
//				}
//			}
//			outfile << "," << observationsPerCamera[j - 1][f][0] << "," << observationsPerCamera[j - 1][f][1] << "," << observationsPerCamera[j - 1][f][2] << "," << observationsPerCamera[j - 1][f][3] << std::endl;
//			CV_Assert(k >= 0);
//		}
//		outfile.close();
//	}
//}