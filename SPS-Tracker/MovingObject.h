#pragma once
#include <opencv.hpp>
#include <memory>
#include <unordered_map>
#include "Field.h"

class MovingObject;
class Field;

struct PreviousBallLoc
{
	std::size_t frameNum;
	std::shared_ptr<MovingObject> det;
};
struct PreviousBallInCam
{
	cv::Point3d camPos;
	std::string camID;
	std::vector<PreviousBallLoc> locs;
};
struct Det2Dinfo
{
	int trackID;
	int camIndex;
	std::string camID;
	std::shared_ptr<MovingObject> bestDet;
	cv::Point3d camPos;
	std::vector<PreviousBallLoc> prevLocation;
};
struct Det3Dinfo
{
	Det3Dinfo(std::vector<Det2Dinfo>& d2, double e, int nd, int nc, const std::vector<PreviousBallInCam>& o, cv::Point3d p3)
		: det2d(&d2), error(e), numDetected(nd), numCameras(nc), observations(o), pos3d(p3)
	{
		
	}
	std::vector<Det2Dinfo>* det2d;
	double error;
	int numDetected;
	int numCameras;
	std::vector<PreviousBallInCam> observations; 
	cv::Point3d pos3d;
};
struct Probability
{
	Probability()
		: value(1.0), log(0.0), name(""), count(0)
	{
		
	}
	Probability(double v, double l, std::string n, int c)
		: value(v), log(l), name(n), count(c)
	{
		
	}
	double value;
	double log;
	std::string name;
	int count;
};
class MovingObject
{
public:
	MovingObject(const std::size_t id, std::string cid, cv::Rect b);
	MovingObject(const std::size_t id, std::string cid, cv::Point3d p, int views, int numDet, float error, const std::unordered_map<std::string, int>& tracks);
	virtual ~MovingObject();
	MovingObject(const MovingObject& other);
	MovingObject& operator=(const MovingObject& other);
	virtual bool operator==(const MovingObject& rhs) const;

	virtual std::string printInfo(void) const;
	virtual std::string printInfo2d(void) const = 0;
	virtual std::string printInfo3d(double ballProbailityThr) const = 0;
	virtual void show(cv::Mat frame, cv::Scalar color) const = 0;
	virtual void show3d(cv::Mat frame, cv::Scalar color, double windowScale) const = 0;
	virtual bool isDetected() const = 0;
	virtual bool isBacktracked() const = 0;
	virtual bool inMask(const std::vector<cv::Point>& contour) const = 0;
	virtual bool inField(const Field& field) const = 0;
	virtual std::shared_ptr<MovingObject> clone() const = 0;

	bool getProbability(std::string name, Probability& prob) const;
	void assignPropabilityS(double value, std::string name);
	void assignPropabilityD(double value, std::string name);
	void removePropabilityS(std::string name);
	void removePropabilityD(std::string name);
	double getStaticProbability(void) const { return staticProb.value; };
	double getDynamicProbability(void) const { return dynamicProb.value; };
	double getProbability(void) const { return getStaticProbability() * getDynamicProbability(); };
	double getNegLogStaticProbability(void) const { return (staticProb.count > 0 ? -staticProb.log + cv::log(staticProb.count) : 0.0); }
	double getNegLogDynamicProbability(void) const { return (dynamicProb.count > 0 ? -dynamicProb.log + cv::log(dynamicProb.count) : 0.0); }
	double getNegLogProbability(void) const { return getNegLogStaticProbability() + getNegLogDynamicProbability(); };

	double dist2d(const std::shared_ptr<MovingObject> obj) const;
	double dist3d(const std::shared_ptr<MovingObject> obj) const;

	inline size_t getDID(void) const { return detID; };
	inline cv::Rect& box()
	{
		return bb_;
	}
	inline cv::Point2i& pos2d()
	{
		return pos2d_;
	}
	inline cv::Point3d& pos3d()
	{
		return pos3d_;
	}
	inline cv::Point3d& pos3dEst()
	{
		return pos3d_est;
	}
	inline Field*& field()
	{
		return field_;
	}
	inline std::string& camID()
	{
		return camID_;
	}

	inline const cv::Rect& box() const 
	{
		return bb_;
	}
	inline const cv::Point2i& pos2d() const
	{
		return pos2d_;
	}
	inline const cv::Point3d& pos3d() const
	{
		return pos3d_;
	}
	inline const cv::Point3d& pos3dEst() const
	{
		return pos3d_est;
	}
	inline const int& fieldID() const
	{
		return field_->fid;
	}
	inline const std::string& camID() const
	{
		return camID_;
	}
	inline const Field* field() const
	{
		return field_;
	}
	inline const int& views() const
	{
		return observedViews_;
	}
	inline const int& numDetected2d() const
	{
		return numDetected_;
	}
	inline const float& recError() const
	{
		return reconstructionError_;
	}

	inline int countProb() const
	{
		return static_cast<int>(probs.size());
	}

	inline bool& isValid()
	{
		return valid;
	}
	inline const bool& isValid() const
	{
		return valid;
	}

	inline int track2dID(std::string camID)
	{
		if (tr2dIDs.size() == 0)
			return -1;
		if (tr2dIDs.find(camID) == tr2dIDs.end())
			return -1;
		return tr2dIDs[camID];
	}
	inline const std::unordered_map<std::string, int>& track2dID() const
	{
		return tr2dIDs;
	}

protected:
	bool valid;
	std::size_t detID;
	cv::Rect bb_;
	cv::Point2i pos2d_;
	cv::Point3d pos3d_;
	cv::Point3d pos3d_est;
	Field* field_;
	std::string camID_;
	int observedViews_;
	int numDetected_;
	float reconstructionError_;
	std::unordered_map<std::string, int> tr2dIDs;

	Probability staticProb;
	Probability dynamicProb;
	std::unordered_map<std::string, Probability> probs;
#ifdef SPS_DEBUB_DETS
public:
	bool forceLearn;
#endif
};

