#include "stdafx.h"
#include "VideoSaver.h"
#include <regex>

VideoSaver::VideoSaver()
	: stop(false), step(false)
	, factorH(1)
	, factorS(1)
	, factorV(1)
	, factor(false)
{
	//code = cv::COLOR_BayerBG2GRAY;
	code = cv::COLOR_BayerBG2BGR;
}


VideoSaver::~VideoSaver()
{
}

void VideoSaver::addJob(std::string name, std::string videoFile, std::string configFile, std::string dir, std::string outputFile)
{
	names.push_back(name);
	videoFiles.push_back(videoFile);
	configFiles.push_back(configFile);
	outputFiles.push_back(outputFile);
	inputDir = dir;
}
void VideoSaver::init()
{
	int closingSize = 2;
	closingElement = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(closingSize * 2 + 1, closingSize * 2 + 1), cv::Point(closingSize, closingSize));
	for (int i = 0; i < names.size(); ++i)
	{
		caps.push_back(cv::VideoCapture(videoFiles[i]));
		calibs.push_back(CameraCalibration(configFiles[i]));

		cv::Mat frameTemp, frameGray, frame, test;
		caps.back().read(frameTemp);

		frameGray.create(frameTemp.rows, frameTemp.cols, CV_8UC1);
		
		std::string maskFile = videoFiles[i].substr(0, videoFiles[i].length() - 3) + "mask";
		cv::Mat fieldMask = cv::imread(maskFile + ".png", cv::IMREAD_GRAYSCALE);
		masks.push_back(fieldMask);

		cv::resize(fieldMask, test, cv::Size(), 0.5, 0.5, cv::INTER_NEAREST);
		fieldMask = test;

		bgs[i] = IBGS::createBackgroundSegmentation();
		static int from_to[] = { 0,0 };
		cv::mixChannels(&frameTemp, 1, &frameGray, 1, from_to, 1);
		bgs[i]->preprocess(frameGray, calibs.back().getImageSize(), frame, code);
		bgs[i]->loadParams();
		bgs[i]->init(frame, fieldMask);
	}
	vid1.resize(outputFiles.size());
	//vid2.resize(outputFiles.size());
	for (int i = 0; i < outputFiles.size(); ++i)
	{
		vid1[i].open(inputDir + "\\" + outputFiles[i] + "_rgb.avi", 0, 25, calibs[i].getImageSize(), true);
		//vid2[i].open(inputDir + "\\" + outputFiles[i] + "_mask.avi", 0, 25, calibs[i].getImageSize(), true);
		CV_Assert(vid1[i].isOpened());
		//CV_Assert(vid2[i].isOpened());
	}
	{
		cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
		file["colorMinBGRA"] >> colorMin;
		file["colorMaxBGRA"] >> colorMax;
		file.release();

	}
}
void VideoSaver::process()
{
	while (true)
	{
		std::cout << "Frame " << systemTime.getFrameNum() << ": " << std::endl;

		int done = 0;;
		for (int i = 0; i < caps.size(); ++i)
		{
			cv::Mat frameTemp, frameGray, frameBGR, frame, test;
			cv::Mat motionMask, motionTemp;
			if (caps[i].read(frameTemp))
			{
				static int from_to[] = { 0,0 };
				frameGray.create(frameTemp.rows, frameTemp.cols, CV_8UC1);
				cv::mixChannels(&frameTemp, 1, &frameGray, 1, from_to, 1);
				
				bgs[i]->preprocess(frameGray, calibs[i].getImageSize(), frame, code);
				//bgs[i]->apply(frame, motionMask);// , systemTime.getFrameNum() < 100);
				
				/*{
					cv::Mat tmpImg;
					bgs[i]->preprocess(frameGray, calibs[i].getImageSize(), tmpImg, cv::COLOR_BayerBG2BGR);
					cv::Mat hsvImg;
					cv::cvtColor(tmpImg, hsvImg, CV_BGR2HSV);
					filterMaskByColor(motionMask, hsvImg);
					cv::morphologyEx(motionMask, motionMask, CV_MOP_CLOSE, closingElement, cv::Point(-1, -1), 1, cv::BORDER_CONSTANT);
				}
				*/

				//cv::Mat m; cv::cvtColor(motionMask, m, CV_GRAY2BGR);
				if (outputFiles.size() > 0)
				{
					if (vid1[i].isOpened())
					{
						vid1[i].write(frame);
					}
					/*if (vid2[i].isOpened())
					{
						vid2[i].write(m);
					}*/
				}

				cv::Mat dispFrame;
				if(frame.channels()==1)
				{
					bgs[i]->preprocess(frameGray, calibs[i].getImageSize(), dispFrame, cv::COLOR_BayerBG2BGR);
				}
				else
				{
					dispFrame = frame;
				}
				/*for (int j = 0; j < fields.size(); ++j)
				{
					fields[j].show(systemTime.getFrameNum(), dispFrame, m, "", calibs[i], cv::Scalar(), true);
				}*/

				cv::imshow(names[i], dispFrame);

				//cv::imshow(names[i]+"mask", m);
				done++;
			}
		}
		if (done < caps.size())
		{
			break;
		}

		handleKey();

		systemTime.advance();
	}
}
void VideoSaver::setFields(const std::vector<std::pair<cv::Rect, std::string>>& f, int margin, int numCameras)
{
	for (int i = 0; i < f.size(); ++i)
	{
		fields.emplace_back(std::move(Field(i, f[i].first, margin, numCameras, f[i].second)));
		fields.back().stats.field = &fields.back();
	}
}
void VideoSaver::handleKey()
{
	int k = 0;

	if (step)
	{
		k = cv::waitKey(0);
		step = false;
	}
	else
	{
		k = cv::waitKey(40);
		if (stop)
		{
			step = true;
		}
	}
	if (k == ' ')
	{
		stop = true;
		step = true;
	}
	if (k == 'g')
	{
		stop = false;
		step = false;
	}
}