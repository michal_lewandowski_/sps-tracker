#pragma once
#include "Worker.h"
#include "TriangulationNViews.h"
#include "TimeWatch.hpp"
#include "KFTracker3d.h"

//#define SPS_PRINT_INFO

class WorkManager
{
public:
	WorkManager();
	~WorkManager();

	bool init(bool show);
	void process(std::vector<std::deque<BallSaveData>>& ballData, std::vector<std::vector<std::vector<int>>>& observationsPerCamera);
	void show();


	void addJob(std::string name, std::string videoFile, std::string configFile);
	void setFields(const std::vector<std::pair<cv::Rect, std::string>>& f, int margin, int numCameras);
	inline std::size_t size() const { return jobs.size(); }
	//inline int64_t time() { return systemTime.measure().getMilliseconds(); };
	inline int64_t time() { return systemTime.getFrameNum(); };
	inline FieldList& getFields() { return fields; };
	inline const FieldList& getFields() const { return fields; };
	inline const std::deque<std::shared_ptr<KFTrack3d>>& tr3dlive() const { return tracks3dlive; }
	inline std::deque<std::shared_ptr<KFTrack3d>>& tr3dlive() { return tracks3dlive; }
	inline const std::deque<std::shared_ptr<KFTrack3d>>& tr3ddead() const { return tracks3ddead; }
	inline std::deque<std::shared_ptr<KFTrack3d>>& tr3ddead() { return tracks3ddead; }
	Field* inField(const std::shared_ptr<MovingObject> obj);
	
protected:
	void handleKey();
	void show(cv::Point3d fieldDisp);

private:
	TimeWatch systemTime;
	std::vector<std::shared_ptr<Worker>> jobs;
	TriangulationNViews TW;
	KFTracker3d tracker3d;
	FieldList fields;
	std::vector<cv::Scalar> colors;
	std::vector<std::shared_ptr<MovingObject>> detections;
	std::deque<std::shared_ptr<KFTrack3d>> tracks3dlive;
	std::deque<std::shared_ptr<KFTrack3d>> tracks3ddead;

	bool showOutput;
	bool stop;
	bool step;
	double windowScale;

public:
	static std::size_t numFrame;
	static double frameRate;
};
#pragma once
