#pragma once

#include <opencv.hpp>
#define TLD_PATCH_SIZE 16
#define TLD_WINDOW_SIZE 5
#define TLD_WINDOW_OFFSET_SIZE 6
#define TLD_BINARY_BLOCK_SIZE 3
#define TLD_FORGET_AFTER_SAMPLES 64
#define TLD_THE_SAME_PATCH 0.95f

#if !defined(__AVX__) && defined(SPS_USE_AVX)
#undef SPS_USE_AVX
#endif

#if defined(SPS_USE_SSE) && defined(SPS_USE_AVX)
#undef SPS_USE_SSE
#endif

#ifdef __AVX__
#define TLD_MEM_ALIGNMENT 32
#else
#define TLD_MEM_ALIGNMENT 16
#endif

#if defined(SPS_USE_AVX)
#define TLD_WIN_SIZE_NCC 16
#elif defined(SPS_USE_SSE)
#define TLD_WIN_SIZE_NCC 10
#else
#define TLD_WIN_SIZE_NCC 10
#endif

class NormalizedPatch
{
public:
	NormalizedPatch();
	~NormalizedPatch();
	NormalizedPatch(const NormalizedPatch& np);
	//NormalizedPatch(NormalizedPatch&& np);
	NormalizedPatch& operator=(const NormalizedPatch& np);
	//NormalizedPatch& operator=(NormalizedPatch&& np);
	
	void extract(const cv::Mat &img, const cv::Mat &intImg, int x, int y, int w, int h);
	void extract(const cv::Mat &img, const cv::Mat &intImg, int* boundary);
	void extract(const cv::Mat &img, const cv::Mat &intImg, const cv::Rect& r);

	cv::Rect roi;
	float* values;
	bool positive;
    float mean;
	float norm;
	float conf;
	int valid;
#ifdef SPS_LAB
	cv::Mat result;
	cv::Mat subImage;
#endif
};

std::istream& operator>>(std::istream& is, NormalizedPatch& np);
std::ostream& operator<<(std::ostream& os, const NormalizedPatch& np);