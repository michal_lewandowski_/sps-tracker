#include "stdafx.h"
#include "VisualBall.h"
#include <boost/lexical_cast.hpp>
#include "Detector.h"

VisualBall::VisualBall(const std::size_t id, std::string cid, cv::Rect b, cv::Mat im, cv::Mat mm)
	: BallObject(id, cid, b, cv::RotatedRect(), 1.0, im, mm)
{
	ebb = cv::RotatedRect(pos2d(), box().size(), 0.0f);
	numDetected_ = 1;
	type = BallType::VISUAL;
}
VisualBall::VisualBall(const std::size_t id, std::string cid, cv::Point3d p, int views, int numDet, float error, const std::unordered_map<std::string, int>& tracks, const std::vector<PreviousBallInCam>& prevLoc)
	: BallObject(id, cid, p, views, numDet, error, tracks, prevLoc)
{
	type = BallType::VISUAL;
}
std::shared_ptr<MovingObject> VisualBall::clone() const
{
	return std::make_shared<VisualBall>(*this);
}
VisualBall::~VisualBall()
{
}

VisualBall::VisualBall(const VisualBall& other)
	: BallObject(other)
{
}

VisualBall& VisualBall::operator=(const VisualBall& other)
{
	if (this != &other)
	{
		this->BallObject::operator=(other);
	}
	return *this;
}

void VisualBall::show(cv::Mat frame, cv::Scalar color) const
{
	cv::ellipse(frame, ebb, color, -1);
	if (isValid())
	{
		cv::ellipse(frame, ebb, cv::Scalar(0, 255, 255), 3);
		cv::ellipse(frame, ebb, cv::Scalar(0, 255, 255), 5);
	}
	else
	{
		cv::ellipse(frame, ebb, cv::Scalar(0, 128, 255), 3);
		cv::ellipse(frame, ebb, cv::Scalar(0, 128, 255), 5);
	}
#ifdef SPS_DEBUB_DETS
	if (forceLearn)
	{
		cv::circle(frame, pos2d(), 20, cv::Scalar(255, 255, 255), 1);
		cv::circle(frame, pos2d(), 25, cv::Scalar(255, 255, 255), 1);
	}
#endif

	cv::putText(frame, boost::lexical_cast<std::string>(detID), cv::Point(bb_.br().x, bb_.tl().y) + cv::Point(7, -7), CV_FONT_HERSHEY_PLAIN, 1, color);
}
void VisualBall::show3d(cv::Mat frame, cv::Scalar color, double windowScale) const
{
	cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 5, color, -1);
	if (isValid())
	{
		cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 8, cv::Scalar(0, 255, 255), 2);
	}
	else
	{
		cv::circle(frame, cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - cvRound(pos3dEst().y / windowScale)), 8, cv::Scalar(0, 128, 255), 2);
	}
	//cv::putText(frame, boost::lexical_cast<std::string>(detID), cv::Point(cvRound(pos3dEst().x / windowScale), frame.rows - (cvRound(pos3dEst().y / windowScale)) + cv::Point(7, -7)), CV_FONT_HERSHEY_PLAIN, 1, color);
}
std::string VisualBall::printInfo2d(void) const
{
	std::stringstream ss;

	ss << MovingObject::printInfo();
	ss << " " << isValid();
	ss << " " << (isDetected() ? "v" : "p");
	ss << " " << pos2d();
	ss << " " << pos3d();
	ss << " " << std::setprecision(2) << ballRatio;
	ss << " <";
	for (const auto& p : probs)
	{
		ss << p.first << " " << std::setprecision(2) << p.second.value << " ";
	}
	ss << "-> " << std::setprecision(3) << getNegLogStaticProbability() << " (" << std::setprecision(3) << Detector::ballProbailityThr << ") p-> " << getProbability();
	ss << ">";
	return ss.str();
}