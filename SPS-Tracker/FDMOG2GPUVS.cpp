#include "stdafx.h"
#include "FDMOG2GPUVS.h"
#include <videostab/ring_buffer.hpp>
#include <highgui.hpp>
#include <iostream>

bool showParams = true;

FDMOG2GPUVT_MASK::FDMOG2GPUVT_MASK()
	: m_backgroundModel()
	, m_learningRate()
{
}

std::string FDMOG2GPUVT_MASK::getName() const
{
	return "VT_MOG2_GPU";
}

FDMOG2GPUVT_MASK::~FDMOG2GPUVT_MASK()
{
}

void FDMOG2GPUVT_MASK::setNumberOfGaussiansPerPixel(unsigned nMixtures)
{
	m_backgroundModel.setNumberOfGaussiansPerPixel(nMixtures);
}

void FDMOG2GPUVT_MASK::setVarianceThreshold(float varThreshold)
{
	m_backgroundModel.setVarianceThreshold(varThreshold);
}

void FDMOG2GPUVT_MASK::setBackgroundRatio(float backgroundRatio)
{
	m_backgroundModel.setBackgroundRatio(backgroundRatio);
}

void FDMOG2GPUVT_MASK::setInitialVariance(float initialVariance)
{
	m_backgroundModel.setInitialVariance(initialVariance);
}

void FDMOG2GPUVT_MASK::setMinVariance(float minVariance)
{
	m_backgroundModel.setMinVariance(minVariance);
}

void FDMOG2GPUVT_MASK::setMaxVariance(float maxVariance)
{
	m_backgroundModel.setMaxVariance(maxVariance);
}

void FDMOG2GPUVT_MASK::setComplexityReductionPrior(float ct)
{
	m_backgroundModel.setComplexityReductionPrior(ct);
}

void FDMOG2GPUVT_MASK::enableShadowDetection(bool shadowDetection)
{
	m_backgroundModel.enableShadowDetection(shadowDetection);
}

void FDMOG2GPUVT_MASK::setShadowThreshold(float tau)
{
	m_backgroundModel.setShadowThreshold(tau);
}

void FDMOG2GPUVT_MASK::setLearningRate(float learningRate)
{
	m_learningRate = learningRate;
}

void FDMOG2GPUVT_MASK::setHfactor(float fHSVFactorH)
{
	m_backgroundModel.setHfactor(fHSVFactorH);
}

void FDMOG2GPUVT_MASK::setSfactor(float fHSVFactorS)
{
	m_backgroundModel.setSfactor(fHSVFactorS);
}

void FDMOG2GPUVT_MASK::setVfactor(float fHSVFactorV)
{
	m_backgroundModel.setVfactor(fHSVFactorV);
}

void FDMOG2GPUVT_MASK::enableHSV(bool hsvInsteadOfRGB)
{
	m_isHSV = hsvInsteadOfRGB;
}

void FDMOG2GPUVT_MASK::enableRegressiveLearning(bool learningIsRegressiv)
{
	m_learningIsRegressiv = learningIsRegressiv;
	if (m_learningIsRegressiv)
	{
		m_learningRate = m_maxLearningRate;
		m_framesToNextDegrase = static_cast<unsigned> (1 / m_regressionFactor);
	}
}

void FDMOG2GPUVT_MASK::setMaxLearningRate(float maxLearningRate)
{
	m_maxLearningRate = maxLearningRate;
	if (m_learningIsRegressiv)
	{
		m_learningRate = m_maxLearningRate;
		m_framesToNextDegrase = static_cast<unsigned> (1 / m_regressionFactor);
	}
}

void FDMOG2GPUVT_MASK::setMinLearningRate(float minLearningRate)
{
	m_minLearningRate = minLearningRate;
}

void FDMOG2GPUVT_MASK::setRegressionFactor(float regressionFactor)
{
	m_regressionFactor = regressionFactor;
	if (m_learningIsRegressiv)
	{
		m_learningRate = m_maxLearningRate;
		m_framesToNextDegrase = static_cast<unsigned> (1 / m_regressionFactor);
	}
}

void FDMOG2GPUVT_MASK::setClosingSize(int closingSize)
{
	if (closingSize > 0)
	{
		m_closingElement = getStructuringElement(cv::MORPH_RECT, cv::Size(closingSize * 2 + 1, closingSize * 2 + 1), cv::Point(closingSize, closingSize));
		m_gpStructuringElem.upload(m_closingElement);
	}
}
void FDMOG2GPUVT_MASK::init(cv::Mat fieldMask)
{
	setNumberOfGaussiansPerPixel(3); // number of gaussian to model the background
	setVarianceThreshold(16.0f); // variance threshold to determine if background or foreground
	setBackgroundRatio(0.94f); // sum of gaussians weights to determine if background or foreground
	setInitialVariance(25.0f); // variance initial value
	setMinVariance(4.0f); // variance minimum value
	setMaxVariance(100.0f); // variance maximum value
	setComplexityReductionPrior(0.05f); // complexity reduction prior constant 0 - no reduction of number of components
	enableShadowDetection(true); // disable / enable shadow detection
	setShadowThreshold(0.5f); //Tau - shadow threshold
	m_backgroundModel.setShadowValue(0);
	setLearningRate(0.009); // background adaption rate

	setHfactor(1.0f); // if HSV, weight for H plane
	setSfactor(1.0f); // if HSV, weight for S plane
	setVfactor(1.0f); // if HSV, weight for V plane
	enableHSV(false); // disable / enable background subration in HSV colour space.
	enableRegressiveLearning(true); // disable / enable adaptive learning rate in the starting phase
	setMaxLearningRate(0.01f); // max learning rate in the begining (value range: 0.001 - 0.03)
	setMinLearningRate(0.0008f); // min learning rate in the end (value range: 0.00005 - 0.001)
	setRegressionFactor(0.1f); // factor used for decreasing the learning factor from max to min (value range: 0.01 - 0.2)

	setClosingSize(2);  // closing element size, 0 => closing disabled

	if (m_learningIsRegressiv)
	{
		m_learningRate = m_maxLearningRate;
		m_framesToNextDegrase = static_cast<unsigned> (1 / m_regressionFactor);
	}
	m_fieldMaskGpuMat.create(fieldMask.size(), CV_8UC1);
	m_fieldMaskGpuMat.upload(fieldMask);
	m_gpuMask.create(fieldMask.size(), CV_8UC1);
	morph = cv::cuda::createMorphologyFilter(CV_MOP_CLOSE, CV_8UC1, m_closingElement);

	cv::FileStorage file(".\\BGS.xml", cv::FileStorage::READ);
	file["history"] >> m_backgroundModel.history;
	file["nmixtures"] >> m_backgroundModel.nmixtures_;
	file["varThreshold"] >> m_backgroundModel.varThreshold;
	file["varThresholdGen"] >> m_backgroundModel.varThresholdGen;
	file["backgroundRatio"] >> m_backgroundModel.backgroundRatio;
	file["VarInit"] >> m_backgroundModel.fVarInit;
	file["VarMin"] >> m_backgroundModel.fVarMin;
	file["VarMax"] >> m_backgroundModel.fVarMax;
	file["ComplexityReductionPrior"] >> m_backgroundModel.fCT;
	file["Tau"] >> m_backgroundModel.fTau;
	file["learningRate"] >> m_learningRate;
	file["HSVFactorH"] >> m_backgroundModel.fHSVFactorH;
	file["fHSVFactorS"] >> m_backgroundModel.fHSVFactorS;
	file["fHSVFactorV"] >> m_backgroundModel.fHSVFactorV;
	file["isHSV"] >> m_isHSV;
	file["learningIsRegressiv"] >> m_learningIsRegressiv;
	file["maxLearningRate"] >>  m_maxLearningRate;
	file["minLearningRate"] >> m_minLearningRate;
	file["regressionFactor"] >> m_regressionFactor;
	file.release();

	if(showParams)
	{
		std::cout << "PARAMETERS:" << std::endl << std::endl;
		std::cout << "learningRate\t" << m_learningRate << std::endl;
		std::cout << "isHSV\t" << m_isHSV << std::endl;
		std::cout << "learningIsRegressiv\t" << m_learningIsRegressiv << std::endl;
		std::cout << "maxLearningRate\t" << m_maxLearningRate << std::endl;
		std::cout << "minLearningRate\t" << m_minLearningRate << std::endl;
		std::cout << "regressionFactor\t" << m_regressionFactor << std::endl << std::endl;
		showParams = false;
	}
}

void FDMOG2GPUVT_MASK::detectForeground(cv::Mat img, cv::Mat& mask)
{
	//Background masking and background subration
	cv::cuda::GpuMat currentImg;
	currentImg.upload(img);

	addaptLearingRate();

	m_backgroundModel(currentImg, m_fieldMaskGpuMat, m_gpuMask, m_learningRate);

	if (!m_closingElement.empty())
	{
		morph->apply(m_gpuMask, m_gpuMask);
	}
	m_gpuMask.download(mask);
}

void FDMOG2GPUVT_MASK::addaptLearingRate()
{
	if (!m_learningIsRegressiv)
	{
		return;
	}
	if (m_framesToNextDegrase <= 0)
	{
		m_learningRate -= (m_maxLearningRate - m_minLearningRate) / 100;
		m_framesToNextDegrase = static_cast<unsigned>(1 / m_regressionFactor);
		if (m_learningRate <= m_minLearningRate)
		{
			m_learningIsRegressiv = false;
		}
	}
	else
	{
		--m_framesToNextDegrase;
	}
}
