#include "stdafx.h"
#include "Templates.h"
#include <boost/filesystem/fstream.hpp>
#include <iostream>
#include "Field.h"

extern double ballRadius;

cv::Mat createBallTemplateSizeMap(const std::string file, const CameraCalibration& cm, FieldList& fields, bool cacheTemplate)
{
	cv::Mat ballOffsetMap(cm.getImageSize(), CV_32FC1);
	bool bCreate = true;

	if (cacheTemplate)
	{
		//std::cout << "Loading cached template ball offset map: " << file << " with size " << cm.getImageSize() << std::endl;
		ballOffsetMap = readBinary(file);
		bCreate = (cm.getImageSize() != ballOffsetMap.size());
		
	}
	if (bCreate)
	{
		ballOffsetMap.create(cm.getImageSize(), CV_32FC1);
		//std::cout << "Creating template ball offset map: " << file << " with size " << cm.getImageSize() << std::endl;
		std::vector<cv::Vec2d> distortedPoints(1);
		std::vector<cv::Vec2d> undistortedPoints(1);
		for (int y = 0; y < ballOffsetMap.rows; ++y)
		{
			for (int x = 0; x < ballOffsetMap.cols; ++x)
			{
				cv::Point2d pixelCoord(x, y);
				cv::Point3d realWorld = cm.transformToRealWorld(pixelCoord, ballRadius, distortedPoints, undistortedPoints);

				cv::Point2d pixLeft;
				cv::Point2d pixRight;
				cv::Point2d pixTop;
				cv::Point2d pixBottom;

				{
					cv::Point3d realWorld1 = realWorld;

					realWorld1.x -= ballRadius;
					cm.transformToPixel(realWorld1, pixLeft);
					realWorld1.x += 2 * ballRadius;
					cm.transformToPixel(realWorld1, pixRight);
				}
				{
					cv::Point3d realWorld1 = realWorld;

					realWorld1.y -= ballRadius;
					cm.transformToPixel(realWorld1, pixTop);
					realWorld1.y += 2 * ballRadius;
					cm.transformToPixel(realWorld1, pixBottom);
				}

				ballOffsetMap.at<float>(y, x) = std::max(static_cast<float>(cv::norm(pixLeft - pixRight)) / 2.0f, static_cast<float>(cv::norm(pixBottom - pixTop)) / 2.0f);
			}
		}
		if (cacheTemplate)
		{
			storeBinary(file, ballOffsetMap);		
		}
	}
	return ballOffsetMap;
}

cv::Mat createUnitMapX(const std::string file, const CameraCalibration& cm, bool cacheTemplate)
{
	cv::Mat unitTemplateMapX;
	bool bCreate = true;

	if (cacheTemplate)
	{
		//std::cout << "Loading cached template unit X map: " << file << " with size " << cm.getImageSize() << std::endl;
		unitTemplateMapX = readBinary(file);
		bCreate = (cm.getImageSize() != unitTemplateMapX.size());
	}
	if (bCreate)
	{
		unitTemplateMapX.create(cm.getImageSize(), CV_64FC1);
		//std::cout << "Creating template unit X map: " << file << " with size " << cm.getImageSize() << std::endl;
		std::vector<cv::Vec2d> distortedPoints(1);
		std::vector<cv::Vec2d> undistortedPoints(1);
		for (int y = 0; y < unitTemplateMapX.rows; ++y)
		{
			auto unitTemplateMapX_y_ = unitTemplateMapX.ptr<double>(y);
			for (int x = 0; x < unitTemplateMapX.cols; ++x)
			{
				cv::Point3d feet = cm.transformToRealWorld(cv::Point2d(x, y), 0.0, distortedPoints, undistortedPoints);
				cv::Point3d shiftFeet;
				if (x<unitTemplateMapX.rows - 1)
				{
					shiftFeet = cm.transformToRealWorld(cv::Point2d(x + 1, y), 0.0, distortedPoints, undistortedPoints);
				}
				else
				{
					shiftFeet = cm.transformToRealWorld(cv::Point2d(x - 1, y), 0.0, distortedPoints, undistortedPoints);
				}
				double dUnitX = cv::norm(shiftFeet - feet);

				unitTemplateMapX_y_[x] = dUnitX;
			}
		}
		if (cacheTemplate)
		{
			storeBinary(file, unitTemplateMapX);
		}
	}
	return unitTemplateMapX;
}
cv::Mat createUnitMapY(const std::string file, const CameraCalibration& cm, bool cacheTemplate)
{
	cv::Mat unitTemplateMapY(cm.getImageSize(), CV_64FC1);
	bool bCreate = true;

	if (cacheTemplate)
	{
		//std::cout << "Loading cached template unit Y map: " << file << " with size " << cm.getImageSize() << std::endl;
		unitTemplateMapY = readBinary(file);
		bCreate = (cm.getImageSize() != unitTemplateMapY.size());
	}
	if (bCreate)
	{
		unitTemplateMapY.create(cm.getImageSize(), CV_64FC1);
		//std::cout << "Creating template unit Y map: " << file << " with size " << cm.getImageSize() << std::endl;
		std::vector<cv::Vec2d> distortedPoints(1);
		std::vector<cv::Vec2d> undistortedPoints(1);
		for (int y = 0; y < unitTemplateMapY.rows; ++y)
		{
			auto unitTemplateMapY_y_ = unitTemplateMapY.ptr<double>(y);
			for (int x = 0; x < unitTemplateMapY.cols; ++x)
			{
				cv::Point3d feet = cm.transformToRealWorld(cv::Point2d(x, y), 0.0, distortedPoints, undistortedPoints);
				cv::Point3d shiftFeet;
				if (x<unitTemplateMapY.rows - 1)
					shiftFeet = cm.transformToRealWorld(cv::Point2d(x + 1, y), 0.0, distortedPoints, undistortedPoints);
				else shiftFeet = cm.transformToRealWorld(cv::Point2d(x - 1, y), 0.0, distortedPoints, undistortedPoints);
				double dUnitY = cm.getApproximateUnit(feet, shiftFeet);

				unitTemplateMapY_y_[x] = dUnitY;
			}
		}
		if (cacheTemplate)
		{
			storeBinary(file, unitTemplateMapY);
		}
	}
	return unitTemplateMapY;
}

void storeBinary(const std::string& filename, const cv::Mat& m)
{
	/*
	FILE * pFile;
	pFile = fopen ( filename.c_str() , "wb" );
	fwrite (reinterpret_cast<const char*>(&m.rows) , 1 , sizeof(m.rows) , pFile );
	fwrite (reinterpret_cast<const char*>(&m.cols) , 1 , sizeof(m.cols) , pFile );
	fwrite (reinterpret_cast<const char*>(&m.data) , 1 , m.rows * m.step * m.channels(), pFile );
	fclose (pFile);
	*/
	boost::filesystem::ofstream ofs(filename, std::ios::out | std::ios::binary);
	ofs.write(reinterpret_cast<const char*>(&m.rows), sizeof(m.rows));
	ofs.write(reinterpret_cast<const char*>(&m.cols), sizeof(m.cols));
	int type = m.type();
	ofs.write(reinterpret_cast<const char*>(&type), sizeof(type));
	ofs.write(reinterpret_cast<const char*>(m.data), m.rows * m.step[0]);
	ofs.close();
}

cv::Mat readBinary(const std::string& filename)
{
	cv::Mat m;

	FILE * pFile = fopen(filename.c_str(), "rb");
	if (pFile == nullptr)
	{
		return m;
	}

	// obtain file size:
	fseek(pFile, 0, SEEK_END);
	long lSize = ftell(pFile);
	rewind(pFile);

	auto bytes4rows = sizeof(m.rows);
	auto bytes4cols = sizeof(m.rows);
	auto bytes4type = sizeof(m.type());
	size_t imgBytes = lSize - bytes4rows - bytes4cols - bytes4type;

	// allocate memory to contain the whole file:
	char* buffer = static_cast<char*>(malloc(sizeof(char)*imgBytes));

	if (buffer == nullptr)
	{
		return m;
	}

	// copy the file into the buffer:
	cv::Size size;
	size_t result;
	result = fread(&size.height, bytes4rows, 1, pFile);
	if (result != 1)
	{
		return m;
	}
	result = fread(&size.width, bytes4cols, 1, pFile);
	if (result != 1)
	{
		return m;
	}
	int type;
	result = fread(&type, bytes4type, 1, pFile);
	if (result != 1)
	{
		return m;
	}
	result = fread(buffer, 1, imgBytes, pFile);
	if (result != imgBytes)
	{
		return m;
	}

	// clean up
	fclose(pFile);

	m = cv::Mat(size, type, buffer, imgBytes / size.height);
	return m;
}