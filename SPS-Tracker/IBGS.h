#pragma once
#include <opencv.hpp>
#include <memory>
#include "TimeWatch.hpp"

class IBGS
{
public:
	IBGS();
	virtual ~IBGS();

	virtual void init(const cv::Mat& oInitImg, const cv::Mat& oROI) = 0;
	virtual void apply(const cv::Mat& image, cv::Mat& fgmask, double learningRateOverride = 0.0) = 0;
	virtual void preprocess(const cv::Mat& iframe, const cv::Size& size, cv::Mat& pframe, int code = cv::COLOR_BayerBG2BGR) = 0;
	virtual void loadParams() = 0;
	virtual void getBackgroundImage(cv::OutputArray backgroundImage) const = 0;

	static std::unique_ptr<IBGS> createBackgroundSegmentation();

protected:
	void filterMaskByColor(cv::Mat& mask, cv::Mat img);
	void startApply();
	void stopApply();

	TimeWatch tw;
	//! background model ROI used for LBSP descriptor extraction (specific to the input image size)
	cv::Mat oROI;
	cv::Scalar colorMin;
	cv::Scalar colorMax;
	bool isHSV;
	cv::Mat closingElement;
	static bool showParams;
};

