#pragma once
#include <core/mat.hpp>
#include "CameraCalibration.h"

class FieldList;
void storeBinary(const std::string& filename, const cv::Mat& m);
cv::Mat readBinary(const std::string& filename);

cv::Mat createBallTemplateSizeMap(const std::string file, const CameraCalibration& cm, FieldList& fields, bool cacheTemplate);
cv::Mat createUnitMapX(const std::string file, const CameraCalibration& cm, bool cacheTemplate);
cv::Mat createUnitMapY(const std::string file, const CameraCalibration& cm, bool cacheTemplate);
