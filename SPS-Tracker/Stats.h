#pragma once
#include <opencv.hpp>
#include <vector>
#include <array>

class CameraCalibration;
class Field;

class Stats
{
public:
	Stats(int fid, std::string gtPath, Field* parent);
	~Stats();
	
	inline bool isLoad() const { return dataGT.size() > 0; };
	inline const std::vector<cv::Point3d>& ballGT() const { return dataGT; }

	void computeError(int numFrame, cv::Point3d ept);
	void computeError(int index, cv::Point3d gpt, cv::Point3d ept, cv::Rect r, double err);
	void computeStats(std::string outputDir, std::string outputName);

	void show(int numFrame, cv::Mat frame, CameraCalibration& calibration) const;
	void show3d(int numFrame, cv::Mat frame, double windowScale) const;

	static const int numStats = 4;
	cv::Scalar gtColor;
	std::vector<cv::Point3d> dataGT;
	Field* field;
	double distThreshold;

	std::array<int, numStats> totalFrames;
	std::array<int, numStats> falsePositivesAll;//no ball GT, ball est
	std::array<int, numStats> falseNegativesAll;//ball GT, no ball est
	std::array<int, numStats> trueNegativesAll; //no ball GT, no ball est
	std::array<int, numStats> truePositiveAll;  //ball GT, ball est
	std::array<int, numStats> falsePositivesThresholded;//no ball GT, ball est
	std::array<int, numStats> falseNegativesThresholded;//ball GT, no ball est
	std::array<int, numStats> trueNegativesThresholded; //no ball GT, no ball est
	std::array<int, numStats> truePositiveThresholded;  //ball GT, ball est
	std::array<std::vector<double>, numStats> errorsAll;
	std::array<std::vector<double>, numStats> errorsThresholded;
};

