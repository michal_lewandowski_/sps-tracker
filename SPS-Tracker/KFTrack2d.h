#pragma once

#include <opencv2/opencv.hpp>
#include "MovingObject.h"
#include "Track.h"
#include "BallObject.h"

class KFTrack2d : public Track
{
public:
	KFTrack2d(const size_t iTrackID, const CameraCalibration& calib, const std::shared_ptr<MovingObject> obj, const int iStateParams, const int iMesaureParams, const int64_t dStartTS, double& maxAbsVelocity, double& maxBallProbability);
	virtual ~KFTrack2d();

	virtual void addTrace(const CameraCalibration* calib, const std::shared_ptr<MovingObject> obj);
	virtual std::shared_ptr<MovingObject> predict(int64_t time) override;
	virtual void update2d(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj) override;
	virtual void update3d(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj) override;
	virtual void update(std::shared_ptr<MovingObject> obj) override;
	virtual double getCost(const std::shared_ptr<MovingObject> obj) const override;
	virtual double getCostThreshold(void) const override;
	virtual double getCostMax(void) const override;
	virtual void computeVelocity();
	void fillPastLocations(std::vector<PreviousBallLoc>& locs);
	void assignProximityProbability(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj);

	inline bool isLearned(void) const { return learnIn2d; }
	inline void setLearned(void) { learnIn2d = true; }

protected:
	KFTrack2d(const KFTrack2d& other);
	KFTrack2d& operator=(const KFTrack2d& other);

	void initMotionModel(int iStateParams, int iMesaureParams, double dDeltaTime);
	double getDist(const std::shared_ptr<MovingObject> obj) const;
	double getProb(const std::shared_ptr<MovingObject> obj) const;

	double getDist(const cv::Point2d& p2, const cv::Rect& r) const;
	double getProb(const cv::Point2d& p2, const cv::Rect& r) const;

public:
	cv::KalmanFilter m_KF;
	cv::Mat m_state;
	cv::Mat m_estimation;
	cv::Mat m_trackParams;

	cv::Mat m_icov;
	double m_det;
	bool updateFrom3D;
	bool learnIn2d;
	int numContinous4observations;
};

