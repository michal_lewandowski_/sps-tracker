#include "stdafx.h"
#include "KFTrack3d.h"
#include <boost/format.hpp>

KFTrack3d::KFTrack3d(const size_t iTrackID, const std::shared_ptr<MovingObject> obj, const int iStateParams, const int iMesaureParams, const int64_t dStartTS, double& maxAbsVelocity, double& maxBallProbability)
	: Track(iTrackID, obj, dStartTS, maxAbsVelocity, maxBallProbability), extended(false)
{
	addTrace(obj);

	m_KF.init(iStateParams, iMesaureParams, 0, CV_64FC1);
	m_state.create(iStateParams, 1, CV_64FC1);
	m_trackParams.create(3, 1, CV_64FC1);

	cv::setIdentity(m_KF.processNoiseCov);
	cv::setIdentity(m_KF.measurementNoiseCov);
	cv::setIdentity(m_KF.errorCovPost);

	double dDeltaTime = 1.0;// / 25.0;
	initMotionModel(iStateParams, iMesaureParams, dDeltaTime);

	m_KF.statePost.create(iStateParams, 1, CV_64FC1);
	m_KF.statePost.setTo(0);
	m_KF.statePost.at<double>(0) = obj->pos3d().x;
	m_KF.statePost.at<double>(1) = obj->pos3d().y;
	m_KF.statePost.at<double>(2) = obj->pos3d().z;
	m_estimation = m_KF.measurementMatrix*m_KF.statePost;

	m_icov = m_KF.measurementMatrix*m_KF.processNoiseCov*m_KF.measurementMatrix.t() + m_KF.measurementNoiseCov;
	m_det = cv::log(cv::determinant(m_icov));
	m_icov = m_icov.inv();
}
void KFTrack3d::initMotionModel(int iStateParams, int iMesaureParams, double dDeltaTime)
{
	if (iStateParams == 6 && iMesaureParams == 3)
	{
		/* (x, y, z, Vx, Vy, Vz) const speed */
		m_KF.transitionMatrix = (cv::Mat_<double>(iStateParams, iStateParams) << 1, 0, 0, dDeltaTime, 0, 0,
			0, 1, 0, 0, dDeltaTime, 0,
			0, 0, 1, 0, 0, dDeltaTime,
			0, 0, 0, 1, 0, 0,
			0, 0, 0, 0, 1, 0,
			0, 0, 0, 0, 0, 1);
		m_KF.measurementMatrix = (cv::Mat_<double>(iMesaureParams, iStateParams) << 1, 0, 0, 0, 0, 0,
			0, 1, 0, 0, 0, 0,
			0, 0, 1, 0, 0, 0);
		return;
	}
	if (iStateParams == 9 && iMesaureParams == 3)
	{
		/* (x, y, z, Vx, Vy, Vz, Ax, Ay, Az) const acceleration */
		m_KF.transitionMatrix = (cv::Mat_<double>(iStateParams, iStateParams) << 1, 0, 0, dDeltaTime, 0, 0, 0.5*dDeltaTime*dDeltaTime, 0, 0,
			0, 1, 0, 0, dDeltaTime, 0, 0, 0.5*dDeltaTime*dDeltaTime, 0,
			0, 0, 1, 0, 0, dDeltaTime, 0, 0, 0.5*dDeltaTime*dDeltaTime,
			0, 0, 0, 1, 0, 0, dDeltaTime, 0, 0,
			0, 0, 0, 0, 1, 0, 0, dDeltaTime, 0,
			0, 0, 0, 0, 0, 1, 0, 0, dDeltaTime,
			0, 0, 0, 0, 0, 0, 1, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 1, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 1);
		m_KF.measurementMatrix = (cv::Mat_<double>(iMesaureParams, iStateParams) << 1, 0, 0, 0, 0, 0,
			0, 1, 0, 0, 0, 0,
			0, 0, 1, 0, 0, 0);
		return;
	}
	CV_Error(CV_StsInternal, (boost::format("no motion model for %d states and %d measures") % iStateParams % iMesaureParams).str());
}
KFTrack3d::KFTrack3d(const KFTrack3d& other)
	: Track(other)
{
	this->m_KF = other.m_KF;
	this->m_state = other.m_state.clone();
	this->m_icov = other.m_icov.clone();
	this->m_det = other.m_det;
	this->m_trackParams = other.m_trackParams.clone();
	this->m_estimation = other.m_estimation.clone();
}
KFTrack3d& KFTrack3d::operator=(const KFTrack3d& other)
{
	if (this != &other)
	{
		this->Track::operator=(other);
		this->m_KF = other.m_KF;
		this->m_state = other.m_state.clone();
		this->m_icov = other.m_icov.clone();
		this->m_det = other.m_det;
		this->m_trackParams = other.m_trackParams.clone();
		this->m_estimation = other.m_estimation.clone();
	}
	return *this;
}

KFTrack3d::~KFTrack3d()
{
}
void KFTrack3d::addTrace(const std::shared_ptr<MovingObject> obj)
{
	CV_Assert(m_trace.size() == 0 || (getCurrentDet()->field()!=nullptr && getCurrentDet()->field()->fid == obj->field()->fid));

	m_trace.push_back(obj);
	traceLength++;

	numDetected += static_cast<int>(obj->isDetected());
	computeVelocity();

	/*if (m_trace.size() == maxTraceLength)
	{
		m_trace.pop_front();
	}*/
}
void KFTrack3d::extendTrace(const std::shared_ptr<MovingObject> obj)
{
	if(m_dStartTimestamp>0)
	{
		m_trace.push_front(obj);
		m_dStartTimestamp--;
		traceLength++;
	}
}
std::shared_ptr<MovingObject> KFTrack3d::predict(int64_t time)
{
	m_dEndTimestamp = time;
	m_state = m_KF.predict();
	m_estimation = m_KF.measurementMatrix*m_state;// +m_KF.measurementNoiseCov.diag(); //m_KF.measurementNoiseCov is constant
	return nullptr;
}
void KFTrack3d::update(std::shared_ptr<MovingObject> obj)
{
	m_iMissedFrames = 0;

	m_trackParams.at<double>(0) = obj->pos3d().x;
	m_trackParams.at<double>(1) = obj->pos3d().y;
	m_trackParams.at<double>(2) = obj->pos3d().z;
	m_estimation = m_KF.measurementMatrix * m_KF.correct(m_trackParams);

	obj->pos3d() = cv::Point3d(static_cast<int>(m_estimation.at<double>(0) + 0.5), static_cast<int>(m_estimation.at<double>(1) + 0.5), static_cast<int>(m_estimation.at<double>(2) + 0.5));
	obj->pos3dEst() = obj->pos3d();
}
void KFTrack3d::update2d(const CameraCalibration*, std::shared_ptr<MovingObject>)
{
	CV_Assert(0);
}
void KFTrack3d::update3d(const CameraCalibration*, std::shared_ptr<MovingObject> obj)
{
	update(obj);
	addTrace(obj);
}
double KFTrack3d::getDist(const cv::Point3d& p3) const
{
	return cv::norm(getCurrentDet()->pos3d() - p3);
}
double KFTrack3d::getDist(const std::shared_ptr<MovingObject> obj) const
{
	return getDist(obj->pos3d());
}
double KFTrack3d::getProb(const cv::Point3d& p3) const
{
	double dd = getDist(p3);

	dd = cv::exp(-dd * dd / distVariance);

	return dd;
}
double KFTrack3d::getProb(const std::shared_ptr<MovingObject> obj) const
{
	return getProb(obj->pos3d());
}
double KFTrack3d::getCost(const std::shared_ptr<MovingObject> obj) const
{
	//return getDist(obj);
	return 1.0 - getProb(obj);
}
double KFTrack3d::getCostThreshold(void) const
{
	//return distCostThreshold;
	return 1.0 - probCostThreshold;
}
double KFTrack3d::getCostMax(void) const
{
	//return maxDist;
	return 1.0 - maxProb;
}
void KFTrack3d::computeVelocity()
{
	m_dAbsVelocity = getAbsoluteVelocity();
	if (isVisible() && m_dAbsVelocity == 0.0 && getNumDetected() > goodFramesThreshold && getAverageProbability() <= goodNegLogProbThreshold)
	{
		m_dAbsVelocity = std::numeric_limits<double>::epsilon();
	}
	if (getLength() > 1 && m_dAbsVelocity > maxAbsVelocityGlobal)
	{
		if (verifyVelocity())
		{
			if (isVisible())
			{
				maxAbsVelocityGlobal = m_dAbsVelocity;
			}
		}
		else
		{
			m_dAbsVelocity = 0.0;
		}
	}
}