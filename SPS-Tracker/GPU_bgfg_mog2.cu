#pragma warning(push, 0)
#include "opencv2/core/cuda/common.hpp"
#include "opencv2/core/cuda/vec_traits.hpp"
#include "opencv2/core/cuda/vec_math.hpp"
#include "opencv2/core/cuda/limits.hpp"
#pragma warning(pop)

using namespace cv::cuda;
using namespace cv::cuda::device;

namespace MOG2
{
	__constant__ int           c_nmixtures;
	__constant__ float         c_Tb;
	__constant__ float         c_TB;
	__constant__ float         c_Tg;
	__constant__ float         c_varInit;
	__constant__ float         c_varMin;
	__constant__ float         c_varMax;
	__constant__ float         c_tau;
	__constant__ unsigned char c_shadowVal;
	__constant__ float         c_HSVFactorH;
	__constant__ float         c_HSVFactorS;
	__constant__ float         c_HSVFactorV;

	__device__ __forceinline__ float cvt(uchar val)
	{
		return val;
	}
	__device__ __forceinline__ float3 cvt(const uchar3& val)
	{
		return make_float3(val.x, val.y, val.z);
	}
	__device__ __forceinline__ float4 cvt(const uchar4& val)
	{
		return make_float4(val.x, val.y, val.z, val.w);
	}

	__device__ __forceinline__ float sqr(float val)
	{
		return val * val;
	}
	__device__ __forceinline__ float sqr(const float3& val)
	{
		return val.x * val.x + val.y * val.y + val.z * val.z;
	}
	__device__ __forceinline__ float sqr(const float4& val)
	{
		return val.x * val.x + val.y * val.y + val.z * val.z;
	}

	__device__ __forceinline__ float sum(float val)
	{
		return val;
	}
	__device__ __forceinline__ float sum(const float3& val)
	{
		return val.x + val.y + val.z;
	}
	__device__ __forceinline__ float sum(const float4& val)
	{
		return val.x + val.y + val.z;
	}

	__device__ __forceinline__ float pixGain(float val)
	{
		return val;
	}
	__device__ __forceinline__ float3 pixGain(const float3& val)
	{
		return make_float3(c_HSVFactorH * val.x, c_HSVFactorS * val.y, c_HSVFactorV * val.z);
	}
	__device__ __forceinline__ float4 pixGain(const float4& val)
	{
		return make_float4(c_HSVFactorH * val.x, c_HSVFactorS * val.y, c_HSVFactorV * val.z, 1 * val.w);
	}

	template <class Ptr2D>
	__device__ __forceinline__ void swap(Ptr2D& ptr, int x, int y, int k, int rows)
	{
		typename Ptr2D::elem_type val = ptr(k * rows + y, x);
		ptr(k * rows + y, x) = ptr((k + 1) * rows + y, x);
		ptr((k + 1) * rows + y, x) = val;
	}

	void setNumberOfGaussiansPerPixel(int nMixtures)
	{
		int m = int(nMixtures);
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_nmixtures), static_cast<const void*>(&m), sizeof(int)));
	}
	void setVarianceThreshold(float varThreshold)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_Tb), reinterpret_cast<const void*>(&varThreshold), sizeof(float)));
	}
	void setBackgroundRatio(float backgroundRatio)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_TB), reinterpret_cast<const void*>(&backgroundRatio), sizeof(float)));
	}
	void setVarianceThresholdGen(float varThresholdGen)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_Tg), reinterpret_cast<const void*>(&varThresholdGen), sizeof(float)));
	}
	void setInitialVariance(float initialVariance)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_varInit), reinterpret_cast<const void*>(&initialVariance), sizeof(float)));
	}
	void setMinVariance(float minVariance)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_varMin), reinterpret_cast<const void*>(&minVariance), sizeof(float)));
	}
	void setMaxVariance(float maxVariance)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_varMax), reinterpret_cast<const void*>(&maxVariance), sizeof(float)));
	}
	void setShadowValue(unsigned char shadowVal)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_shadowVal), reinterpret_cast<const void*>(&shadowVal), sizeof(unsigned char)));
	}
	void setShadowThreshold(float tau)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_tau), reinterpret_cast<const void*>(&tau), sizeof(float)));
	}
	void setHfactor(float fHSVFactorH)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_HSVFactorH), reinterpret_cast<const void*>(&fHSVFactorH), sizeof(float)));
	}
	void setSfactor(float fHSVFactorS)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_HSVFactorS), reinterpret_cast<const void*>(&fHSVFactorS), sizeof(float)));
	}
	void setVfactor(float fHSVFactorV)
	{
		cudaSafeCall(cudaMemcpyToSymbol(reinterpret_cast<const void*>(&c_HSVFactorV), reinterpret_cast<const void*>(&fHSVFactorV), sizeof(float)));
	}

	template <bool detectShadows, typename SrcT, typename WorkT>
	__global__ void vt_mog2(const cv::cuda::PtrStepSz<SrcT> frame, cv::cuda::PtrStepb validmask, cv::cuda::PtrStepb fgmask, cv::cuda::PtrStepb modesUsed, cv::cuda::PtrStepf gmm_weight, cv::cuda::PtrStepf gmm_variance, cv::cuda::PtrStep<WorkT> gmm_mean, const float alphaT, const float alpha1, const float prune)
	{
		const int x = blockIdx.x * blockDim.x + threadIdx.x;
		const int y = blockIdx.y * blockDim.y + threadIdx.y;

		if (x >= frame.cols || y >= frame.rows)
		{
			return;
		}
		if (static_cast<int>(validmask(y, x)) > 128)
		{
			fgmask(y, x) = 0;
			return;
		}

		WorkT pix = cvt(frame(y, x));

		pix = pixGain(pix);

		//calculate distances to the modes (+ sort)
		//here we need to go in descending order!!!

		bool background = false; // true - the pixel classified as background

		//internal:

		bool fitsPDF = false; //if it remains zero a new GMM mode will be added

		int nmodes = modesUsed(y, x);
		int nNewModes = nmodes; //current number of modes in GMM

		float totalWeight = 0.0f;

		//go through all modes

		for (int mode = 0; mode < nmodes; ++mode)
		{
			//need only weight if fit is found
			float weight = alpha1 * gmm_weight(mode * frame.rows + y, x) + prune;
			int swap_count = 0;
			//fit not found yet
			if (!fitsPDF)
			{
				//check if it belongs to some of the remaining modes
				float var = gmm_variance(mode * frame.rows + y, x);

				WorkT mean = gmm_mean(mode * frame.rows + y, x);

				//calculate difference and distance
				WorkT diff = mean - pix;
				float dist2 = sqr(diff);

				//background? - Tb - usually larger than Tg
				if (totalWeight < c_TB && dist2 < c_Tb * var)
					background = true;

				//check fit
				if (dist2 < c_Tg * var)
				{
					//belongs to the mode
					fitsPDF = true;

					// check if is playerPosition
					if (static_cast<int>(validmask(y, x)) == 0)
					{
						//update distribution

						//update weight
						weight += alphaT;
						float k = alphaT / weight;

						//update mean
						gmm_mean(mode * frame.rows + y, x) = mean - k * diff;

						//update variance
						float varnew = var + k * (dist2 - var);

						//limit the variance
						varnew = ::fmaxf(varnew, c_varMin);
						varnew = ::fminf(varnew, c_varMax);

						gmm_variance(mode * frame.rows + y, x) = varnew;

						//sort
						//all other weights are at the same place and
						//only the matched (iModes) is higher -> just find the new place for it

						for (int i = mode; i > 0; --i)
						{
							//check one up
							if (weight < gmm_weight((i - 1) * frame.rows + y, x))
								break;

							swap_count++;
							//swap one up
							swap(gmm_weight, x, y, i - 1, frame.rows);
							swap(gmm_variance, x, y, i - 1, frame.rows);
							swap(gmm_mean, x, y, i - 1, frame.rows);
						}

						//belongs to the mode - bFitsPDF becomes 1
					}
				}
			} // !fitsPDF

			//check prune
			if (weight < -prune)
			{
				weight = 0.0;
				nmodes--;
			}

			gmm_weight((mode - swap_count) * frame.rows + y, x) = weight; //update weight by the calculated value
			totalWeight += weight;
		}

		//renormalize weights

		totalWeight = 1.f / totalWeight;
		for (int mode = 0; mode < nmodes; ++mode)
			gmm_weight(mode * frame.rows + y, x) *= totalWeight;

		nmodes = nNewModes;

		//make new mode if needed and exit

		if (!fitsPDF)
		{
			// check if is playerPosition
			if (static_cast<int>(validmask(y, x)) == 0)
			{

				// replace the weakest or add a new one
				int mode = nmodes == c_nmixtures ? c_nmixtures - 1 : nmodes++;


				if (nmodes == 1)
					gmm_weight(mode * frame.rows + y, x) = 1.f;
				else
				{
					gmm_weight(mode * frame.rows + y, x) = alphaT;

					// renormalize all other weights

					for (int i = 0; i < nmodes - 1; ++i)
						gmm_weight(i * frame.rows + y, x) *= alpha1;
				}

				// init

				gmm_mean(mode * frame.rows + y, x) = pix;
				gmm_variance(mode * frame.rows + y, x) = c_varInit;

				//sort
				//find the new place for it

				for (int i = nmodes - 1; i > 0; --i)
				{
					// check one up
					if (alphaT < gmm_weight((i - 1) * frame.rows + y, x))
						break;

					//swap one up
					swap(gmm_weight, x, y, i - 1, frame.rows);
					swap(gmm_variance, x, y, i - 1, frame.rows);
					swap(gmm_mean, x, y, i - 1, frame.rows);
				}
			}
		}

		//set the number of modes
		modesUsed(y, x) = nmodes;

		bool isShadow = false;
		if (detectShadows && !background)
		{
			float tWeight = 0.0f;

			// check all the components  marked as background:
			for (int mode = 0; mode < nmodes; ++mode)
			{
				WorkT mean = gmm_mean(mode * frame.rows + y, x);

				WorkT pix_mean = pix * mean;

				float numerator = sum(pix_mean);
				float denominator = sqr(mean);

				// no division by zero allowed
				if (denominator == 0)
					break;

				// if tau < a < 1 then also check the color distortion
				if (numerator <= denominator && numerator >= c_tau * denominator)
				{
					float a = numerator / denominator;

					WorkT dD = a * mean - pix;

					if (sqr(dD) < c_Tb * gmm_variance(mode * frame.rows + y, x) * a * a)
					{
						isShadow = true;
						break;
					}
				};

				tWeight += gmm_weight(mode * frame.rows + y, x);
				if (tWeight > c_TB)
					break;
			}
		}

		fgmask(y, x) = background ? 0 : isShadow ? c_shadowVal : 255;
	}

	template <typename SrcT, typename WorkT>
	void vt_mog2_caller(cv::cuda::PtrStepSzb frame, cv::cuda::PtrStepSzb validmask, cv::cuda::PtrStepSzb fgmask, cv::cuda::PtrStepSzb modesUsed, cv::cuda::PtrStepSzf weight, cv::cuda::PtrStepSzf variance, cv::cuda::PtrStepSzb mean, float alphaT, float prune, bool detectShadows, cudaStream_t stream)
	{
		dim3 block(32, 8);
		dim3 grid(static_cast<unsigned int>(cv::cuda::device::divUp(static_cast<int>(frame.cols), static_cast<int>(block.x))), static_cast<unsigned int>(cv::cuda::device::divUp(static_cast<int>(frame.rows), static_cast<int>(block.y))));

		const float alpha1 = 1.0f - alphaT;

		if (detectShadows)
		{
			cudaSafeCall(cudaFuncSetCacheConfig(vt_mog2<true, SrcT, WorkT>, cudaFuncCachePreferL1));

			vt_mog2<true, SrcT, WorkT> << <grid, block, 0, stream >> > (static_cast<cv::cuda::PtrStepSz<SrcT>>(frame), validmask, fgmask, modesUsed, weight, variance, static_cast<cv::cuda::PtrStepSz<WorkT>>(mean), alphaT, alpha1, prune);
		}
		else
		{
			cudaSafeCall(cudaFuncSetCacheConfig(vt_mog2<false, SrcT, WorkT>, cudaFuncCachePreferL1));

			vt_mog2<false, SrcT, WorkT> << <grid, block, 0, stream >> > (static_cast<cv::cuda::PtrStepSz<SrcT>>(frame), validmask, fgmask, modesUsed, weight, variance, static_cast<cv::cuda::PtrStepSz<WorkT>>(mean), alphaT, alpha1, prune);
		}

		cudaSafeCall(cudaGetLastError());

		if (stream == 0)
			cudaSafeCall(cudaDeviceSynchronize());
	}

	void digest(cv::cuda::PtrStepSzb frame, int cn, cv::cuda::PtrStepSzb validmask, cv::cuda::PtrStepSzb fgmask, cv::cuda::PtrStepSzb modesUsed, cv::cuda::PtrStepSzf weight, cv::cuda::PtrStepSzf variance, cv::cuda::PtrStepSzb mean, float alphaT, float prune, bool detectShadows, cudaStream_t stream)
	{
		typedef void(*func_t)(cv::cuda::PtrStepSzb frame, cv::cuda::PtrStepSzb validmask, cv::cuda::PtrStepSzb fgmask, cv::cuda::PtrStepSzb modesUsed, cv::cuda::PtrStepSzf weight, cv::cuda::PtrStepSzf variance, cv::cuda::PtrStepSzb mean, float alphaT, float prune, bool detectShadows, cudaStream_t stream);

		static const func_t funcs[] =
		{
			0, vt_mog2_caller<uchar, float>, 0, vt_mog2_caller<uchar3, float3>, vt_mog2_caller < uchar4, float4 >
		};

		funcs[cn](frame, validmask, fgmask, modesUsed, weight, variance, mean, alphaT, prune, detectShadows, stream);
	}

	template <typename WorkT, typename OutT>
	__global__ void getBackgroundImage(const cv::cuda::PtrStepSzb modesUsed, const cv::cuda::PtrStepf gmm_weight, const cv::cuda::PtrStep<WorkT> gmm_mean, cv::cuda::PtrStep<OutT> dst)
	{
		const int x = blockIdx.x * blockDim.x + threadIdx.x;
		const int y = blockIdx.y * blockDim.y + threadIdx.y;

		if (x >= modesUsed.cols || y >= modesUsed.rows)
			return;

		int nmodes = modesUsed(y, x);

		WorkT meanVal = VecTraits<WorkT>::all(0.0f);
		float totalWeight = 0.0f;

		for (int mode = 0; mode < nmodes; ++mode)
		{
			float weight = gmm_weight(mode * modesUsed.rows + y, x);

			WorkT mean = gmm_mean(mode * modesUsed.rows + y, x);
			meanVal = meanVal + weight * mean;

			totalWeight += weight;

			if (totalWeight > c_TB)
				break;
		}

		meanVal = meanVal * (1.f / totalWeight);

		dst(y, x) = cv::cuda::device::saturate_cast<OutT>(meanVal);
	}

	template <typename WorkT, typename OutT>
	void getBackgroundImage_caller(cv::cuda::PtrStepSzb modesUsed, cv::cuda::PtrStepSzf weight, cv::cuda::PtrStepSzb mean, cv::cuda::PtrStepSzb dst, cudaStream_t stream)
	{
		dim3 block(32, 8);
		dim3 grid(static_cast<unsigned int>(cv::cuda::device::divUp(modesUsed.cols, static_cast<int>(block.x))), static_cast<unsigned int>(cv::cuda::device::divUp(modesUsed.rows, static_cast<int>(block.y))));

		cudaSafeCall(cudaFuncSetCacheConfig(getBackgroundImage<WorkT, OutT>, cudaFuncCachePreferL1));

		getBackgroundImage<WorkT, OutT> << <grid, block, 0, stream >> > (modesUsed, weight, static_cast<cv::cuda::PtrStepSz<WorkT>>(mean), static_cast<cv::cuda::PtrStepSz<OutT>>(dst));
		cudaSafeCall(cudaGetLastError());

		if (stream == 0)
			cudaSafeCall(cudaDeviceSynchronize());
	}

	void getBackgroundImage(int cn, cv::cuda::PtrStepSzb modesUsed, cv::cuda::PtrStepSzf weight, cv::cuda::PtrStepSzb mean, cv::cuda::PtrStepSzb dst, cudaStream_t stream)
	{
		typedef void(*func_t)(cv::cuda::PtrStepSzb modesUsed, cv::cuda::PtrStepSzf weight, cv::cuda::PtrStepSzb mean, cv::cuda::PtrStepSzb dst, cudaStream_t stream);

		static const func_t funcs[] =
		{
			0, getBackgroundImage_caller<float, uchar>, 0, getBackgroundImage_caller<float3, uchar3>, getBackgroundImage_caller < float4, uchar4 >
		};

		funcs[cn](modesUsed, weight, mean, dst, stream);
	}
}
