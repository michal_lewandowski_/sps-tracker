#pragma once
#include <opencv.hpp>
#include <memory>
#include <unordered_map>
#include "Stats.h"
#include "DetectionResult.h"
#include "NormalisedPatch.h"

class DetectorCascade;
class ColorBallMap;
class CameraCalibration;
class KFTrack2d;
class KFTrack3d;
class NPClassifier;

enum class VisualState : int
{
	UNKNOWN = 0,
	DETECTOR = 1,
	TRACKER = 2,
	NEW = 3,
	NONE = 4
};

class Field
{
public:
	Field(int id, cv::Rect r, int margin, int camsNum, std::string gtPath);
	virtual ~Field();
	Field(const Field& f);
	Field(Field&& f);

	void show(int64_t frameNum, cv::Mat frame, double windowScale, std::vector<int> countData, int modify=0);
	void show(int64_t frameNum, cv::Mat frame1, cv::Mat frame2, std::string camID, CameraCalibration& calibration, cv::Scalar color, bool show);
	void prepare();
	std::vector<cv::Point> project2d(cv::Rect f, CameraCalibration& calibration) const;
	bool isInPlayerZone(const cv::Point3d& pos) const;
	bool isInGameZone(const cv::Point3d& pos) const;
	std::pair<cv::Point3d, cv::Rect> closestPlayerZone(cv::Point3d pos);
	void generateMask(CameraCalibration& calibration);

	void addConfidenceIndex(int idx);
	bool fuseHypothesis(cv::Ptr<NPClassifier> expertClassifier);
	void storePreviousData();
	void initialLearning(const cv::Mat& frame, const cv::Mat& intImg, const cv::Mat& intSqImg, const DetectorCascade& detCasc);
	void learn(const cv::Mat& frame, const cv::Mat& intImg, const cv::Mat& intSqImg, const DetectorCascade& detCasc);
	cv::Mat showPositiveDetectionModel();
	cv::Mat showNegativeDetectionModel();
	void showSizeMap(std::string camID, cv::Mat frame, int scaleIndex);

	void initWindowOffsets();
	void initWindowsAndScales(const cv::Mat& ballSizeMap, const cv::Mat& mask);
	void initFeatureOffsets();
	void reinit(const cv::Mat& ballSizeMap, const cv::Mat& mask);

	int fid;
	cv::Rect field;
	cv::Rect fieldExtended;
	std::vector<int> numObservations;
	std::vector<cv::Rect> zonePlayer1;
	std::vector<cv::Rect> zonePlayer2;
	cv::Rect zoneGame;
	cv::Rect detectionWindow;
	cv::Mat fieldMask;

	double maxAbsVelocityGlobal;
	double maxBallProbabilityGlobal;

	Stats stats;
	std::shared_ptr<ColorBallMap> ballColorMap;
	std::shared_ptr<KFTrack2d> ballTrack2d;
	std::shared_ptr<KFTrack3d> ballTrack3d;

	int binaryP;
	int binaryN;
	float minVar;
	float *posteriors;
	int *positives;
	int *negatives;
	int numWindows;
	int numScales;
	std::vector<int> windows;
	std::vector<int> windowOffsets;
	std::vector<int> featureOffsets;
	std::vector<cv::Size> scales;

	bool isInitTLD;
	bool isLearning;
	int valid;
	int wasValid;
	float currConf;
	int currLabel;
	VisualState currState;
	cv::Rect *prevBB;
	cv::Rect *currBB;

	TrackResult trackResult;
	DetectionResult detectionResult;

	std::deque<NormalizedPatch> falsePositives;
	std::deque<NormalizedPatch> truePositives;
};

class FieldList : public std::vector<Field>
{
public:
	Field* inField(const cv::Point3d& pos);
	const Field* inField(const cv::Point3d& pos) const;

	Field* find(int fid);
	const Field* find(int fid) const;

	void addConfidenceIndex(int i);
};