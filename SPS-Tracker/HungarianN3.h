#pragma once

#pragma warning(push, 0)
#include <opencv.hpp>
#include <vector>
#pragma warning(pop)

namespace cv
{
	class Mat;
}

//!This code is adopted from http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=hungarianAlgorithm by x-ray
namespace xray
{

	class HungarianN3
	{
	public:
		HungarianN3(const std::vector<std::vector<double>>& costMatrix);
		~HungarianN3();
		double solve(std::vector<size_t>& assignementsOut);

		static void assignmentSolve(const cv::Mat& costMatrixIn, std::vector<size_t>& assignmentOut);
		static size_t NOT_ASSIGNED() { return std::numeric_limits<size_t>::max(); }

	private:
		void update_labels();
		void add_to_tree(size_t x, size_t prevx);
		void augment();

		std::vector<std::vector<double>> costMatrix;          //cost matrix
		size_t n, max_match;     //n workers and n jobs
		std::vector<double> lx;  //labels of X parts
		std::vector<double> ly;  //labels of Y parts
		std::vector<size_t> xy;  //xy[x] - vertex that is matched with x,
		std::vector<size_t> yx;  //yx[y] - vertex that is matched with y
		std::vector<bool> S;	 //set S in algorithm
		std::vector<bool> T;     //set T in algorithm
		std::vector<double> slack; //as in the algorithm description
		std::vector<size_t> slackx; //slackx[y] such a vertex, that
									// l(slackx[y]) + l(y) - w(slackx[y],y) = slack[y]
		std::vector<size_t> prev; //array for memorizing alternating paths
		size_t originalWorkers, originalJobs;
		double cost;
	};

}
