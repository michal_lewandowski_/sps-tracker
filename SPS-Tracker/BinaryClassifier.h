#pragma once

#include "ICascadeFilter.h"

class DetectionResult;

class BinaryClassifier : public ICascadeFilter
{
public:
	BinaryClassifier();
	virtual ~BinaryClassifier();

	virtual void init();
	virtual void release();

	virtual bool filter(int idx, FieldList& fields);

	static bool compareBinary(int a1, int a2, int b1, int b2);

	static unsigned int diffAtMost;
};

