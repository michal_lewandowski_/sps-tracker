#pragma once

#include <opencv.hpp>

class NormalizedPatch;
class FieldList;
class Field;
class DetectionResult;

class Clustering
{
public:
	Clustering();
	virtual ~Clustering();
	void release();
	void clusterConfidentIndices(FieldList& fields, int globalNumWindows);

	void calcMeanRect(Field& f, const std::vector<int>& confidentIndices);
	void calcMeanRect(Field& f, const std::vector<int>& clusterIndices, std::vector<cv::Rect>& dets);

	void calcDistances(Field& f, std::vector<float>& distances);
	void cluster(Field& f, const std::vector<float>& distances, std::vector<int>& clusterIndices);

	float cutoff;
};
