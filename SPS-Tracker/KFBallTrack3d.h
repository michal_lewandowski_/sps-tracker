#pragma once
#include <opencv2/opencv.hpp>
#include "KFTrack3d.h"

class KFBallTrack3d : public KFTrack3d
{
public:
	KFBallTrack3d(const size_t iTrackI, const std::shared_ptr<MovingObject> obj, const int iStateParams, const int iMesaureParams, const int64_t dStartTS, double& maxAbsVelocity, double& maxBallProbability);
	virtual ~KFBallTrack3d();

	virtual std::shared_ptr<MovingObject> missFrame(const CameraCalibration* calib) override;
	virtual void addTrace(const std::shared_ptr<MovingObject> obj) override;
	virtual bool isDead(const std::vector<cv::Point>& Mask, const FieldList& fields) override;
	virtual void printInfo(const std::vector<cv::Point>& Mask, const FieldList& fields) const override;
	virtual void show(cv::Mat frame, cv::Scalar color) const override;
	virtual void show(cv::Mat frame, double windowScale, cv::Scalar color) const;
	virtual void show(cv::Mat frame, const CameraCalibration* calib, cv::Scalar color) const;
	virtual bool assignProbability() override;
	virtual void assignProbability(std::shared_ptr<MovingObject> obj) override;
	virtual void removeProbability(std::shared_ptr<MovingObject> obj) override;
	virtual bool isGoodEnoughFor() const override;

	bool isInROI(cv::Rect& bb) const;
	void setROI(const std::shared_ptr<MovingObject> obj);
	inline const cv::Rect& roi() const { return m_roi; }

protected:
	KFBallTrack3d(const KFBallTrack3d& other);
	KFBallTrack3d& operator=(const KFBallTrack3d& other);
	bool verifyVelocity(void) const;

protected:
	cv::Point detectionUncertainness;
	cv::Rect m_roi;
};
