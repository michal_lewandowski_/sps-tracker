#pragma once
#include <memory>
#include "MovingObject.h"
#include <unordered_map>
#include "CameraCalibration.h"

class Track
{
public:

	Track(const size_t iTrackID, const std::shared_ptr<MovingObject> obj, const int64_t dStartTS, double& maxAbsVelocity, double& maxBallProbability);
	Track(const Track& other);
	Track& operator=(const Track& other);

	virtual ~Track();

	inline size_t getTID(void) const { return m_iTrackID; };
	inline cv::Rect getBox(void) const { return getCurrentDet()->box(); };
	inline cv::Point getPosition(void) const { return getCurrentDet()->pos2d(); };
	inline bool isNew(void) const { return getLength() == 1; }
	inline int getLength(void) const { return static_cast<int>(traceLength); }
	inline Field* field(void) { return getCurrentDet()->field(); }
	inline const Field* field(void) const { return getCurrentDet()->field(); }
	inline int fieldID(void) { return getCurrentDet()->fieldID(); }
	inline const int fieldID(void) const { return getCurrentDet()->fieldID(); }
	inline std::size_t getMissedFrames(void) const { return m_iMissedFrames; };
	inline std::size_t getNumDetected(void) const { return numDetected; };
	inline bool isVisible(void) const { return numDetected > showTraceAfter; };
	inline bool isKilled(void) const { return m_bKillTrack; };
	inline int64_t getStartTimestamp(void) const { return m_dStartTimestamp; };
	inline int64_t getEndTimestamp(void) const { return m_dEndTimestamp; };
	inline void setStartTimestamp(int64_t t) { m_dStartTimestamp = t; };
	inline void setEndTimestamp(int64_t t) { m_dEndTimestamp = t; };
	inline const std::deque<std::shared_ptr<MovingObject>>& trace(void) const { return m_trace; };

	std::shared_ptr<MovingObject> getFirstDet(void);
	std::shared_ptr<MovingObject> getFirstDet(void) const;
	std::shared_ptr<MovingObject> getCurrentDet(void);
	std::shared_ptr<MovingObject> getCurrentDet(void) const;
	std::shared_ptr<MovingObject> getPreviousDet(void);
	std::shared_ptr<MovingObject> getPreviousDet(void) const;
	std::shared_ptr<MovingObject> getDet(int i);
	std::shared_ptr<MovingObject> getDet(int i) const;

	virtual std::shared_ptr<MovingObject> predict(int64_t time) = 0;
	virtual void update2d(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj) = 0;
	virtual void update3d(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj) = 0;
	virtual void update(std::shared_ptr<MovingObject> obj) = 0;
	virtual double getCost(const std::shared_ptr<MovingObject> obj) const = 0;
	virtual double getCostThreshold(void) const = 0;
	virtual double getCostMax(void) const = 0;
	virtual std::shared_ptr<MovingObject> missFrame(const CameraCalibration* calib) = 0;
	virtual bool isDead(const std::vector<cv::Point>& Mask, const FieldList& fields) = 0;
	virtual void printInfo(const std::vector<cv::Point>& Mask, const FieldList& fields) const = 0;
	virtual void show(cv::Mat frame, cv::Scalar color) const = 0;
	virtual void show(cv::Mat frame, double windowScale, cv::Scalar color) const = 0;
	virtual void show(cv::Mat frame, const CameraCalibration* calib, cv::Scalar color) const = 0;
	virtual bool assignProbability() = 0;
	virtual void assignProbability(std::shared_ptr<MovingObject> obj) = 0;
	virtual void removeProbability(std::shared_ptr<MovingObject> obj) = 0;
	virtual bool isGoodEnoughFor() const = 0;
	virtual void computeVelocity() = 0;
	inline double getNegLogProbability(void) const { return (probability.count > 0 ? -probability.log + log(probability.count) : 0.0); }
	inline double getAverageProbability(void) const { return getNegLogProbability() / static_cast<double>(getLength()); }
	inline double getProbabilityThreshold(void) const { return ballProbailityThr; }

	cv::Point3d getVelocity(void) const;
	double getAbsoluteVelocity(void) const;
	cv::Point2d getDisplacement(void) const;
	bool verifyVelocity(void) const;
	void computeTresholds(void);

public:
	std::size_t maxMissedFrames;
	std::size_t maxTraceLength;
	std::size_t traceLength;
	int showTraceAfter;
	std::size_t m_iTrackID;
	std::size_t m_iMissedFrames;
	std::size_t numDetected;
	
	int64_t m_dStartTimestamp;
	int64_t m_dEndTimestamp;
	bool m_bKillTrack;
	double m_dAbsVelocity;
	std::deque<std::shared_ptr<MovingObject>> m_trace;
	Probability probability;
	std::unordered_map<std::string, double> probImportanceMap;
	
	double ballProbailityThr;
	double distCostThreshold;
	double probCostThreshold;
	double distVariance;
	double maxDist;
	double maxProb;
	int goodFramesThreshold;
	double goodNegLogProbThreshold;
	int goodDetectionsThreshold;
	double& maxAbsVelocityGlobal;
	double& maxBallProbabilityGlobal;

public:
	static double maxRecorderBallSpeedMMperFrame;
	static const double maxRecorderBallSpeedMMperSec;
};
