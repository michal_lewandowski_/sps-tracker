#pragma once
#include <string>
#include <vector>
#include <core/mat.hpp>
#include "CameraCalibration.h"

class MaskGenerator
{
public:
	MaskGenerator();
	~MaskGenerator();

	void addJob(std::string name, std::string videoFile, std::string configFile);
	void process();
	void init(std::vector<cv::Point3d> m);
	void setMasks(std::vector<cv::Point3d>& m);
	//void generateAllMask();
	cv::Mat generateMask(std::string maskFile, cv::Size s);

	static void CallBackFunc(int event, int x, int y, int flags, void* userdata);
	static void drawOverlayMask(MaskGenerator* mg, int R);

private:
	CameraCalibration calibration;
	std::vector<std::tuple<std::string, std::string, std::string>> data;
	cv::Mat frame;
	std::vector<cv::Point2d> masks2d;
	std::vector<cv::Point3d> masks3d;
	std::string camID;
	std::string maskFile;
	const int R = 5;
};
