﻿#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/info_parser.hpp>
#include "CameraCalibration.h"
#include "clipper.hpp"

CameraCalibration::CameraCalibration()
	: m_intrinsicMatrix(cv::Matx33d::eye())
	, m_distortionCoefficients(cv::Mat::zeros(8, 1, CV_64FC1))
	, m_imageSize(cv::Size(0, 0))
	, m_rotationMatrix()
	, m_translationVector()
	, isInit(false)
{
	
}
CameraCalibration::CameraCalibration(std::string configFile)
	: CameraCalibration()
{
	if(!init(configFile))
	{
		exit(1);
	}
	else
	{
		isInit = true;
	}
}

bool CameraCalibration::init(std::string configFile)
{
	if(!configFile.empty())
	{
		try
		{
			boost::property_tree::read_info(configFile, m_configTree);
			isInit = true;
		}
		catch (boost::property_tree::info_parser::info_parser_error&)
		{
			std::cout << "WRONG FOFMRAT OF CALIBRAITON FILE " << configFile << std::endl;
			return false;
		}
	}

	m_imageSize = cv::Size(m_configTree.get<int>("CameraModel.Image.widthInPxl"), m_configTree.get<int>("CameraModel.Image.heightInPxl"));
	setFocalLengthX(m_configTree.get("CameraModel.focalLength.x", 1.0));
	setFocalLengthY(m_configTree.get("CameraModel.focalLength.y", 1.0));
	double principalPointX = m_configTree.get("CameraModel.principalPoint.xInPxl", m_imageSize.width * 0.5 + 0.5);
	double principalPointY = m_configTree.get("CameraModel.principalPoint.yInPxl", m_imageSize.height * 0.5 + 0.5);
	setPrincipalPoint(cv::Point2d(principalPointX, principalPointY));

	m_min = cv::Point(0, 0);
	m_max = cv::Point(m_imageSize.width, m_imageSize.height);

	const double deg2rad = M_PI / 180.;
	double tiltDeg = m_configTree.get("CameraModel.rotation.tiltInDeg", 0.);
	double panDeg = m_configTree.get("CameraModel.rotation.panInDeg", 0.);
	double rollDeg = m_configTree.get("CameraModel.rotation.rollInDeg", 0.);
	double camPosX_MM = m_configTree.get("CameraModel.position.xInMM", 0.);
	double camPosY_MM = m_configTree.get("CameraModel.position.yInMM", 0.);
	double camPosZ_MM = m_configTree.get("CameraModel.position.zInMM", 0.);
	setRotation(tiltDeg * deg2rad, panDeg * deg2rad, rollDeg * deg2rad, cv::Point3d(camPosX_MM, camPosY_MM, camPosZ_MM));

	double k1 = m_configTree.get("CameraModel.distortion.k1", 0.);
	double k2 = m_configTree.get("CameraModel.distortion.k2", 0.);
	double p1 = m_configTree.get("CameraModel.distortion.p1", 0.);
	double p2 = m_configTree.get("CameraModel.distortion.p2", 0.);
	double k3 = m_configTree.get("CameraModel.distortion.k3", 0.);
	double k4 = m_configTree.get("CameraModel.distortion.k4", 0.);
	double k5 = m_configTree.get("CameraModel.distortion.k5", 0.);
	double k6 = m_configTree.get("CameraModel.distortion.k6", 0.);
    setDistortionCoefficients(k1, k2, p1, p2, k3, k4, k5, k6);

	return true;
}

CameraCalibration::CameraCalibration(const CameraCalibration& other)
	: m_intrinsicMatrix(other.m_intrinsicMatrix)
	, m_distortionCoefficients(other.m_distortionCoefficients.clone())
	, m_imageSize(other.m_imageSize)
	, m_rotationMatrix(other.m_rotationMatrix)
	, m_translationVector(other.m_translationVector)
	, m_min(other.m_min)
	, m_max(other.m_max)
	, m_cameraPosition(other.m_cameraPosition)
	, m_configTree(other.m_configTree)
	, unitTemplateMapX(other.unitTemplateMapX.clone())
	, unitTemplateMapY(other.unitTemplateMapY.clone())
	, ballMap(other.ballMap.clone())
{
}

CameraCalibration& CameraCalibration::operator=(const CameraCalibration& other)
{
	if(this != &other)
	{
		m_intrinsicMatrix = other.m_intrinsicMatrix;
		m_distortionCoefficients = other.m_distortionCoefficients.clone();
		m_imageSize = other.m_imageSize;
		m_min = other.m_min;
		m_max = other.m_max;
		m_rotationMatrix = other.m_rotationMatrix;
		m_translationVector = other.m_translationVector;
		m_cameraPosition = other.m_cameraPosition;
		m_configTree = other.m_configTree;
		unitTemplateMapX = other.unitTemplateMapX.clone();
		unitTemplateMapY = other.unitTemplateMapY.clone();
		ballMap = other.ballMap.clone();
	}
	return *this;
}
CameraCalibration& CameraCalibration::setFocalLengthX(double focalLengthX)
{
	m_intrinsicMatrix(0, 0) = focalLengthX;
	return *this;
}

CameraCalibration& CameraCalibration::setFocalLengthY(double focalLengthY)
{
	m_intrinsicMatrix(1, 1) = focalLengthY;
	m_intrinsicMatrix(2, 2) = focalLengthY > 0 ? 1 : -1;
	return *this;
}

CameraCalibration& CameraCalibration::setPrincipalPoint(const cv::Point2f& principalPoint)
{
	m_intrinsicMatrix(0, 2) = principalPoint.x;
	m_intrinsicMatrix(1, 2) = principalPoint.y;
	return *this;
}
CameraCalibration& CameraCalibration::setPosition(const cv::Point3d& cameraPosition)
{
	// c = -R't -> t = -Rc
	m_translationVector = -(m_rotationMatrix * cameraPosition);
	m_cameraPosition = cameraPosition;
	return *this;
}
CameraCalibration& CameraCalibration::setRotation(double tiltInRad, double panInRad,double rollInRad,const cv::Point3d& cameraPosition)
{
	m_rotationMatrix(0, 0) = cos(rollInRad) * cos(panInRad);
	m_rotationMatrix(0, 1) = -cos(rollInRad) * sin(panInRad);
	m_rotationMatrix(0, 2) = sin(rollInRad);
	m_rotationMatrix(1, 0) = cos(tiltInRad) * sin(panInRad) + cos(panInRad) * sin(tiltInRad) * sin(rollInRad);
	m_rotationMatrix(1, 1) = cos(tiltInRad) * cos(panInRad) - sin(tiltInRad) * sin(rollInRad) * sin(panInRad);
	m_rotationMatrix(1, 2) = -cos(rollInRad) * sin(tiltInRad);
	m_rotationMatrix(2, 0) = sin(tiltInRad) * sin(panInRad) - cos(tiltInRad) * cos(panInRad) * sin(rollInRad);
	m_rotationMatrix(2, 1) = cos(panInRad) * sin(tiltInRad) + cos(tiltInRad) * sin(rollInRad) * sin(panInRad);
	m_rotationMatrix(2, 2) = cos(tiltInRad) * cos(rollInRad);

	setPosition(cameraPosition);

	return *this;
}
CameraCalibration& CameraCalibration::setDistortionCoefficients(double k1,double k2,double p1,double p2,double k3,double k4,double k5,double k6)
{
	m_distortionCoefficients = cv::Mat(8, 1, CV_64FC1);
	m_distortionCoefficients.at<double>(0, 0) = k1;
	m_distortionCoefficients.at<double>(1, 0) = k2;
	m_distortionCoefficients.at<double>(2, 0) = p1;
	m_distortionCoefficients.at<double>(3, 0) = p2;
	m_distortionCoefficients.at<double>(4, 0) = k3;
	m_distortionCoefficients.at<double>(5, 0) = k4;
	m_distortionCoefficients.at<double>(6, 0) = k5;
	m_distortionCoefficients.at<double>(7, 0) = k6;
	
	return *this;
}
double CameraCalibration::getFocalLengthX() const
{
	return m_intrinsicMatrix(0, 0);
}

double CameraCalibration::getFocalLengthY() const
{
	return m_intrinsicMatrix(1, 1);
}

cv::Point2d CameraCalibration::getPrincipalPoint() const
{
	return cv::Point2d(m_intrinsicMatrix(0, 2), m_intrinsicMatrix(1, 2));
}

const cv::Size& CameraCalibration::getImageSize() const
{
	return m_imageSize;
}
cv::Matx33d CameraCalibration::getIntrinsicMatrix() const
{
	return m_intrinsicMatrix;
}
cv::Matx34d CameraCalibration::getExtrinsicMatrix() const
{
	cv::Mat extrinsicMat(3, 4, CV_64FC1);
	extrinsicMat.at<double>(0, 3) = m_translationVector(0);
	extrinsicMat.at<double>(1, 3) = m_translationVector(1);
	extrinsicMat.at<double>(2, 3) = m_translationVector(2);
	cv::Rodrigues(getRotationVector(), extrinsicMat(cv::Rect(0, 0, 3, 3)));

	return extrinsicMat;
}
cv::Matx34d CameraCalibration::getCameraMatrix() const
{
	return getIntrinsicMatrix() * getExtrinsicMatrix();
}
cv::Point3d CameraCalibration::transformToRealWorld(const cv::Point& pixel, double height) const
{
	CV_DbgAssert(isInit);
	return transformToRealWorld(cv::Point2d(pixel.x, pixel.y), height);
}

cv::Point3d CameraCalibration::transformToRealWorld(const cv::Point2f& pixel, double height) const
{
	CV_DbgAssert(isInit);
	return transformToRealWorld(cv::Point2d(pixel.x, pixel.y), height);
}

double CameraCalibration::getTilt() const
{
	return atan2(-m_rotationMatrix(1, 2), m_rotationMatrix(2, 2));
}

double CameraCalibration::getRoll() const
{
	return asin(m_rotationMatrix(0, 2));
}

double CameraCalibration::getPan() const
{
	return atan2(-m_rotationMatrix(0, 1), m_rotationMatrix(0, 0));
}

cv::Point3d CameraCalibration::getCameraPosition() const
{
	return m_cameraPosition;
}

cv::Vec3d CameraCalibration::getRotationVector() const
{
	cv::Vec3d rot;
	cv::Rodrigues(m_rotationMatrix, rot);
	return rot;
}

cv::Vec3d CameraCalibration::getTranslationVector() const
{
	return m_translationVector;
}

cv::Mat CameraCalibration::getDistortionCoefficients(void) const
{
	return m_distortionCoefficients;
}

void CameraCalibration::getDistortionCoefficients(double* k1, double* k2, double* p1, double* p2, double* k3, double* k4, double* k5, double* k6) const
{
	if (k1) {
		if (m_distortionCoefficients.rows > 0) {
			*k1 = m_distortionCoefficients.at<double>(0, 0);
		}
		else {
			*k1 = 0;
		}
	}
	if (k2) {
		if (m_distortionCoefficients.rows > 1) {
			*k2 = m_distortionCoefficients.at<double>(1, 0);
		}
		else {
			*k2 = 0;
		}
	}
	if (p1) {
		if (m_distortionCoefficients.rows > 2) {
			*p1 = m_distortionCoefficients.at<double>(2, 0);
		}
		else {
			*p1 = 0;
		}
	}
	if (p2) {
		if (m_distortionCoefficients.rows > 3) {
			*p2 = m_distortionCoefficients.at<double>(3, 0);
		}
		else {
			*p2 = 0;
		}
	}
	if (k3) {
		if (m_distortionCoefficients.rows > 4) {
			*k3 = m_distortionCoefficients.at<double>(4, 0);
		}
		else {
			*k3 = 0;
		}
	}
	if (k4) {
		if (m_distortionCoefficients.rows > 5) {
			*k4 = m_distortionCoefficients.at<double>(5, 0);
		}
		else {
			*k4 = 0;
		}
	}
	if (k5) {
		if (m_distortionCoefficients.rows > 6) {
			*k5 = m_distortionCoefficients.at<double>(6, 0);
		}
		else {
			*k5 = 0;
		}
	}
	if (k6) {
		if (m_distortionCoefficients.rows > 7) {
			*k6 = m_distortionCoefficients.at<double>(7, 0);
		}
		else {
			*k6 = 0;
		}
	}
}

bool CameraCalibration::transformToPixel(const cv::Point3d& realWorld, cv::Point2d& pixel) const
{
	/*
	std::vector<cv::Point3f> realWorldPoints(1);
	realWorldPoints[0] = realWorld;
	std::vector<cv::Point2f> pixels(1);
	cv::projectPoints(realWorldPoints, m_rotationVector, m_translationVector, m_intrinsicMatrix, m_distortionCoefficients, pixels);
	return pixels[0];
	/*/
	const double X = realWorld.x, Y = realWorld.y, Z = realWorld.z;

	const double fx = m_intrinsicMatrix(0, 0);
	const double fy = m_intrinsicMatrix(1, 1);
	const double cx = m_intrinsicMatrix(0, 2);
	const double cy = m_intrinsicMatrix(1, 2);
	assert(m_distortionCoefficients.isContinuous());
	const double* k = reinterpret_cast<const double*>(m_distortionCoefficients.data);
	const double* R = m_rotationMatrix.val;
	double x = R[0] * X + R[1] * Y + R[2] * Z + m_translationVector[0];
	double y = R[3] * X + R[4] * Y + R[5] * Z + m_translationVector[1];
	double z = R[6] * X + R[7] * Y + R[8] * Z + m_translationVector[2];
	double r2, r4, r6, a1, a2, a3, cdist, icdist2, px, py;
	double xd, yd;

	z = z ? 1. / z : 1;
	x *= z;
	y *= z;

	/*if (x < m_min.x || x > m_max.x || y < m_min.y || y > m_max.y)
	{
		return false;
	}*/

	cdist = 1;
	icdist2 = 1;
	px = 0;
	py = 0;
	if (m_distortionCoefficients.cols > 0) {
		r2 = x * x + y * y;

		cdist += k[0] * r2;
		if (m_distortionCoefficients.cols > 1) {
			r4 = r2 * r2;
			cdist += k[1] * r4;
			if (m_distortionCoefficients.cols > 2) {
				a1 = 2 * x * y;
				a3 = r2 + 2 * y * y;
				px = k[2] * a1;
				py = k[2] * a3;
				if (m_distortionCoefficients.cols > 3) {
					a2 = r2 + 2 * x * x;
					px += k[3] * a2;
					py += k[3] * a1;
					if (m_distortionCoefficients.cols > 4) {
						r6 = r4 * r2;
						cdist += k[4] * r6;
						if (m_distortionCoefficients.cols > 5) {
							icdist2 += k[5] * r2;
							if (m_distortionCoefficients.cols > 6) {
								icdist2 += k[6] * r4;
								if (m_distortionCoefficients.cols > 7) {
									icdist2 += k[7] * r6;
								}
							}
							icdist2 = 1. / icdist2;
						}
					}
				}
			}
		}
	}

	xd = x * cdist * icdist2 + px;
	yd = y * cdist * icdist2 + py;

	pixel.x = xd * fx + cx;
	pixel.y = yd * fy + cy;
	return true;
}

inline double cubicRoot(double x)
{
	static double third = 1. / 3.;
	if (x < 0) {
		return -pow(-x, third);
	}
	else {
		return pow(x, third);
	}
}

inline double asinh(double x)
{
	return log(x + sqrt(x * x + 1));
}

inline double acosh(double x)
{
	return fabs(x) < 1.0 ?
		log(x + sqrt(1.0 - x * x))
		: log(x + sqrt(x * x - 1.0));
}

cv::Point3d CameraCalibration::transformToRealWorld(const cv::Point2d& pixel, double height) const
{
	CV_DbgAssert(isInit);
	std::vector<cv::Vec2d> buffer(1);
	std::vector<cv::Vec2d> outBuffer(1);
	return transformToRealWorld(pixel, height, buffer, outBuffer);
}

cv::Point3d CameraCalibration::transformToRealWorld(const cv::Point2d& pixel, double height, std::vector<cv::Vec2d>& distortedPoints, std::vector<cv::Vec2d>& undistortedPoints) const
{
	CV_DbgAssert(isInit);
	//transform pixel to image plane
	double u = pixel.x;
	double v = pixel.y;
	/*
	// (u,v) is the input point, (u', v') is the output point
	// camera_matrix=[fx 0 cx; 0 fy cy; 0 0 1]
	// P=[fx' 0 cx' tx; 0 fy' cy' ty; 0 0 1 tz]
	x" = (u - cx)/fx
	y" = (v - cy)/fy
	(x',y') = undistort(x",y",dist_coeffs)
	[X,Y,W]T = R*[x' y' 1]T
	x = X/W, y = Y/W
	// only performed if P=[fx' 0 cx' [tx]; 0 fy' cy' [ty]; 0 0 1 [tz]] is specified
	u' = x*fx' + cx'
	v' = y*fy' + cy',
	*/
	cv::Vec2d distortedPoint(u, v); //xdd, ydd;
	distortedPoints.resize(1);
	distortedPoints[0] = distortedPoint;
	undistortedPoints.resize(1); //(xd, yd);
	cv::undistortPoints(distortedPoints, undistortedPoints, m_intrinsicMatrix,
		m_distortionCoefficients);
	cv::Vec2d& undistortedPoint = undistortedPoints[0];

	double x_minus_t = undistortedPoint[0] - m_translationVector[0];
	double y_minus_t = undistortedPoint[1] - m_translationVector[1];
	double z_minus_t = 1. - m_translationVector[2];
	// s = R'([x y 1]' - t)
	const double* R = m_rotationMatrix.val;
	double sX = R[0] * x_minus_t + R[3] * y_minus_t + R[6] * z_minus_t;
	double sY = R[1] * x_minus_t + R[4] * y_minus_t + R[7] * z_minus_t;
	double sZ = R[2] * x_minus_t + R[5] * y_minus_t + R[8] * z_minus_t;

	//intersect plane on height with ray from cameraPosition through s*(X Y Z)^T
	//(0,0,height)' + X * (1,0,0) + Y * (0,1,0) = camPos + lambda * (camPos - realWorld);
	cv::Point3d camPos = getCameraPosition();
	double lambda = (height - camPos.z) / (camPos.z - sZ);
	const double X = camPos.x + lambda * (camPos.x - sX);
	const double Y = camPos.y + lambda * (camPos.y - sY);
	const double Z = height;

	return cv::Point3d(X, Y, Z);
}
double CameraCalibration::getApproximateUnit(const cv::Point3d pt1, const cv::Point3d pt2) const
{
	/*cv::Mat camPos = m_extrinsicMatrix(cv::Rect(0, 0, 3, 3)) * -m_cameraPosition;
	camPos.at<double>(2, 0) = 0;*/

	cv::Point3d camPos(m_cameraPosition);
	camPos.z = 0;

	cv::Point3d g1 = pt2 - pt1;
	cv::Point3d g2 = pt1 - camPos;

	double d = cv::norm(g2);
	g2.x = g2.x / d;
	g2.y = g2.y / d;
	g2.z = g2.z / d;

	d = cv::norm(g2.cross(g1));

	return d;
}


#pragma warning(push, 0)
#include "clipper.hpp"
#pragma warning(pop)

void CameraCalibration::getVisiblePolygonOnGroundPlane(const cv::Point2f& minRealWorldCoordinateInMM, const cv::Point2f& maxRealWorldCoordinateInMM, std::vector<cv::Point2f>& polygonInMM) const
{
	polygonInMM.clear();
	cv::Point3d tmp = transformToRealWorld(cv::Point2d(0, 0), 0.);
	cv::Point2d transformedTopLeft(tmp.x, tmp.y);
	tmp = transformToRealWorld(cv::Point2d(getImageSize().width - 1, 0), 0.);
	cv::Point2d transformedTopRight(tmp.x, tmp.y);
	tmp = transformToRealWorld(cv::Point2d(getImageSize().width - 1, getImageSize().height - 1), 0.);
	cv::Point2d transformedBottomRight(tmp.x, tmp.y);
	tmp = transformToRealWorld(cv::Point2d(0, getImageSize().height - 1), 0.);
	cv::Point2d transformedBottomLeft(tmp.x, tmp.y);

	//intersect with min/max rectangle
	ClipperLib::Path field, camera;
	field << ClipperLib::IntPoint(static_cast<int>(minRealWorldCoordinateInMM.x), static_cast<int>(minRealWorldCoordinateInMM.y));
	field << (ClipperLib::IntPoint(static_cast<int>(maxRealWorldCoordinateInMM.x), static_cast<int>(minRealWorldCoordinateInMM.y)));
	field << (ClipperLib::IntPoint(static_cast<int>(maxRealWorldCoordinateInMM.x), static_cast<int>(maxRealWorldCoordinateInMM.y)));
	field << (ClipperLib::IntPoint(static_cast<int>(minRealWorldCoordinateInMM.x), static_cast<int>(maxRealWorldCoordinateInMM.y)));

	camera << (ClipperLib::IntPoint(static_cast<int>(transformedTopLeft.x), static_cast<int>(transformedTopLeft.y)));
	camera << (ClipperLib::IntPoint(static_cast<int>(transformedTopRight.x), static_cast<int>(transformedTopRight.y)));
	camera << (ClipperLib::IntPoint(static_cast<int>(transformedBottomRight.x), static_cast<int>(transformedBottomRight.y)));
	camera << (ClipperLib::IntPoint(static_cast<int>(transformedBottomLeft.x), static_cast<int>(transformedBottomLeft.y)));

	//perform intersection ...
	ClipperLib::Clipper c;
	c.AddPath(field, ClipperLib::ptSubject, true);
	c.AddPath(camera, ClipperLib::ptClip, true);
	ClipperLib::Paths solution;
	c.Execute(ClipperLib::ctIntersection, solution, ClipperLib::pftNonZero, ClipperLib::pftNonZero);

	for (const ClipperLib::Path& path : solution)
	{
		for (const ClipperLib::IntPoint& p : path)
		{
			polygonInMM.push_back(cv::Point2f(static_cast<float>(p.X), static_cast<float>(p.Y)));
		}
	}
}
void CameraCalibration::getVisiblePolygonOnGroundPlane(const cv::Point2f& minRealWorldCoordinateInMM, const cv::Point2f& maxRealWorldCoordinateInMM, const std::vector<cv::Point>& maskContour, std::vector<cv::Point2f>& polygonInMM) const
{
	if (maskContour.size() == 0)
	{
		getVisiblePolygonOnGroundPlane(minRealWorldCoordinateInMM, maxRealWorldCoordinateInMM, polygonInMM);
	}
	else
	{
		polygonInMM.clear();

		//intersect with min/max rectangle
		ClipperLib::Path field;
		ClipperLib::Path mask;
		field << ClipperLib::IntPoint(static_cast<int>(minRealWorldCoordinateInMM.x), static_cast<int>(minRealWorldCoordinateInMM.y));
		field << (ClipperLib::IntPoint(static_cast<int>(maxRealWorldCoordinateInMM.x), static_cast<int>(minRealWorldCoordinateInMM.y)));
		field << (ClipperLib::IntPoint(static_cast<int>(maxRealWorldCoordinateInMM.x), static_cast<int>(maxRealWorldCoordinateInMM.y)));
		field << (ClipperLib::IntPoint(static_cast<int>(minRealWorldCoordinateInMM.x), static_cast<int>(maxRealWorldCoordinateInMM.y)));

		std::vector<cv::Point2f> validAreaWorld;
		getVisiblePolygonOnGroundPlane(maskContour, validAreaWorld);
		cv::Rect bb = cv::boundingRect(validAreaWorld);
		bb = cv::Rect(bb.tl() - cv::Point(500, 500), bb.br() + cv::Point(500, 500));

		mask << (ClipperLib::IntPoint(static_cast<int>(bb.tl().x), static_cast<int>(bb.tl().y)));
		mask << (ClipperLib::IntPoint(static_cast<int>(bb.br().x), static_cast<int>(bb.tl().y)));
		mask << (ClipperLib::IntPoint(static_cast<int>(bb.br().x), static_cast<int>(bb.br().y)));
		mask << (ClipperLib::IntPoint(static_cast<int>(bb.tl().x), static_cast<int>(bb.br().y)));

		//perform intersection ...
		ClipperLib::Clipper c;
		c.AddPath(field, ClipperLib::ptSubject, true);
		c.AddPath(mask, ClipperLib::ptClip, true);
		ClipperLib::Paths solution;
		c.Execute(ClipperLib::ctIntersection, solution, ClipperLib::pftNonZero, ClipperLib::pftNonZero);

		for (const ClipperLib::Path& path : solution)
		{
			for (const ClipperLib::IntPoint& p : path)
			{
				polygonInMM.push_back(cv::Point2f(static_cast<float>(p.X), static_cast<float>(p.Y)));
			}
		}
	}
}
void CameraCalibration::getVisiblePolygonOnGroundPlane(const std::vector<cv::Point>& maskContour, std::vector<cv::Point2f>& polygonInMM) const
{
	polygonInMM.resize(maskContour.size());
	for (unsigned int i = 0; i < maskContour.size(); ++i)
	{
		cv::Point3d pt = transformToRealWorld(maskContour[i]);
		polygonInMM[i].x = static_cast<float>(pt.x);
		polygonInMM[i].y = static_cast<float>(pt.y);
	}
}
void CameraCalibration::showSizeMap(std::string camID, cv::Mat frame, cv::Mat mask)
{
	CV_Assert(ballSizeMap().size() == frame.size());
	CV_Assert(mask.size() == frame.size());
	cv::Mat map = frame.clone();
	for (int y = 0; y < frame.rows; y+=30)
	{
		for (int x = 0; x < frame.cols; x+=30)
		{
			if (mask.at<uchar>(y, x) == 0)
			{
				float radius = ballSizeMap().at<float>(y, x);
				cv::Rect r(cv::Point(x - radius, y - radius), cv::Point(x + radius, y + radius));
				cv::rectangle(map, r.tl(), r.br(), cv::Scalar(0, 255, 0), 1);
			}
		}
	}
	cv::imshow(camID + " map", map);
}