#pragma once
#include <opencv2/opencv.hpp>
#include "AdaptiveBinning.h"
#include "MovingObject.h"
#include "TimeWatch.hpp"

class ColorBallHistAdaptive
{
public:
	ColorBallHistAdaptive(TimeWatch& sysTime, std::shared_ptr<AdaptiveBinning<cv::Vec3b>> adaptiveBinning);
	virtual ~ColorBallHistAdaptive();

	int64_t learn(const std::shared_ptr<MovingObject> ball);
	double classify(std::shared_ptr<MovingObject> ball);
	void prepare(void);

	void assignProbability(std::shared_ptr<MovingObject> ball) const;

	static cv::Mat getImage(std::shared_ptr<AdaptiveBinning<cv::Vec3b>> adaptiveBinning, FixedBinHistogram<cv::Vec3b, double>& hist, int width = 380, int height = 50);
	cv::Mat getImage(int width = 380, int height = 50) const;
	cv::Mat getProbImage(std::stringstream& ss);

	bool isReady() const;

	std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> getHistogram() const;
	std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> getNormalizedHistogram(cv::Rect const& boundingBox, cv::Mat const& imageBGR, cv::Mat const& foregroundMask, bool trainAdaptiveBinning) const;
	std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> getNormalizedHistogram(cv::Mat const& imageBGR, cv::Mat const& foregroundMask, bool trainAdaptiveBinning) const;
	std::function<size_t(const double&)> getBucketIndexFunc() const;
	size_t getBucketIndex(const double& d) const;
	void reset();

protected:
	std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>> ballHistogram;

public:
	std::shared_ptr<AdaptiveBinning<cv::Vec3b>> adaptiveBinningPtr;
	std::shared_ptr<FixedBinHistogram<double, double>> ballDists;
	std::shared_ptr<FixedBinHistogram<double, double>> notBallDists;
	std::shared_ptr<FixedBinHistogram<double, double>> ballDistsNorm;
	std::shared_ptr<FixedBinHistogram<double, double>> notBallDistsNorm;
	std::map<double, size_t> binning;
	std::vector<double> dists;

	TimeWatch& systemTime;
	double dSmallestDist;
	int iSmallestIndex;
	bool isProbModelReady;
	unsigned int probModelWaitForNumSamples;
	double sumDistance;
	double dNowLearningRate;
	bool bIsReady;
	int binsColour;
	int binsProb;
	int resetColorModelAfter;
	int64_t lastUpdateTimestamp;
};
