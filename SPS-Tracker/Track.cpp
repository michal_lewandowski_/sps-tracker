#include "stdafx.h"
#include "Track.h"
#include <unordered_map>
#include "WorkManager.h"

extern double ballRadius;

double Track::maxRecorderBallSpeedMMperFrame = std::numeric_limits<double>::max();
const double Track::maxRecorderBallSpeedMMperSec = 62500;//max ball speed ever recorder is approximately 225km/h which gives 62500mm/s

Track::Track(const size_t iTrackID, const std::shared_ptr<MovingObject> obj, const int64_t dStartTS, double& maxAbsVelocity, double& maxBallProbability)
	: m_iTrackID(iTrackID)
	, m_iMissedFrames(0)
	, numDetected(0)
	, traceLength(0)
	, m_dStartTimestamp(dStartTS)
	, m_dEndTimestamp(dStartTS)
	, m_bKillTrack(false)
	, m_dAbsVelocity(0.0)
	, maxMissedFrames(5)
	, maxTraceLength(75)
	, showTraceAfter(5)
	, probability({ 1, cv::log(1), "", 0 })
	, distCostThreshold(0.0)
	, goodFramesThreshold(std::numeric_limits<int>::max())
	, goodNegLogProbThreshold(cv::log(1))
	, goodDetectionsThreshold(std::numeric_limits<int>::max())
	, maxAbsVelocityGlobal(maxAbsVelocity) 
	, maxBallProbabilityGlobal(maxBallProbability)
{
	computeTresholds();

	//valid just between 0-1 - the smaller value the much longer history required to be more probable
	//1.00 = around 10 history samples gives probability 1, while history of 5 gives 0.9933
	//0.75 = around 15 history samples gives probability 1, while history of 5 gives 0.9765
	//0.50 = around 20 history samples gives probability 1, while history of 5 gives 0.9179
	//0.25 = around 40 history samples gives probability 1, while history of 5 gives 0.7135
	//0.10 = around 100 history samples gives probability 1, while history of 5 gives 0.3935
	//0.05 = around 200 history samples gives probability 1, while history of 5 gives 0.2212
	//0.01 = around 1000 history samples gives probability 1, while history of 5 gives 0.0488
	probImportanceMap.insert(std::make_pair("pl", 0.05));
	//valid just between 0-1 - the smaller value than more invariant overall probability to number of missed frames
	//1.0 = probability drops to 0.3679 for 1 missed frame and 0.0067 for 5 missed frames
	//0.1 = probability drops to 0.9048 for 1 missed frame and 0.6065 for 5 missed frames
	//0.01 = probability drops to 0.9900 for 1 missed frame and 0.9512 for 5 missed frames
	probImportanceMap.insert(std::make_pair("pm", 0.1));
}
Track::Track(const Track& other) 
	: m_iTrackID(other.m_iTrackID)
	, m_iMissedFrames(other.m_iMissedFrames)
	, numDetected(other.numDetected)
	, m_dStartTimestamp(other.m_dStartTimestamp)
	, m_dEndTimestamp(other.m_dEndTimestamp)
	, m_bKillTrack(other.m_bKillTrack)
	, m_dAbsVelocity(other.m_dAbsVelocity)
	, m_trace(other.m_trace)
	, maxMissedFrames(other.maxMissedFrames)
	, maxTraceLength(other.maxTraceLength)
	, showTraceAfter(other.showTraceAfter)
	, probability(other.probability)
	, traceLength(other.traceLength)
	, distCostThreshold(other.distCostThreshold)
	, probCostThreshold(other.probCostThreshold)
	, distVariance(other.distVariance)
	, maxDist(other.maxDist)
	, maxProb(other.maxProb)
	, goodFramesThreshold(other.goodFramesThreshold)
	, goodNegLogProbThreshold(other.goodNegLogProbThreshold)
	, goodDetectionsThreshold(other.goodDetectionsThreshold)
	, maxAbsVelocityGlobal(other.maxAbsVelocityGlobal)
	, maxBallProbabilityGlobal(other.maxBallProbabilityGlobal)
{
}
Track& Track::operator=(const Track& other)
{
	if(this!=&other)
	{
		this->m_iTrackID = other.m_iTrackID;
		this->m_iMissedFrames = other.m_iMissedFrames;
		this->numDetected = other.numDetected;
		this->m_dStartTimestamp = other.m_dStartTimestamp;
		this->m_dEndTimestamp = other.m_dEndTimestamp;
		this->m_bKillTrack = other.m_bKillTrack;
		this->m_dAbsVelocity = other.m_dAbsVelocity;
		this->m_trace = other.m_trace;
		this->maxMissedFrames = other.maxMissedFrames;
		this->maxTraceLength = other.maxTraceLength;
		this->showTraceAfter = other.showTraceAfter;
		this->probability = other.probability;
		this->traceLength = other.traceLength;
		this->distCostThreshold = other.distCostThreshold;
		this->probCostThreshold = other.probCostThreshold;
		this->distVariance = other.distVariance;
		this->maxDist = other.maxDist;
		this->maxProb = other.maxProb;
		this->goodFramesThreshold = other.goodFramesThreshold;
		this->goodNegLogProbThreshold = other.goodNegLogProbThreshold;
		this->goodDetectionsThreshold = other.goodDetectionsThreshold;
		this->maxAbsVelocityGlobal = other.maxAbsVelocityGlobal;
		this->maxBallProbabilityGlobal = other.maxBallProbabilityGlobal;
	}
	return *this;
}
Track::~Track(void) 
{
}
std::shared_ptr<MovingObject> Track::getFirstDet(void)
{
	return getDet(0);
}
std::shared_ptr<MovingObject> Track::getFirstDet(void) const
{
	return getDet(0);
}
std::shared_ptr<MovingObject> Track::getCurrentDet(void)
{
	return m_trace.back();
}
std::shared_ptr<MovingObject> Track::getCurrentDet(void) const
{
	return m_trace.back();
}
std::shared_ptr<MovingObject> Track::getPreviousDet(void)
{
	CV_DbgAssert(m_trace.size() >= 2);
	return getDet(static_cast<int>(m_trace.size()) - 2); 
}
std::shared_ptr<MovingObject> Track::getPreviousDet(void) const
{
	CV_DbgAssert(m_trace.size() >= 2);
	return getDet(static_cast<int>(m_trace.size()) - 2);
}
std::shared_ptr<MovingObject> Track::getDet(int i)
{
	CV_DbgAssert(i < m_trace.size());
	return m_trace[i];
}
std::shared_ptr<MovingObject> Track::getDet(int i) const
{
	CV_DbgAssert(i < m_trace.size());
	return m_trace[i];
}
//v= s / t: s - displacement of point between consecutive frames in mm, t - m_dDeltaTime (time between frames in ms)
//divided by WorkManager::frameRate, but then we multiply by WorkManager::frameRate so we skip it
cv::Point3d Track::getVelocity(void) const
{
	cv::Point3d vec;
	if (m_trace.size() > 1)
		vec = (getCurrentDet()->pos3d() - getPreviousDet()->pos3d());
	else vec = cv::Point3d(0.0, 0.0, 0.0);
	return cv::Point3d(abs(vec.x), abs(vec.y), abs(vec.z));
}
//speed - magnitute of velocity in m_dDeltaTime (time between frames)
double Track::getAbsoluteVelocity(void) const
{
	return cv::norm(getVelocity());
}
//displacement of point between consecutive frames in mm, velocity vector multiple time s=v*t
cv::Point2d Track::getDisplacement(void) const
{
	cv::Point2d vec;
	if (m_trace.size() > 1)
		vec = (getCurrentDet()->pos2d() - getPreviousDet()->pos2d());
	else vec = cv::Point2d(0.0, 0.0);
	return cv::Point2d(abs(vec.x), abs(vec.y));
}
bool Track::verifyVelocity(void) const
{
	return (m_dAbsVelocity < maxRecorderBallSpeedMMperFrame);
}
void Track::computeTresholds(void)
{
	if(distCostThreshold>0.0)
	{
		distVariance = 2.0 * distCostThreshold * distCostThreshold;
		probCostThreshold = cv::exp(-distCostThreshold * distCostThreshold / distVariance);
	}
	else
	{
		distVariance = 2.0;
		probCostThreshold = 1.0;
	}
	maxDist = sqrt(std::numeric_limits<double>::max());
	maxProb = cv::exp(-maxDist * maxDist / distVariance);
}