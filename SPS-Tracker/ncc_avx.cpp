#include "StdAfx.h"
#include "ncc.h"
#include "MedianFlowTracker.h"

#ifdef SPS_USE_AVX
float ncc(const NormalizedPatch& np1, const NormalizedPatch& np2)
{
	float score = 0.0f;
	__m256* a1f = (__m256*) np1.values;
	__m256* a2f = (__m256*) np2.values;

	__m256 sxy = _mm256_setzero_ps();
	__m256 sx2 = _mm256_set1_ps(np1.norm);
	__m256 sy2 = _mm256_set1_ps(np2.norm);
	__m256 s1 = _mm256_set1_ps(1.0);
	__m256 s05 = _mm256_set1_ps(0.5);

	for (int i = 0; i < TLD_PATCH_SIZE * TLD_PATCH_SIZE / 8; ++i)
	{
		//sxy = _mm256_add_ps(sxy, _mm256_mul_ps(a1f[i], a2f[i]));
		sxy = _mm256_fmadd_ps(a1f[i], a2f[i], sxy);
	}
	sxy = _mm256_hadd_ps(sxy, sxy);
	sxy = _mm256_add_ps(sxy, _mm256_permute2f128_ps(sxy, sxy, 0x1));
	sxy = _mm256_hadd_ps(sxy, sxy);

	__m256 ns = _mm256_mul_ps(sx2, sy2);
	ns = _mm256_sqrt_ps(ns);
	ns = _mm256_div_ps(sxy, ns);
	ns = _mm256_add_ps(ns, s1);
	ns = _mm256_mul_ps(ns, s05);
	_mm_store_ss(&score, _mm256_castps256_ps128(ns));
	return score;
}

float ncc(const float *f1, const float *f2)
{
	float score = 0.0f;
	__m256* a1f = (__m256*) f1;
	__m256* a2f = (__m256*) f2;

	__m256 sxy = _mm256_setzero_ps();
	__m256 sx2 = _mm256_setzero_ps();
	__m256 sy2 = _mm256_setzero_ps();
	__m256 mx = _mm256_setzero_ps();
	__m256 my = _mm256_setzero_ps();
	//__m256 s1 = _mm256_set_ss(1.0);
	//__m256 s05 = _mm256_set_ss(0.5);
	__m256 N = _mm256_set1_ps(TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC);
	for (int i = 0; i < TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC / 8; ++i)
	{
		mx = _mm256_add_ps(mx, a1f[i]);
		my = _mm256_add_ps(my, a2f[i]);
	}
	mx = _mm256_hadd_ps(mx, mx);
	mx = _mm256_add_ps(mx, _mm256_permute2f128_ps(mx, mx, 0x1));
	mx = _mm256_hadd_ps(mx, mx);
	my = _mm256_hadd_ps(my, my);
	my = _mm256_add_ps(my, _mm256_permute2f128_ps(my, my, 0x1));
	my = _mm256_hadd_ps(my, my);
	mx = _mm256_div_ps(mx, N);
	my = _mm256_div_ps(my, N);

	for (int i = 0; i < TLD_WIN_SIZE_NCC * TLD_WIN_SIZE_NCC / 8; ++i)
	{
		a1f[i] = _mm256_sub_ps(a1f[i], mx);
		a2f[i] = _mm256_sub_ps(a2f[i], my);

		/*sxy = _mm256_add_ps(sxy, _mm256_mul_ps(a1f[i], a2f[i]));
		sx2 = _mm256_add_ps(sx2, _mm256_mul_ps(a1f[i], a1f[i]));
		sy2 = _mm256_add_ps(sy2, _mm256_mul_ps(a2f[i], a2f[i]));*/

		sxy = _mm256_fmadd_ps(a1f[i], a2f[i], sxy);
		sx2 = _mm256_fmadd_ps(a1f[i], a1f[i], sx2);
		sy2 = _mm256_fmadd_ps(a2f[i], a2f[i], sy2);
	}
	sxy = _mm256_hadd_ps(sxy, sxy);
	sxy = _mm256_add_ps(sxy, _mm256_permute2f128_ps(sxy, sxy, 0x1));
	sxy = _mm256_hadd_ps(sxy, sxy);
	sx2 = _mm256_hadd_ps(sx2, sx2);
	sx2 = _mm256_add_ps(sx2, _mm256_permute2f128_ps(sx2, sx2, 0x1));
	sx2 = _mm256_hadd_ps(sx2, sx2);
	sy2 = _mm256_hadd_ps(sy2, sy2);
	sy2 = _mm256_add_ps(sy2, _mm256_permute2f128_ps(sy2, sy2, 0x1));
	sy2 = _mm256_hadd_ps(sy2, sy2);

	__m256 ns = _mm256_mul_ps(sx2, sy2);
	ns = _mm256_sqrt_ps(ns);
	ns = _mm256_div_ps(sxy, ns);
	//ns = _mm256_add_ps(ns, s1);
	//ns = _mm256_mul_ps(ns, s05);
	_mm_store_ss(&score, _mm256_castps256_ps128(ns));
	return score;
}
void normCrossCorrelationError(const cv::Mat& pimg, const cv::Mat& cimg, const cv::Mat& pPatch, const cv::Mat& cPatch, CvPoint2D32f *points0, CvPoint2D32f *points1, int nPts, const char* status, float *match)
{
	for (int i = 0; i < nPts; i++)
	{
		if (status[i] == 1)
		{
			cv::getRectSubPix(pimg, MedianFlowTracker::winSizeNCC, cv::Point2f(points0[i].x, points0[i].y), pPatch, CV_32FC1);
			cv::getRectSubPix(cimg, MedianFlowTracker::winSizeNCC, cv::Point2f(points1[i].x, points1[i].y), cPatch, CV_32FC1);

			match[i] = ncc(reinterpret_cast<float*>(pPatch.data), reinterpret_cast<float*>(cPatch.data));
		}
		else
		{
			match[i] = 0.0;
		}
	}
}
#endif