#pragma once
#include <opencv2/opencv.hpp>

template <typename Data, typename T>
class FixedBinHistogram
{
public:
	friend class FixedBinHistogram<Data, size_t>;

	FixedBinHistogram(size_t nBuckets, std::function<size_t(const Data&)> getBucketIndexFunc);
	FixedBinHistogram(const std::vector<T> count, std::function<size_t(const Data&)> getBucketIndexFunc);
	~FixedBinHistogram();

	FixedBinHistogram& operator<<(const Data& d);

	FixedBinHistogram& reset();
	double distance(const FixedBinHistogram& rhs) const;
	size_t size() const;
	T count() const;
	const T& operator[](size_t idx) const;
	
	std::shared_ptr<FixedBinHistogram<Data, double>> createNormalized(double eps = 0) const;
	FixedBinHistogram<Data, double>& normalize(double eps = 0);
	FixedBinHistogram& add(const FixedBinHistogram& rhs, double factor);

	void rebin(const std::map<uint32_t, size_t>& oldBinning, const std::map<uint32_t, size_t>& newBinning);

	std::ostream& operator >> (std::ostream& out) const;

private:
	std::vector<T> _count;
	std::function<size_t(const Data&)> _getBucketIndexFunc;
};

template <typename Data, typename T>
FixedBinHistogram<Data, T>::FixedBinHistogram(size_t nBuckets, std::function<size_t(const Data&)> getBucketIndexFunc)
	: _count(nBuckets)
	, _getBucketIndexFunc(getBucketIndexFunc)
{
}

template <typename Data, typename T>
FixedBinHistogram<Data, T>::FixedBinHistogram(std::vector<T> const count, std::function<size_t(const Data&)> getBucketIndexFunc)
	: _count(count)
	, _getBucketIndexFunc(getBucketIndexFunc)
{
}

template <typename Data, typename T>
FixedBinHistogram<Data, T>::~FixedBinHistogram()
{
}

template <typename Data, typename T>
FixedBinHistogram<Data, T>& FixedBinHistogram<Data, T>::operator<<(Data const& d)
{
	++_count[_getBucketIndexFunc(d)];
	return *this;
}

template <typename Data, typename T>
FixedBinHistogram<Data, T>& FixedBinHistogram<Data, T>::reset()
{
	for (T& count : _count)
	{
		count = 0;
	}
	return *this;
}

template <typename Data, typename T>
double FixedBinHistogram<Data, T>::distance(FixedBinHistogram const& rhs) const
{
	assert(_count.size() == rhs._count.size());

	//chi-square distance
	/*double H_dash = 0;
	double chiSquared = 0;
	for (size_t i = 0; i < _count.size(); ++i)
	{
	H_dash = (_count[i] + rhs._count[i]) / 2.0;
	if (H_dash>DBL_EPSILON)
	chiSquared += (_count[i] - H_dash)*(_count[i] - H_dash) / H_dash;
	}
	return chiSquared;*/

	//bhattacharyya distance
	double bhattacharyya = 0;
	double s1 = 0;
	double s2 = 0;
	for (size_t i = 0; i < _count.size(); ++i)
	{
		bhattacharyya += std::sqrt(_count[i] * rhs._count[i]);
		s1 += _count[i];
		s2 += rhs._count[i];
	}
	s1 *= s2;
	s1 = s1 > DBL_EPSILON ? 1.0 / std::sqrt(s1) : 1.0;
	bhattacharyya = std::sqrt(std::min(1.0 - bhattacharyya*s1, 1.0));

	return bhattacharyya;
}

template <typename Data, typename T>
size_t FixedBinHistogram<Data, T>::size() const
{
	return _count.size();
}

template <typename Data, typename T>
T FixedBinHistogram<Data, T>::count() const
{
	T sumHits = 0;
	for (T hits : _count)
	{
		sumHits += hits;
	}
	return sumHits;
}

template <typename Data, typename T>
const T& FixedBinHistogram<Data, T>::operator[](size_t idx) const
{
	return _count[idx];
}

template <typename Data, typename T>
std::shared_ptr<FixedBinHistogram<Data, double>> FixedBinHistogram<Data, T>::createNormalized(double eps) const
{
	std::shared_ptr<FixedBinHistogram<Data, double>> outPtr(new FixedBinHistogram<Data, double>(_count.size(), std::function<size_t(const Data&)>()));
	double sumHits = 0;
	for (T hits : _count)
	{
		sumHits += hits;
	}
	if (sumHits == 0)
		sumHits = std::numeric_limits<double>::epsilon();
	double factor = 1. / sumHits;
	for (unsigned i = 0; i < _count.size(); ++i)
	{
		outPtr->_count[i] = factor * _count[i];
		if (outPtr->_count[i] == 0)
			outPtr->_count[i] = eps;
	}
	return outPtr;
}

template <typename Data, typename T>
FixedBinHistogram<Data, double>& FixedBinHistogram<Data, T>::normalize(double eps)
{
	double sumHits = 0;
	for (T hits : _count)
	{
		sumHits += hits;
	}
	if (sumHits == 0)
	{
		sumHits = std::numeric_limits<double>::epsilon();
	}
	double factor = 1. / sumHits;
	for (unsigned i = 0; i < _count.size(); ++i)
	{
		_count[i] = factor * _count[i];
		if (_count[i] == 0)
		{
			_count[i] = eps;
		}
	}
	return *this;
}

template <typename Data, typename T>
void FixedBinHistogram<Data, T>::rebin(std::map<uint32_t, size_t> const& oldBinning, std::map<uint32_t, size_t> const& newBinning)
{
	CV_Assert(!newBinning.empty());

	std::vector<T> count(size());
	for (const std::pair<uint32_t, size_t>& bitInterleaved_index : oldBinning)
	{
		std::map<uint32_t, size_t>::const_iterator it = newBinning.lower_bound(bitInterleaved_index.first);
		size_t idx;
		if (it == newBinning.end())
		{
			idx = newBinning.size() - 1;
		}
		else
		{
			idx = it->second;
		}
		count[idx] += static_cast<T>((*this)[bitInterleaved_index.second]);
	}

	_count = count;
}

template <typename Data, typename T>
FixedBinHistogram<Data, T>& FixedBinHistogram<Data, T>::add(FixedBinHistogram const& rhs, double factor)
{
	assert(_count.size() == rhs._count.size());
	const double oneMinusFactor = 1. - factor;
	for (unsigned i = 0; i < _count.size(); ++i)
	{
		_count[i] = static_cast<T>(_count[i] * oneMinusFactor + factor * rhs._count[i]);
	}

	return *this;
}

template <typename Data, typename T>
std::ostream& FixedBinHistogram<Data, T>::operator >> (std::ostream& out) const
{
	out << "FixedBinHistogram{";
	for (unsigned int j = 0; j < _count.size() - 1; ++j)
	{
		out << _count[j] << ", ";
	}
	if (!_count.empty())
	{
		out << _count.back();
	}
	out << "}";
	return out;
}
