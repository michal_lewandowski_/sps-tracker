#pragma once

#include <opencv2/opencv.hpp>
#include "MovingObject.h"
#include "Track.h"
#include "KFTrack2d.h"

class Worker;

class KFTracker2d
{
public:
	KFTracker2d(Worker* w);
	virtual ~KFTracker2d();

	void track(const cv::Mat& frame, const cv::Mat& mask, const std::vector<std::shared_ptr<MovingObject>>& detections, std::deque<std::shared_ptr<KFTrack2d>>& liveTracks, std::deque<std::shared_ptr<KFTrack2d>>& deadTracks);

protected:
	std::shared_ptr<KFTrack2d> createTrack(const CameraCalibration& calib, const std::shared_ptr<MovingObject> obj, const int64_t dStartTS, const size_t iNumTracks, const cv::Size imgSize, const int detectionWindow);

private:
	KFTracker2d(const KFTracker2d& other);
	KFTracker2d& operator=(const KFTracker2d& other);

public:
	Worker* worker;
	int stateParams;
	int mesaureParams;
	std::size_t maxNumTracks;
	double distThreshold;
	double probThreshold;
	double distVariance;
	double maxDist;
	double maxProb;
};
