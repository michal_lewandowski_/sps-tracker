#pragma once
#include "BallObject.h"
#include <memory>

class DetectedBall : public BallObject
{
public:
	DetectedBall(const std::size_t id, std::string cid, cv::Rect b, cv::RotatedRect e, double br, cv::Mat im, cv::Mat mm);
	DetectedBall(const std::size_t id, std::string cid, cv::Point3d p, int views, int numDet, float error, const std::unordered_map<std::string, int>& tracks, const std::vector<PreviousBallInCam>& prevLoc);
	virtual ~DetectedBall();
	DetectedBall(const DetectedBall& other);
	DetectedBall& operator=(const DetectedBall& other);

	virtual std::shared_ptr<MovingObject> clone() const override;
	virtual bool isDetected(void) const override { return true; }
	virtual bool isBacktracked() const override { return false; }
	virtual void show(cv::Mat frame, cv::Scalar color) const override;
	virtual void show3d(cv::Mat frame, cv::Scalar color, double windowScale) const override;
};
