#pragma once

#include <opencv.hpp>
#include "Worker.h"

struct BallSaveData
{
	BallSaveData()
		: frameNum(0), trackNegLogProb(0), frameStart(0), type(BallType::UNKNOWN), trackID(0)
	{

	}

	BallSaveData(double p, std::size_t num, std::size_t start, cv::Point3d pt, double e, int o, bool b, BallType t, std::size_t tid)
		: frameNum(num), frameStart(start), trackNegLogProb(p), pos(pt), err(e), observations(o), detected(b), type(t), trackID(tid)
	{

	}
	double trackNegLogProb;
	std::size_t frameNum;
	std::size_t frameStart;
	cv::Point3d pos;
	double err;
	int observations;
	bool detected;
	BallType type;
	std::size_t trackID;
};

struct HomogeneousPoint
{
	HomogeneousPoint()
		: pt(0.0,0.0,0.0), w(0.0)
	{
		
	}
	HomogeneousPoint(cv::Mat m)
	{
		pt.x = m.at<double>(0);
		pt.y = m.at<double>(1);
		pt.z = m.at<double>(2);
		w = m.at<double>(3);
	}
	cv::Point3d toWorldCoords()
	{
		return cv::Point3d(pt.x / w, pt.y / w, pt.z / w);
	}
	cv::Point3d pt;
	double w;
};

class TriangulationNViews
{
public:
	TriangulationNViews()
		: err(-1.0)
	{
		double err;
		cv::FileStorage file(".\\params.xml", cv::FileStorage::READ);
		file["triangulationError1"] >> err;
		reconstructionThreshold.insert(std::make_pair(1, err));
		file["triangulationError2"] >> err;
		reconstructionThreshold.insert(std::make_pair(2, err));
		file["triangulationError3"] >> err;
		reconstructionThreshold.insert(std::make_pair(3, err));
		file["triangulationError4"] >> err;
		reconstructionThreshold.insert(std::make_pair(4, err));
		file.release();

		std::cout << "Error for 1 observations: " << reconstructionThreshold[1] << std::endl;
		std::cout << "Triangulation Error for 2 observations: " << reconstructionThreshold[2] << std::endl;
		std::cout << "Triangulation Error for 3 observations: " << reconstructionThreshold[3] << std::endl;
		std::cout << "Triangulation Error for 4 observations: " << reconstructionThreshold[4] << std::endl;

	}
	void add(const cv::Point3d& camPos, const cv::Point3d& ballPos);
	std::vector<std::shared_ptr<MovingObject>> triangulate(std::vector<std::shared_ptr<Worker>>& jobs, FieldList& fields);
	void TriangulationNViews::triangulate(std::vector<std::shared_ptr<Worker>>& jobs, std::deque<std::shared_ptr<KFTrack3d>>& tracks);
	double getError()    const;
	
protected:
	void clear();
	cv::Point3d TriangulationNViews::triangulate();

	std::vector<std::pair<cv::Matx31d, cv::Matx33d>> views;

	mutable double err;  // retprojection error, mutable since modified in compute(...) const;

	std::map<int, double> reconstructionThreshold;
};
