#pragma once

#include <opencv2/opencv.hpp>
#include "MovingObject.h"
#include "Track.h"

class KFTrack3d : public Track
{
public:
	KFTrack3d(const size_t iTrackID, const std::shared_ptr<MovingObject> obj, const int iStateParams, const int iMesaureParams, const int64_t dStartTS, double& maxAbsVelocity, double& maxBallProbability);
	virtual ~KFTrack3d();

	virtual void addTrace(const std::shared_ptr<MovingObject> obj);
	virtual std::shared_ptr<MovingObject> predict(int64_t time) override;
	virtual void update2d(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj) override;
	virtual void update3d(const CameraCalibration* calib, std::shared_ptr<MovingObject> obj) override;
	virtual void update(std::shared_ptr<MovingObject> obj) override;
	virtual double getCost(const std::shared_ptr<MovingObject> obj) const override;
	virtual double getCostThreshold(void) const override;
	virtual double getCostMax(void) const override;
	virtual void computeVelocity();
	inline bool isExtended() const { return extended; }
	inline void setExtended() { extended = true; }
	void extendTrace(const std::shared_ptr<MovingObject> obj);

protected:
	KFTrack3d(const KFTrack3d& other);
	KFTrack3d& operator=(const KFTrack3d& other);

	void initMotionModel(int iStateParams, int iMesaureParams, double dDeltaTime);
	double getDist(const std::shared_ptr<MovingObject> obj) const;
	double getProb(const std::shared_ptr<MovingObject> obj) const;

	double getDist(const cv::Point3d& p3) const;
	double getProb(const cv::Point3d& p3) const;

protected:
	cv::KalmanFilter m_KF;
	cv::Mat m_state;
	cv::Mat m_estimation;
	cv::Mat m_trackParams;

	cv::Mat m_icov;
	double m_det;
	bool extended;
};

