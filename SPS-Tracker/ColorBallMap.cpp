#include "stdafx.h"
#include "ColorBallMap.h"

ColorBallMap::ColorBallMap(TimeWatch& sysTime, double rt)
	: systemTime(sysTime)
	, time(0)
	, timeDelay(0)
	, ratioThr(rt)
	, rebinEvery(15)
{
	int ratioNum = 2;
	colorQuantiziser = std::make_shared<ColorQuantiziser>();
	adaptiveBinning = std::make_shared<AdaptiveBinning<cv::Vec3b>>(16, colorQuantiziser);

	/*double fStep = (1.0 - ratioThr) / ratioNum;
	for (int i = 0; i < ratioNum; ++i)
	{
		hists.insert(std::make_pair(cvFloor((ratioThr + i*fStep)*10.0 + 0.5) / 10.0, std::make_shared<ColorBallHistAdaptive>(adaptiveBinning)));
		dists.insert(std::make_pair(cvFloor((ratioThr + i*fStep)*10.0 + 0.5) / 10.0, std::vector<double>()));
	}*/
	//if two ratios available, then you have to introduce penalty for the ball which goes to second ratio (diable), while first ratio is enabled
	//if two ratios are disabled then no penanlty
	//ColorBallHistAdaptive::assignProbability

	hists.insert(std::make_pair(ratioThr, std::make_shared<ColorBallHistAdaptive>(sysTime, adaptiveBinning)));
	dists.insert(std::make_pair(ratioThr, std::vector<double>()));
}
ColorBallMap::~ColorBallMap(void)
{

}
bool ColorBallMap::learn(const std::shared_ptr<MovingObject> obj)
{
	std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(obj);
	double ratio = ball->sizeRatio();

	CV_Assert(ratio >= ratioThr);

	if (ratio >= ratioThr)
	{
		std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.lower_bound(ratio);
		int64_t lastUpdateTimestamp = it->second->learn(ball);
		
		++time;
		if (time > rebinEvery)
		{
			time = 0;
			timeDelay = 0;
			std::vector<std::shared_ptr<FixedBinHistogram<cv::Vec3b, double>>> histograms;
			for (auto ratio_colorBallModelIt = hists.begin(); ratio_colorBallModelIt != hists.end(); ++ratio_colorBallModelIt)
			{
				histograms.push_back(ratio_colorBallModelIt->second->getHistogram());
			}
			adaptiveBinning->rebin(&histograms);
		}
		return true;
	}
	return false;
}
void ColorBallMap::learn(const std::shared_ptr<KFTrack2d> trBall)
{
	if (!trBall->isLearned())
	{
		if (learn(trBall->getCurrentDet()))
		{
			trBall->setLearned();
		}
	}
}
void ColorBallMap::prepare(void)
{
	for (std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::iterator it = hists.begin(); it != hists.end(); ++it)
	{
		it->second->prepare();
		dists[it->first].clear();
	}
}
void ColorBallMap::classify(std::shared_ptr<MovingObject> obj)
{
	std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(obj);
	CV_Assert(ball);

	for (std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::iterator it = hists.begin(); it != hists.end(); ++it)
	{
		dists[it->first].push_back(it->second->classify(ball));
	}
	double ratio = ball->sizeRatio();
	CV_Assert(ratio >= ratioThr);

	std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.lower_bound(ratio);
	ball->colorDist() = dists[it->first].back();
}
void ColorBallMap::assignProbability(std::shared_ptr<MovingObject> ball)
{
	double ratio = std::static_pointer_cast<BallObject>(ball)->sizeRatio();
	CV_Assert(ratio >= ratioThr);

	std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.lower_bound(ratio);
	it->second->assignProbability(ball);
}
bool ColorBallMap::isReady(const std::shared_ptr<MovingObject> ball)
{
	if(ball)
	{
		double ratio = std::static_pointer_cast<BallObject>(ball)->sizeRatio();
		CV_Assert(ratio >= ratioThr);

		std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.lower_bound(ratio);
		return it->second->isReady();
	}
	else
	{
		std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.lower_bound(1.0);
		return it->second->isReady();
	}
}
size_t ColorBallMap::getRebinTime(void)
{
	return rebinEvery - time+1;
}
cv::Mat ColorBallMap::getImage(const std::shared_ptr<KFTrack2d> trBall, std::string& prefixStr) const
{
	std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(trBall->getCurrentDet());
	double ratio = ball->sizeRatio();
	CV_Assert(ratio >= ratioThr);

	std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.lower_bound(ratio);
	prefixStr = std::to_string(it->first);
	return it->second->getImage();
}
cv::Mat ColorBallMap::getProbImage(const std::shared_ptr<KFTrack2d> trBall, std::stringstream& ss, std::string& prefixStr)
{
	std::shared_ptr<BallObject> ball = std::static_pointer_cast<BallObject>(trBall->getCurrentDet());
	double ratio = ball->sizeRatio();
	CV_Assert(ratio >= ratioThr);

	for (std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::iterator it = hists.begin(); it != hists.end(); ++it)
	{
		it->second->getProbImage(ss);
	}
	std::stringstream ss1;
	std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.lower_bound(ratio);
	prefixStr = std::to_string(it->first);
	return it->second->getProbImage(ss1);
}
cv::Mat ColorBallMap::getImage(int width, int height) const
{
	cv::Mat m;
	for (std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.begin(); it != hists.end(); ++it)
	{
		if(m.empty())
		{
			m = it->second->getImage(width, height);
		}
		else
		{
			cv::vconcat(m, it->second->getImage(width, height), m);
		}
	}
	return m;
}
cv::Mat ColorBallMap::getProbImage(std::stringstream& ss)
{
	cv::Mat m;
	for (std::map<double, std::shared_ptr<ColorBallHistAdaptive>, std::greater<double>>::const_iterator it = hists.begin(); it != hists.end(); ++it)
	{
		if (m.empty())
		{
			m = it->second->getProbImage(ss);
		}
		else
		{
			cv::vconcat(m, it->second->getProbImage(ss), m);
		}
		ss << std::endl;
	}
	return m;
}