#pragma once

#include "ICascadeFilter.h"

class Field;
class FieldList;
class DetectionResult;

class EnsembleClassifier : public ICascadeFilter
{
public:
	EnsembleClassifier();
	virtual ~EnsembleClassifier();
	
	virtual void init();
	virtual void release();

	virtual bool filter(int idx, FieldList& fields);

	void initFeatureLocations();
	void initFeatureOffsets();

	void learn(Field& f, int positive, int *featureVector);
	void updatePosterior(Field& f, int treeIdx, int idx, int positive, int amount);
	void updatePosteriors(Field& f, int *featureVector, int positive, int amount);
	float calcConfidence(Field& f, int *featureVector);
	
	int calcFernFeature(Field& f, int windowIdx, int treeIdx);;
	int calcFernFeature(Field& f, cv::Rect bb, int treeIdx);
	void calcFeatureVector(Field& f, cv::Rect bb, int* featureVector);
	void calcFeatureVector(Field& f, int windowIdx, int *featureVector);
	
	static int numIndices;
	static float *features;
	static const int numTrees;
	static const int numFeatures;
};
