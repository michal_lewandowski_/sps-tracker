#include "NormalisedPatch.h"
#include "DetectorCascade.h"
#include "MotionFilter.h"
#include "VarianceFilter.h"
#include "BinaryClassifier.h"
#include "EnsembleClassifier.h"
#include "NPClassifier.h"
#include "Field.h"
#include "Clustering.h"

int DetectorCascade::minSize = -1;
int DetectorCascade::maxSize = -1;
int DetectorCascade::imgWidth = -1;
int DetectorCascade::imgHeight = -1;
int DetectorCascade::imgWidthStep = -1;
int DetectorCascade::minScale = -1;
int DetectorCascade::maxScale = 1;
float DetectorCascade::shift = 0.1f;

DetectorCascade::DetectorCascade()
	: globalNumWindows(0)
{
    initialised = false;

	motionFilter = cv::makePtr<MotionFilter>();
	varianceFilter = cv::makePtr<VarianceFilter>();
	binaryClassifier = cv::makePtr<BinaryClassifier>();
	ensembleClassifier = cv::makePtr<EnsembleClassifier>();
	expertClassifier = cv::makePtr<NPClassifier>();
	clustering = cv::makePtr<Clustering>();
}

DetectorCascade::~DetectorCascade()
{
    release();
}

void DetectorCascade::init(bool reserve)
{
	CV_Assert(!(imgWidth == -1 || imgHeight == -1 || imgWidthStep == -1));

	motionFilter->init();
	varianceFilter->init();
	binaryClassifier->init();
	ensembleClassifier->init();
	expertClassifier->init();

    initialised = true;
}

void DetectorCascade::reinit()
{
	if (!initialised)
	{
		return;
	}

	initialised = false;

	CV_Assert(!(imgWidth == -1 || imgHeight == -1 || imgWidthStep == -1));
	
	initialised = true;
}
void DetectorCascade::release()
{
	if (!initialised)
	{
		return;
	}

	initialised = false;

	motionFilter->release();
	varianceFilter->release();
	binaryClassifier->release();
	ensembleClassifier->release();
	expertClassifier->release();
	clustering->release();
	globalNumWindows = 0;
}
void DetectorCascade::updateGlobalWindows(const Field& f)
{
	if (f.numWindows > globalNumWindows)
		globalNumWindows = f.numWindows;
}
void DetectorCascade::detect(FieldList& fields)
{
    if(!initialised)
    {
        return;
    }
	
	tw.reset();


	/*#pragma omp parallel
    {
		int tid = omp_get_thread_num();
		printf("Hello world from thread = %d \n", tid);
		if (tid == 0)
		{
			int nthreads = omp_get_num_threads();
			printf("Number of threads = %d\n", nthreads);
		}
    }*/
	/*
	int r0 = 0;
	int r1 = 0;
	int r2 = 0;
	int r3 = 0;
	int r4 = 0;
	int r5 = 0;*/
    #pragma omp parallel for
	for (int i = 0; i < globalNumWindows; ++i)
    {
		//CV_DbgAssert(TLD_WINDOW_SIZE * i < TLD_WINDOW_SIZE * numWindows);
        //int *window = &windows[TLD_WINDOW_SIZE * i];

		if (!motionFilter->filter(i, fields))
		{
			//#pragma omp critical
			//r0++;
			continue;
		}
		//else r0++;

        if(!varianceFilter->filter(i, fields))
        {
			//#pragma omp critical
			//r1++;
            continue;
        }
		//else r1++;

		if (!binaryClassifier->filter(i, fields))
		{
			//#pragma omp critical
			//r2++;
			continue;
		}
		//else r2++;

		if (!ensembleClassifier->filter(i, fields))
        {
			//#pragma omp critical
			//r3++;
            continue;
        }
		//else r3++;

		if (!expertClassifier->filter(i, fields))
        {
			//#pragma omp critical
			//r4++;
            continue;
        }
		//else r4++;

		//#pragma omp critical
		fields.addConfidenceIndex(i);
		//#pragma omp critical
		//r5++;
    }
	clustering->clusterConfidentIndices(fields, globalNumWindows);

	//std::cout << "(" << r0 << " " << r1 << " " << r2 << " " << r3 << " " << r4 << "->" << r5 << ") ";
	//CV_Assert(r1 + r2 + r3 + r4 == globalNumWindows);

	std::cout << "Windows " << globalNumWindows << " in time " << tw.measure().getMilliseconds() << " ms with mask " << cv::countNonZero(motionFilter->img1) << std::endl;;
}